# QMapOD

Le plugin QMapOD est compatible avec QGIS 3.x.

## Fonctionnalités

QMapOD se présente sous la forme d'une extension (plugin) QGIS qui permet de cartographier les résultats d'enquêtes origine / destination réalisées sur un réseau de transport en commun, en réalisant des filtrages multicritères sur un jeu de données d’enquêtes, puis en élaborant des analyses cartographiques à partir des données filtrées.

Fonctionnalités standard :
- Filtrage par lignes/sens ;
- Filtrage par critères signalétiques (catégorie socio-professionnelle, tranche d'âge, sexe, pmr) ;
- Filtrage par critères voyage (titre, fréquence, motif, modes amont/aval, tranche horaire) ;
- Filtrage cartographique par communes, par zones, par arrêts ;
- Affichage des montées/descentes par arrêts ;
- Affichage des montées/descentes par variable par arrêts ;
- Affichage des montée/descentes par communes, par zones ;
- Affichage des montées/descentes par variable par communes/zones ;
- Affichage des serpents d'offre, de charge et de performance ;
- Affichage des correspondances amont/aval par arrêts ;
- Affichage des flux par zones (dégradé, oursins, symboles proportionnels) ;
- Affichage des flux par arrêts.

Fonctionnalités optionnelles :
- Filtrage par type de jour
- Filtrage par courses
- Affichage des serpents de charges avec répartition avec/sans correspondance
- Affichage des serpents de charges avec correspondances amont/aval
- Affichage des flux par zones avec répartition selon critères voyage et signalétique (camembert)
- Affichage des flux principaux (seuil de sélection, flèches intra-zone, flèches inter-zones)

Les fonctionnalités disponibles peuvent varier selon les variables enquêtées.

## Pour contribuer au plugin

### Sous Linux

1. Cloner le [dépôt Gitlab](https://gitlab.com/Oslandia/qgis/qmapod) dans un dossier local. Par exemple, en SSH :
```shell
git clone git@gitlab.com:Oslandia/qgis/qmapod.git
```

ou en HTTP :
```shell
git clone https://gitlab.com/Oslandia/qgis/qmapod.git
```

2. Installer les outils de développement de Qt :
```shell
sudo apt install pyqt5-dev-tools
```

3. Exécuter le script de déploiement :

* Sous Linux :
```shell
bash deploy.sh
```

* Sous Windows :
```shell
deploy.bat
```

ou dérouler manuellement les étapes suivantes en cas de besoin :

- créer un fichier de paramètres `QMapOD.ini` pour le plugin, à partir du modèle fourni `QMapODDefault.ini`. Ce fichier `QMapOD.ini` est mis à jour lorsqu'on modifie le paramétrage du plugin dans QGIS afin de conserver ce paramétrage pour les futures exécutions.

```shell
cp QMapODDefault.ini QMapOD.ini
```

- recopier les données d'exemple dans le dossier `data` du plugin :
```shell
mkdir -p data
cp demo_db_creation/* data
```

- le cas échéant, changer le niveau de log dans le fichier `data/demo.json`, à la ligne 207 :
```json
"loglevel": "DEBUG"
```

à adapter en utilisant un des niveaux de log suivants : `"DEBUG", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"`.


4. Ajouter une variable d'environnement dans QGIS pour ajouter le dossier contenant le plugin dans le gestionnaire d'extensions : dans `Préférences / Options / Système / Environnement`, ajouter une variable `QGIS_PLUGINPATH` valant `path/to/dir`, où `dir` est le dossier parent du plugin. Activer l'extension depuis le gestionnaire d'extension


Remarque : pour recharger le plugin après l'avoir modifié, vous pouvez :
- fermer et rouvrir QGIS ;
ou
- utiliser le plugin "Plugin reloader" pour recharger la configuration.

## Données

Une base de données de test `demo.sqlite`, avec un fichier de paramètres associé `demo.json` sont mis à disposition dans le dossier `data`. Ils servent de démonstration pour le plugin, ils sont basés sur un réseau inspiré de celui de l'agglomération de Rennes en 2023, mais réduit en termes de nombre de lignes. Les données d'enquête associées sont fictives, générées aléatoirement.

### Pour générer une base de données vierge pour héberger vos propres données d'enquête

#### Sous Linux

- Installer Spatialite :

```shell
sudo apt install spatialite-bin
```

### Pour créer une base de données

#### Base vierge dans laquelle saisir les données via un SIG

```shell
spatialite data/my_db.sqlite < sql/demo_db_creation/create_empty_db.sql
```
Pour manipuler, requêter les données, vous pouvez utiliser notamment `spatialite-gui`, sous Linux :

```shell
sudo apt install spatialite-gui
```

Lancer Spatialite-GUI :
```shell
spatialite-gui
```

QGIS permet également de requêter et de visualiser les données géométriques.

#### Base vierge dans laquelle importer des données shape existantes

```shell
spatialite data/my_db.sqlite
```

Pour importer des données au format `.shp`, utiliser des commandes telles que celles ci-dessous :
```shell
spatialite_tool -i -shp myfolder/arrets -d data/my_db.sqlite -t arrets -s 2154 -c UTF-8 --type POINT
spatialite_tool -i -shp myfolder/communes -d data/my_db.sqlite -t communes -s 2154 -c UTF-8 --type MULTIPOLYGON
spatialite_tool -i -shp myfolder/troncons -d data/my_db.sqlite -t troncons -s 2154 -c UTF-8 --type LINESTRING
spatialite_tool -i -shp myfolder/zones_od -d data/my_db.sqlite -t zones_od -s 2154 -c UTF-8 --type POLYGON
spatialite_tool -i -dbf myfolder/arrets_zones.dbf -d data/my_db.sqlite -t arrets_zones -c UTF-8
spatialite_tool -i -dbf myfolder/code_freq.dbf -d data/my_db.sqlite -t code_freq -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne.dbf -d data/my_db.sqlite -t code_ligne -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne_coram.dbf -d data/my_db.sqlite -t code_ligne_coram -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne_corav.dbf -d data/my_db.sqlite -t code_ligne_corav -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne_var.dbf -d data/my_db.sqlite -t code_ligne_var -c UTF-8
spatialite_tool -i -dbf myfolder/code_modap.dbf -d data/my_db.sqlite -t code_modap -c UTF-8
spatialite_tool -i -dbf myfolder/code_motif.dbf -d data/my_db.sqlite -t code_motif -c UTF-8
spatialite_tool -i -dbf myfolder/code_pmr.dbf -d data/my_db.sqlite -t code_pmr -c UTF-8
spatialite_tool -i -dbf myfolder/code_residence.dbf -d data/my_db.sqlite -t code_residence -c UTF-8
spatialite_tool -i -dbf myfolder/code_sens.dbf -d data/my_db.sqlite -t code_sens -c UTF-8
spatialite_tool -i -dbf myfolder/code_statut.dbf -d data/my_db.sqlite -t code_statut -c UTF-8
spatialite_tool -i -dbf myfolder/code_titre.dbf -d data/my_db.sqlite -t code_titre -c UTF-8
spatialite_tool -i -dbf myfolder/code_trage.dbf -d data/my_db.sqlite -t code_trage -c UTF-8
spatialite_tool -i -dbf myfolder/code_trhor.dbf -d data/my_db.sqlite -t code_trhor -c UTF-8
spatialite_tool -i -dbf myfolder/code_trhor_typjour.dbf -d data/my_db.sqlite -t code_trhor_typjour -c UTF-8
spatialite_tool -i -dbf myfolder/code_type_jour.dbf -d data/my_db.sqlite -t code_type_jour -c UTF-8
spatialite_tool -i -dbf myfolder/code_type_ligne.dbf -d data/my_db.sqlite -t code_type_ligne -c UTF-8
spatialite_tool -i -dbf myfolder/comptages.dbf -d data/my_db.sqlite -t comptages -c UTF-8
spatialite_tool -i -dbf myfolder/data_licenses.dbf -d data/my_db.sqlite -t data_licenses -c UTF-8
spatialite_tool -i -dbf myfolder/enquetes.dbf -d data/my_db.sqlite -t enquetes -c UTF-8
spatialite_tool -i -dbf myfolder/parcours.dbf -d data/my_db.sqlite -t parcours -c UTF-8
spatialite_tool -i -dbf myfolder/schemas.dbf -d data/my_db.sqlite -t schemas -c UTF-8
```

## Auteurs et licence

MapOD a été initialement développé par la société Sigeal, sous Access et ArcGis / MapInfo. Il a ensuite été réécrit sous la forme d'un plugin QGIS, intégrant une base de données Spatialite, pour devenir QMapOD. Les développements ont été financés par les sociétés Test SA. et Oslandia. Oslandia assure aujourd'hui la maintenance du plugin.

QGIS et QMapOD sont diffusés sous licence Open Source (GNU General Public License).

Contact : contact@oslandia.com
