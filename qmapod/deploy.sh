cp QMapODDefault.ini QMapOD.ini
mkdir -p data
cp demo_db_creation/demo.sqlite data
cp demo_db_creation/demo.json data
sudo chmod 777 data/demo.sqlite
mkdir -p ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins/qmapod_os
cp -r ./* ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins/qmapod_os/
