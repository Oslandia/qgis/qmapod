"""
/***************************************************************************
 QMapODConfig
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import io
import json
import logging
import os.path
import re
from collections import OrderedDict

from qgis.PyQt.QtCore import QSettings

module_logger = logging.getLogger("QMapOD.config")


class QMapODConfig:
    """
    Chargement de la configuration de l'application
    """

    def __init__(self):
        """
        Constructeur
        """

        # Dossier d'installation du plugin
        self.pluginDir = os.path.dirname(__file__)

        self.logger = logging.getLogger("QMapOD.config.QMapODConfig")
        self.logger.info("Creating an instance of QMapODConfig")

        # Liste des réseaux trouvés
        self.listeReseaux = []

        # Recherche des bases de données présentes dans le sous-dossier /data
        dbDir = os.path.join(self.pluginDir, "data")
        results = []
        for f in os.listdir(dbDir):
            if re.search(".json", f):
                results += [f]

        # Si une seule base est trouvée on vérifie l'existence de son fichier
        # de configuration et on le charge
        if len(results) == 1:
            self.reseauCourant = os.path.splitext(results[0])[0]
            jsonFile = "data/" + self.reseauCourant + ".sqlite"
            if os.path.isfile(os.path.join(self.pluginDir, jsonFile)):
                self.listeReseaux.append(self.reseauCourant)
            else:
                return

        # Si plusieurs bases sont trouvées on prend la première
        # et on liste celles pour lesquelles il y a un fichier
        # de configuration
        elif len(results) > 1:
            self.logger.info(results)
            self.reseauCourant = os.path.splitext(results[0])[0]
            jsonFile = "data/" + self.reseauCourant + ".json"
            for reseau in results:
                if os.path.isfile(os.path.join(self.pluginDir, jsonFile)):
                    self.listeReseaux.append(os.path.splitext(reseau)[0])
        else:
            return

        self.loadConfig(self.reseauCourant)

    # -------------------------------------------------------------------------

    def loadConfig(self, reseau):
        """
        Chargement de la configuration d'un des réseaux disponibles
        """

        if reseau:
            self.reseauCourant = reseau

        # Chargement du fichier de configuration json
        jsonFile = "data/" + self.reseauCourant + ".json"
        if os.path.isfile(os.path.join(self.pluginDir, jsonFile)):
            # Chargement dans l'ordre du fichier json
            with io.open(os.path.join(self.pluginDir, jsonFile), encoding="utf-8") as f:
                qmapodConfig = json.load(f, object_pairs_hook=OrderedDict)
        else:
            self.logger.warning("Fichier de configuration introuvable")
            return

        # Chemin d'accès à la base de données
        dbFile = "data/" + self.reseauCourant + ".sqlite"
        self.db = os.path.join(self.pluginDir, dbFile)

        # Mémorisation de la connection à la base dans les paramètres QGIS
        qgisSettings = QSettings()
        qgisSettings.setValue(
            "SpatiaLite/connections/" + self.reseauCourant + "/sqlitepath", self.db
        )

        # Liste des QListWidget de critères et de leurs paramètres
        # L'utilisation d'objets OrderedDict permet de conserver
        # l'ordre des listes

        # Critère type de jour
        dctTypeJour = qmapodConfig["dctTypeJour"]
        self.odtTypeJour = OrderedDict(dctTypeJour.items())

        # Critères signalétique
        dctCritereSignaletique = qmapodConfig["dctCritereSignaletique"]
        self.odtCritereSignaletique = OrderedDict(dctCritereSignaletique.items())

        # Critères voyage
        dctCritereVoyage = qmapodConfig["dctCritereVoyage"]
        self.odtCritereVoyage = OrderedDict(dctCritereVoyage.items())

        # Critères autres
        dctCritereAutre = qmapodConfig["dctCritereAutre"]
        self.odtCritereAutre = OrderedDict(dctCritereAutre.items())

        # Liste des zonages disponibles
        dctZonage = qmapodConfig["dctZonage"]
        self.odtZonage = OrderedDict(dctZonage.items())

        # Variables analysables
        dctVariableAnalysable = qmapodConfig["dctVariableAnalysable"]
        self.odtVariableAnalysable = OrderedDict(dctVariableAnalysable.items())

        # Zones analysables
        dctZonageAnalysable = qmapodConfig["dctZonageAnalysable"]
        self.odtZonageAnalysable = OrderedDict(dctZonageAnalysable.items())

        # Paramètres généraux
        """
        'epsg': 27572,              # Code EPSG du SRID des couches
        'nomreseau': u'Réseau x',   # Nom du réseau
        'nblignes': 29,             # Nombre total de lignes
        'typlign': 'typlign',       # Type de ligne (code_ligne)
        'typjour': 'typjour',       # Code de type de jour (enquetes)
        'ligne': 'ligne',           # Code de ligne (enquetes)
        'sens': 'sens',             # Code de sens (enquetes)
        'parcours': 'parcours',     # Code de parcours (enquetes)
        'course': 'course',         # Code de course (enquetes)
        'poidsligne': 'poidsl',     # Poids sur la ligne (enquetes)
        'poidsreseau': 'poidsr',    # Poids sur le réseau (enquetes)
        'arretdebvoy': 'arreta2',   # Arrêt de début du voyage (enquetes)
        'arretfinvoy': 'arretb2',   # Arrêt de fin du voyage (enquetes)
        'arretdebdep': 'arretdeb2', # Arrêt de début du déplacement (enquetes)
        'arretfindep': 'arretfin2', # Arrêt de fin du déplacement (enquetes)
        'ligneamont': 'lignem1',    # Ligne de correspondance amont N°1
        'ligneaval': 'lignep1',     # Ligne de correspondance aval N°1
        'defaultligne': '20'        # Numéro de la ligne cochée par défaut
        'couleurslignes': 'True',   # Affichage des couleurs de lignes
        'affreseau': 'True'         # Affichage du réseau au démarrage
        """
        self.dctParam = qmapodConfig["dctParam"]

        # Liste des couleurs pour les diagrammes
        self.lstColors = qmapodConfig["lstColors"]

    # -------------------------------------------------------------------------
