"""
/***************************************************************************
 QMapODLab
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2019 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


class QMapOdLab:
    """
    Etiquetage des arrêts/couches en fonction de
    l'analyse et choix du type d'affichage

    Modes d'étiquetage
        0  : Aucun
        1  : Concaténation des valeurs
        2  : Somme des valeurs
        3  : Nom - Somme des valeurs
        4  : Nom - Concaténation des valeurs
        5  : Nom
        6  : ID
    """

    def lbl(self, idCol, nomCol, lstMode, lstColumns):
        """
        Sélection de l'expression d'affichage en fonction de affMode
        """

        strNom = f'"{nomCol}"'
        print(strNom + " : " + strNom)
        strCols = '" = 0 and "'.join(lstColumns)
        strTest = f'"{strCols}" = 0'
        print(strTest + " : " + strTest)
        strCols = '" + "'.join(lstColumns)
        strSum = f'round("{strCols}")'
        print(strSum + " : " + strSum)
        strCols = "\") || '/'|| round(\"".join(lstColumns)
        strJoin = f'round("{strCols}")'
        print(strJoin + " : " + strJoin)

        lstExp = [
            "",
            "if(" + strTest + ", '', " + strJoin + ")",
            "if(" + strTest + ", '', " + strSum + ")",
            "if(" + strTest + ", ''," + strNom + " || '\\n' || " + strSum + ")",
            "if(" + strTest + ", ''," + strNom + " || '\\n' || " + strJoin + ")",
            strNom,
            idCol,
        ]
        try:
            return lstExp[lstMode]
        except IndexError:
            return ""
