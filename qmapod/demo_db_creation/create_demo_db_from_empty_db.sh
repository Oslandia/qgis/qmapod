db1=./data/qmapod_01_star_2023.sqlite
db2=./demo_db_creation/demo.sqlite
shp_folder=./demo_db_creation/shp
csv_folder=./demo_db_creation/csv

mkdir -p $shp_folder
mkdir -p $csv_folder

rm $db2

spatialite $db2 < ./sql/demo_db_creation/create_empty_db.sql

spatialite_tool -e -shp $shp_folder/arrets -d $db1 -t arrets -c UTF-8 -g geometry -s 2154 --type POINT
spatialite_tool -e -shp $shp_folder/troncons -d $db1 -t troncons -c UTF-8 -g geometry -s 2154 --type LINESTRING
spatialite_tool -e -shp $shp_folder/zones_od -d $db1 -t zones_od_2023 -c UTF-8 -g geometry -s 2154 --type POLYGON
spatialite_tool -e -shp $shp_folder/communes -d $db1 -t communes -c UTF-8 -g geometry -s 2154 --type POLYGON

spatialite_tool -i -shp $shp_folder/arrets -d $db2 -t arrets_ -c UTF-8 -g geometry -s 2154
spatialite_tool -i -shp $shp_folder/troncons -d $db2 -t troncons_ -c UTF-8 -g geometry -s 2154
spatialite_tool -i -shp $shp_folder/zones_od -d $db2 -t zones_od_ -c UTF-8 -g geometry -s 2154
spatialite_tool -i -shp $shp_folder/communes -d $db2 -t communes_ -c UTF-8 -g geometry -s 2154

sqlite3 -header -csv $db1 "SELECT
                                id_type_jour,
                                lib_type_jour,
                                type_jour
                           FROM code_type_jour" > $csv_folder/code_type_jour.csv
sqlite3 -header -csv $db1 "SELECT
                                id_ligne,
                                num_ligne,
                                lib_ligne,
                                typ_ligne,
                                rvb
                           FROM code_ligne
                           WHERE id_ligne = 1001
                              OR id_ligne = 4
                              OR id_ligne = 10
                              OR id_ligne = 11
                              OR id_ligne = 12
                              OR id_ligne = 52" > $csv_folder/code_ligne.csv
sqlite3 -header -csv $db1 "SELECT
                                id_sens,
                                id_ligne,
                                lib_sens
                           FROM code_sens
                           WHERE id_ligne = 1001
                              OR id_ligne = 4
                              OR id_ligne = 10
                              OR id_ligne = 11
                              OR id_ligne = 12
                              OR id_ligne = 52" > $csv_folder/code_sens.csv
sqlite3 -header -csv $db1 "SELECT
                                id_ligne,
                                id_sens,
                                id_parcours,
                                id_arret,
                                ordre,
                                desserte,
                                id_tron
                           FROM parcours
                           WHERE id_ligne = 1001
                              OR id_ligne = 4
                              OR id_ligne = 10
                              OR id_ligne = 11
                              OR id_ligne = 12
                              OR id_ligne = 52" > $csv_folder/parcours.csv
sqlite3 -header -csv $db1 "SELECT
                                id_ligne,
                                num_ligne,
                                id_sens,
                                id_parcours,
                                id_course,
                                id_type_jour,
                                id_type_ligne,
                                hdeb,
                                hfin,
                                id_arret_deb,
                                nom_arret_deb,
                                id_arret_fin,
                                nom_arret_fin,
                                poids
                           FROM courses
                           WHERE id_ligne = 1001
                              OR id_ligne = 4
                              OR id_ligne = 10
                              OR id_ligne = 11
                              OR id_ligne = 12
                              OR id_ligne = 52" > $csv_folder/courses.csv
sqlite3 -header -csv $db1 "SELECT
                                id_residence,
                                lib_residence,
                                residence
                            FROM code_residence" > $csv_folder/code_residence.csv
sqlite3 -header -csv $db1 "SELECT
                                id_modav,
                                lib_modav,
                                modav
                           FROM code_modav" > $csv_folder/code_modav.csv
sqlite3 -header -csv $db1 "SELECT
                                id_modap,
                                lib_modap,
                                modap
                           FROM code_modap" > $csv_folder/code_modap.csv
sqlite3 -header -csv $db1 "SELECT
                                id_motif,
                                lib_motif,
                                motifod
                           FROM code_motif" > $csv_folder/code_motif.csv
sqlite3 -header -csv $db1 "SELECT
                                id_pmr,
                                lib_pmr,
                                pmr
                           FROM code_pmr" > $csv_folder/code_pmr.csv
sqlite3 -header -csv $db1 "SELECT
                                id_titre,
                                lib_titre,
                                titre
                           FROM code_titre" > $csv_folder/code_titre.csv
sqlite3 -header -csv $db1 "SELECT
                                id_trage,
                                lib_trage,
                                case when trage = '1618ans' then 't1618ans'
                                     when trage = '1926ans' then 't1926ans'
                                     when trage = '2764ans' then 't2764ans'
                                     when trage = '65plus' then 'p65ans'
                                     else trage
                                end as trage
                           FROM code_trage" > $csv_folder/code_trage.csv
sqlite3 -header -csv $db1 "SELECT
                                id_trhor,
                                lib_trhor,
                                trhor,
                                hdeb,
                                hfin
                           FROM code_trhor" > $csv_folder/code_trhor.csv
sqlite3 -header -csv $db1 "SELECT
                                id_trhor,
                                id_typjour,
                                lib_trhor,
                                trhor,
                                hdeb,
                                hfin
                           FROM code_trhor_typjour" > $csv_folder/code_trhor_typjour.csv
sqlite3 -header -csv $db1 "SELECT
                                id_typlign,
                                lib_typlign,
                                '' as icon_typlign
                           FROM code_type_ligne
                           WHERE id_typlign IN (
                                SELECT typ_ligne
                                FROM code_ligne
                                WHERE id_ligne = 1001
                                   OR id_ligne = 4
                                   OR id_ligne = 10
                                   OR id_ligne = 11
                                   OR id_ligne = 12
                                   OR id_ligne = 52
                            )" > $csv_folder/code_type_ligne.csv

# Extract 3000 interviews for the underground line (1001) and 2000 interviews for other lines (bus lines)
# randomly chosen among all available, with a ponderation reinitialized to 1
sqlite3 -header -csv $db1 "
                    SELECT
                        ident,
                        typjour,
                        ligne,
                        sens,
                        course,
                        parcours,
                        trhor as tranche,
                        arreta2,
                        arretb2,
                        arretdeb2,
                        arretfin2,
                        modav as nbav,
                        modap as nbap,
                        lignem1,
                        lignep1,
                        motifod,
                        freq as sexe,
                        trage,
                        titre,
                        trhor,
                        modav,
                        modap,
                        1 as poidsl,
                        1 as poidr
                    FROM enquetes
                    WHERE ligne IN (
                        SELECT id_ligne
                        FROM code_ligne
                        WHERE id_ligne = 1001
                    )
                    ORDER BY random() limit 3000" > $csv_folder/enquetes.csv

sqlite3 -csv $db1 "
                    SELECT
                        ident,
                        typjour,
                        ligne,
                        sens,
                        course,
                        parcours,
                        trhor as tranche,
                        arreta2,
                        arretb2,
                        arretdeb2,
                        arretfin2,
                        modav as nbav,
                        modap as nbap,
                        lignem1,
                        lignep1,
                        motifod,
                        freq as sexe,
                        trage,
                        titre,
                        trhor,
                        modav,
                        modap,
                        1 as poidsl,
                        1 as poidr
                    FROM enquetes
                    WHERE ligne IN (
                        SELECT id_ligne
                        FROM code_ligne
                        WHERE id_ligne = 4
                    )
                    ORDER BY random() limit 2000" >> $csv_folder/enquetes.csv

sqlite3 -csv $db1 "
                    SELECT
                        ident,
                        typjour,
                        ligne,
                        sens,
                        course,
                        parcours,
                        trhor as tranche,
                        arreta2,
                        arretb2,
                        arretdeb2,
                        arretfin2,
                        modav as nbav,
                        modap as nbap,
                        lignem1,
                        lignep1,
                        motifod,
                        freq as sexe,
                        trage,
                        titre,
                        trhor,
                        modav,
                        modap,
                        1 as poidsl,
                        1 as poidr
                    FROM enquetes
                    WHERE ligne IN (
                        SELECT id_ligne
                        FROM code_ligne
                        WHERE id_ligne = 10
                    )
                    ORDER BY random() limit 2000" >> $csv_folder/enquetes.csv

sqlite3 -csv $db1 "
                    SELECT
                        ident,
                        typjour,
                        ligne,
                        sens,
                        course,
                        parcours,
                        trhor as tranche,
                        arreta2,
                        arretb2,
                        arretdeb2,
                        arretfin2,
                        modav as nbav,
                        modap as nbap,
                        lignem1,
                        lignep1,
                        motifod,
                        freq as sexe,
                        trage,
                        titre,
                        trhor,
                        modav,
                        modap,
                        1 as poidsl,
                        1 as poidr
                    FROM enquetes
                    WHERE ligne IN (
                        SELECT id_ligne
                        FROM code_ligne
                        WHERE id_ligne = 11
                    )
                    ORDER BY random() limit 2000" >> $csv_folder/enquetes.csv

sqlite3 -csv $db1 "
                    SELECT
                        ident,
                        typjour,
                        ligne,
                        sens,
                        course,
                        parcours,
                        trhor as tranche,
                        arreta2,
                        arretb2,
                        arretdeb2,
                        arretfin2,
                        modav as nbav,
                        modap as nbap,
                        lignem1,
                        lignep1,
                        motifod,
                        freq as sexe,
                        trage,
                        titre,
                        trhor,
                        modav,
                        modap,
                        1 as poidsl,
                        1 as poidr
                    FROM enquetes
                    WHERE ligne IN (
                        SELECT id_ligne
                        FROM code_ligne
                        WHERE id_ligne = 12
                    )
                    ORDER BY random() limit 2000" >> $csv_folder/enquetes.csv

sqlite3 -csv $db1 "
                    SELECT
                        ident,
                        typjour,
                        ligne,
                        sens,
                        course,
                        parcours,
                        trhor as tranche,
                        arreta2,
                        arretb2,
                        arretdeb2,
                        arretfin2,
                        modav as nbav,
                        modap as nbap,
                        lignem1,
                        lignep1,
                        motifod,
                        freq as sexe,
                        trage,
                        titre,
                        trhor,
                        modav,
                        modap,
                        1 as poidsl,
                        1 as poidr
                    FROM enquetes
                    WHERE ligne IN (
                        SELECT id_ligne
                        FROM code_ligne
                        WHERE id_ligne = 52
                    )
                    ORDER BY random() limit 2000" >> $csv_folder/enquetes.csv

sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_type_ligne.csv code_type_ligne"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_trhor.csv code_trhor"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_trage.csv code_trage"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_type_jour.csv code_type_jour"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_sens.csv code_sens"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_modav.csv code_modav"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_modap.csv code_modap"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_motif.csv code_motif"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_pmr.csv code_pmr"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_titre.csv code_titre"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_ligne.csv code_ligne"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_residence.csv code_residence"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/code_trhor_typjour.csv code_trhor_typjour"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/enquetes.csv enquetes"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/parcours.csv parcours"
sqlite3 $db2 ".import --csv --skip 1 $csv_folder/courses.csv courses"

spatialite $db2 < ./sql/demo_db_creation/post_processing.sql
