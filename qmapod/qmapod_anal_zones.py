"""
/***************************************************************************
 QMapODAnalZone
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

# Import des librairies QGIS
from qgis.core import (
    QgsDataDefinedSizeLegend,
    QgsDataSourceUri,
    QgsDiagramLayerSettings,
    QgsDiagramSettings,
    QgsLayerTreeLayer,
    QgsLinearlyInterpolatedDiagramRenderer,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsPieDiagram,
    QgsProject,
    QgsRenderContext,
    QgsSimpleFillSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)

# Import des librairies PyQt
from qgis.PyQt.QtCore import QSizeF
from qgis.PyQt.QtGui import QColor, QFont
from qgis.PyQt.QtWidgets import QApplication

from .qmapod_lab import QMapOdLab

module_logger = logging.getLogger("QMapOD.anal_zones")


class QMapODAnalZone(object):
    """
    Analyses cartographiques par zones
    """

    def __init__(
        self,
        iface,
        tbn,
        clsFiltrage,
        typAnal,
        nbLignes,
        tabZone,
        colIdZone,
        colNomZone,
        titleZone,
        qmapodConfig,
        qmapodSettings,
    ):
        """
        Constructor
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Récupération de l'instance de la classe filtrage
        self.currentFiltrage = clsFiltrage
        # Type d'analyse ("md", "m" ou "d")
        self.typAnal = typAnal
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Couche générée
        self.zmdLayer = None
        # Caractéristiques diagrammes
        self.maxSize = 0
        self.maxValue = 0
        # Titre de la couche
        self.strNom = ""

        # Récupération des noms de colonnes pour la variable analysée
        self.typAnal = typAnal
        self.tabZone = tabZone
        self.colIdZone = colIdZone
        self.colNomZone = colNomZone
        self.titleZone = titleZone

        self.logger = logging.getLogger("QMapOD.anal_zones.QMapODAnalZones")
        self.logger.info("Creating an instance of QMapODAnalZones")

    def _addLayerZonesTmp(self):
        """
        Ajout de la couche zones_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "md":
            strZone = "Montées/descentes"
        elif self.typAnal == "m":
            strZone = "Montées"
        elif self.typAnal == "d":
            strZone = "Descentes"

        self.strNom = Template("$str_zone par $title_zone").substitute(
            str_zone=strZone, title_zone=self.titleZone
        )

        # Ajout de la couche
        uriZones = QgsDataSourceUri()
        uriZones.setDatabase(self.db)
        uriZones.setDataSource("", "zones_tmp", "geometry")
        zonesLayer = QgsVectorLayer(uriZones.uri(), self.strNom, "spatialite")
        if zonesLayer.isValid():
            # Ajout des couches et déplacement au-dessus
            # des couches existantes (api QgsLayerTree)
            root = QgsProject.instance().layerTreeRoot()

            self.zmdLayer = QgsProject.instance().addMapLayer(zonesLayer, False)

            self.zmdLayer.destroyed.connect(self._deleted)
            zonesTreeLayer = QgsLayerTreeLayer(zonesLayer)

            zonesTreeLayer.setName(zonesLayer.name())
            root.insertChildNode(0, zonesTreeLayer)

    # -------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.zmdLayer = None
        self.tbn.setChecked(False)

    # --------------------------------------------------------------------------

    def _getPoids(self, nbLignes, modeEnq):
        """
        Choix du mode de pondération à utiliser
        """

        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Modifié le 15/09/2015 : autorisation du mode voyage
        # lorsque tout le réseau est sélectionné
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        # ou sur la ligne selon la logique d'analyse
        # Voyage : poids sur la ligne - Déplacement : poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            if modeEnq == 1:
                return self.qmapodConfig.dctParam["poidsligne"]
            elif modeEnq == 2:
                return self.qmapodConfig.dctParam["poidsreseau"]
            else:
                return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # -------------------------------------------------------------------------

    def _genSqlZone(self, nbLignes, tabZone, colIdZone, colNomZone):
        """
        Génération de la requête de création de la table zones_tmp
        """

        # Mode de prise en compte des enquêtes
        modeEnq = int(self.qmapodSettings.value("params/buttonGroupZoneMode", 1))
        if modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]

        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/anal_zones_1.sql")
        ) as f:
            strCreateZmd = Template(f.read()).substitute(
                col_id_zone=colIdZone,
                col_nom_zone=colNomZone,
                tab_zone=tabZone,
                poids_=self._getPoids(nbLignes, modeEnq),
                arret_fin=arretFin,
                arret_deb=arretDeb,
            )

        return strCreateZmd

    # -------------------------------------------------------------------------

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = Template(
                "\nSELECT max($col_) \n\
                 FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            max = cursor.fetchone()[0]
            if max is None:
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _createTableZonesTmp(self, nbLignes):
        """
        Création de la table zones_tmp
        """

        # Génération de la requête de création de table

        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table zones_tmp si elle existe
            s = "DROP TABLE IF EXISTS zones_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            strSql = self._genSqlZone(
                nbLignes, self.tabZone, self.colIdZone, self.colNomZone
            )
            self.logger.debug(strSql)
            cursor.execute(strSql)
            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
                 WHERE f_table_name = 'zones_tmp'"
            self.logger.debug(s)
            cursor.execute(s)
            # Insertion de la déclaration de la colonne geometry
            # (type 3 = polygone)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_zones_2.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------
    def _labelZoneMD(self):
        """
        Etiquetage de la couche
        """
        # on instancie la class de génération des expressions d'étiquetage
        lab = QMapOdLab()
        # Paramètres d'étiquetage
        intMode = int(self.qmapodSettings.value("params/cbxZoneLabel", 4))
        # on définit où le nom à afficher dans l'étiquette doit être récupéré
        nomCol = "nom_zone"
        idCol = "id_zone"
        lstMode = 1
        lstColumns = []
        affMessage = False
        nomColManq = ""

        # selection du nom des données manquantes en fonction de l'analyse,
        # validation de l'affichage du message d'avertissement
        #  suppression de l'étiquetage
        if intMode in [3, 4]:
            if self.typAnal == "m":
                nomColManq = "Descentes"
                affMessage = True
                lstMode = 0
            elif self.typAnal == "d":
                nomColManq = "Montées"
                affMessage = True
                lstMode = 0
        # affichages ne dépendant pas de l'analyse
        if intMode in (0, 5, 6):
            lstMode = intMode

        # pour analyse montées descentes tous les affichages sont possibles
        elif self.typAnal == "md":
            lstColumns = ["montees", "descentes"]
            if intMode == 3:
                lstMode = 2
            elif intMode == 2:
                lstMode = 4
            elif intMode == 4:
                lstMode = 3
        # pour analyse montées seules les données de montées sont affichées
        # on ne sélectionne que le champ montees
        elif self.typAnal == "m":
            lstColumns = ["montees"]
            if intMode == 2:
                lstMode = 3

        # fonctionnement identique pour l'analyse descente
        elif self.typAnal == "d":
            lstColumns = ["descentes"]
            if intMode == 2:
                lstMode = 3

        if affMessage:
            # affichage message d'avertissement
            msgBar = self.iface.messageBar()
            msg = msgBar.createMessage(
                Template(
                    "Affichage demandé non significatif données de $nom_col_manq absentes"
                ).substitute(nom_col_manq=format(nomColManq))
            )
            # premier chiffre niveau deuxième chiffre durée d'affichage
            msgBar.pushWidget(msg, 1, 5)

        blnExp = True

        # calcul de l'expression d'étiquetage
        strField = lab.lbl(idCol, nomCol, lstMode, lstColumns)

        palLayer = QgsPalLayerSettings()
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        palLayer.placement = QgsPalLayerSettings.AroundPoint
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderUnit.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 8, QFont.Normal))
        txtFormat.setSize(8)
        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.zmdLayer.setLabelsEnabled(True)
        self.zmdLayer.setLabeling(labeling)

    # ---------------------------  LEGENDE -------------------------

    def _legend(self):
        """
        Définition du type de légende et de l'échelle
        """
        lgd = QgsDataDefinedSizeLegend()
        # on définit le type de légende
        lgd.setLegendType(QgsDataDefinedSizeLegend.LegendCollapsed)
        lgd.setTitle("Légende")
        lgds = QgsMarkerSymbol()
        # couleur de remplissage de la légende
        lgds.symbolLayer(0).setColor(QColor("#d5d5d5"))
        # couleur de bordure de la légende
        lgds.symbolLayer(0).setStrokeColor(QColor("#ffffff"))
        # on définit 3 tailles de légendes: max, 1/2 et 1/10
        lgd.setClasses(
            [
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue), str(int(self.maxValue))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 2), str(int(self.maxValue / 2))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 10), str(int(self.maxValue / 10))
                ),
            ]
        )
        lgd.setSymbol(lgds)
        return lgd

    # -------------------------------------------------------------------------

    def delZoneMD(self):
        """
        Suppression de l'analyse en montées / descentes par zones
        """

        if isinstance(self.zmdLayer, QgsVectorLayer):

            QgsProject.instance().removeMapLayer(self.zmdLayer.id())
        self.zmdLayer = None

    # -------------------------------------------------------------------------

    def addZoneMD(self, nbLignes):
        """
        Affichage de l'analyse en montées / descentes par zones
        """

        self._createTableZonesTmp(nbLignes)
        self._addLayerZonesTmp()

        # Suppression de la symbologie par défaut (entités polygonales)
        symbolLayer = QgsSimpleFillSymbolLayer()
        symbolLayer.setStrokeWidth(0.4)
        symbolLayer.setStrokeColor(QColor(0, 0, 0, 255))
        symbolLayer.setFillColor(QColor(255, 255, 255, 0))
        self.zmdLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayer
        )

        # Load the diagram automatically
        # l = balayer
        # print(l.diagramRenderer().rendererName())

        # line numbers in comments refer to qgsdiagramproperties.cpp
        diagram = QgsPieDiagram()  # 500 - Type de diagramme

        ds = QgsDiagramSettings()  # 507
        # ds.font = QFont('Arial', 12)  # 508
        # ds.Left = 2  # ?
        # ds.Right = 3  # ?

        # transparency -> Opacity
        paramTransp = float(self.qmapodSettings.value("params/spxZoneTranspDiag", 30))
        fltAlpha = 1.0 - (paramTransp / 100)

        ds.opacity = fltAlpha
        colM = self.qmapodSettings.value(
            "params/btnZoneColMontees", QColor(255, 0, 0, 255), QColor
        )
        colM.setAlphaF(fltAlpha)
        colD = self.qmapodSettings.value(
            "params/btnZoneColDescentes", QColor(255, 0, 0, 255), QColor
        )
        colD.setAlphaF(fltAlpha)

        # Sélection des champs à prendre en compte pour les diagrammes
        if self.typAnal == "md":
            lstColumns = ["montees", "descentes"]  # Liste des champs
            lstColors = [colM, colD]  # Liste des couleurs
            lstLabels = ["Montées", "Descentes"]
            exp = "montees + descentes"
        elif self.typAnal == "m":
            lstColumns = ["montees"]  # Liste des champs
            lstColors = [colM]  # Liste des couleurs
            lstLabels = ["Montées"]
            exp = "montees"
        elif self.typAnal == "d":
            lstColumns = ["descentes"]  # Liste des champs
            lstColors = [colD]  # Liste des couleurs
            lstLabels = ["Descentes"]
            exp = "descentes"

        # ds.categoryAttributes = dAttributes.values()
        # ds.categoryColors = dColors.values()
        ds.categoryAttributes = lstColumns
        ds.categoryColors = lstColors
        ds.categoryLabels = lstLabels  # QGis 2.10

        ds.size = QSizeF(100.0, 100.0)  # 522 - Si taille fixe ?
        # ds.sizeType = 0  # 523 - mm(0), map units (1)
        ds.sizeType = QgsUnitTypes.RenderUnit.RenderMillimeters
        ds.labelPlacementMethod = 1  # 524 - from an existing example...
        ds.scaleByArea = True  # 525
        ds.minimumSize = 0.0  # 533

        # Couleur du contour des diagrammes
        # ds.backgroundColor = QColor(255, 255, 255, 0)  # 536 Transp White
        # ds.penColor = QColor(255, 255, 255, 255)  # 537
        outlineColor = self.qmapodSettings.value(
            "params/btnZoneColOutline", QColor(255, 255, 255, 255), QColor
        )
        penColor = outlineColor
        penColor.setAlphaF(fltAlpha)
        ds.penColor = penColor
        outlineWidth = float(
            self.qmapodSettings.value("params/spxZoneOutlineWidth", 0.4)
        )
        ds.penWidth = outlineWidth
        ds.diagramOrientation = 2  # 551 - May only be required for histograms
        # ds.barWidth = 5.0  # 553
        # We want a linear size interpolated version,
        # so that Total_BA = 0 means they disappear
        # 564-572
        # Voir aussi : QgsSingleCategoryDiagramRenderer
        dr = QgsLinearlyInterpolatedDiagramRenderer()
        dr.setLowerValue(0.0)
        dr.setLowerSize(QSizeF(0.0, 0.0))

        # Pour la légende
        self.maxValue = self._getMaxVal("zones_tmp", exp)
        dr.setUpperValue(self.maxValue)

        # Expression SQL spécifiant la taille
        dr.setClassificationAttributeExpression(exp)
        # La taille est spécifiée par une expression
        dr.setClassificationAttributeIsExpression(True)

        # Pour la légende
        # Taille de diagramme correspondant à la valeur maximale
        self.maxSize = int(self.qmapodSettings.value("params/spxZoneDiagMaxSize", 40))
        dr.setUpperSize(QSizeF(self.maxSize, self.maxSize))

        # dr.setClassificationAttribute(4) # Colonne spécifiant la taille
        dr.setDiagram(diagram)
        dr.setDiagramSettings(ds)

        # And now finally put it into the layer... :-)
        self.zmdLayer.setDiagramRenderer(dr)  # 572

        dls = QgsDiagramLayerSettings()  # 575
        dls.setDistance(0)
        dls.setIsObstacle(False)
        dls.setPriority(1)
        dls.setPlacement(QgsDiagramLayerSettings.OverPoint)  # 588
        dls.setShowAllDiagrams(True)
        dls.setZIndex(-1.0)
        self.zmdLayer.setDiagramLayerSettings(dls)  # 593

        # affichage légende dans boite Couches
        dr.setAttributeLegend(True)
        dr.setDataDefinedSizeLegend(self._legend())

        # Application du filtrage carto pour les zones
        self.zmdLayer.setSubsetString(self.currentFiltrage.dctWhereCarto[self.tabZone])

        # Refresh map
        self.zmdLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.zmdLayer.id())

        # Etiquetage de la couche
        self._labelZoneMD()

        # Pour contournement du bug sur majZoneMD
        self.tbn.setChecked(True)

    # -------------------------------------------------------------------------

    def majZoneMD(self, nbLignes):
        """
        Mise à jour de l'analyse en montées / descentes par zones
        """

        # Mise à jour de la table arrets_tmp
        self._createTableZonesTmp(nbLignes)

        # Sélection des champs à prendre en compte pour le calcul
        # de la valeur maximale
        if self.typAnal == "md":
            exp = "montees + descentes"
        elif self.typAnal == "m":
            exp = "montees"
        elif self.typAnal == "d":
            exp = "descentes"

        # Mise à jour de la valeur maximum des variables analysées
        dr = self.zmdLayer.diagramRenderer()
        # Pour la légende
        self.maxValue = self._getMaxVal("zones_tmp", exp)
        dr.setUpperValue(self.maxValue)
