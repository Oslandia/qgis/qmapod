"""
/***************************************************************************
 QMapODAnalFluxPP
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2023-07-31
        git sha              : $Format:%H$
        copyright            : (C) 2023 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

"""
TODOS :
- Couche zones polygones DONE
- Paramètrage des styles DONE
- Étiquetage des flèches et des zones DONE
- Mise à jour dynamique des nombres de flux DONE
- Coloration des flèches selon sens (flux par ligne) DONE
"""

import logging
import os.path
import sqlite3
from string import Template

# Import des librairies QGIS
from qgis.core import (
    Qgis,
    QgsArrowSymbolLayer,
    QgsCategorizedSymbolRenderer,
    QgsDataDefinedSizeLegend,
    QgsDataSourceUri,
    QgsFillSymbol,
    QgsLayerTreeGroup,
    QgsLayerTreeLayer,
    QgsLayerTreeNode,
    QgsLineSymbol,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsProject,
    QgsProperty,
    QgsPropertyCollection,
    QgsRenderContext,
    QgsRendererCategory,
    QgsSimpleFillSymbolLayer,
    QgsSimpleLineSymbolLayer,
    QgsSingleSymbolRenderer,
    QgsSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)
from qgis.PyQt.QtGui import QColor, QFont
from qgis.PyQt.QtWidgets import QApplication
from qgis.utils import spatialite_connect

# Import des librairies PyQt


module_logger = logging.getLogger("QMapOD.flux_principaux")


class QMapODFluxPP(object):
    """
    Analyses cartographiques par zones
    """

    def __init__(
        self,
        iface,
        tbn,
        clsFiltrage,
        typAnal,
        tabZone,
        colIdZone,
        colNomZone,
        titleZone,
        qmapodConfig,
        qmapodSettings,
    ):
        """
        Constructor
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Récupération de l'instance de la classe filtrage
        self.currentFiltrage = clsFiltrage
        # Type d'analyse ('pp' ou 'ppl')
        self.typAnal = typAnal
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Couche des flèches de flux
        self.flxppLayer = None
        # Couche des zones concernées
        self.selLayer = None
        # Groupe de couche des flux principaux
        self.fzGroup = None
        # Caractéristiques flèches
        self.maxSize = 0
        self.maxValue = 0
        # Titre de la couche
        self.strNom = ""

        # Récupération des noms de colonnes pour la variable analysée
        self.typAnal = typAnal
        self.tabZone = tabZone
        self.colIdZone = colIdZone
        self.colNomZone = colNomZone
        self.titleZone = titleZone

        self.logger = logging.getLogger("QMapOD.flux_principaux.QMapODFluxPP")
        self.logger.info("Creating an instance of QMapODFluxPP")

    def _addLayerFluxPPTmp(self):
        """
        Ajout de la couche zones_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "pp":
            strZone = "principaux"
        elif self.typAnal == "ppl":
            strZone = "principaux par ligne"

        self.strNom = Template("$str_zone par $title_zone").substitute(
            str_zone=strZone, title_zone=self.titleZone
        )

        # Ajout de la couche
        uriFluxPP = QgsDataSourceUri()
        uriFluxPP.setDatabase(self.db)
        uriFluxPP.setDataSource("", "fluxpp_tmp", "geometry")
        fluxppLayer = QgsVectorLayer(uriFluxPP.uri(), self.strNom, "spatialite")

        if fluxppLayer.isValid():
            # Ajout des couches et déplacement au-dessus
            # des couches existantes (api QgsLayerTree)
            root = QgsProject.instance().layerTreeRoot()

            self.fzGroup = root.insertGroup(0, "Flux principaux")
            # self.fzGroup.setExpanded(True)
            self.flxppLayer = QgsProject.instance().addMapLayer(fluxppLayer, False)

            self.flxppLayer.destroyed.connect(self._deleted)
            fluxPPTreeLayer = QgsLayerTreeLayer(fluxppLayer)

            fluxPPTreeLayer.setName(fluxppLayer.name())
            self.fzGroup.insertChildNode(0, fluxPPTreeLayer)

    # -------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.flxppLayer = None
        self.tbn.setChecked(False)

    # --------------------------------------------------------------------------

    def _getPoids(self, nbLignes, modeEnq):
        """
        Choix du mode de pondération à utiliser
        """

        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Modifié le 15/09/2015 : autorisation du mode voyage
        # lorsque tout le réseau est sélectionné
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        # ou sur la ligne selon la logique d'analyse
        # Voyage : poids sur la ligne - Déplacement : poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            if modeEnq == 1:
                return self.qmapodConfig.dctParam["poidsligne"]
            elif modeEnq == 2:
                return self.qmapodConfig.dctParam["poidsreseau"]
            else:
                return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # -------------------------------------------------------------------------

    def _genSqlFluxPP(self, nbMax, nbLignes, gpLigne, tabZone, colIdZone, colNomZone):
        """
        Génération de la requête de création de la table fluxpp_tmp
        """

        # Mode de prise en compte des enquêtes (celui des flux par zone)
        modeEnq = int(self.qmapodSettings.value("params/buttonGroupFluxZoneMode", 1))
        if modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]

        poids = self._getPoids(nbLignes, modeEnq)
        strGrp = ""
        strPart = ""
        if gpLigne:
            strGrp = "ligne, sens,"
            strPart = "PARTITION BY ligne, sens"

        # Chaîne formatée
        with open(
            os.path.join(
                self.qmapodConfig.pluginDir, "sql/queries/flux_principaux_1.sql"
            )
        ) as f:
            strSelectPP = Template(f.read()).substitute(
                str_grp=strGrp,
                col_id_zone=colIdZone,
                col_nom_zone=colNomZone,
                poids_=poids,
                arret_deb=arretDeb,
                arret_fin=arretFin,
                str_part=strPart,
                tab_zone=tabZone,
                nb_max=nbMax,
            )

        strCreatePP = Template("CREATE TABLE fluxpp_tmp AS $str_select_pp").substitute(
            str_select_pp=strSelectPP
        )

        return strCreatePP

    # -------------------------------------------------------------------------

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = spatialite_connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = Template(
                "\nSELECT max($col_) \n\
                 FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            max = cursor.fetchone()[0]
            self.logger.debug("getMaxVal : " + str(max))
            if max is None:
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s: " % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _createTableFluxPPTmp(self, nbMax, nbLignes, gpLigne):
        """
        Création de la table fluxpp_tmp
        """

        try:
            # Connexion à la base de données
            # db = sqlite3.connect(self.db)
            db = spatialite_connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table fluxpp_tmp si elle existe
            s = "DROP TABLE IF EXISTS fluxpp_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            strSql = self._genSqlFluxPP(
                nbMax, nbLignes, gpLigne, self.tabZone, self.colIdZone, self.colNomZone
            )
            self.logger.debug(strSql)
            cursor.execute(strSql)
            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
                 WHERE f_table_name = 'fluxpp_tmp'"
            self.logger.debug(s)
            cursor.execute(s)
            # Insertion de la déclaration de la colonne geometry
            # (type 2 = polyligne)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_principaux_2.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------
    def _labelFluxPP(self):
        """
        Étiquetage de la couche
        """

        # Étiquetage des flèches
        blnExp = True
        strField = 'round( "flux", 0)'

        palLayer = QgsPalLayerSettings()
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        palLayer.placement = Qgis.LabelPlacement.Horizontal
        palLayer.lineSettings().setLineAnchorPercent(1.0)

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setSizeUnit(QgsUnitTypes.RenderUnit.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 8, QFont.Bold))
        txtFormat.setSize(8)

        if self.typAnal == "pp":
            color = self.qmapodSettings.value(
                "params/btnFluxPPCol", QColor(31, 117, 180, 255), QColor
            )
            txtFormat.setColor(color)
        elif self.typAnal == "ppl":
            color1 = self.qmapodSettings.value(
                "params/btnFluxPPLColSens1", QColor(31, 117, 180, 255), QColor
            )
            color2 = self.qmapodSettings.value(
                "params/btnFluxPPLColSens2", QColor(31, 117, 180, 255), QColor
            )
            exp = Template(
                "\
                CASE WHEN 'sens' = 1 THEN '$red1, $green1, $blue1'\
                     WHEN 'sens' = 2 THEN '$red2, $green2, $blue2'\
                     ELSE '0, 0, 0'\
                END"
            ).substitute(
                red1=color1.red(),
                green1=color1.green(),
                blue1=color1.blue(),
                red2=color2.red(),
                green2=color2.green(),
                blue2=color2.blue(),
            )
            propCol = QgsPropertyCollection("qpc")
            prop = QgsProperty()
            prop.setExpressionString(exp)
            propCol.setProperty(QgsPalLayerSettings.Color, prop)
            palLayer.setDataDefinedProperties(propCol)

        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.flxppLayer.setLabelsEnabled(True)
        self.flxppLayer.setLabeling(labeling)

        # Étiquetage des zones concernées
        blnExp = False
        strField = self.colNomZone

        palLayerZone = QgsPalLayerSettings()
        palLayerZone.fieldName = strField
        palLayerZone.isExpression = blnExp

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayerZone.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderUnit.RenderPoints)  # Points
        font = QFont("Arial Narrow", 10, QFont.Bold)
        font.setItalic(True)
        txtFormat.setFont(font)
        txtFormat.setSize(10)
        palLayerZone.setFormat(txtFormat)

        labelingZone = QgsVectorLayerSimpleLabeling(palLayerZone)
        # TOFIX
        # self.selLayer.setLabelsEnabled(True)
        # self.selLayer.setLabeling(labelingZone)

    # ---------------------------  LEGENDE -------------------------

    def _legend(self):
        """
        Définition du type de légende et de l'échelle
        """
        lgd = QgsDataDefinedSizeLegend()
        # on définit le type de légende
        lgd.setLegendType(QgsDataDefinedSizeLegend.LegendCollapsed)
        lgd.setTitle("Légende")
        lgds = QgsMarkerSymbol()
        # couleur de remplissage de la légende
        lgds.symbolLayer(0).setColor(QColor("#d5d5d5"))
        # couleur de bordure de la légende
        lgds.symbolLayer(0).setStrokeColor(QColor("#ffffff"))
        # on définit 3 tailles de légendes: max, 1/2 et 1/10
        lgd.setClasses(
            [
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue), str(int(self.maxValue))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 2), str(int(self.maxValue / 2))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 10), str(int(self.maxValue / 10))
                ),
            ]
        )
        lgd.setSymbol(lgds)
        return lgd

    # -------------------------------------------------------------------------

    def _getZonesIds(self):
        """
        Récupération des identifiants des zones concernées
        """

        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()

            # Récupération des id des zones concernées
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_principaux_3.sql"
                )
            ) as f:
                strSql = Template(f.read()).substitute(
                    col_id_zone=self.colIdZone, tab_zone=self.tabZone
                )
                self.logger.debug(strSql)
                cursor.execute(strSql)

            records = cursor.fetchall()
            listIds = []
            for row in records:
                listIds.append(row[0])
            cursor.close()

        # Catch the exception
        except Exception as e:
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

        return listIds

    # -------------------------------------------------------------------------

    def _addSelZones(self):
        """
        Ajout de la couche des zones concernées par les flux
        """

        listIds = self._getZonesIds()
        if not listIds:
            return

        # Ajout de la couche des zones sélectionnés si elle n'existe pas
        if self.selLayer is None:
            uri = QgsDataSourceUri()
            uri.setDatabase(self.qmapodConfig.db)
            uri.setDataSource("", self.tabZone, "geometry")
            self.selLayer = QgsVectorLayer(uri.uri(), self.titleZone, "spatialite")
            self.selLayer.setSubsetString(
                Template("$col_id_zone IN($list_ids)").substitute(
                    col_id_zone=self.colIdZone, list_ids=", ".join(map(str, listIds))
                )
            )
            if self.selLayer.isValid():
                # Ajout des couches et déplacement au-dessus
                # des couches existantes (api QgsLayerTree)
                root = QgsProject.instance().layerTreeRoot()
                self.selLayerId = QgsProject.instance().addMapLayer(
                    self.selLayer, False
                )
                selTreeLayer = QgsLayerTreeLayer(self.selLayer)
                selTreeLayer.setName(self.selLayer.name())
                self.fzGroup.insertChildNode(1, selTreeLayer)
                self.logger.warning(isinstance(self.fzGroup, QgsLayerTreeGroup))
                self.logger.warning(isinstance(self.fzGroup.parent(), QgsLayerTreeNode))
                self.fzGroup.parent().setExpanded(False)
                self.logger.warning(self.fzGroup.parent().isExpanded())
                self.fzGroup.parent().setExpanded(True)
                self.logger.warning(self.fzGroup.parent().isExpanded())

                symbolLayer = QgsSimpleFillSymbolLayer()
                paramTransp = float(
                    self.qmapodSettings.value("params/spxFluxPPTranspZones", 50)
                )
                fltAlpha = 1.0 - (paramTransp / 100)
                color = self.qmapodSettings.value(
                    "params/btnFluxPPColZones", QColor(100, 100, 100, 255), QColor
                )
                color.setAlphaF(fltAlpha)
                symbolLayer.setFillColor(color)  # Paramétrable
                color = self.qmapodSettings.value(
                    "params/btnFluxPPColOutlineZones",
                    QColor(100, 100, 100, 255),
                    QColor,
                )
                symbolLayer.setStrokeColor(color)  # Paramétrable
                symbolLayer.setStrokeWidth(
                    float(
                        self.qmapodSettings.value(
                            "params/spxFluxPPOutlineWidthZones", 0.4
                        )
                    )
                )  # paramétrable

                self.selLayer.renderer().symbols(QgsRenderContext())[
                    0
                ].changeSymbolLayer(0, symbolLayer)
                self.selLayer.triggerRepaint()
                self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())

        # Mise à jour du sous-ensemble de la couche dans le cas contraire
        else:
            self.selLayer.setSubsetString(
                Template("$col_id_zone IN($list_ids)").substitute(
                    col_id_zone=self.colIdZone, list_ids=", ".join(map(str, listIds))
                )
            )

    # -------------------------------------------------------------------------

    def delFluxPP(self):
        """
        Suppression de l'analyse des flux principaux
        """

        # Groupe de couche des flux par zones
        if isinstance(self.fzGroup, QgsLayerTreeGroup):
            root = QgsProject.instance().layerTreeRoot()
            root.removeChildNode(self.fzGroup)
        self.fzGroup = None

        # Couches des flux flux principaux
        if isinstance(self.flxppLayer, QgsVectorLayer):
            QgsProject.instance().removeMapLayer(self.flxppLayer.id())
        self.flxppLayer = None

        # Couche des zones concernées
        if isinstance(self.selLayer, QgsVectorLayer):
            QgsProject.instance().removeMapLayer(self.selLayer.id())
        self.selLayer = None

    # -------------------------------------------------------------------------

    def addFluxPP(self, nbMax, nbLignes, gpLigne):
        """
        Affichage de l'analyse des flux principaux
        """

        self._createTableFluxPPTmp(nbMax, nbLignes, gpLigne)
        self._addLayerFluxPPTmp()
        self._addSelZones()

        # Suppression de la symbologie par défaut (entités linéaires)
        symbolLayer = QgsSimpleLineSymbolLayer()
        symbolLayer.setWidth(2)
        symbolLayer.setColor(QColor(0, 0, 0, 255))
        self.flxppLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayer
        )

        # Création du symbole ligne
        lineSymbol = QgsLineSymbol()

        # Création du symbole flèche
        arrowSymLayer = QgsArrowSymbolLayer()
        arrowSymLayer.setHeadType(QgsArrowSymbolLayer.HeadSingle)
        arrowSymLayer.setArrowType(QgsArrowSymbolLayer.ArrowPlain)
        # arrowSymLayer.setArrowWidth(8) # data defined
        self.maxValue = self._getMaxVal("fluxpp_tmp", "flux")
        # Taille de flèche correspondant à la valeur maximale
        self.maxSize = int(
            self.qmapodSettings.value("params/spxFluxPPLargMaxFleches", 12)
        )
        exp = Template(
            "scale_exp('flux', 0, $max_value, 0, $max_size, 0.57)"
        ).substitute(max_value=self.maxValue, max_size=self.maxSize)
        arrowSymLayer.setDataDefinedProperty(
            QgsSymbolLayer.PropertyArrowWidth, QgsProperty.fromExpression(exp)
        )
        arrowSymLayer.setArrowStartWidth(0)
        # arrowSymLayer.setHeadLength(8) # data defined
        arrowSymLayer.setDataDefinedProperty(
            QgsSymbolLayer.PropertyArrowHeadLength, QgsProperty.fromExpression(exp)
        )
        # arrowSymLayer.setHeadThickness(8) # data defined
        arrowSymLayer.setDataDefinedProperty(
            QgsSymbolLayer.PropertyArrowHeadThickness, QgsProperty.fromExpression(exp)
        )
        arrowSymLayer.setIsCurved(True)
        arrowSymLayer.setIsRepeated(False)

        # Création du symbole remplissage
        fillSymbol = QgsFillSymbol()
        fillSymLayer = QgsSimpleFillSymbolLayer()
        # transparency -> Opacity
        paramTransp = float(
            self.qmapodSettings.value("params/spxFluxPPTranspFleches", 50)
        )
        fltAlpha = 1.0 - (paramTransp / 100)
        color = self.qmapodSettings.value(
            "params/btnFluxPPCol", QColor(255, 0, 0, 255), QColor
        )
        color.setAlphaF(fltAlpha)
        fillSymLayer.setFillColor(color)  # paramétrable
        color = self.qmapodSettings.value(
            "params/btnFluxPPColOutline", QColor(255, 0, 0, 255), QColor
        )
        fillSymLayer.setStrokeColor(color)  # paramétrable
        fillSymLayer.setStrokeWidth(
            float(self.qmapodSettings.value("params/spxFluxPPOutlineWidth", 0.4))
        )  # paramétrable
        fillSymbol.changeSymbolLayer(0, fillSymLayer)

        # Assemblage du symbole flèche
        arrowSymLayer.setSubSymbol(fillSymbol)
        lineSymbol.changeSymbolLayer(0, arrowSymLayer)

        # Application du renderer
        if gpLigne:
            # Sens 1
            color = self.qmapodSettings.value(
                "params/btnFluxPPLColSens1", QColor(31, 117, 180, 128), QColor
            )
            color.setAlphaF(fltAlpha)
            lineSymbol.setColor(color)
            color = self.qmapodSettings.value(
                "params/btnFluxPPLColOutlineSens1", QColor(31, 117, 180, 255), QColor
            )
            lineSymbol.symbolLayers()[0].subSymbol().symbolLayers()[0].setStrokeColor(
                color
            )
            lineSymbol.symbolLayers()[0].subSymbol().symbolLayers()[0].setStrokeWidth(
                float(self.qmapodSettings.value("params/spxFluxPPOutlineWidth", 0.4))
            )
            catSens1 = QgsRendererCategory(1, lineSymbol.clone(), "Sens 1")

            # Sens 2
            color = self.qmapodSettings.value(
                "params/btnFluxPPLColSens2", QColor(51, 160, 44, 128), QColor
            )
            color.setAlphaF(fltAlpha)
            lineSymbol.setColor(color)
            color = self.qmapodSettings.value(
                "params/btnFluxPPLColOutlineSens2", QColor(51, 160, 44, 255), QColor
            )
            lineSymbol.symbolLayers()[0].subSymbol().symbolLayers()[0].setStrokeColor(
                color
            )
            lineSymbol.symbolLayers()[0].subSymbol().symbolLayers()[0].setStrokeWidth(
                float(self.qmapodSettings.value("params/spxFluxPPOutlineWidth", 0.4))
            )
            catSens2 = QgsRendererCategory(2, lineSymbol.clone(), "Sens 2")

            catRenderer = QgsCategorizedSymbolRenderer()
            catRenderer.setClassAttribute("sens")
            catRenderer.addCategory(catSens1)
            catRenderer.addCategory(catSens2)
            catRenderer.setSourceSymbol(lineSymbol)
            self.flxppLayer.setRenderer(catRenderer)
        else:
            self.flxppLayer.setRenderer(QgsSingleSymbolRenderer(lineSymbol))

        # Etiquetage de la couche
        self._labelFluxPP()

        # Refresh map
        self.flxppLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.flxppLayer.id())

        # Pour contournement du bug sur majFluxPP
        self.tbn.setChecked(True)

    # -------------------------------------------------------------------------

    def majFluxPP(self, nbMax, nbLignes, gpLigne):
        """
        Mise à jour de l'analyse en montées / descentes par zones
        """

        # Mise à jour de la table arrets_tmp
        self._createTableFluxPPTmp(nbMax, nbLignes, gpLigne)

        # Mise à jour de la valeur maximum de la variable analysée
        self.maxValue = self._getMaxVal("fluxpp_tmp", "flux")
        # Taille de flèche correspondant à la valeur maximale
        self.maxSize = 12
        exp = Template(
            "scale_exp('flux', 0, $max_value, 0, $max_size, 0.57)"
        ).substitute(max_value=self.maxValue, max_size=self.maxSize)

        flxPPRenderer = self.flxppLayer.renderer()
        arrowSymLayer = flxPPRenderer.symbols(QgsRenderContext())[0].symbolLayers()[0]
        arrowSymLayer.setDataDefinedProperty(
            QgsSymbolLayer.PropertyArrowWidth, QgsProperty.fromExpression(exp)
        )
        arrowSymLayer.setDataDefinedProperty(
            QgsSymbolLayer.PropertyArrowHeadLength, QgsProperty.fromExpression(exp)
        )
        arrowSymLayer.setDataDefinedProperty(
            QgsSymbolLayer.PropertyArrowHeadThickness, QgsProperty.fromExpression(exp)
        )

        # Mise à jour de la couche des zones concernées
        if isinstance(self.selLayer, QgsVectorLayer):
            QgsProject.instance().removeMapLayer(self.selLayer.id())
        self.selLayer = None
        self._addSelZones()

        # Etiquetage de la couche
        self._labelFluxPP()

        # Refresh map
        self.flxppLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.flxppLayer.id())
