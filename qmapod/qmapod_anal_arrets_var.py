"""
/***************************************************************************
 QMapODAnalArretVar
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

# Import des librairies QGIS
from qgis.core import (
    Qgis,
    QgsDataDefinedSizeLegend,
    QgsDataSourceUri,
    QgsDiagramLayerSettings,
    QgsDiagramSettings,
    QgsLayerTreeLayer,
    QgsLinearlyInterpolatedDiagramRenderer,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsPieDiagram,
    QgsProject,
    QgsRenderContext,
    QgsSimpleMarkerSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)
from qgis.PyQt.QtCore import QSizeF
from qgis.PyQt.QtGui import QColor, QFont
from qgis.PyQt.QtWidgets import QApplication

from .qmapod_lab import QMapOdLab

# Import des librairies PyQt


module_logger = logging.getLogger("QMapOD.anal_arrets_var")


class QMapODAnalArretVar(object):
    """
    Analyses cartographiques par arrêts sur une variable
    """

    def __init__(
        self,
        iface,
        tbn,
        clsFiltrage,
        typAnal,
        tabVar,
        colVar,
        colIdVar,
        colLibVar,
        titleVar,
        qmapodConfig,
        qmapodSettings,
    ):
        """
        Constructor
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Récupération de l'instance de la classe filtrage
        self.currentFiltrage = clsFiltrage
        # Couche générée
        self.avarLayer = None
        # Type d'analyse ("m" ou "d")
        self.typAnal = typAnal
        # Caractéristiques diagrammes
        self.maxSize = 0
        self.maxValue = 0
        # Titre de la couche
        self.strNom = ""

        # Mode de prise en compte des enquêtes
        self.modeEnq = int(qmapodSettings.value("params/buttonGroupArretMode", 1))
        if self.modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif self.modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]
        # Colonne arrêt selon type d'analyse (montées ou descentes)
        if typAnal == "m":
            self.colArret = arretDeb
        elif typAnal == "d":
            self.colArret = arretFin
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Paramètres d'analyse
        self.tabVar = tabVar
        self.colVar = colVar
        self.colIdVar = colIdVar
        self.colLibVar = colLibVar
        self.titleVar = titleVar

        self.logger = logging.getLogger("QMapOD.anal_arrets_var.QMapODAnalArretVar")
        self.logger.info("Creating an instance of QMapODAnalArretVar")

    def _majParamVar(self):
        """
        Vérification de l'existence d'un jeu de données filtrées
        et récupération des noms de colonne pour la variable analysée
        """

        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()

            # Création de la table code_ligne_var pour les lignes filtrées
            if self.tabVar == "code_ligne_var":
                strSql = "\nDROP TABLE IF EXISTS code_ligne_var;"
                self.logger.debug(strSql)
                cursor.execute(strSql)
                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_var_1.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(
                        id_ligne=self.qmapodConfig.dctParam["ligne"]
                    )
                self.logger.debug(strSql)
                cursor.execute(strSql)

            elif self.tabVar == "code_ligne_coram":
                strSql = "\nDROP TABLE IF EXISTS code_ligne_coram;"
                self.logger.debug(strSql)
                cursor.execute(strSql)
                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_var_2.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(
                        id_ligne_amont=self.qmapodConfig.dctParam["ligneamont"]
                    )
                self.logger.debug(strSql)
                cursor.execute(strSql)

            elif self.tabVar == "code_ligne_corav":
                strSql = "\nDROP TABLE IF EXISTS code_ligne_corav;"
                self.logger.debug(strSql)
                cursor.execute(strSql)
                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_var_3.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(
                        id_ligne_aval=self.qmapodConfig.dctParam["ligneaval"]
                    )
                self.logger.debug(strSql)
                cursor.execute(strSql)

            # Récupération des noms de colonnes pour la variable analysée
            strSql = Template("\nSELECT $col_var FROM $tab_var;").substitute(
                col_var=self.colVar, tab_var=self.tabVar
            )
            self.logger.debug(strSql)
            cursor.execute(strSql)
            self.valueVarAnal = cursor.fetchall()
            # Récupération des libellés pour la variable analysée
            strSql = Template("\nSELECT $col_lib_var FROM $tab_var;").substitute(
                col_lib_var=self.colLibVar, tab_var=self.tabVar
            )
            self.logger.debug(strSql)
            cursor.execute(strSql)
            self.libVarAnal = cursor.fetchall()

            # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

        # Vérification de la présence de valeurs de la variable analysée
        # pour le sous-réseau sélectionné
        if len(self.valueVarAnal) == 0:
            return False

        # on récupère la valeur de transparence des paramètres
        paramTransp = float(self.qmapodSettings.value("params/spxArretTranspDiag", 30))
        # on la transforme en opacité
        # TODO modifier nom paramètre et récuperer directement la valeur
        fltAlpha = 1.0 - (paramTransp / 100)

        # Génération de paramètres d'analyse
        self.lstValueVarAnal = [str(a[0]) for a in self.valueVarAnal]
        self.lstLibVarAnal = [(a[0]) for a in self.libVarAnal]

        # Couleurs aléatoires
        # Création des couleurs de la configuration json
        # (issues de http://www.zonums.com/online/color_palette/)
        lstColor = []
        for lstRGB in self.qmapodConfig.lstColors:
            colA = QColor(lstRGB[0], lstRGB[1], lstRGB[2])
            colA.setAlphaF(fltAlpha)
            lstColor.append(colA)
        # Ajout des couleurs en bouclant s'il en faut plus
        # que les 33 de la liste
        self.lstColorVarAnal = []
        # for i in range(0, 1 + (len(self.valueVarAnal) / len(lstColor))):
        for i in range(0, 1 + (len(self.valueVarAnal) // len(lstColor))):
            self.lstColorVarAnal.extend(lstColor)

        self.expVarAnal = " + ".join(str(a[0]) for a in self.valueVarAnal)

        return True

    # ------------------------------------------------------------------------

    def _addLayerArretsTmp(self):
        """
        Ajout de la couche arrets_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "m":
            strArret = "montée"
        elif self.typAnal == "d":
            strArret = "descente"

        self.strNom = Template("$title_var par arrêts de $str_arret").substitute(
            title_var=self.titleVar, str_arret=strArret
        )

        # Ajout de la couche
        uriArrets = QgsDataSourceUri()
        uriArrets.setDatabase(self.db)
        uriArrets.setDataSource("", "arrets_tmp_var", "geometry")
        arretsLayer = QgsVectorLayer(uriArrets.uri(), self.strNom, "spatialite")
        if arretsLayer.isValid():
            # Ajout des couches et déplacement au-dessus
            # des couches existantes (api QgsLayerTree)
            root = QgsProject.instance().layerTreeRoot()

            self.avarLayer = QgsProject.instance().addMapLayer(arretsLayer, False)
            self.avarLayer.destroyed.connect(self._deleted)
            arretsTreeLayer = QgsLayerTreeLayer(arretsLayer)
            arretsTreeLayer.setName(arretsLayer.name())

            root.insertChildNode(0, arretsTreeLayer)

    # ------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.avarLayer = None
        self.tbn.setChecked(False)

    # ------------------------------------------------------------------------

    def _getPoids(self, nbLignes, modeEnq):
        """
        Choix du mode de pondération à utiliser
        """

        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        # ou sur la ligne selon la logique d'analyse
        # Voyage : poids sur la ligne - Déplacement : poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            if modeEnq == 1:
                return self.qmapodConfig.dctParam["poidsligne"]
            elif modeEnq == 2:
                return self.qmapodConfig.dctParam["poidsreseau"]
            else:
                return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # -------------------------------------------------------------------------

    def _genSqlVar(self, nbLignes, colArret, tabVar, colVar, colIdVar, valueVarAnal):
        """
        Génération de la requête de création de la table arrets_tmp_var
        """

        poids = self._getPoids(nbLignes, self.modeEnq)

        if len(valueVarAnal) == 0:
            valueVarAnal.append("toto")

        # Requête sans arrondis
        # TODO BUG : les deux derniers colvar correspondent à enqfield

        # 17/12/2019 - Correction d'une erreur d'arrondi - Ajout fonctions round()
        strVar = Template(", round(cast(sum(ifnull(CASE WHEN v.$col_var='").substitute(
            col_var=colVar
        )
        s = strVar.join(
            (
                Template(
                    "$value' THEN v.NB END, 0)) AS float), 0) AS $value"
                ).substitute(value=str(a[0]))
            )
            for a in valueVarAnal
        )

        strCol2 = Template(
            ", round(cast(sum(ifnull(CASE WHEN v.$col_var='$var"
        ).substitute(col_var=colVar, var=s)

        strSelectVar = Template(
            "\n(\n\
                SELECT $col_arret$str_col2 \n\
                FROM ( \n\
                    SELECT $col_arret, ct.$col_var, cast(sum(e.$poids_) AS float) NB \n\
                    FROM enquetes_tmp e LEFT JOIN $tab_var ct ON e.$col_var = ct.$col_id_var \n\
                    GROUP BY $col_arret, e.$col_var \n\
                ) v \n\
                GROUP BY v.$col_arret \n\
            ) var"
        ).substitute(
            col_arret=colArret,
            str_col2=strCol2,
            col_var=colVar,
            poids_=poids,
            tab_var=tabVar,
            col_id_var=colIdVar,
        )

        strCol1 = ", ".join(
            (Template("var.$value").substitute(value=str(a[0]))) for a in valueVarAnal
        )

        with open(
            os.path.join(
                self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_var_4.sql"
            )
        ) as f:
            strCreateAmdVar = Template(f.read()).substitute(
                str_col1=strCol1, str_select_var=strSelectVar, col_arret=colArret
            )

        return strCreateAmdVar

    # -------------------------------------------------------------------------

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Get the maximum of the column
            strSql = Template(
                "\nSELECT max($col_) \n\
                 FROM $tab_"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(strSql)
            cursor.execute(strSql)
            max = cursor.fetchone()[0]
            if max is None:
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _createTableArretsTmp(self, nbLignes):
        """
        Création de la table arrets_tmp_var
        """
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()

            # Suppression de la table arrets_tmp_var si elle existe
            s = "\nDROP TABLE IF EXISTS arrets_tmp_var;"
            self.logger.debug(s)
            cursor.execute(s)

            # Création de la table arrets_tmp_var
            s = self._genSqlVar(
                nbLignes,
                self.colArret,
                self.tabVar,
                self.colVar,
                self.colIdVar,
                self.valueVarAnal,
            )
            self.logger.debug(s)
            cursor.execute(s)

            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns\
                 WHERE f_table_name = 'arrets_tmp_var'"
            self.logger.debug(s)
            cursor.execute(s)

            # Insertion de la déclaration de la colonne geometry
            # (type 1 = point)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_var_5.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    code_epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)

            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            self.logger.error("Error %s:" % e.args[0])
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

    # ---------------- ETIQUETAGE ARRETS --------------------------------------

    def _labelArretVar(self):
        """
        Etiquetage simplifié de la couche

        Options d'étiquetage de la couche: intMode
        0  : Aucun
        1  : Selon analyse
        2  : Nom - Selon analyse
        3  : Montees + Descentes
        4  : Nom - Montees + Descentes
        5  : Nom
        6  : ID

        Modes d'étiquetage: lstMode
        0  : Aucun
        1  : Concaténation des valeurs
        2  : Somme des valeurs
        3  : Nom - Somme des valeurs
        4  : Nom - Concaténation des valeurs
        5  : Nom
        6  : ID
        """

        # on instancie la class de génération des expressions d'étiquetage
        lab = QMapOdLab()
        # on récupère le type d'étiquetage demandé
        intMode = int(self.qmapodSettings.value("params/cbxArretLabel", 4))
        # on définit où le nom à afficher dans l'étiquette doit être récupéré
        nomCol = "nom_arret"
        idCol = "id_arret"
        # Sélection des champs à prendre en compte pour les étiquettes
        lstColumns = self.lstValueVarAnal
        lstMode = 1

        if intMode == (0, 5, 6):
            lstMode = intMode
        elif self.typAnal in ("m", "d"):
            if intMode in (1, 2):
                lstMode = intMode + 1
            else:
                # modifier ci-dessous pour les cas non significatifs
                # 0: rien 2: selon analyse
                if self.typAnal == "m":
                    nomColManq = "Descentes"
                else:
                    nomColManq = "Montees"

                # affichage message d'avertissement
                msgBar = self.iface.messageBar()
                msg = msgBar.createMessage(
                    Template(
                        "Affichage demandé non significatif: \
                        données absentes $nom_col_manq"
                    ).substitute(nom_col_manq=nomColManq)
                )

                # premier chiffre niveau deuxième chiffre durée d'affichage
                msgBar.pushWidget(msg, 1, 5)

                lstMode = 0
        else:
            lstMode = 0

        blnExp = True

        # calcul de l'expression d'étiquetage
        strField = lab.lbl(idCol, nomCol, lstMode, lstColumns)

        palLayer = QgsPalLayerSettings()
        # palLayer.enabled = blnEnabled
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        palLayer.placement = QgsPalLayerSettings.AroundPoint
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderUnit.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 8, QFont.Normal))
        txtFormat.setSize(8)
        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.avarLayer.setLabelsEnabled(True)
        self.avarLayer.setLabeling(labeling)

    # ---------------------------  LEGENDE -------------------------

    def _legend(self):
        """
        Définition du type de légende et de l'échelle
        """
        lgd = QgsDataDefinedSizeLegend()
        # on définit le type de légende
        lgd.setLegendType(QgsDataDefinedSizeLegend.LegendCollapsed)
        lgd.setTitle("Légende")
        lgds = QgsMarkerSymbol()
        # couleur de remplissage de la légende
        lgds.symbolLayer(0).setColor(QColor("#d5d5d5"))
        # couleur de bordure de la légende
        lgds.symbolLayer(0).setStrokeColor(QColor("#ffffff"))
        # on définit 3 tailles de légendes: max, 1/2 et 1/10
        lgd.setClasses(
            [
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue), str(int(self.maxValue))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 2), str(int(self.maxValue / 2))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 10), str(int(self.maxValue / 10))
                ),
            ]
        )
        lgd.setSymbol(lgds)
        return lgd

    # ------------------------------------------------------------------------

    def delArretVar(self):
        """
        Suppression de l'analyse en montées / descentes par variable par arrêts
        """

        if isinstance(self.avarLayer, QgsVectorLayer):
            QgsProject.instance().removeMapLayer(self.avarLayer.id())
        self.avarLayer = None

    # ------------------------------------------------------------------------

    def addArretVar(self, nbLignes):
        """
        Affichage de l'analyse en montées / descentes par variable par arrêts
        """
        # Mise à jour des tables de codage
        # Permet d'afficher uniquement les lignes filtrées dans la légende
        # pour les lignes, lignes amont, lignes aval
        # Le traitement est interrompu s'il n'y a pas de valeurs
        # pour la variable et le sous-réseau sélectionné
        res = self._majParamVar()
        if not res:
            msgBar = self.iface.messageBar()
            msg = msgBar.createMessage(
                "Aucune valeur pour la variable et le sous-réseau sélectionnés."
            )
            msgBar.pushWidget(msg, Qgis.Warning, 3)
            self.tbn.setChecked(False)
            return

        self._createTableArretsTmp(nbLignes)
        self._addLayerArretsTmp()

        # Suppression de la symbologie par défaut (entités ponctuelles)
        symbolLayer = QgsSimpleMarkerSymbolLayer()
        symbolLayer.setSize(0.0)
        symbolLayer.setColor(QColor(255, 255, 255, 255))
        self.avarLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayer
        )

        # line numbers in comments refer to qgsdiagramproperties.cpp
        diagram = QgsPieDiagram()  # 500 - Type de diagramme

        ds = QgsDiagramSettings()  # 507

        # Récuparation paramètre transparence
        paramTransp = float(self.qmapodSettings.value("params/spxArretTranspDiag", 30))
        fltAlpha = 1.0 - (paramTransp / 100)

        ds.opacity = fltAlpha

        # Sélection des champs à prendre en compte pour les diagrammes
        exp = self.expVarAnal
        ds.categoryAttributes = self.lstValueVarAnal  # Liste des champs
        ds.categoryColors = self.lstColorVarAnal  # Liste des couleurs
        ds.categoryLabels = self.lstLibVarAnal

        ds.size = QSizeF(100.0, 100.0)  # 522 - Si taille fixe ?
        # ds.sizeType = 0  # 523 - mm(0), map units (1)
        ds.sizeType = QgsUnitTypes.RenderUnit.RenderMillimeters
        ds.labelPlacementMethod = 1  # 524 - from an existing example...
        ds.scaleByArea = True  # 525
        ds.minimumSize = 0.0  # 533

        # Couleur du contour des diagrammes
        outlineColor = self.qmapodSettings.value(
            "params/btnArretColOutline", QColor(255, 255, 255, 255), QColor
        )
        penColor = outlineColor
        penColor.setAlphaF(fltAlpha)
        ds.penColor = penColor
        outlineWidth = float(
            self.qmapodSettings.value("params/spxArretOutlineWidth", 0.4)
        )
        ds.penWidth = outlineWidth
        ds.diagramOrientation = 2  # 551 - May only be required for histograms
        dr = QgsLinearlyInterpolatedDiagramRenderer()
        dr.setLowerValue(0.0)
        dr.setLowerSize(QSizeF(0.0, 0.0))

        # Pour la légende
        self.maxValue = self._getMaxVal("arrets_tmp_var", exp)
        dr.setUpperValue(self.maxValue)

        # Expression SQL spécifiant la taille
        dr.setClassificationAttributeExpression(exp)
        # La taille est spécifiée par une expression
        dr.setClassificationAttributeIsExpression(True)

        # Pour la légende
        # Taille de diagramme correspondant à la valeur maximale
        self.maxSize = int(self.qmapodSettings.value("params/spxArretDiagMaxSize", 40))
        dr.setUpperSize(QSizeF(self.maxSize, self.maxSize))

        # dr.setClassificationAttribute(4)  # Colonne spécifiant la taille
        dr.setDiagram(diagram)
        dr.setDiagramSettings(ds)

        # And now finally put it into the layer... :-)
        self.avarLayer.setDiagramRenderer(dr)  # 572

        dls = QgsDiagramLayerSettings()
        dls.setDistance(0)
        dls.setIsObstacle(False)
        dls.setPriority(1)
        dls.setPlacement(QgsDiagramLayerSettings.OverPoint)
        dls.setShowAllDiagrams(True)
        dls.setZIndex(-1.0)
        self.avarLayer.setDiagramLayerSettings(dls)  # 593

        # Affichage de la légende
        dr.setDataDefinedSizeLegend(self._legend())

        # Application du filtrage carto pour les arrêts
        self.avarLayer.setSubsetString(self.currentFiltrage.dctWhereCarto["id_arret"])

        # Refresh map
        self.avarLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.avarLayer.id())

        # Eiquetage couche
        self._labelArretVar()

        # Pour contournement du bug sur majArretVar
        self.tbn.setChecked(True)

    # ------------------------------------------------------------------------

    def majArretVar(self, nbLignes):
        """
        Mise à jour de l'analyse en montées / descentes par variable par arrêts
        """

        # Mise à jour de la table arrets_tmp
        self._createTableArretsTmp(nbLignes)

        # Sélection des champs à prendre en compte pour le calcul
        # de la valeur maximale
        exp = self.expVarAnal

        # Mise à jour de la valeur maximum des variables analysées
        dr = self.avarLayer.diagramRenderer()
        # Pour la légende
        self.maxValue = self._getMaxVal("arrets_tmp_var", exp)
        dr.setUpperValue(self.maxValue)

        # Eiquetage couche
        self._labelArretVar()
