"""
/***************************************************************************
 QMapODAnalZoneVar
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

from qgis.core import (
    Qgis,
    QgsDataDefinedSizeLegend,
    QgsDataSourceUri,
    QgsDiagramLayerSettings,
    QgsDiagramSettings,
    QgsLayerTreeLayer,
    QgsLinearlyInterpolatedDiagramRenderer,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsPieDiagram,
    QgsProject,
    QgsRenderContext,
    QgsSimpleFillSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)

# Import des librairies PyQt
from qgis.PyQt.QtCore import QSizeF
from qgis.PyQt.QtGui import QColor, QFont
from qgis.PyQt.QtWidgets import QApplication

from .qmapod_lab import QMapOdLab

module_logger = logging.getLogger("QMapOD.anal_zones_var")


class QMapODAnalZoneVar(object):
    """
    Analyses cartographiques par zones sur une variable
    """

    def __init__(
        self,
        iface,
        tbn,
        clsFiltrage,
        typAnal,
        tabZone,
        colIdZone,
        colNomZone,
        titleZone,
        tabVar,
        colVar,
        colIdVar,
        colLibVar,
        titleVar,
        qmapodConfig,
        qmapodSettings,
    ):
        """
        Constructor
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Récupération de l'instance de la classe filtrage
        self.currentFiltrage = clsFiltrage
        # Type d'analyse ("md", "m" ou "d")
        self.typAnal = typAnal
        # Couche générée
        self.zvarLayer = None
        # Caractéristiques diagrammes
        self.maxSize = 0
        self.maxValue = 0
        # Titre de la couche
        self.strNom = ""

        # Type d'analyse ("m" ou "d")
        self.typAnal = typAnal
        # Mode de prise en compte des enquêtes
        self.modeEnq = int(qmapodSettings.value("params/buttonGroupZoneMode", 1))
        if self.modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif self.modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]
        # Colonne arrêt selon type d'analyse (montées ou descentes)
        if typAnal == "m":
            self.colArret = arretDeb
        elif typAnal == "d":
            self.colArret = arretFin
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Récupération des noms de colonnes pour la variable analysée
        self.tabZone = tabZone
        self.colIdZone = colIdZone
        self.colNomZone = colNomZone
        self.titleZone = titleZone
        self.tabVar = tabVar
        self.colVar = colVar
        self.colIdVar = colIdVar
        self.colLibVar = colLibVar
        self.titleVar = titleVar

        self.logger = logging.getLogger("QMapOD.anal_zones_var.QMapODAnalZonesVar")
        self.logger.info("Creating an instance of QMapODAnalZonesVar")

    def _majParamVar(self):
        """
        Vérification de l'existence d'un jeu de données filtrées
        et récupération des noms de colonne pour la variable analysée
        """

        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()

            # Création de la table code_ligne_var pour les lignes filtrées
            if self.tabVar == "code_ligne_var":
                s = "DROP TABLE IF EXISTS code_ligne_var;"
                self.logger.debug(s)
                cursor.execute(s)

                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/anal_zones_var_1.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(
                        ligne=self.qmapodConfig.dctParam["ligne"]
                    )
                    self.logger.debug(strSql)
                    cursor.execute(strSql)

            elif self.tabVar == "code_ligne_coram":
                s = "DROP TABLE IF EXISTS code_ligne_coram;"
                self.logger.debug(s)
                cursor.execute(s)

                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/anal_zones_var_2.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(
                        ligne_amont=self.qmapodConfig.dctParam["ligneamont"]
                    )
                    self.logger.debug(strSql)
                    cursor.execute(strSql)

            elif self.tabVar == "code_ligne_corav":
                s = "\nDROP TABLE IF EXISTS code_ligne_corav;"
                self.logger.debug(s)
                cursor.execute(s)

                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/anal_zones_var_3.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(
                        ligne_aval=self.qmapodConfig.dctParam["ligneaval"]
                    )
                    self.logger.debug(strSql)
                    cursor.execute(strSql)

            # Récupération des noms de colonnes pour la variable analysée
            s = Template(
                "\nSELECT $col_var \n\
                     FROM $tab_var;"
            ).substitute(col_var=self.colVar, tab_var=self.tabVar)
            self.logger.debug(s)
            cursor.execute(s)

            self.valueVarAnal = cursor.fetchall()

            # Récupération des libellés pour la variable analysée
            s = Template(
                "\nSELECT $col_lib_var \n\
                     FROM $tab_var;"
            ).substitute(col_lib_var=self.colLibVar, tab_var=self.tabVar)
            self.logger.debug(s)
            cursor.execute(s)

            self.libVarAnal = cursor.fetchall()

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

        # Vérification de la présence de valeurs de la variable analysée
        # pour le sous-réseau sélectionné
        if len(self.valueVarAnal) == 0:
            return False

        # on récupère le paramètre transparency -> Opacity
        paramTransp = float(self.qmapodSettings.value("params/spxZoneTranspDiag", 30))
        fltAlpha = 1.0 - (paramTransp / 100)

        # Génération de paramètres d'analyse
        self.lstValueVarAnal = [str(a[0]) for a in self.valueVarAnal]
        self.lstLibVarAnal = [(a[0]) for a in self.libVarAnal]

        # Couleurs de la configuration json
        # (issues de http://www.zonums.com/online/color_palette/)
        lstColor = []
        for lstRGB in self.qmapodConfig.lstColors:
            colA = QColor(lstRGB[0], lstRGB[1], lstRGB[2])
            colA.setAlphaF(fltAlpha)
            lstColor.append(colA)
        # Ajout des couleurs en bouclant s'il en faut plus que les 33 de la liste
        self.lstColorVarAnal = []

        ### M à J QGis 3.4  erreur ne renvoie pas un entier
        # for i in range(0, 1 + (len(self.valueVarAnal) / len(lstColor))):
        for i in range(0, 1 + (len(self.valueVarAnal) // len(lstColor))):
            self.lstColorVarAnal.extend(lstColor)

        self.expVarAnal = " + ".join(str(a[0]) for a in self.valueVarAnal)

        return True

    # -------------------------------------------------------------------------

    def _addLayerZonesTmp(self):
        """
        Ajout de la couche zones_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "m":
            strType = "montées"
        elif self.typAnal == "d":
            strType = "descentes"

        self.strNom = Template("$title_var par $title_zone de $str_type").substitute(
            title_var=self.titleVar, title_zone=self.titleZone, str_type=strType
        )

        # Ajout de la couche
        uriZones = QgsDataSourceUri()
        uriZones.setDatabase(self.db)
        uriZones.setDataSource("", "zones_tmp_var", "geometry")
        zonesLayer = QgsVectorLayer(uriZones.uri(), self.strNom, "spatialite")
        if zonesLayer.isValid():
            # Ajout des couches et déplacement au-dessus des couches existantes
            # (api QgsLayerTree)
            root = QgsProject.instance().layerTreeRoot()

            self.zvarLayer = QgsProject.instance().addMapLayer(zonesLayer, False)
            self.zvarLayer.destroyed.connect(self._deleted)
            zonesTreeLayer = QgsLayerTreeLayer(zonesLayer)
            zonesTreeLayer.setName(zonesLayer.name())

            root.insertChildNode(0, zonesTreeLayer)

    # -------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.zvarLayer = None
        self.tbn.setChecked(False)

    def _getPoids(self, nbLignes, modeEnq):
        """
        Choix du mode de pondération à utiliser
        """

        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Modifié le 15/09/2015 : autorisation du mode voyage
        # lorsque tout le réseau est sélectionné
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        # ou sur la ligne selon la logique d'analyse
        # Voyage : poids sur la ligne - Déplacement : poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            if modeEnq == 1:
                return self.qmapodConfig.dctParam["poidsligne"]
            elif modeEnq == 2:
                return self.qmapodConfig.dctParam["poidsreseau"]
            else:
                return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # -------------------------------------------------------------------------

    def _genSqlZoneVar(
        self,
        nbLignes,
        colArret,
        tabZone,
        colIdZone,
        colNomZone,
        tabVar,
        colVar,
        colIdVar,
        valueVarAnal,
    ):
        """
        Génération de la requête de création de la table zones_tmp
        """

        poids = self._getPoids(nbLignes, self.modeEnq)

        # 17/12/2019 - Correction d'une erreur d'arrondi
        strVar = Template(
            ", round(cast(sum(ifnull(CASE WHEN ev.$col_var = '"
        ).substitute(col_var=colVar)
        strCol2 = Template(
            ", round(cast(sum(ifnull(CASE WHEN ev.$col_var = '$str_var"
        ).substitute(
            col_var=colVar,
            str_var=strVar.join(
                Template(
                    "$val1' THEN ev.poids END, 0)) AS float), 0) AS $val1"
                ).substitute(val1=str(a[0]))
                for a in valueVarAnal
            ),
        )

        strCol1 = Template("var.$a").substitute(
            a=", var.".join(str(a[0]) for a in valueVarAnal)
        )

        with open(
            os.path.join(
                self.qmapodConfig.pluginDir, "sql/queries/anal_zones_var_4.sql"
            )
        ) as f:
            strCreateZmdVar = Template(f.read()).substitute(
                col_id_zone=colIdZone,
                col_nom_zone=colNomZone,
                str_col1=strCol1,
                str_col_2=strCol2,
                tab_zone=tabZone,
                col_arret=colArret,
                col_var=colVar,
                poids_=poids,
                tab_var=tabVar,
                col_id_var=colIdVar,
            )

        return strCreateZmdVar

    # -------------------------------------------------------------------------

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = Template(
                "\nSELECT max($col_) \n\
                     FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            max = cursor.fetchone()[0]
            if max is None:
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _createTableZonesTmp(self, nbLignes):
        """
        Création de la table zones_tmp_var
        """

        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table zones_tmp_var si elle existe
            s = "DROP TABLE IF EXISTS zones_tmp_var"
            self.logger.debug(s)
            cursor.execute(s)

            # Création de la table zones_tmp_var
            strSql = self._genSqlZoneVar(
                nbLignes,
                self.colArret,
                self.tabZone,
                self.colIdZone,
                self.colNomZone,
                self.tabVar,
                self.colVar,
                self.colIdVar,
                self.valueVarAnal,
            )
            self.logger.debug(strSql)
            cursor.execute(strSql)

            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns\n\
                 WHERE f_table_name = 'zones_tmp_var'"
            self.logger.debug(s)
            cursor.execute(s)

            # Insertion de la déclaration de la colonne geometry
            # (type 3 = polygone)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_zones_var_5.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
            self.logger.debug(strInsert)
            cursor.execute(strInsert)

            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------
    def _labelZoneVar(self):
        """
        Etiquetage simplifié de la couche

        Options d'étiquetage de la couche: intMode
        0  : Aucun
        1  : Selon analyse
        2  : Nom - Selon analyse
        3  : Montees + Descentes
        4  : Nom - Montees + Descentes
        5  : Nom
        6  : ID

        Modes d'étiquetage: lstMode
        0  : Aucun
        1  : Concaténation des valeurs
        2  : Somme des valeurs
        3  : Nom - Somme des valeurs
        4  : Nom - Concaténation des valeurs
        5  : Nom
        6  : ID
        """
        # on instancie la class de génération des expressions d'étiquetage
        lab = QMapOdLab()
        # on récupère le type d'étiquetage demandé
        intMode = int(self.qmapodSettings.value("params/cbxZoneLabel", 4))
        # on définit où le nom à afficher dans l'étiquette doit être récupéré
        nomCol = "nom_zone"
        idCol = "id_zone"
        # Sélection des champs à prendre en compte pour les étiquettes
        lstColumns = self.lstValueVarAnal
        lstMode = 1

        if intMode in (0, 5, 6):
            lstMode = intMode
        elif self.typAnal in ("m", "d"):
            if intMode in (1, 2):
                lstMode = intMode + 1
            else:
                # modifier ci-dessous pour les cas non significatifs
                # 0: rien 2: selon analyse
                if self.typAnal == "m":
                    nomColManq = "Descentes"
                else:
                    nomColManq = "Montees"

                # affichage message d'avertissement
                msgBar = self.iface.messageBar()
                msg = msgBar.createMessage(
                    Template(
                        "Affichage demandé non significatif: \
                        données absentes $nom_col_manq"
                    ).substitute(nom_col_manq=nomColManq)
                )

                # premier chiffre niveau deuxième chiffre durée d'affichage
                msgBar.pushWidget(msg, 1, 5)

                lstMode = 0
        else:
            lstMode = 0
        blnExp = True

        # calcul de l'expression d'étiquetage
        strField = lab.lbl(idCol, nomCol, lstMode, lstColumns)

        palLayer = QgsPalLayerSettings()
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        palLayer.placement = QgsPalLayerSettings.AroundPoint
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderUnit.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 8, QFont.Normal))
        txtFormat.setSize(8)
        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.zvarLayer.setLabelsEnabled(True)
        self.zvarLayer.setLabeling(labeling)

    # ------------------- LEGENDE  -------------------------------------------

    def _legend(self):
        """
        Définition du type de légende et de l'échelle
        """
        lgd = QgsDataDefinedSizeLegend()
        # on définit le type de légende
        lgd.setLegendType(QgsDataDefinedSizeLegend.LegendCollapsed)
        lgd.setTitle("Légende")
        lgds = QgsMarkerSymbol()
        # couleur de remplissage de la légende
        lgds.symbolLayer(0).setColor(QColor("#d5d5d5"))
        # couleur de bordure de la légende
        lgds.symbolLayer(0).setStrokeColor(QColor("#ffffff"))
        # on définit 3 tailles de légendes: max, 1/2 et 1/10
        lgd.setClasses(
            [
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue), str(int(self.maxValue))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 2), str(int(self.maxValue / 2))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 10), str(int(self.maxValue / 10))
                ),
            ]
        )
        lgd.setSymbol(lgds)
        return lgd

    # -------------------------------------------------------------------------

    def delZoneVar(self):
        """
        Suppression de l'analyse en montées / descentes par variable par zones
        """

        if isinstance(self.zvarLayer, QgsVectorLayer):
            QgsProject.instance().removeMapLayer(self.zvarLayer.id())
        self.zvarLayer = None

    # -------------------------------------------------------------------------

    def addZoneVar(self, nbLignes):
        """
        Affichage de l'analyse en montées / descentes par variable par zones
        """

        # Mise à jour des tables de codage
        # Permet d'afficher uniquement les lignes filtrées dans la légende
        # pour les lignes, lignes amont, lignes aval
        # Le traitement est interrompu s'il n'y a pas de valeurs
        # pour la variable et le sous-réseau sélectionné
        res = self._majParamVar()
        if not res:
            msgBar = self.iface.messageBar()
            msg = msgBar.createMessage(
                "\
                Aucune valeur pour la variable et le sous-réseau sélectionnés.\
            "
            )
            # msgBar.pushWidget(msg, QgsMessageBar.WARNING, 3)
            msgBar.pushWidget(msg, Qgis.Warning, 3)
            self.tbn.setChecked(False)
            return

        self._createTableZonesTmp(nbLignes)
        self._addLayerZonesTmp()

        # Suppression de la symbologie par défaut (entités polygonales)
        symbolLayer = QgsSimpleFillSymbolLayer()
        symbolLayer.setStrokeWidth(0.4)
        symbolLayer.setStrokeColor(QColor(0, 0, 0, 255))
        symbolLayer.setFillColor(QColor(255, 255, 255, 0))

        self.zvarLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayer
        )

        # line numbers in comments refer to qgsdiagramproperties.cpp
        diagram = QgsPieDiagram()  # 500 - Type de diagramme

        ds = QgsDiagramSettings()  # 507
        # ds.font = QFont('Arial', 12)  # 508
        # ds.Left = 2  # ?
        # ds.Right = 3  # ?

        # Transparency -> Opacity
        paramTransp = float(self.qmapodSettings.value("params/spxZoneTranspDiag", 30))
        intTransp = 257 * paramTransp / 100  # Pourquoi 257 ?
        fltAlpha = 1.0 - (paramTransp / 100)
        # ds.transparency = 77  # 509 - de 0 à 255 (77 -> 30%) ?
        ds.transparency = intTransp

        # Sélection des champs à prendre en compte pour les diagrammes
        exp = self.expVarAnal
        ds.categoryAttributes = self.lstValueVarAnal  # Liste des champs
        ds.categoryColors = self.lstColorVarAnal  # Liste des couleurs
        ds.categoryLabels = self.lstLibVarAnal  # QGis 2.10

        ds.size = QSizeF(100.0, 100.0)  # 522 - Si taille fixe ?
        # ds.sizeType = 0  # 523 - mm(0), map units (1)
        ds.sizeType = QgsUnitTypes.RenderUnit.RenderMillimeters
        ds.labelPlacementMethod = 1  # 524 - from an existing example...
        ds.scaleByArea = True  # 525
        ds.minimumSize = 0.0  # 533

        # Couleur du contour des diagrammes
        # ds.backgroundColor = QColor(255, 255, 255, 0)  # 536 Transp White
        # ds.penColor = QColor(255, 255, 255, 255)  # 537
        outlineColor = self.qmapodSettings.value(
            "params/btnZoneColOutline", QColor(255, 255, 255, 255), QColor
        )
        penColor = outlineColor
        penColor.setAlphaF(fltAlpha)
        ds.penColor = penColor
        # ds.penWidth = 0.4  # 538
        outlineWidth = float(
            self.qmapodSettings.value("params/spxZoneOutlineWidth", 0.4)
        )
        ds.penWidth = outlineWidth
        # ds.minScaleDenominator = -1;  # 546
        # ds.maxScaleDenominator = -1;  # 547
        ds.diagramOrientation = 2  # 551 - May only be required for histograms
        # ds.barWidth = 5.0  # 553
        # We want a linear size interpolated version,
        # so that Total_BA = 0 means they disappear
        # 564-572
        # Voir aussi : QgsSingleCategoryDiagramRenderer
        dr = QgsLinearlyInterpolatedDiagramRenderer()
        dr.setLowerValue(0.0)
        dr.setLowerSize(QSizeF(0.0, 0.0))

        # Pour la légende
        # dr.setUpperValue(self._getMaxVal('zones_tmp_var', exp))
        self.maxValue = self._getMaxVal("zones_tmp_var", exp)
        dr.setUpperValue(self.maxValue)

        # Expression SQL spécifiant la taille
        dr.setClassificationAttributeExpression(exp)
        # La taille est spécifiée par une expression
        dr.setClassificationAttributeIsExpression(True)

        # Pour la légende
        # Taille de diagramme correspondant à la valeur maximale
        self.maxSize = int(self.qmapodSettings.value("params/spxZoneDiagMaxSize", 40))
        dr.setUpperSize(QSizeF(self.maxSize, self.maxSize))

        # dr.setClassificationAttribute(4)  # Colonne spécifiant la taille
        dr.setDiagram(diagram)
        dr.setDiagramSettings(ds)

        # And now finally put it into the layer... :-)
        self.zvarLayer.setDiagramRenderer(dr)  # 572

        dls = QgsDiagramLayerSettings()  # 575
        dls.setDistance(0)
        dls.setIsObstacle(False)
        dls.setPriority(1)
        dls.setPlacement(QgsDiagramLayerSettings.OverPoint)  # 588
        dls.setShowAllDiagrams(True)
        dls.setZIndex(-1.0)
        self.zvarLayer.setDiagramLayerSettings(dls)  # 593

        # Application du filtrage carto pour les zones
        self.zvarLayer.setSubsetString(self.currentFiltrage.dctWhereCarto[self.tabZone])

        # affichage légende dans boite Couches
        dr.setAttributeLegend(True)
        dr.setDataDefinedSizeLegend(self._legend())

        # Etiquetage de des zones

        self._labelZoneVar()

        self.zvarLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.zvarLayer.id())

        # Pour contournement du bug sur majZoneVar
        self.tbn.setChecked(True)

    # -------------------------------------------------------------------------

    def majZoneVar(self, nbLignes):
        """
        Mise à jour de l'analyse en montées / descentes par variable par zones
        """

        # Mise à jour de la table arrets_tmp
        self._createTableZonesTmp(nbLignes)

        # Sélection des champs à prendre en compte
        # pour le calcul de la valeur maximale
        exp = self.expVarAnal

        # Mise à jour de la valeur maximum des variables analysées
        dr = self.zvarLayer.diagramRenderer()
        # Pour la légende
        self.maxValue = self._getMaxVal("zones_tmp_var", exp)
        dr.setUpperValue(self.maxValue)

        # Etiquetage de des zones

        self._labelZoneVar()

    # -------------------------------------------------------------------------
