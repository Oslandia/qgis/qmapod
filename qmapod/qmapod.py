"""
/***************************************************************************
 QMapOD 2
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2017-11-27
        git sha              : $Format:%H$
        copyright            : (C) 2023 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import functools
import logging
import os
import sqlite3
import sys
import traceback
import webbrowser
from string import Template

# Import des librairies QGIS
from qgis.core import (
    Qgis,
    QgsDataSourceUri,
    QgsFeature,
    QgsLabelingEngineSettings,
    QgsLayerTreeLayer,
    QgsPalLayerSettings,
    QgsProject,
    QgsRenderContext,
    QgsSimpleFillSymbolLayer,
    QgsSimpleLineSymbolLayer,
    QgsSimpleMarkerSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)

# Import des librairies PyQt4
from qgis.PyQt.QtCore import (
    QCoreApplication,
    QFile,
    QFileInfo,
    QSettings,
    Qt,
    QTranslator,
    QUrl,
    qVersion,
)
from qgis.PyQt.QtGui import QColor, QCursor, QFont, QIcon
from qgis.PyQt.QtWebKitWidgets import QWebView
from qgis.PyQt.QtWidgets import (
    QAction,
    QApplication,
    QFileDialog,
    QListWidget,
    QListWidgetItem,
    QMenu,
    QMessageBox,
    QToolButton,
)

from .models import PlgLogLevel

# Import de la classe d'analyse par arrêts
from .qmapod_anal_arrets import QMapODAnalArret

# Import de la classe d'analyse par arrêts par variable
from .qmapod_anal_arrets_var import QMapODAnalArretVar

# Import de la classe d'analyse par serpents
from .qmapod_anal_serpent import QMapODAnalSerpent

# Import de la classe d'analyse par zones
from .qmapod_anal_zones import QMapODAnalZone

# Import de la classe d'analyse par zones par variable
from .qmapod_anal_zones_var import QMapODAnalZoneVar

# Chargement de la configuration de l'application
from .qmapod_config import QMapODConfig

# Import the code for the dockable widget
from .qmapod_dock import QMapODDock

# Import the code for the filtrage class
from .qmapod_filtrage import QMapODFiltrage

# Import de la classe d'analyse de flux par arrêts
from .qmapod_flux_arrets import QMapODFluxArrets

# Import de la classe d'analyse des flux principaux
from .qmapod_flux_principaux import QMapODFluxPP

# Import de la classe d'analyse de flux par zones
from .qmapod_flux_zones import QMapODFluxZones

# Import de la classe d'affichage du sous-réseau
from .qmapod_sous_reseau import QMapODSousReseau

# Impot des outils de tests
from .qmapod_test_tool import QMapODTestTool

# Import de la classe de paramétrage de l'étiquetage


def _waitingCursor(func):
    """
    Modification du curseur
    Fonction utilisée via le motif décorateur (decorator pattern)
    TODO : Gérer un nombre quelconque d'argument, y compris self
    """

    @functools.wraps(func)
    def waitingCursorFunction(self, **kwargs):
        # Application du curseur d'attente
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        try:
            return func(self, **kwargs)
        # Catch the exception
        except Exception as e:
            self.logger.error("Erreur %s :" % e.args[0])
            _, _, tb = sys.exc_info()
            traceback.print_tb(tb)
            raise e
        finally:
            # Restauration du curseur
            QApplication.restoreOverrideCursor()

    return waitingCursorFunction


class QMapOD(object):
    """
    Implémentation du Plugin de cartographie d'enquête O/D.
    """

    def __init__(self, iface):
        """Constructor

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        :dock: panneau ancrable
        :dlg: dialog modal
        """

        # Version de Qgis
        self.qgisVersionInt = Qgis.QGIS_VERSION_INT

        # Chargement de la configuration
        self.qmapodConfig = QMapODConfig()
        self.qmapodConfig.loadConfig(self.qmapodConfig.listeReseaux[0])

        # -- LOGGER --
        self.logger = logging.getLogger("QMapOD")
        log_level_str: str = self.qmapodConfig.dctParam.get("loglevel", "INFO").upper()
        # Map the string to the enum, defaulting to LogLevel.INFO if not found
        log_level_enum = PlgLogLevel.__members__.get(log_level_str, PlgLogLevel.INFO)
        self.logger.setLevel(log_level_enum.value)
        fh = logging.FileHandler(
            filename=os.path.join(self.qmapodConfig.pluginDir, ".log"), mode="w"
        )
        fh.setFormatter(
            logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        )
        self.logger.addHandler(fh)

        # Save reference to the QGIS interface
        self.iface = iface
        # Save reference to the QGIS canvas
        self.canvas = self.iface.mapCanvas()

        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        locale_path = os.path.join(
            self.qmapodConfig.pluginDir,
            "i18n",
            Template("QMapOD_$locale_.qm").substitute(locale_=locale),
        )

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > "4.3.3":
                QCoreApplication.installTranslator(self.translator)

        # Paramétrage du plugin (fichier QMapOD.ini dans le dossier du plugin)
        self.qmapodSettings = QSettings(
            os.path.join(self.qmapodConfig.pluginDir, "QMapOD.ini"), QSettings.IniFormat
        )
        self.qmapodSettings.setValue("version/version", self.qgisVersionInt)

        # Création d'un panneau ancrable
        self.dock = QMapODDock(self.qmapodConfig, self.qmapodSettings)
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dock)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr("&QMapOD 2")
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar("QMapOD 2")
        self.toolbar.setObjectName("QMapOD 2")

        # Nom du réseau chargé
        self.nomReseau = self.qmapodConfig.dctParam["nomreseau"]

        # Possibilité d'afficher les couleurs de lignes pour le sous-réseau
        if "couleurslignes" not in self.qmapodConfig.dctParam:
            self.blnRvb = False
        else:
            self.blnRvb = bool(self.qmapodConfig.dctParam["couleurslignes"])

        # Instanciation de la classe filtrage
        self.clsFiltrage = QMapODFiltrage(self.iface, self.dock, self.qmapodConfig)
        self._initFiltrage()

        # Indicateur de tranches horaires variables selon le type de jour
        self.blnTrhorTypjour = False
        # Initialisation du type de jour (nécessaires si les tranches horaires en dépendent)
        self.cbxTypeJourAction(0)

        # Nombre de lignes sélectionnées pour le filtrage
        self.nbLignes = 0

        # Analyses existantes
        self.sousReseau = None
        self.analArrets = None
        self.analArretsVar = None
        self.analZones = None
        self.analZonesVar = None
        self.analSerpent = None
        self.analFluxZones = None
        self.analFluxPP = None
        self.analFluxPPL = None
        self.analFluxArrets = None
        self.outilTest = None

        # Affichage des diagrammes et des étiquettes superposés
        self.showAllLabels(Qt.Checked)

        # Chargement des modèles de composeurs
        """
        TODO
        from PyQt4.QtXml import *

        strCompFile = 'composers/A4H.qpt'
        strCompDir = os.path.join(
            'C:/Users/SIGEAL/.qgis2/python/plugins/QMapOD', strCompFile)

        myMapRenderer = iface.mapCanvas().mapRenderer()
        myComposition = QgsComposition(myMapRenderer)
        myTemplateFile = file(strCompDir, 'rt')
        myTemplateContent = myTemplateFile.read()
        myTemplateFile.close()
        myDocument = QDomDocument()
        myDocument.setContent(myTemplateContent)
        myComposition.loadFromTemplate(myDocument)
        """

    # -------------------------------------------------------------------------

    def _initFiltrage(self):
        """
        Initialisation du filtrage et de la table enquetes_tmp
        """

        # Récupération du sous-réseau et application du filtrage des enquêtes
        self.clsFiltrage.getSousReseau()
        self.clsFiltrage.filtreEnquetes(self.qmapodSettings)

        # Application du filtrage cartographique
        self.clsFiltrage.filtreCarto(self.qmapodSettings)

        blnEnquetes = True
        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = "\nSELECT count(*) \n\
                 FROM sqlite_master \n\
                 WHERE type='table' AND name='enquetes_tmp';"
            self.logger.debug(s)
            cursor.execute(s)
            if cursor.fetchone()[0] == 0:
                db.close()
                blnEnquetes = False

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

        # Affichage d'un message s'il y a un problème avec le filtrage
        if self.clsFiltrage.sqlSousReseau() == "":
            self.iface.messageBar().pushMessage(
                "Erreur QMapOD", "Clause de filtrage introuvable.", level=Qgis.Warning
            )
            return

        # Affichage d'un message la table enquetes_tmp est introuvable
        if not blnEnquetes:
            self.iface.messageBar().pushMessage(
                "Erreur QMapOD",
                "Table des enquêtes filtrées introuvable.",
                level=Qgis.Warning,
            )
            return

    # -------------------------------------------------------------------------

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker, PyArgumentList, PyCallByClass
        return QCoreApplication.translate("QMapOD", message)

    # -------------------------------------------------------------------------

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None,
    ):
        """Add a toolbar icon to the InaSAFE toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(self.menu, action)

        self.actions.append(action)

        return action

    # -------------------------------------------------------------------------

    def initGui(self):
        """
        Création et configuration de la barre d'outils QMapOD
        """

        # Affichage/masquage du panneau ancrable
        strPluginDir = self.qmapodConfig.pluginDir
        self.add_action(
            QIcon(os.path.join(strPluginDir, "icons/qmapod.svg")),
            text=self.tr("Panneau QMapOD 2"),
            callback=self.run,
            parent=self.iface.mainWindow(),
        )

        # Choix du réseau (s'il y a plusieurs couples .sqlite/.json dans le dossier /data)
        if len(self.qmapodConfig.listeReseaux) > 1:
            self.popupReseaux = QMenu(self.iface.mainWindow())
            self.listeReseauAction = {}
            for res in self.qmapodConfig.listeReseaux:
                self.listeReseauAction[res] = QAction(
                    QIcon(os.path.join(strPluginDir, "icons/reseaux.svg")),
                    "Réseau " + res,
                    self.iface.mainWindow(),
                )
                self.popupReseaux.addAction(self.listeReseauAction[res])
                self.listeReseauAction[res].triggered.connect(
                    functools.partial(self.loadReseauAction, reseau=res)
                )

            # Ajout du menu déroulant Réseaux à la barre d'outils
            self.tbnReseaux = QToolButton()
            self.tbnReseaux.setMenu(self.popupReseaux)
            self.tbnReseaux.setDefaultAction(
                self.listeReseauAction[self.qmapodConfig.listeReseaux[0]]
            )
            self.tbnReseaux.setPopupMode(QToolButton.InstantPopup)

            self.toolbar.addWidget(self.tbnReseaux)

        # Chargement des couches QMapOD
        self.add_action(
            QIcon(os.path.join(strPluginDir, "icons/layers.svg")),
            text=self.tr("Chargement des couches QMapOD"),
            callback=self.loadMapODLayers,
            parent=self.iface.mainWindow(),
        )

        # Configuration de QMapOD
        self.add_action(
            QIcon(os.path.join(strPluginDir, "icons/config.svg")),
            text=self.tr("Configuration de QMapOD"),
            callback=self.config,
            parent=self.iface.mainWindow(),
        )

        # Aide de QMapOD
        self.web = QWebView()
        self.web.setWindowTitle("Aide de QMapOD")
        self.web.load(
            QUrl.fromLocalFile(
                os.path.realpath(os.path.join(strPluginDir, "doc/aide_qmapod.html"))
            )
        )

        # Définition des actions à ajouter au bouton déroulant
        strIconPath = os.path.join(strPluginDir, "icons/aide.svg")
        self.hlpIndexAction = QAction(
            QIcon(strIconPath), "Index de l'aide", self.iface.mainWindow()
        )
        self.hlpFiltrageEnquetesAction = QAction(
            QIcon(strIconPath), "Filtrage des enquêtes", self.iface.mainWindow()
        )
        self.hlpFiltrageCartoAction = QAction(
            QIcon(strIconPath), "Filtrage cartographique", self.iface.mainWindow()
        )
        self.hlpParamAction = QAction(
            QIcon(strIconPath), "Paramètres", self.iface.mainWindow()
        )
        self.hlpOutilsAction = QAction(
            QIcon(strIconPath), "Outils", self.iface.mainWindow()
        )
        self.hlpAnalArretsAction = QAction(
            QIcon(strIconPath), "Analyses sur les arrêts", self.iface.mainWindow()
        )
        self.hlpAnalZonesAction = QAction(
            QIcon(strIconPath), "Analyses sur les zonage", self.iface.mainWindow()
        )
        self.hlpAnalTronconsAction = QAction(
            QIcon(strIconPath), "Analyses sur les tronçons", self.iface.mainWindow()
        )
        self.hlpAnalFluxAction = QAction(
            QIcon(strIconPath), "Analyses de flux", self.iface.mainWindow()
        )
        self.hlpRappelEnqueteOD = QAction(
            QIcon(strIconPath), "Rappels enquêtes O/D", self.iface.mainWindow()
        )

        self.popupHelp = QMenu(self.iface.mainWindow())

        # Ajout des actions au menu déroulant et connexion des actions
        # à la méthode helpQMapOD
        self.popupHelp.addAction(self.hlpIndexAction)
        self.hlpIndexAction.triggered.connect(functools.partial(self.helpQMapOD, rub=0))
        self.popupHelp.addAction(self.hlpFiltrageEnquetesAction)
        self.hlpFiltrageEnquetesAction.triggered.connect(
            functools.partial(self.helpQMapOD, rub=1)
        )
        self.popupHelp.addAction(self.hlpFiltrageCartoAction)
        self.hlpFiltrageCartoAction.triggered.connect(
            functools.partial(self.helpQMapOD, rub=2)
        )
        self.popupHelp.addAction(self.hlpParamAction)
        self.hlpParamAction.triggered.connect(functools.partial(self.helpQMapOD, rub=3))
        self.popupHelp.addAction(self.hlpOutilsAction)
        self.hlpOutilsAction.triggered.connect(
            functools.partial(self.helpQMapOD, rub=4)
        )
        self.popupHelp.addAction(self.hlpAnalArretsAction)
        self.hlpAnalArretsAction.triggered.connect(
            functools.partial(self.helpQMapOD, rub=5)
        )
        self.popupHelp.addAction(self.hlpAnalZonesAction)
        self.hlpAnalZonesAction.triggered.connect(
            functools.partial(self.helpQMapOD, rub=6)
        )
        self.popupHelp.addAction(self.hlpAnalTronconsAction)
        self.hlpAnalTronconsAction.triggered.connect(
            functools.partial(self.helpQMapOD, rub=7)
        )
        self.popupHelp.addAction(self.hlpAnalFluxAction)
        self.hlpAnalFluxAction.triggered.connect(
            functools.partial(self.helpQMapOD, rub=8)
        )
        self.popupHelp.addAction(self.hlpRappelEnqueteOD)
        self.hlpRappelEnqueteOD.triggered.connect(
            functools.partial(self.helpQMapOD, rub=9)
        )

        # Ajout du menu déroulant Aide à la barre d"outils
        self.tbnHelp = QToolButton()
        self.tbnHelp.setMenu(self.popupHelp)
        self.tbnHelp.setDefaultAction(self.hlpIndexAction)
        self.tbnHelp.setPopupMode(QToolButton.InstantPopup)

        self.toolbar.addWidget(self.tbnHelp)

        # Ajout du bouton Aide au menu déroulant
        self.iface.addPluginToMenu(self.menu, self.hlpIndexAction)

        # Configuration de QMapOD
        self.add_action(
            QIcon(os.path.join(strPluginDir, "icons/apropos.svg")),
            text=self.tr("À propos de QMapOD 2"),
            callback=self.aboutQMapOD,
            parent=self.iface.mainWindow(),
        )

        # Création d'une légende diagramme lors de la création
        # d'une légende dans un composeur
        # TODO

        # Initialisation du panneau ancrable
        self.initDock()

        # Désactivation des outils QMapOD lorsqu'un outil Qgis est activé
        self.iface.mapCanvas().mapToolSet.connect(self._mapToolChanged)

        # Chargement des couches de référence si configuré
        try:
            affReseau = self.qmapodConfig.dctParam["affreseau"]
        except KeyError:  # Gérer NameError ?
            affReseau = False
        if affReseau:
            # Vérification de l'existence du groupe <self.nomReseau>
            root = QgsProject.instance().layerTreeRoot()
            if root.findGroup(self.nomReseau) is None:
                self.loadMapODLayers()

    # -------------------------------------------------------------------------

    def initDock(self):
        """
        Initialisation des actions du panneau ancrable
        """

        # Vérification de l'existence de la table trhor_typjour
        self.blnTrhorTypjour = True
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()

            # Vérification de l'existence de la table trhor_typjour
            s = "\nSELECT count(*) \n\
                 FROM sqlite_master \n\
                 WHERE type='table' AND name='trhor_typjour';"
            self.logger.debug(s)
            cursor.execute(s)

            if cursor.fetchone()[0] == 0:
                db.close()
                self.blnTrhorTypjour = False

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

        """
        Onglet Filtrage enquêtes
        """
        # Masquage de la liste type de jour s'il n'y en a qu'un
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()

            # Vérification du nombre de types de jour
            s = "\nSELECT count(*)\n\
                 FROM code_type_jour;"
            self.logger.debug(s)
            cursor.execute(s)

            nbTypeJour = cursor.fetchone()[0]

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

        if nbTypeJour <= 1:
            self.dock.widgetTypeJour.setVisible(False)

        # Ajout d'une action sur la liste cbxTypeJour
        self.dock.cbxTypeJour.currentIndexChanged.connect(self.cbxTypeJourAction)

        # Ajout d'une action sur le bouton btnFiltreEnquete
        self.dock.btnFiltreEnquetes.clicked.connect(self.btnFiltreEnquetesAction)

        # Ajout d'une action pour afficher le sous-réseau
        self.dock.tbnSousReseau.clicked.connect(self.tbnSousReseauAction)

        # Ajout d'une action pour réinitialiser les critères
        # de filtrage enquêtes
        self.dock.btnRazFiltreEnquetes.clicked.connect(self.btnRazFiltreEnquetesAction)

        """
        Onglet Filtrage carto
        """
        # Ajout d'une action sur le bouton btnFiltreCarto
        self.dock.btnFiltreCarto.clicked.connect(self.btnFiltreCartoAction)

        # Ajout d'une action pour réinitialiser les critères de filtrage carto
        self.dock.btnRazFiltreCarto.clicked.connect(self.btnRazFiltreCartoAction)

        """
        Onglet Paramètres
        """
        # Action du bouton appliquer les paramètres
        self.dock.btnValidParam.clicked.connect(self.btnValidParamAction)

        # Action du bouton recharger les paramètres par défaut
        self.dock.btnDefaultParams.clicked.connect(self.btnDefaultParamsAction)

        # Action des boutons radio type d'affichage du sous-réseau
        self.dock.rbnSingleColor.toggled.connect(self.rbnSingleColorAction)
        self.dock.rbnLineColors.toggled.connect(self.rbnLineColorsAction)
        if self.blnRvb:
            # Affichage uniquement des paramètres correspondant
            # au type de représentation sélectionné
            # Couleur unique
            if (
                int(self.qmapodSettings.value("params/buttonGroupSousReseauMode", 1))
                == 1
            ):
                # Couleur arrêts
                self.dock.widgetColArrets.setVisible(True)
                # Couleur tronçons
                self.dock.widgetColTroncons.setVisible(True)
                # Couleur troncs communs
                self.dock.widgetColTronCom.setVisible(False)
            # Couleurs des lignes
            elif (
                int(self.qmapodSettings.value("params/buttonGroupSousReseauMode", 1))
                == 2
            ):
                # Couleur arrêts
                self.dock.widgetColArrets.setVisible(False)
                # Couleur tronçons
                self.dock.widgetColTroncons.setVisible(False)
                # Couleur troncs communs
                self.dock.widgetColTronCom.setVisible(True)
            # Défaut
            else:
                # Couleur arrêts
                self.dock.widgetColArrets.setVisible(True)
                # Couleur tronçons
                self.dock.widgetColTroncons.setVisible(True)
                # Couleur troncs communs
                self.dock.widgetColTronCom.setVisible(False)
        else:
            self.dock.groupBox_2.setVisible(False)
            # Couleur arrêts
            self.dock.widgetColArrets.setVisible(True)
            # Couleur tronçons
            self.dock.widgetColTroncons.setVisible(True)
            # Couleur troncs communs
            self.dock.widgetColTronCom.setVisible(False)

        # Action des boutons radio type d'analyse
        self.dock.rbnGraduated.toggled.connect(self.rbnGraduatedAction)
        self.dock.rbnUrchin.toggled.connect(self.rbnUrchinAction)
        self.dock.rbnDiagram.toggled.connect(self.rbnDiagramAction)

        # Test mise à jour dynamique
        # TODO : étendre à d'autres paramètres ?
        # Cohérence ergonomique avec le filtrage ?
        # self.dock.spxTronconsWidth.valueChanged.connect(
        #    self.spxTronconsWidthAction)

        """
        Onglet Outils
        """
        # Ajout d'une action sur la case à cocher chkShowingAllLabels
        self.dock.chkShowingAllLabels.stateChanged.connect(self.showAllLabels)

        # Action du bouton exporter projet
        self.dock.btnExportProject.clicked.connect(self.btnExportProjectAction)
        self.dock.btnExportLayer.clicked.connect(self.btnExportLayerAction)

        """
        Onglet Test
        """
        # Ajout d'une action sur un bouton
        self.dock.btnApply.clicked.connect(self.testBtn)

        # Ajout d'une action sur le bouton btnTest
        self.dock.btnTest.clicked.connect(self.btnTest)

        # Ajout d'une action sur l'outil tbnOutilTest
        self.dock.tbnOutilTest.clicked.connect(self.tbnOutilTestSelect)

        # Ajout d'une action sur la case à cocher chkTest
        self.dock.chkTest.stateChanged.connect(self.chkTestAction)

        """
        Onglet Arrêts
        """
        # Action du bouton montées/descentes par arrêt
        self.dock.tbnMDArret.clicked.connect(self.tbnMDArretAction)

        # Action du bouton montées par arrêt
        self.dock.tbnMArret.clicked.connect(self.tbnMArretAction)

        # Action du bouton descentes par arrêt
        self.dock.tbnDArret.clicked.connect(self.tbnDArretAction)

        # Action de la liste des variables analysables (arrêts)
        self.dock.cbxVarArret.currentIndexChanged.connect(self.cbxVarArretAction)

        # Action du bouton montées par sens par arrêt
        self.dock.tbnMSArret.clicked.connect(self.tbnMSArretAction)

        # Action du bouton descentes par sens par arrêt
        self.dock.tbnDSArret.clicked.connect(self.tbnDSArretAction)

        # Action du bouton montées par variable par arrêt
        self.dock.tbnMVarArret.clicked.connect(self.tbnMVarArretAction)

        # Action du bouton descentes par variable par arrêt
        self.dock.tbnDVarArret.clicked.connect(self.tbnDVarArretAction)

        # Action du bouton effacer carto par arrêts
        self.dock.btnRazCartoArrets.clicked.connect(self.btnRazCartoArretsAction)

        """
        Onglet Zones
        """
        # Action de la liste des zones analysables
        self.dock.cbxMDZone.currentIndexChanged.connect(self.cbxMDZoneAction)

        # Action du bouton montées/descentes par zone
        self.dock.tbnMDZone.clicked.connect(self.tbnMDZoneAction)

        # Action du bouton montées par zone
        self.dock.tbnMZone.clicked.connect(self.tbnMZoneAction)

        # Action du bouton descentes par zone
        self.dock.tbnDZone.clicked.connect(self.tbnDZoneAction)

        # Action de la liste des variables analysables (zone)
        self.dock.cbxVarZone.currentIndexChanged.connect(self.cbxVarZoneAction)

        # Action du bouton montées par variable par zone
        self.dock.tbnMVarZone.clicked.connect(self.tbnMVarZoneAction)

        # Action du bouton descentes par variable par zone
        self.dock.tbnDVarZone.clicked.connect(self.tbnDVarZoneAction)

        # Action du bouton effacer carto par arrêts
        self.dock.btnRazCartoZones.clicked.connect(self.btnRazCartoZonesAction)

        """
        Onglet Tronçons
        """
        # Action du bouton serpent d'offre
        self.dock.tbnSOffre.clicked.connect(self.tbnSOffreAction)

        # Action du bouton serpent de charge
        self.dock.tbnSCharge.clicked.connect(self.tbnSChargeAction)

        # Action du bouton serpent de performance
        self.dock.tbnSPerf.clicked.connect(self.tbnSPerfAction)

        # Action du bouton effacer carto par arrêts
        self.dock.btnRazCartoTroncons.clicked.connect(self.btnRazCartoTronconsAction)

        """
        Onglet Flux
        """
        # Action de la liste des zonages pour les flux
        self.dock.cbxFluxZone.currentIndexChanged.connect(self.cbxFluxZoneAction)

        # Action de l'outil flux par zone(s) de montée (self.tbnFluxZonesMD)
        self.dock.tbnFluxZonesMD.clicked.connect(self.tbnFluxZonesMDSelect)

        # Action de l'outil flux par zone(s) de descente (self.tbnFluxZonesDM)
        self.dock.tbnFluxZonesDM.clicked.connect(self.tbnFluxZonesDMSelect)

        # Suppression des flux par zone(s) self.tbnDelFluxZones)
        self.dock.tbnDelFluxZones.clicked.connect(self.tbnDelFluxZonesAction)

        # Action de l'outil flux principaux (self.tbnFluxPP)
        self.dock.tbnFluxPP.clicked.connect(self.tbnFluxPPAction)

        # Action de la spinbox nombre de flux principaux (self.spxNbFluxPP)
        self.dock.spxNbFluxPP.valueChanged.connect(self.spxNbFluxPPAction)

        # Action de l'outil flux principaux par ligne (self.tbnFluxPPL)
        self.dock.tbnFluxPPL.clicked.connect(self.tbnFluxPPLAction)

        # Action de la spinbox nombre de flux principaux par ligne (self.spxNbFluxPPL)
        self.dock.spxNbFluxPPL.valueChanged.connect(self.spxNbFluxPPLAction)

        # Action de l'outil flux par arrêt(s) de montée (self.tbnFluxArretsMD)
        self.dock.tbnFluxArretsMD.clicked.connect(self.tbnFluxArretsMDSelect)

        # Action de l'outil flux par arrêt(s) de montée (self.tbnFluxArretsDM)
        self.dock.tbnFluxArretsDM.clicked.connect(self.tbnFluxArretsDMSelect)

        # Suppression des flux par arrêt(s) self.tbnDelFluxArrets)
        self.dock.tbnDelFluxArrets.clicked.connect(self.tbnDelFluxArretsAction)

    # -------------------------------------------------------------------------

    def tbnOutilTestSelect(self, checked):
        """
        Gestion de l'activation de l'outil tbnFluxZonesMD
        TODO : Améliorer l'ergonomie (maintenir l'activation de l'outil
               lorsqu'il n'y a pas de flux)
        """

        # Si l'outil est coché
        if checked:
            # Initialisation de la couche de zonage pour les flux
            if isinstance(self.outilTest, QMapODTestTool):
                self.outilTest = None
            self.outilTest = QMapODTestTool(
                self.iface,
                self.dock,
                self.dock.sender(),
                "m",
                self.qmapodConfig,
                self.qmapodSettings,
            )
            self.outilTest.setButton(self.dock.sender())
            self.outilTest.geomIdentified.connect(self.tbnOutilTestIdentAction)
            self.outilTest.geomUnidentified.connect(self.tbnOutilTestUnidentAction)

            # Activation de l'outil
            self.iface.mapCanvas().setMapTool(self.outilTest)

        # Si l'outil est décoché
        else:
            # Désactivation de l'outil
            self.iface.mapCanvas().unsetMapTool(self.outilTest)
            if isinstance(self.outilTest, QMapODTestTool):
                self.outilTest = None

    # -------------------------------------------------------------------------

    def tbnOutilTestIdentAction(self, layer, feature):
        """
        Action de l'outil tbnFluxZonesMD
        """

        # Traitement de la zone sélectionnée
        if isinstance(layer, QgsVectorLayer) and isinstance(feature, QgsFeature):
            self.logger.info("traitement feature")
            self.outilTest.processFeature(feature)

    # -------------------------------------------------------------------------

    def tbnOutilTestUnidentAction(self):
        """
        Action de l'outil tbnFluxZonesMD
        """
        self.logger.info("pas de feature")

    @_waitingCursor
    def loadReseauAction(self, reseau):
        """
        Chargement du réseau et de sa configuration
        """
        # Chargement du réseau sélectionné
        self.qmapodConfig.loadConfig(reseau)

        # Mise à jour du nom du réseau
        self.nomReseau = self.qmapodConfig.dctParam["nomreseau"]

        # Possibilité d'afficher les couleurs de lignes pour le sous-réseau
        if "couleurslignes" not in self.qmapodConfig.dctParam:
            self.blnRvb = False
        else:
            self.blnRvb = bool(self.qmapodConfig.dctParam["couleurslignes"])

        # Suppression du dock existant et création d'un nouveau dock
        self.iface.removeDockWidget(self.dock)
        self.dock = None
        self.dock = QMapODDock(self.qmapodConfig, self.qmapodSettings)
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dock)

        self.initDock()

        # Paramétrage du plugin (fichier QMapOD.ini dans le dossier du plugin)
        self.qmapodSettings = QSettings(
            os.path.join(self.qmapodConfig.pluginDir, "QMapOD.ini"), QSettings.IniFormat
        )

        # Ré-instanciation de la classe filtrage
        self.clsFiltrage = None
        self.clsFiltrage = QMapODFiltrage(self.iface, self.dock, self.qmapodConfig)
        self._initFiltrage()

        # Chargement des couches de référence si configuré
        try:
            affReseau = self.qmapodConfig.dctParam["affreseau"]
        except KeyError:  # Gérer NameError ?
            affReseau = False
        if affReseau:
            # Vérification de l'existence du groupe <self.nomReseau>
            root = QgsProject.instance().layerTreeRoot()
            if root.findGroup(self.nomReseau) is None:
                self.loadMapODLayers()

    # -------------------------------------------------------------------------

    def helpQMapOD(self, rub):
        """
        Aide en ligne de l'application QMapOD
        """

        # Dictionnaire d'association des balises anchor de rubriques
        dctHelp = {
            0: ("consultation-cartographique-" + "denqu%C3%AAtes-origine-destination"),
            1: "211-onglet-de-filtrage-des-enqu%C3%AAtes",
            2: "212-onglet-de-filtrage-cartographique",
            3: "213-onglet-des-param%C3%A8tres-de-lapplication",
            4: "214-onglet-des-outils-de-lapplication",
            5: "221-onglet-danalyses-cartographiques-sur-les-arr%C3%AAts",
            6: "222-onglet-danalyses-cartographiques-sur-les-zonages",
            7: "223-onglet-danalyses-cartographiques-sur-les-tron%C3%A7ons",
            8: "224-onglet-danalyses-cartographiques-de-flux",
            9: "31-la-pond%C3%A9ration-des-enqu%C3%AAtes",
        }
        self.logger.info(dctHelp[rub])

        # Affichage de l'aide à la rubrique sélectionnée
        self.web.show()
        self.web.page().mainFrame().evaluateJavaScript(
            'window.location.hash="' + dctHelp[rub] + '";'
        )
        self.web.activateWindow()

    # -------------------------------------------------------------------------

    def aboutQMapOD(self):
        """
        À propos de l'application QMapOD
        """

        version = self.iface.pluginManagerInterface().pluginMetadata("qmapod")[
            "version_installed"
        ]
        date = self.iface.pluginManagerInterface().pluginMetadata("qmapod")[
            "update_date"
        ]
        description = self.iface.pluginManagerInterface().pluginMetadata("qmapod")[
            "description"
        ]
        author_name = self.iface.pluginManagerInterface().pluginMetadata("qmapod")[
            "author_name"
        ]
        author_email = self.iface.pluginManagerInterface().pluginMetadata("qmapod")[
            "author_email"
        ]
        create_date = self.iface.pluginManagerInterface().pluginMetadata("qmapod")[
            "create_date"
        ]

        QMessageBox.information(
            self.iface.mainWindow(),
            "À propos de QMapOD",
            (
                "QMapOD\n"
                + f"{description}\n"
                + "---------------------------------------------"
                + "---------------------------------------\n"
                + f"Développement par {author_name}\n"
                + f"Depuis {create_date}\n"
                + f"Version {version} du {date}\n"
                + "GNU General Public License\n"
                + f"Contact : {author_email}"
            ),
        )

    # -------------------------------------------------------------------------

    def cbxTypeJourAction(self, currentIndex):
        """
        Mise à jour des tranches horaires selon le type de jour
        La table trhor_typjour doit exister dans la base de données
        et la requête SQL permettant d'extraire les tranches horaires
        selon le type de jour doit être dans le json de configuration
        (lwgTrhor)
        """

        if self.blnTrhorTypjour:
            # Vérification de la présence de la requête
            # dans le fichier de configuration
            cnfTrhor = self.qmapodConfig.odtCritereVoyage["lwgTrhor"]
            if "sqltypjour" in cnfTrhor:
                strSqlTypjour = cnfTrhor["sqltypjour"]

                # Chargement des tranches horaires correspondant
                # au type de jour sélectionné
                try:
                    # Connexion à la base de données
                    db = sqlite3.connect(self.qmapodConfig.db)
                    # Création d'un curseur
                    cursor = db.cursor()
                    # Récupération des nouvelles tranches horaires
                    s = Template("$sql_type_jour$cbx_type_jour").substitute(
                        sql_type_jour=strSqlTypjour,
                        cbx_type_jour=self.dock.cbxTypeJour.itemData(currentIndex),
                    )
                    self.logger.debug(s)
                    cursor.execute(s)
                    data = cursor.fetchall()

                # Récupération de l'exception
                except Exception as e:
                    self.logger.error("Error %s:" % e.args[0])
                    raise e
                finally:
                    # Fermeture de la base de données
                    db.close()

                # Mise à jour de la liste des tranches horaires
                lwgTrhor = self.dock.pageVoyage.findChild(QListWidget, "lwgTrhor")
                lwgTrhor.clear()
                for item in data:
                    # self.dock.lwgTrhor.addItem(unicode(item[1]), item[0])
                    lib_item = QListWidgetItem(str(item[0]))
                    lib_item.setText(item[1])
                    lib_item.setData(Qt.UserRole, item[0])
                    lib_item.setFlags(
                        Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled
                    )
                    lib_item.setCheckState(Qt.Checked)
                    lwgTrhor.addItem(lib_item)

            # Affichage d'un message d'erreur si la requête est absente
            else:
                msgBar = self.iface.messageBar()
                strMsg = "Requête tranches horaires/type de jour introuvable."
                msgBar.pushMessage(strMsg, Qgis.Warning)

    # -------------------------------------------------------------------------

    def loadTrhor(self):
        """
        Mise à jour des tranches horaires selon le type de jour
        La table code_trhor_typjour doit exister dans la base de données
        et la requête SQL permettant d'extraire les tranches horaires
        selon le type de jour doit être dans le json de configuration
        (lwgTrhor)
        """

        if self.blnTrhorTypjour:
            # Vérification de la présence de la requête
            # dans le fichier de configuration
            cnfTrhor = self.qmapodConfig.odtCritereVoyage["lwgTrhor"]
            if "sqltypjour" in cnfTrhor:
                strSqlTypjour = cnfTrhor["sqltypjour"]

                # Chargement des tranches horaires correspondant
                # au type de jour sélectionné
                try:
                    # Connexion à la base de données
                    db = sqlite3.connect(self.qmapodConfig.db)
                    # Création d'un curseur
                    cursor = db.cursor()

                    # Suppression des tranches horaires existantes
                    s = "\nDELETE FROM code_trhor"
                    self.logger.debug(s)
                    cursor.execute(s)

                    # Insertion des nouvelles tranches horaires dans la table code_trhor
                    strSql = Template(
                        "\nINSERT INTO code_trhor $sql_type_jour$cbx_type_jour"
                    ).substitute(
                        sql_type_jour=strSqlTypjour,
                        cbx_type_jour=self.dock.cbxTypeJour.itemData(
                            self.dock.cbxTypeJour.currentIndex()
                        ),
                    )
                    self.logger.debug(strSql)
                    cursor.execute(strSql)

                    db.commit()

                # Récupération de l'exception
                except Exception as e:
                    self.logger.error("Error %s:" % e.args[0])
                    raise e
                finally:
                    # Fermeture de la base de données
                    db.close()

            # Affichage d'un message d'erreur si la requête est absente
            else:
                msgBar = self.iface.messageBar()
                strMsg = "Requête tranches horaires/type de jour introuvable."
                msgBar.pushMessage(strMsg, Qgis.Warning)

    # -------------------------------------------------------------------------

    @_waitingCursor
    def btnFiltreEnquetesAction(self):
        """
        Validation du filtrage des enquêtes
        """

        # Mise à jour des tranches horaires selon le type de jour (le cas échéant)
        if self.blnTrhorTypjour:
            self.loadTrhor()

        # Récupération du sous-réseau sélectionné (pour avoir le nombre de lignes)
        self.clsFiltrage.getSousReseau()

        # Mise à jour du nombre de lignes du sous-réseau courant
        self.nbLignes = self.clsFiltrage.nbLignes

        # Activation/désactivation du choix d'analyse voyage/déplacement selon le
        # nombre de lignes et mise à jour des paramètres d'analyse le cas échéant
        # Si toutes les lignes sont sélectionnées
        if self.nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            # Analyses par arrêts
            self.dock.gbxArretVD.setDisabled(False)
            self.dock.rbnArretVoyage.setDisabled(False)
            self.dock.rbnArretDeplacement.setDisabled(False)

            self.dock.gbxZoneVD.setDisabled(False)
            self.dock.rbnZoneVoyage.setDisabled(False)
            self.dock.rbnZoneDeplacement.setDisabled(False)

            # Flux par zones
            self.dock.gbxFluxZoneVD.setDisabled(True)
            self.dock.rbnFluxZoneVoyage.setDisabled(True)
            self.dock.rbnFluxZoneVoyage.setChecked(False)
            self.dock.rbnFluxZoneDeplacement.setDisabled(True)
            self.dock.rbnFluxZoneDeplacement.setChecked(True)
            self.qmapodSettings.setValue("params/buttonGroupFluxZoneMode", 2)

            # Flux par arrêts
            self.dock.gbxFluxArretVD.setDisabled(True)
            self.dock.rbnFluxArretVoyage.setDisabled(True)
            self.dock.rbnFluxArretVoyage.setChecked(False)
            self.dock.rbnFluxArretDeplacement.setDisabled(True)
            self.dock.rbnFluxArretDeplacement.setChecked(True)
            self.qmapodSettings.setValue("params/buttonGroupFluxArretMode", 2)

        # Sinon
        else:

            # Analyses par arrêts
            self.dock.gbxArretVD.setDisabled(True)
            self.dock.rbnArretVoyage.setDisabled(True)
            self.dock.rbnArretVoyage.setChecked(True)
            self.dock.rbnArretDeplacement.setDisabled(True)
            self.dock.rbnArretDeplacement.setChecked(False)
            self.qmapodSettings.setValue("params/buttonGroupArretMode", 1)

            # Analyses par zones
            self.dock.gbxZoneVD.setDisabled(False)
            self.dock.rbnZoneVoyage.setDisabled(False)
            self.dock.rbnZoneDeplacement.setDisabled(False)

            # Flux par zones
            self.dock.gbxFluxZoneVD.setDisabled(False)
            self.dock.rbnFluxZoneVoyage.setDisabled(False)
            self.dock.rbnFluxZoneDeplacement.setDisabled(False)

            # Flux par arrêts
            self.dock.gbxFluxArretVD.setDisabled(False)
            self.dock.rbnFluxArretVoyage.setDisabled(False)
            self.dock.rbnFluxArretDeplacement.setDisabled(False)

        # Application du filtrage des enquêtes
        self.clsFiltrage.filtreEnquetes(self.qmapodSettings)

        """
        Mise à jour des analyses existantes
        """
        # Affichage du sous-réseau courant
        if isinstance(self.sousReseau, QMapODSousReseau):
            # Si la couche existe, on met à jour l'analyse
            if self.sousReseau is not None:
                if (
                    self.sousReseau.srArretLayer is not None
                    and self.sousReseau.srTronconLayer is not None
                ):
                    self.sousReseau.majSousReseau()

        # Analyses en montées / descentes par arrêts
        if isinstance(self.analArrets, QMapODAnalArret):
            # Si la couche existe, on met à jour l'analyse
            if self.analArrets is not None:
                if self.analArrets.amdLayer is not None:
                    self.analArrets.delArretMD()
                    self.analArrets.addArretMD(self.nbLignes)

        # Analyses en montées / descentes par variable par arrêts
        if isinstance(self.analArretsVar, QMapODAnalArretVar):
            # Si la couche existe, on met à jour l'analyse
            if self.analArretsVar is not None:
                if self.analArretsVar.avarLayer is not None:
                    self.analArretsVar.delArretVar()
                    self.analArretsVar.addArretVar(self.nbLignes)

        # Analyses en montées / descentes par zones
        if isinstance(self.analZones, QMapODAnalZone):
            # Si la couche existe, on met à jour l'analyse
            if self.analZones is not None:
                if self.analZones.zmdLayer is not None:
                    self.analZones.delZoneMD()
                    self.analZones.addZoneMD(self.nbLignes)

        # Analyses en montées / descentes par variable par zones
        if isinstance(self.analZonesVar, QMapODAnalZoneVar):
            # Si la couche existe, on met à jour l'analyse
            if self.analZonesVar is not None:
                if self.analZonesVar.zvarLayer is not None:
                    self.analZonesVar.delZoneVar()
                    self.analZonesVar.addZoneVar(self.nbLignes)

        # Analyses en serpents
        if isinstance(self.analSerpent, QMapODAnalSerpent):
            # Si la couche existe, on met à jour l'analyse
            if self.analSerpent is not None:
                if self.analSerpent.serpLayer is not None:
                    self.analSerpent.majSerpent()

        # Flux par zones
        if isinstance(self.analFluxZones, QMapODFluxZones):
            # Si la couche existe, on met à jour l'analyse
            if self.analFluxZones is not None:
                if self.analFluxZones.fzLayer is not None:
                    self.analFluxZones.majFluxZones(
                        self.nbLignes,
                        int(
                            self.qmapodSettings.value(
                                "params/buttonGroupVisuFluxZone", 3
                            )
                        ),
                    )

        # Flux principaux
        if isinstance(self.analFluxPP, QMapODFluxPP):
            # Si la couche existe, on met à jour l'analyse
            if self.analFluxPP is not None:
                if self.analFluxPP.flxppLayer is not None:
                    self.analFluxPP.delFluxPP()
                    self.analFluxPP.addFluxPP(
                        self.dock.spxNbFluxPP.value(), self.nbLignes, False
                    )

        # Flux principaux par ligne
        if isinstance(self.analFluxPPL, QMapODFluxPP):
            # Si la couche existe, on met à jour l'analyse
            if self.analFluxPPL is not None:
                if self.analFluxPPL.flxppLayer is not None:
                    self.analFluxPPL.delFluxPP()
                    self.analFluxPPL.addFluxPP(
                        self.dock.spxNbFluxPP.value(), self.nbLignes, True
                    )

        # Flux par arrêts
        if isinstance(self.analFluxArrets, QMapODFluxArrets):
            # Si la couche existe, on met à jour l'analyse
            if self.analFluxArrets is not None:
                if self.analFluxArrets.faLayer is not None:
                    self.analFluxArrets.majFluxArrets(self.nbLignes)

        # Rafraichissement de la carte
        self.iface.mapCanvas().refresh()

        # Récupération des paramètres d'analyse
        strModeEnq = {1: "voyage", 2: "déplacement"}
        strArret = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupArretMode", 1))
        ]
        strZone = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupZoneMode", 1))
        ]
        strFluxZone = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupFluxZoneMode", 1))
        ]
        strFluxArret = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupFluxArretMode", 1))
        ]
        msgBar = self.iface.messageBar()
        strMsg = (
            "Critères de filtrage d'enquêtes appliqués - "
            + Template(
                "Arrêts : logique $str_arret - Zones : logique $str_zone - "
            ).substitute(str_arret=strArret, str_zone=strZone)
            + Template(
                "Flux zones : logique $str_zone - Flux arrêts : logique $str_flux_arret"
            ).substitute(str_zone=strFluxZone, str_flux_arret=strFluxArret)
        )
        msgBar.pushMessage("QMapOD", strMsg, Qgis.Info, 5)

    # -------------------------------------------------------------------------

    def btnRazFiltreEnquetesAction(self):
        """
        Réinitialisation des critères de filtrage des enquêtes
        """

        self.clsFiltrage.razFiltreEnquetes()

        msgBar = self.iface.messageBar()
        strMsg = "Critères de filtrage d'enquêtes réinitialisés."
        msgBar.pushMessage(strMsg, Qgis.Info)

    # -------------------------------------------------------------------------

    @_waitingCursor
    def btnFiltreCartoAction(self):
        """
        Validation du filtrage cartographique
        TODO : Prendre en compte le filtrage carto pour la classification
        des valeurs (mise à jour de la valeur maxi)
        """

        # Application du filtrage cartographique
        self.clsFiltrage.filtreCarto(self.qmapodSettings)
        odtWhereCarto = self.clsFiltrage.dctWhereCarto

        """
        Mise à jour des analyses existantes
        """
        # Analyses en montées / descentes par arrêts
        if isinstance(self.analArrets, QMapODAnalArret):
            # Si la couche existe, on met à jour le sous-ensemble
            if self.analArrets is not None:
                self.analArrets.amdLayer.setSubsetString(odtWhereCarto["id_arret"])

        # Analyses en montées / descentes par variable par arrêts
        if isinstance(self.analArretsVar, QMapODAnalArretVar):
            # Si la couche existe, on met à jour le sous-ensemble
            if self.analArretsVar is not None:
                self.analArretsVar.avarLayer.setSubsetString(odtWhereCarto["id_arret"])

        # Analyses en montées / descentes par zones
        if isinstance(self.analZones, QMapODAnalZone):
            # Si la couche existe, on met à jour le sous-ensemble
            if self.analZones is not None:
                self.analZones.zmdLayer.setSubsetString(
                    odtWhereCarto[self.analZones.tabZone]
                )

        # Analyses en montées / descentes par variable par zones
        if isinstance(self.analZonesVar, QMapODAnalZoneVar):
            # Si la couche existe, on met à jour le sous-ensemble
            if self.analZonesVar is not None:
                self.analZonesVar.zvarLayer.setSubsetString(
                    odtWhereCarto[self.analZones.tabZone]
                )

        # Flux par zones
        if isinstance(self.analFluxZones, QMapODFluxZones):
            # Si la couche existe, on met à jour le sous-ensemble
            if self.analFluxZones is not None:
                strCarto = odtWhereCarto[self.analFluxZones.typeZone]
                strTypeZone = self.qmapodConfig.odtZonageAnalysable[
                    self.analFluxZones.typeZone
                ]
                strCarto = strCarto.replace("id_zone", strTypeZone["colIdZone"])
                self.analFluxZones.fzLayer.setSubsetString(strCarto)

        # Flux par arrêts
        if isinstance(self.analFluxArrets, QMapODFluxArrets):
            # Si la couche existe, on met à jour le sous-ensemble
            if self.analFluxArrets is not None:
                self.analFluxArrets.faLayer.setSubsetString(odtWhereCarto["id_arret"])

        # Rafraichissement de la carte
        self.iface.mapCanvas().refresh()

        msgBar = self.iface.messageBar()
        strMsg = "Critères de filtrage cartographique appliqués."
        msgBar.pushMessage(strMsg, Qgis.Info)

    # -------------------------------------------------------------------------

    def btnRazFiltreCartoAction(self):
        """
        Réinitialisation des critères de filtrage carto
        """

        self.clsFiltrage.razFiltreCarto()

        msgBar = self.iface.messageBar()
        strMsg = "Critères de filtrage cartographiques réinitialisés."
        msgBar.pushMessage(strMsg, Qgis.Info)

    # -------------------------------------------------------------------------

    def rbnSingleColorAction(self, enabled):
        """
        Choix de la visualisation du sous-réseau couleur unique
        """

        # Activation du panneau correspondant au type
        # de visualisation sélectionné
        if enabled:
            # Couleur arrêts
            self.dock.widgetColArrets.setVisible(True)
            # Couleur tronçons
            self.dock.widgetColTroncons.setVisible(True)
            # Couleur troncs communs
            self.dock.widgetColTronCom.setVisible(False)

    # -------------------------------------------------------------------------

    def rbnLineColorsAction(self, enabled):
        """
        Choix de la visualisation du sous-réseau couleurs des lignes
        """

        # Activation du panneau correspondant au type
        # de visualisation sélectionné
        if enabled:
            # Couleur arrêts
            self.dock.widgetColArrets.setVisible(False)
            # Couleur tronçons
            self.dock.widgetColTroncons.setVisible(False)
            # Couleur troncs communs
            self.dock.widgetColTronCom.setVisible(True)

    # -------------------------------------------------------------------------

    def rbnGraduatedAction(self, enabled):
        """
        Choix de la visualisation des flux par zones en dégradé
        """

        # Activation du panneau correspondant au type
        # de visualisation sélectionné
        if enabled:
            self.dock.gpbGraduated.setVisible(True)
            self.dock.gpbUrchin.setVisible(False)
            self.dock.gpbDiagram.setVisible(False)

    # -------------------------------------------------------------------------

    def rbnUrchinAction(self, enabled):
        """
        Choix de la visualisation des flux par zones en oursins
        """

        # Activation du panneau correspondant au type
        # de visualisation sélectionné
        if enabled:
            self.dock.gpbGraduated.setVisible(False)
            self.dock.gpbUrchin.setVisible(True)
            self.dock.gpbDiagram.setVisible(False)

    # -------------------------------------------------------------------------

    def rbnDiagramAction(self, enabled):
        """
        Choix de la visualisation des flux par zones en diagrammes
        """

        # Activation du panneau correspondant au type
        # de visualisation sélectionné
        if enabled:
            self.dock.gpbGraduated.setVisible(False)
            self.dock.gpbUrchin.setVisible(False)
            self.dock.gpbDiagram.setVisible(True)

    # -------------------------------------------------------------------------

    @_waitingCursor
    def btnValidParamAction(self):
        """
        Validation du paramétrage des visualisations
        """

        # Rubrique sous-réseau
        # Couleur des tronçons
        self.qmapodSettings.setValue(
            "params/buttonGroupSousReseauMode",
            self.dock.buttonGroupSousReseauMode.checkedId(),
        )
        self.qmapodSettings.setValue(
            "params/btnColTronCom", self.dock.btnColTronCom.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColTronCom", self.dock.btnColTronCom.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColTroncons", self.dock.btnColTroncons.color()
        )
        self.qmapodSettings.setValue(
            "params/spxTronconsWidth", self.dock.spxTronconsWidth.value()
        )
        self.qmapodSettings.setValue(
            "params/btnColArrets", self.dock.btnColArrets.color()
        )
        self.qmapodSettings.setValue(
            "params/spxArretsSize", self.dock.spxArretsSize.value()
        )
        self.qmapodSettings.setValue(
            "params/spxTranspSousReseau", self.dock.spxTranspSousReseau.value()
        )
        self.qmapodSettings.setValue(
            "params/cbxArretSousReseauLabel",
            self.dock.cbxArretSousReseauLabel.currentIndex(),
        )

        # Rubrique Arrêts
        self.qmapodSettings.setValue(
            "params/spxArretDiagMaxSize", self.dock.spxArretDiagMaxSize.value()
        )
        self.qmapodSettings.setValue(
            "params/btnArretColMontees", self.dock.btnArretColMontees.color()
        )
        self.qmapodSettings.setValue(
            "params/btnArretColDescentes", self.dock.btnArretColDescentes.color()
        )
        self.qmapodSettings.setValue(
            "params/btnArretColMonteesS1", self.dock.btnArretColMonteesS1.color()
        )
        self.qmapodSettings.setValue(
            "params/btnArretColMonteesS2", self.dock.btnArretColMonteesS2.color()
        )
        self.qmapodSettings.setValue(
            "params/btnArretColDescentesS1", self.dock.btnArretColDescentesS1.color()
        )
        self.qmapodSettings.setValue(
            "params/btnArretColDescentesS2", self.dock.btnArretColDescentesS2.color()
        )
        self.qmapodSettings.setValue(
            "params/btnArretColOutline", self.dock.btnArretColOutline.color()
        )
        self.qmapodSettings.setValue(
            "params/spxArretOutlineWidth", self.dock.spxArretOutlineWidth.value()
        )
        self.qmapodSettings.setValue(
            "params/spxArretTranspDiag", self.dock.spxArretTranspDiag.value()
        )
        self.qmapodSettings.setValue(
            "params/cbxArretLabel", self.dock.cbxArretLabel.currentIndex()
        )
        self.qmapodSettings.setValue(
            "params/buttonGroupArretMode", self.dock.buttonGroupArretMode.checkedId()
        )

        # Rubrique Zones
        self.qmapodSettings.setValue(
            "params/spxZoneDiagMaxSize", self.dock.spxZoneDiagMaxSize.value()
        )
        self.qmapodSettings.setValue(
            "params/btnZoneColMontees", self.dock.btnZoneColMontees.color()
        )
        self.qmapodSettings.setValue(
            "params/btnZoneColDescentes", self.dock.btnZoneColDescentes.color()
        )
        self.qmapodSettings.setValue(
            "params/btnZoneColOutline", self.dock.btnZoneColOutline.color()
        )
        self.qmapodSettings.setValue(
            "params/spxZoneOutlineWidth", self.dock.spxZoneOutlineWidth.value()
        )
        self.qmapodSettings.setValue(
            "params/spxZoneTranspDiag", self.dock.spxZoneTranspDiag.value()
        )
        self.qmapodSettings.setValue(
            "params/cbxZoneLabel", self.dock.cbxZoneLabel.currentIndex()
        )
        self.qmapodSettings.setValue(
            "params/buttonGroupZoneMode", self.dock.buttonGroupZoneMode.checkedId()
        )

        # Rubrique Tronçons
        self.qmapodSettings.setValue(
            "params/spxSerpMinWidth", self.dock.spxSerpMinWidth.value()
        )
        self.qmapodSettings.setValue(
            "params/spxSerpMaxWidth", self.dock.spxSerpMaxWidth.value()
        )
        self.qmapodSettings.setValue(
            "params/spxSerpNbClasses", self.dock.spxSerpNbClasses.value()
        )
        self.qmapodSettings.setValue(
            "params/cbxSerpClassification",
            self.dock.cbxSerpClassification.currentIndex(),
        )
        self.qmapodSettings.setValue(
            "params/btnColSOffre", self.dock.btnColSOffre.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColSCharge", self.dock.btnColSCharge.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColSPerf", self.dock.btnColSPerf.color()
        )
        self.qmapodSettings.setValue(
            "params/spxTransparencySerpent", self.dock.spxTransparencySerpent.value()
        )
        self.qmapodSettings.setValue(
            "params/cbxLabelSerpent", self.dock.cbxLabelSerpent.currentIndex()
        )

        # Rubrique flux principaux
        self.qmapodSettings.setValue(
            "params/spxFluxPPLargMaxFleches", self.dock.spxFluxPPLargMaxFleches.value()
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPCol", self.dock.btnFluxPPCol.color()
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPColOutline", self.dock.btnFluxPPColOutline.color()
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPLColSens1", self.dock.btnFluxPPLColSens1.color()
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPLColOutlineSens1",
            self.dock.btnFluxPPLColOutlineSens1.color(),
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPLColSens2", self.dock.btnFluxPPLColSens2.color()
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPLColOutlineSens2",
            self.dock.btnFluxPPLColOutlineSens2.color(),
        )
        self.qmapodSettings.setValue(
            "params/spxFluxPPOutlineWidth", self.dock.spxFluxPPOutlineWidth.value()
        )
        self.qmapodSettings.setValue(
            "params/spxFluxPPTranspFleches", self.dock.spxFluxPPTranspFleches.value()
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPColZones", self.dock.btnFluxPPColZones.color()
        )
        self.qmapodSettings.setValue(
            "params/btnFluxPPColOutlineZones",
            self.dock.btnFluxPPColOutlineZones.color(),
        )
        self.qmapodSettings.setValue(
            "params/spxFluxPPOutlineWidthZones",
            self.dock.spxFluxPPOutlineWidthZones.value(),
        )
        self.qmapodSettings.setValue(
            "params/spxFluxPPTranspZones", self.dock.spxFluxPPTranspZones.value()
        )

        # Rubrique flux par zones
        self.qmapodSettings.setValue(
            "params/buttonGroupVisuFluxZone",
            self.dock.buttonGroupVisuFluxZone.checkedId(),
        )
        self.qmapodSettings.setValue(
            "params/cbxLabelFluxZone", self.dock.cbxLabelFluxZone.currentIndex()
        )
        self.qmapodSettings.setValue(
            "params/buttonGroupFluxZoneMode",
            self.dock.buttonGroupFluxZoneMode.checkedId(),
        )
        self.qmapodSettings.setValue(
            "params/btnColMiniGraduatedM", self.dock.btnColMiniGraduatedM.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColMaxiGraduatedM", self.dock.btnColMaxiGraduatedM.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColMiniGraduatedD", self.dock.btnColMiniGraduatedD.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColMaxiGraduatedD", self.dock.btnColMaxiGraduatedD.color()
        )
        self.qmapodSettings.setValue(
            "params/spxTransparencyGraduated",
            self.dock.spxTransparencyGraduated.value(),
        )
        self.qmapodSettings.setValue(
            "params/spxNbClassesGraduated", self.dock.spxNbClassesGraduated.value()
        )
        self.qmapodSettings.setValue(
            "params/cbxClassificationGraduated",
            self.dock.cbxClassificationGraduated.currentIndex(),
        )
        # Flux oursins
        self.qmapodSettings.setValue(
            "params/spxUrchinWidth", self.dock.spxUrchinWidth.value()
        )
        self.qmapodSettings.setValue(
            "params/btnColMiniUrchinM", self.dock.btnColMiniUrchinM.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColMaxiUrchinM", self.dock.btnColMaxiUrchinM.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColMiniUrchinD", self.dock.btnColMiniUrchinD.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColMaxiUrchinD", self.dock.btnColMaxiUrchinD.color()
        )
        self.qmapodSettings.setValue(
            "params/spxTransparencyUrchin", self.dock.spxTransparencyUrchin.value()
        )
        self.qmapodSettings.setValue(
            "params/spxNbClassesUrchin", self.dock.spxNbClassesUrchin.value()
        )
        self.qmapodSettings.setValue(
            "params/cbxClassificationUrchin",
            self.dock.cbxClassificationUrchin.currentIndex(),
        )
        # Flux diagrammes
        self.qmapodSettings.setValue(
            "params/spxFluxZoneDiagMaxSize", self.dock.spxFluxZoneDiagMaxSize.value()
        )
        self.qmapodSettings.setValue(
            "params/btnColBgDiagramM", self.dock.btnColBgDiagramM.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColBgDiagramD", self.dock.btnColBgDiagramD.color()
        )
        self.qmapodSettings.setValue(
            "params/spxTransparencyDiagram", self.dock.spxTransparencyDiagram.value()
        )
        self.qmapodSettings.setValue(
            "params/btnColOutlineDiagram", self.dock.btnColOutlineDiagram.color()
        )
        self.qmapodSettings.setValue(
            "params/spxWidthOutlineDiagram", self.dock.spxWidthOutlineDiagram.value()
        )

        # Rubrique flux par arrêts
        self.qmapodSettings.setValue(
            "params/buttonGroupFluxArretMode",
            self.dock.buttonGroupFluxArretMode.checkedId(),
        )
        self.qmapodSettings.setValue(
            "params/cbxLabelFluxArret", self.dock.cbxLabelFluxArret.currentIndex()
        )
        self.qmapodSettings.setValue(
            "params/spxFluxArretsDiagMaxSize",
            self.dock.spxFluxArretsDiagMaxSize.value(),
        )
        self.qmapodSettings.setValue(
            "params/btnColFluxArretM", self.dock.btnColFluxArretM.color()
        )
        self.qmapodSettings.setValue(
            "params/btnColFluxArretD", self.dock.btnColFluxArretD.color()
        )
        self.qmapodSettings.setValue(
            "params/spxTransparencySymbol", self.dock.spxTransparencySymbol.value()
        )
        self.qmapodSettings.setValue(
            "params/btnColOutlineSymbol", self.dock.btnColOutlineSymbol.color()
        )
        self.qmapodSettings.setValue(
            "params/spxWidthOutlineSymbol", self.dock.spxWidthOutlineSymbol.value()
        )

        """
        Mise à jour des analyses existantes
        """
        # Affichage du sous-réseau courant
        if isinstance(self.sousReseau, QMapODSousReseau):
            # Si la couche existe, on met à jour l'analyse
            if self.sousReseau is not None:
                if (
                    self.sousReseau.srArretLayer is not None
                    and self.sousReseau.srTronconLayer is not None
                ):
                    self.sousReseau.majSousReseau()

        # Analyses en montées / descentes par arrêts
        if isinstance(self.analArrets, QMapODAnalArret):
            # Si la couche existe, on met à jour l'analyse
            if self.analArrets is not None:
                if self.analArrets.amdLayer is not None:
                    self.analArrets.delArretMD()
                    self.analArrets.addArretMD(self.nbLignes)

        # Analyses en montées / descentes par variable par arrêts
        if isinstance(self.analArretsVar, QMapODAnalArretVar):
            # Si la couche existe, on met à jour l'analyse
            if self.analArretsVar is not None:
                if self.analArretsVar.avarLayer is not None:
                    self.analArretsVar.delArretVar()
                    self.analArretsVar.addArretVar(self.nbLignes)

        # Analyses en montées / descentes par zones
        if isinstance(self.analZones, QMapODAnalZone):
            # Si la couche existe, on met à jour l'analyse
            if self.analZones is not None:
                if self.analZones.zmdLayer is not None:
                    self.analZones.delZoneMD()
                    self.analZones.addZoneMD(self.nbLignes)

        # Analyses en montées / descentes par variable par zones
        if isinstance(self.analZonesVar, QMapODAnalZoneVar):
            # Si la couche existe, on met à jour l'analyse
            if self.analZonesVar is not None:
                if self.analZonesVar.zvarLayer is not None:
                    self.analZonesVar.delZoneVar()
                    self.analZonesVar.addZoneVar(self.nbLignes)

        # Analyses en serpents
        if isinstance(self.analSerpent, QMapODAnalSerpent):
            # Si la couche existe, on met à jour l'analyse
            if self.analSerpent.serpLayer is not None:
                self.analSerpent.delSerpent()
                self.analSerpent.addSerpent()

        # Flux principaux
        if isinstance(self.analFluxPP, QMapODFluxPP):
            if self.analFluxPP.flxppLayer is not None:
                # self.analSerpent.majFluxPP()
                self.analFluxPP.delFluxPP()
                self.analFluxPP.addFluxPP(
                    self.dock.spxNbFluxPP.value(), self.nbLignes, False
                )

        # Flux principaux par ligne
        if isinstance(self.analFluxPPL, QMapODFluxPP):
            if self.analFluxPPL.flxppLayer is not None:
                # self.analSerpent.majFluxPP()
                self.analFluxPPL.delFluxPP()
                self.analFluxPPL.addFluxPP(
                    self.dock.spxNbFluxPPL.value(), self.nbLignes, True
                )

        # Flux par zones
        if isinstance(self.analFluxZones, QMapODFluxZones):
            # Si la couche existe, on met à jour l'analyse
            if self.analFluxZones.fzLayer is not None:
                typVisu = self.analFluxZones.typVisu
                if self.dock.buttonGroupVisuFluxZone.checkedId() == typVisu:
                    self.analFluxZones.majFluxZones(
                        self.nbLignes,
                        int(
                            self.qmapodSettings.value(
                                "params/buttonGroupVisuFluxZone", 3
                            )
                        ),
                    )
                else:
                    # Suppression de l'analyse courante si le type
                    # de visualisation est modifié
                    self.analFluxZones.delFluxZones()
                    self.analFluxZones.fzLayer = None

        # Flux par arrêts
        if isinstance(self.analFluxArrets, QMapODFluxArrets):
            # Si la couche existe, on met à jour l'analyse
            if self.analFluxArrets.faLayer is not None:
                self.analFluxArrets.majFluxArrets(self.nbLignes)

        # Rafraichissement de la carte
        self.iface.mapCanvas().refresh()

        # Mise à jour des paramètres d'analyse dans le récapitulatif
        self.clsFiltrage.majRecap(self.qmapodSettings)

        # Récupération des paramètres d'analyse
        strModeEnq = {1: "voyage", 2: "déplacement"}
        strArret = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupArretMode", 1))
        ]
        strZone = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupZoneMode", 1))
        ]
        strFluxZone = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupFluxZoneMode", 1))
        ]
        strFluxArret = strModeEnq[
            int(self.qmapodSettings.value("params/buttonGroupFluxArretMode", 1))
        ]
        msgBar = self.iface.messageBar()
        strMsg = Template(
            "Paramètres d'affichage validés - "
            + "Arrêts : logique $str_arret - Zones : logique $str_zone - "
            + "Flux zones : logique $str_flux_zone - "
            + "Flux arrêts : logique $str_flux_arret"
        ).substitute(
            str_arret=strArret,
            str_zone=strZone,
            str_flux_zone=strFluxZone,
            str_flux_arret=strFluxArret,
        )
        msgBar.pushMessage(strMsg, Qgis.Info)

    # -------------------------------------------------------------------------

    def btnDefaultParamsAction(self):
        """
        Rechargement des paramètres par défaut
        """

        qmapodDefaultSettings = QSettings(
            os.path.join(self.qmapodConfig.pluginDir, "QMapODDefault.ini"),
            QSettings.IniFormat,
        )
        self.dock._initParams(qmapodDefaultSettings)
        msgBar = self.iface.messageBar()
        strMsg = "Paramètres d'affichage par défaut rechargés."
        msgBar.pushMessage(strMsg, Qgis.Info)

    # -------------------------------------------------------------------------

    def spxTronconsWidthAction(self):
        self.qmapodSettings.setValue(
            "params/spxTronconsWidth", self.dock.spxTronconsWidth.value()
        )
        # Affichage du sous-réseau courant
        if isinstance(self.sousReseau, QMapODSousReseau):
            # Si la couche existe, on met à jour l'analyse
            if self.sousReseau is not None:
                self.sousReseau.majSpxTronconsWidth(self.dock.spxTronconsWidth.value())

    # -------------------------------------------------------------------------

    def btnExportProjectAction(self):
        """
        Export du projet courant
        """

        outputDir = "D:/QMapOD/test_qcons"
        # copy project file
        projectFile = QgsProject.instance().fileName()
        f = QFile(projectFile)
        newProjectFile = outputDir + "/" + QFileInfo(projectFile).fileName()
        f.copy(newProjectFile)

        self.workThread.start()

    # -------------------------------------------------------------------------

    def btnExportLayerAction(self):
        """
        Export de la couche courante
        """

        currentLayer = self.iface.activeLayer()
        if currentLayer is not None:
            self.logger.info(currentLayer.name())
            filename = QFileDialog(self).getSaveFileName()
            if filename != "":
                self.logger.info(filename)

    # -------------------------------------------------------------------------

    def unload(self):
        """
        Removes the plugin menu item and icon from QGIS GUI.
        """

        for action in self.actions:
            self.iface.removePluginMenu(self.tr("&QMapOD"), action)
            self.iface.removeToolBarIcon(action)

        self.dock.setVisible(False)
        self.iface.mainWindow().removeDockWidget(self.dock)
        self.dock = None
        self.iface.mainWindow().removeToolBar(self.toolbar)
        self.toolbar = None

        # Déconnexion des signaux relatifs aux outils
        self.iface.mapCanvas().mapToolSet.disconnect(self._mapToolChanged)

    # -------------------------------------------------------------------------

    def run(self):
        """
        Affichage/masquage du panneau ancrable
        """

        if self.dock.isVisible():
            self.dock.hide()
        else:
            self.dock.show()

    # -------------------------------------------------------------------------

    def showAllLabels(self, state):
        """
        Activation / désactivation globale de l'affichage des diagrammes
        et des étiquettes superposés
        TODO : gérer couche par couche ?
        """

        if state == Qt.Checked:
            # Activation globale de la superposition des étiquettes
            les = QgsProject.instance().labelingEngineSettings()
            les.setFlag(QgsLabelingEngineSettings.UseAllLabels, True)
            QgsProject.instance().setLabelingEngineSettings(les)

        else:
            # Désactivation globale de la superposition des étiquettes
            les = QgsProject.instance().labelingEngineSettings()
            les.setFlag(QgsLabelingEngineSettings.UseAllLabels, False)
            QgsProject.instance().setLabelingEngineSettings(les)

        # Rafraichissement de la carte (toutes les couches)
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def loadMapODLayers(self):
        """
        Chargement des couches QMapOD
        """

        # TODO reprendre symbologie de flux_arrets ligne 807

        """
        ATTENTION AU NOM DES COLONNES GEOMETRY POUR EVITER UN PLANTAGE AU CHARGEMENT DES COUCHES !!!
        """
        # Chargement des zonages
        lstLayers = []
        for layer, dctParam in self.qmapodConfig.odtZonage.items():
            uriLayer = QgsDataSourceUri()
            uriLayer.setDatabase(self.qmapodConfig.db)
            uriLayer.setDataSource(
                "", dctParam["table"], "geometry", "", dctParam["enqfield"]
            )
            objLayer = QgsVectorLayer(uriLayer.uri(), dctParam["label"], "spatialite")
            if objLayer.isValid():
                lstLayers.append(objLayer)
            # Symbologie par défaut (entités polygonales)
            res = objLayer.loadDefaultStyle()
            if not res[1]:
                symbolLayer = QgsSimpleFillSymbolLayer()
                symbolLayer.setStrokeWidth(0.4)
                symbolLayer.setStrokeColor(QColor(100, 100, 100, 255))
                symbolLayer.setFillColor(QColor(0, 0, 0, 0))
                objLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
                    0, symbolLayer
                )
            objLayer.updateExtents()

        # Couche des troncons
        uriTroncons = QgsDataSourceUri()
        uriTroncons.setDatabase(self.qmapodConfig.db)
        uriTroncons.setDataSource("", "troncons", "geometry", "", "id_tron")
        tronconsLayer = QgsVectorLayer(uriTroncons.uri(), "Tronçons", "spatialite")
        if tronconsLayer.isValid():
            lstLayers.append(tronconsLayer)
        # Symbologie par défaut (entités linéaires)
        res = tronconsLayer.loadDefaultStyle()
        if not res[1]:
            symbolLayer = QgsSimpleLineSymbolLayer()
            symbolLayer.setWidth(0.2)
            symbolLayer.setColor(QColor(154, 125, 238, 255))
            tronconsLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
                0, symbolLayer
            )
        tronconsLayer.updateExtents()

        # Couche des arrêts
        uriArrets = QgsDataSourceUri()
        uriArrets.setDatabase(self.qmapodConfig.db)
        uriArrets.setDataSource("", "arrets", "geometry", "", "id_arret")
        arretsLayer = QgsVectorLayer(uriArrets.uri(), "Arrêts", "spatialite")
        # arretsLayer.setSubsetString("fictif = "F"")
        arretsLayer.setSubsetString("fictif = 0")
        if arretsLayer.isValid():
            lstLayers.append(arretsLayer)
        # Symbologie par défaut (entités ponctuelles)
        res = arretsLayer.loadDefaultStyle()
        if not res[1]:
            symbolLayer = QgsSimpleMarkerSymbolLayer()
            symbolLayer.setSize(1.2)
            symbolLayer.setColor(QColor(154, 125, 238, 255))
            arretsLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
                0, symbolLayer
            )
            arretsLayer.updateExtents()

        # Ajout des couches et déplacement dans un groupe (api QgsLayerTree)
        root = QgsProject.instance().layerTreeRoot()
        grpLayer = root.insertGroup(0, self.nomReseau)
        for layer in lstLayers:
            QgsProject.instance().addMapLayer(layer, False)
            treeLayer = QgsLayerTreeLayer(layer)
            treeLayer.setName(layer.name())
            layer.updateExtents()
            grpLayer.insertChildNode(0, treeLayer)
            layer.triggerRepaint()

        # Étiquetage de la couche arrets
        palLayer = QgsPalLayerSettings()
        strField = "coalesce(id_arret, '') || \"\n\" || coalesce(nom_arret, '')"
        palLayer.isExpression = True
        palLayer.enabled = True
        palLayer.fieldName = strField
        palLayer.placement = QgsPalLayerSettings.AroundPoint
        palLayer.dist = 1
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter
        palLayer.minimumScale = 20000
        palLayer.maximumScale = 1  # TODO : Rendre paramétrable
        palLayer.scaleVisibility = True

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 8, QFont.Normal))
        txtFormat.setSize(8)
        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        arretsLayer.setLabeling(labeling)

        # Rafraichissement de la carte
        self.iface.mapCanvas().refresh()
        self.iface.mapCanvas().zoomToFullExtent()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnSousReseauAction(self):
        """
        Affichage du sous-réseau
        """

        # Si le bouton est coché affichage du sous-réseau
        if self.dock.sender().isChecked():
            self.sousReseau = QMapODSousReseau(
                self.iface,
                self.dock,
                self.dock.sender(),
                self.clsFiltrage,
                self.qmapodConfig,
                self.qmapodSettings,
            )
            self.sousReseau.addSousReseau()

        # Sinon suppression de l'affichage du sous-réseau
        else:
            self.sousReseau.delSousReseau()
            self.sousReseau = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnMDArretAction(self):
        """
        Affichage des montées/descentes par arrêts.
        """

        if self.analArrets is not None:
            self.analArrets.delArretMD()
        self.analArrets = QMapODAnalArret(
            self.iface,
            self.dock,
            self.dock.sender(),
            self.clsFiltrage,
            "md",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées/descentes par arrêts
            self.analArrets.addArretMD(self.nbLignes)

        else:
            # Suppression de l'affichage des montées/descentes par arrêts
            self.analArrets.delArretMD()
            self.analArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnMArretAction(self):
        """Affichage des montées par arrêts."""

        if self.analArrets is not None:
            self.analArrets.delArretMD()
        self.analArrets = QMapODAnalArret(
            self.iface,
            self.dock,
            self.dock.sender(),
            self.clsFiltrage,
            "m",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées par arrêts
            self.analArrets.addArretMD(self.nbLignes)

        else:
            # Suppression de l'affichage des montées par arrêts
            self.analArrets.delArretMD()
            self.analArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnDArretAction(self):
        """Affichage des descentes par arrêts."""

        if self.analArrets is not None:
            self.analArrets.delArretMD()
        self.analArrets = QMapODAnalArret(
            self.iface,
            self.dock,
            self.dock.sender(),
            self.clsFiltrage,
            "d",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des descentes par arrêts
            self.analArrets.addArretMD(self.nbLignes)

        else:
            # Suppression de l'affichage des descentes par arrêts
            self.analArrets.delArretMD()
            self.analArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnMSArretAction(self):
        """Affichage des montées par sens par arrêts."""

        if self.analArrets is not None:
            self.analArrets.delArretMD()
        self.analArrets = QMapODAnalArret(
            self.iface,
            self.dock,
            self.dock.sender(),
            self.clsFiltrage,
            "ms",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées par sens par arrêts
            self.analArrets.addArretMD(self.nbLignes)

        else:
            # Suppression de l'affichage des montées par sens par arrêts
            self.analArrets.delArretMD()
            self.analArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnDSArretAction(self):
        """Affichage des descentes par sens par arrêts."""

        if self.analArrets is not None:
            self.analArrets.delArretMD()
        self.analArrets = QMapODAnalArret(
            self.iface,
            self.dock,
            self.dock.sender(),
            self.clsFiltrage,
            "ds",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des descentes par sens par arrêts
            self.analArrets.addArretMD(self.nbLignes)

        else:
            # Suppression de l'affichage des descentes par sens par arrêts
            self.analArrets.delArretMD()
            self.analArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def cbxVarArretAction(self):
        """
        Mise à jour de l'analyse des montées/descentes par variable par arrêts.
        """

        # Récupération des paramètres d'analyse
        varAnal = self.dock.sender().itemData(self.dock.sender().currentIndex())
        cnfVarAnal = self.qmapodConfig.odtVariableAnalysable[varAnal]
        tabVar = cnfVarAnal["tabVar"]
        colVar = cnfVarAnal["colVar"]
        colIdVar = cnfVarAnal["colIdVar"]
        colLibVar = cnfVarAnal["colLibVar"]
        titleVar = cnfVarAnal["title"]

        # Gestion de l'activation des boutons tbnMVarArret et tbnDVarArret
        # Désactivation de tbnMVarArret si sélection des correspondances aval
        if tabVar == "code_ligne_corav":
            self.dock.tbnMVarArret.setDisabled(True)
            self.dock.tbnDVarArret.setDisabled(False)
        # Désactivation de tbnDVarArret si sélection des correspondances amont
        elif tabVar == "code_ligne_coram":
            self.dock.tbnMVarArret.setDisabled(False)
            self.dock.tbnDVarArret.setDisabled(True)
        # Pour les autre variables, activation des deux boutons
        else:
            self.dock.tbnMVarArret.setDisabled(False)
            self.dock.tbnDVarArret.setDisabled(False)

        # Mise à jour de l'analyse courante
        if self.analArretsVar is not None:
            # Récupération du type de l'analyse courante
            if self.analArretsVar.typAnal == "m":
                tbn = self.dock.tbnMVarArret
            elif self.analArretsVar.typAnal == "d":
                tbn = self.dock.tbnDVarArret

            # Suppression de l'analyse courante
            self.analArretsVar.delArretVar()

            # Régénération de l'analyse avec la nouvelle variable
            self.analArretsVar = QMapODAnalArretVar(
                self.iface,
                tbn,
                self.clsFiltrage,
                self.analArretsVar.typAnal,
                tabVar,
                colVar,
                colIdVar,
                colLibVar,
                titleVar,
                self.qmapodConfig,
                self.qmapodSettings,
            )

            # Affichage des montées ou des descentes par arrêts par variable
            # Sauf pour les correspondances aval par arrêts de montée
            # et les correspondances amont par arrêts de descente
            if not (
                (tabVar == "code_ligne_corav" and self.analArretsVar.typAnal == "m")
                or (tabVar == "code_ligne_coram" and self.analArretsVar.typAnal == "d")
            ):
                self.analArretsVar.addArretVar(self.nbLignes)

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnMVarArretAction(self):
        """Affichage des montées par une variable par arrêts."""

        # varAnal = self.dock.cbxVarArret.currentText()
        varAnal = self.dock.cbxVarArret.itemData(self.dock.cbxVarArret.currentIndex())
        cnfVarAnal = self.qmapodConfig.odtVariableAnalysable[varAnal]
        tabVar = cnfVarAnal["tabVar"]
        colVar = cnfVarAnal["colVar"]
        colIdVar = cnfVarAnal["colIdVar"]
        colLibVar = cnfVarAnal["colLibVar"]
        titleVar = cnfVarAnal["title"]
        if self.analArretsVar is not None:
            self.analArretsVar.delArretVar()
        self.analArretsVar = QMapODAnalArretVar(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "m",
            tabVar,
            colVar,
            colIdVar,
            colLibVar,
            titleVar,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées par arrêts par variable
            self.analArretsVar.addArretVar(self.nbLignes)

        else:
            # Suppression de l'affichage des montées par arrêts par variable
            self.analArretsVar.delArretVar()
            self.analArretsVar = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnDVarArretAction(self):
        """Affichage des descentes par une variable par arrêts."""

        # varAnal = self.dock.cbxVarArret.currentText()
        varAnal = self.dock.cbxVarArret.itemData(self.dock.cbxVarArret.currentIndex())
        cnfVarAnal = self.qmapodConfig.odtVariableAnalysable[varAnal]
        tabVar = cnfVarAnal["tabVar"]
        colVar = cnfVarAnal["colVar"]
        colIdVar = cnfVarAnal["colIdVar"]
        colLibVar = cnfVarAnal["colLibVar"]
        titleVar = cnfVarAnal["title"]
        if self.analArretsVar is not None:
            self.analArretsVar.delArretVar()
        self.analArretsVar = QMapODAnalArretVar(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "d",
            tabVar,
            colVar,
            colIdVar,
            colLibVar,
            titleVar,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des descentes par arrêts par variable
            self.analArretsVar.addArretVar(self.nbLignes)

        else:
            # Suppression de l'affichage des descentes par arrêts par variable
            self.analArretsVar.delArretVar()
            self.analArretsVar = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def btnRazCartoArretsAction(self):
        """
        Effacement des analyses cartographiques par arrêts.
        """

        # Analyses en montées/descentes
        if isinstance(self.analArrets, QMapODAnalArret):
            self.analArrets.delArretMD()
            self.analArrets = None

        # Analyses en montées/descentes par variable
        if isinstance(self.analArretsVar, QMapODAnalArretVar):
            self.analArretsVar.delArretVar()
            self.analArretsVar = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def cbxMDZoneAction(self):
        """
        Mise à jour de l'analyse des montées/descentes par zone.
        """

        # Récupération des paramètres d'analyse
        zoneAnal = self.dock.sender().itemData(self.dock.sender().currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        varAnal = self.dock.cbxVarZone.itemData(self.dock.cbxVarZone.currentIndex())
        cnfVarAnal = self.qmapodConfig.odtVariableAnalysable[varAnal]
        tabVar = cnfVarAnal["tabVar"]
        colVar = cnfVarAnal["colVar"]
        colIdVar = cnfVarAnal["colIdVar"]
        colLibVar = cnfVarAnal["colLibVar"]
        titleVar = cnfVarAnal["title"]

        # Mise à jour de l'analyse courante (montées/descentes)
        if self.analZones is not None:
            # Récupération du type de l'analyse courante
            if self.analZones.typAnal == "md":
                tbn = self.dock.tbnMDZone
            if self.analZones.typAnal == "m":
                tbn = self.dock.tbnMZone
            elif self.analZones.typAnal == "d":
                tbn = self.dock.tbnDZone

            # Suppression de l'analyse courante
            self.analZones.delZoneMD()

            # Régénération de l'analyse avec la nouvelle variable
            self.analZones = QMapODAnalZone(
                self.iface,
                tbn,
                self.clsFiltrage,
                self.analZones.typAnal,
                self.nbLignes,
                tabZone,
                colIdZone,
                colNomZone,
                titleZone,
                self.qmapodConfig,
                self.qmapodSettings,
            )

            # Affichage des montées/descentes par zones
            self.analZones.addZoneMD(self.nbLignes)

        # Mise à jour de l'analyse courante (montées/descentes par variable)
        if self.analZonesVar is not None:
            # Récupération du type de l'analyse courante
            if self.analZonesVar.typAnal == "m":
                tbn = self.dock.tbnMVarZone
            elif self.analZonesVar.typAnal == "d":
                tbn = self.dock.tbnDVarZone

            # Suppression de l'analyse courante
            self.analZonesVar.delZoneVar()

            # Régénération de l'analyse avec la nouvelle variable
            self.analZonesVar = QMapODAnalZoneVar(
                self.iface,
                tbn,
                self.clsFiltrage,
                self.analZonesVar.typAnal,
                tabZone,
                colIdZone,
                colNomZone,
                titleZone,
                tabVar,
                colVar,
                colIdVar,
                colLibVar,
                titleVar,
                self.qmapodConfig,
                self.qmapodSettings,
            )

            # Affichage des montées ou des descentes par zones par variable
            self.analZonesVar.addZoneVar(self.nbLignes)

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnMDZoneAction(self):
        """Affichage des montées/descentes par zones."""

        zoneAnal = self.dock.cbxMDZone.itemData(self.dock.cbxMDZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        if self.analZones is not None:
            self.analZones.delZoneMD()
        self.analZones = QMapODAnalZone(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "md",
            self.nbLignes,
            tabZone,
            colIdZone,
            colNomZone,
            titleZone,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées/descentes par zones
            self.analZones.addZoneMD(self.nbLignes)

        else:
            # Suppression de l'affichage des montées/descentes par zones
            self.analZones.delZoneMD()
            self.analZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnMZoneAction(self):
        """Affichage des montées/descentes par zones."""

        zoneAnal = self.dock.cbxMDZone.itemData(self.dock.cbxMDZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]

        if self.analZones is not None:
            self.analZones.delZoneMD()
        self.analZones = QMapODAnalZone(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "m",
            self.nbLignes,
            tabZone,
            colIdZone,
            colNomZone,
            titleZone,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées par zones
            self.analZones.addZoneMD(self.nbLignes)

        else:
            # Suppression de l'affichage des montées par zones
            self.analZones.delZoneMD()
            self.analZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnDZoneAction(self):
        """Affichage des montées/descentes par zones."""

        zoneAnal = self.dock.cbxMDZone.itemData(self.dock.cbxMDZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        if self.analZones is not None:
            self.analZones.delZoneMD()
        self.analZones = QMapODAnalZone(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "d",
            self.nbLignes,
            tabZone,
            colIdZone,
            colNomZone,
            titleZone,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des descentes par zones
            self.analZones.addZoneMD(self.nbLignes)

        else:
            # Suppression de l"affichage des descentes par zones
            self.analZones.delZoneMD()
            self.analZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def cbxVarZoneAction(self):
        """
        Mise à jour de l'analyse des montées/descentes par variable par zones.
        """

        # Récupération des paramètres d'analyse
        zoneAnal = self.dock.cbxMDZone.itemData(self.dock.cbxMDZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        varAnal = self.dock.sender().itemData(self.dock.sender().currentIndex())
        cnfVarAnal = self.qmapodConfig.odtVariableAnalysable[varAnal]
        tabVar = cnfVarAnal["tabVar"]
        colVar = cnfVarAnal["colVar"]
        colIdVar = cnfVarAnal["colIdVar"]
        colLibVar = cnfVarAnal["colLibVar"]
        titleVar = cnfVarAnal["title"]

        # Gestion de l'activation des boutons tbnMVarZone et tbnDVarZone
        # Désactivation de tbnMVarZone si sélection des correspondances aval
        if tabVar == "code_ligne_corav":
            self.dock.tbnMVarZone.setDisabled(True)
            self.dock.tbnDVarZone.setDisabled(False)
        # Désactivation de tbnDVarZone si sélection des correspondances amont
        elif tabVar == "code_ligne_coram":
            self.dock.tbnMVarZone.setDisabled(False)
            self.dock.tbnDVarZone.setDisabled(True)
        # Pour les autre variables, activation des deux boutons
        else:
            self.dock.tbnMVarZone.setDisabled(False)
            self.dock.tbnDVarZone.setDisabled(False)

        # Mise à jour de l'analyse courante
        if self.analZonesVar is not None:
            # Récupération du type de l"analyse courante
            if self.analZonesVar.typAnal == "m":
                tbn = self.dock.tbnMVarZone
            elif self.analZonesVar.typAnal == "d":
                tbn = self.dock.tbnDVarZone

            # Suppression de l'analyse courante
            self.analZonesVar.delZoneVar()

            # Régénération de l'analyse avec la nouvelle variable
            self.analZonesVar = QMapODAnalZoneVar(
                self.iface,
                tbn,
                self.clsFiltrage,
                self.analZonesVar.typAnal,
                tabZone,
                colIdZone,
                colNomZone,
                titleZone,
                tabVar,
                colVar,
                colIdVar,
                colLibVar,
                titleVar,
                self.qmapodConfig,
                self.qmapodSettings,
            )

            # Affichage des montées ou des descentes par zones par variable
            # Sauf pour les correspondances aval par arrêts de montée
            # et les correspondances amont par arrêts de descente
            if not (
                (tabVar == "code_ligne_corav" and self.analZonesVar.typAnal == "m")
                or (tabVar == "code_ligne_coram" and self.analZonesVar.typAnal == "d")
            ):
                self.analZonesVar.addZoneVar(self.nbLignes)

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnMVarZoneAction(self):
        """Affichage des montées par une variable par zones."""

        zoneAnal = self.dock.cbxMDZone.itemData(self.dock.cbxMDZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        varAnal = self.dock.cbxVarZone.itemData(self.dock.cbxVarZone.currentIndex())
        cnfVarAnal = self.qmapodConfig.odtVariableAnalysable[varAnal]
        tabVar = cnfVarAnal["tabVar"]
        colVar = cnfVarAnal["colVar"]
        colIdVar = cnfVarAnal["colIdVar"]
        colLibVar = cnfVarAnal["colLibVar"]
        titleVar = cnfVarAnal["title"]
        if self.analZonesVar is not None:
            self.analZonesVar.delZoneVar()
        self.analZonesVar = QMapODAnalZoneVar(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "m",
            tabZone,
            colIdZone,
            colNomZone,
            titleZone,
            tabVar,
            colVar,
            colIdVar,
            colLibVar,
            titleVar,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées par zones par variable
            self.analZonesVar.addZoneVar(self.nbLignes)

        else:
            # Suppression de l'affichage des montées par zones par variable
            self.analZonesVar.delZoneVar()
            self.analZonesVar = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnDVarZoneAction(self):
        """Affichage des descentes par une variable par zones."""

        zoneAnal = self.dock.cbxMDZone.itemData(self.dock.cbxMDZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        varAnal = self.dock.cbxVarZone.itemData(self.dock.cbxVarZone.currentIndex())
        cnfVarAnal = self.qmapodConfig.odtVariableAnalysable[varAnal]
        tabVar = cnfVarAnal["tabVar"]
        colVar = cnfVarAnal["colVar"]
        colIdVar = cnfVarAnal["colIdVar"]
        colLibVar = cnfVarAnal["colLibVar"]
        titleVar = cnfVarAnal["title"]
        if self.analZonesVar is not None:
            self.analZonesVar.delZoneVar()
        self.analZonesVar = QMapODAnalZoneVar(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "d",
            tabZone,
            colIdZone,
            colNomZone,
            titleZone,
            tabVar,
            colVar,
            colIdVar,
            colLibVar,
            titleVar,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des descentes par zones par variable
            self.analZonesVar.addZoneVar(self.nbLignes)

        else:
            # Suppression de l'affichage des descentes par zones par variable
            self.analZonesVar.delZoneVar()
            self.analZonesVar = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def btnRazCartoZonesAction(self):
        """
        Effacement des analyses cartographiques par zones.
        """

        # Analyses en montées/descentes
        if isinstance(self.analZones, QMapODAnalZone):
            self.analZones.delZoneMD()
            self.analZones = None

        # Analyses en montées/descentes par variable
        if isinstance(self.analZonesVar, QMapODAnalZoneVar):
            self.analZonesVar.delZoneVar()
            self.analZonesVar = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnSOffreAction(self):
        """
        Affichage du serpent d'offre
        """
        if self.analSerpent is not None:
            self.analSerpent.delSerpent()
        self.analSerpent = QMapODAnalSerpent(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "o",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage du serpent d'offre
            self.analSerpent.addSerpent()

        else:
            # Suppression de l'affichage du serpent d'offre
            self.analSerpent.delSerpent()
            self.analSerpent = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnSChargeAction(self):
        """
        Affichage du serpent de charge
        """

        if self.analSerpent is not None:
            self.analSerpent.delSerpent()
        self.analSerpent = QMapODAnalSerpent(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "c",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage du serpent de charge
            self.analSerpent.addSerpent()

        else:
            # Suppression de l'affichage du serpent de charge
            self.analSerpent.delSerpent()
            self.analSerpent = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnSPerfAction(self):
        """
        Affichage du serpent de performance
        """
        if self.analSerpent is not None:
            self.analSerpent.delSerpent()
        self.analSerpent = QMapODAnalSerpent(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "p",
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage du serpent de performance
            self.analSerpent.addSerpent()

        else:
            # Suppression de l'affichage du serpent de performance
            self.analSerpent.delSerpent()
            self.analSerpent = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def btnRazCartoTronconsAction(self):
        """
        Effacement des analyses cartographiques par tronçon
        """

        # Analyses par serpents
        if isinstance(self.analSerpent, QMapODAnalSerpent):
            self.analSerpent.delSerpent()
            self.analSerpent = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def cbxFluxZoneAction(self, currentIndex):
        """
        Gestion du changement de sélection de zonage pour les flux par zones
        TODO : mise à jour de l'analyse existante ?
        """

        # Ré-initialisation de la couche de zonage pour les flux
        if isinstance(self.analFluxZones, QMapODFluxZones):
            self.analFluxZones.initFluxZoneLayer(currentIndex)
            self.analFluxZones.setLayer(self.analFluxZones.zoneLayer)

            # Suppression des analyses existantes
            # (impossible de les convertir suite au changement de zonage)
            self.analFluxZones.delFluxZones()

        # Mise à jour des couches de flux principaux
        # Récupération des paramètres d'analyse
        zoneAnal = self.dock.cbxFluxZone.itemData(currentIndex)
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        nbMax = self.dock.spxNbFluxPP.value()

        # Affichage des flux principaux
        if self.analFluxPP is not None:
            self.analFluxPP.delFluxPP()

            self.analFluxPP = QMapODFluxPP(
                self.iface,
                self.dock.tbnFluxPP,
                self.clsFiltrage,
                "pp",
                self.nbLignes,
                tabZone,
                colIdZone,
                colNomZone,
                titleZone,
                self.qmapodConfig,
                self.qmapodSettings,
            )

            self.analFluxPP.addFluxPP(nbMax, self.nbLignes, False)

        # Affichage des flux principaux par ligne
        if self.analFluxPPL is not None:
            self.analFluxPPL.delFluxPP()

            self.analFluxPPL = QMapODFluxPP(
                self.iface,
                self.dock.tbnFluxPPL,
                self.clsFiltrage,
                "ppl",
                self.nbLignes,
                tabZone,
                colIdZone,
                colNomZone,
                titleZone,
                self.qmapodConfig,
                self.qmapodSettings,
            )

            self.analFluxPPL.addFluxPP(nbMax, self.nbLignes, True)

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnFluxZonesMDSelect(self, checked):
        """
        Gestion de l'activation de l'outil tbnFluxZonesMD
        TODO : Améliorer l'ergonomie
        (maintenir l'activation de l'outil lorsqu'il n'y a pas de flux)
        """

        self.dock.lblNbZones.setText("")
        # Si l'outil est coché
        if checked:
            # Initialisation de la couche de zonage pour les flux
            if isinstance(self.analFluxZones, QMapODFluxZones):
                self.analFluxZones.delFluxZones()
                self.dock.lblNbZones.setText("")
                self.analFluxZones = None
            self.analFluxZones = QMapODFluxZones(
                self.iface,
                self.dock,
                self.dock.sender(),
                "m",
                self.nbLignes,
                self.dock.cbxFluxZone.currentIndex(),
                self.qmapodConfig,
                self.qmapodSettings,
            )
            self.analFluxZones.setButton(self.dock.sender())
            self.analFluxZones.geomIdentified.connect(self.tbnFluxZonesMDAction)

            # Activation de l'outil
            self.iface.mapCanvas().setMapTool(self.analFluxZones)

        # Si l'outil est décoché
        else:
            # Désactivation de l'outil
            self.iface.mapCanvas().unsetMapTool(self.analFluxZones)
            if isinstance(self.analFluxZones, QMapODFluxZones):
                self.analFluxZones.delFluxZones()
                self.dock.lblNbZones.setText("")
                self.analFluxZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnFluxZonesMDAction(self, layer, feature):
        """
        Action de l"outil tbnFluxZonesMD
        """

        # Traitement de la zone sélectionnée
        if isinstance(layer, QgsVectorLayer) and isinstance(feature, QgsFeature):
            self.analFluxZones.processZone(feature)
            self.dock.lblNbZones.setText(
                Template("($lst_zones zones)").substitute(
                    lst_zones=len(self.analFluxZones.lstZones)
                )
            )
        else:
            self.analFluxZones.delFluxZones()
            self.dock.lblNbZones.setText("")
            self.analFluxZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnFluxZonesDMSelect(self, checked):
        """
        Gestion de l'activation de l'outil tbnFluxZonesDM
        """

        self.dock.lblNbZones.setText("")
        # Si l'outil est coché
        if checked:
            # Initialisation de la couche de zonage pour les flux
            if isinstance(self.analFluxZones, QMapODFluxZones):
                self.analFluxZones.delFluxZones()
                self.dock.lblNbZones.setText("")
                self.analFluxZones = None
            self.analFluxZones = QMapODFluxZones(
                self.iface,
                self.dock,
                self.dock.sender(),
                "d",
                self.nbLignes,
                self.dock.cbxFluxZone.currentIndex(),
                self.qmapodConfig,
                self.qmapodSettings,
            )
            self.analFluxZones.setButton(self.dock.sender())
            self.analFluxZones.geomIdentified.connect(self.tbnFluxZonesDMAction)

            # Activation de l'outil
            self.iface.mapCanvas().setMapTool(self.analFluxZones)

        # Si l'outil est décoché
        else:
            # Désactivation de l'outil
            self.iface.mapCanvas().unsetMapTool(self.analFluxZones)
            if isinstance(self.analFluxZones, QMapODFluxZones):
                self.analFluxZones.delFluxZones()
                self.dock.lblNbZones.setText("")
                self.analFluxZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnFluxZonesDMAction(self, layer, feature):
        """
        Action de l'outil tbnFluxZonesDM
        """

        # Traitement de la zone sélectionnée
        if isinstance(layer, QgsVectorLayer) and isinstance(feature, QgsFeature):
            self.analFluxZones.processZone(feature)
            self.dock.lblNbZones.setText(
                Template("($lst_zones zones)").substitute(
                    lst_zones=len(self.analFluxZones.lstZones)
                )
            )
        else:
            self.analFluxZones.delFluxZones()
            self.dock.lblNbZones.setText("")
            self.analFluxZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnDelFluxZonesAction(self):
        """
        Suppression des flux par zones
        """

        if isinstance(self.analFluxZones, QMapODFluxZones):
            self.analFluxZones.delFluxZones()
            self.dock.lblNbZones.setText("")
            self.analFluxZones = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnFluxPPAction(self):
        """
        Affichage des flux principaux
        """

        # Récupération des paramètres d'analyse
        zoneAnal = self.dock.cbxFluxZone.itemData(self.dock.cbxFluxZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        nbMax = self.dock.spxNbFluxPP.value()

        if self.analFluxPP is not None:
            self.analFluxPP.delFluxPP()

        self.analFluxPP = QMapODFluxPP(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "pp",
            tabZone,
            colIdZone,
            colNomZone,
            titleZone,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des flux principaux
            self.analFluxPP.addFluxPP(nbMax, self.nbLignes, False)

        else:
            # Suppression de l'affichage des flux principaux
            self.analFluxPP.delFluxPP()
            self.analFluxPP = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def spxNbFluxPPAction(self):
        """
        Mise à jour du nombre de flux principaux
        """

        nbMax = self.dock.spxNbFluxPP.value()
        if isinstance(self.analFluxPP, QMapODFluxPP):
            self.analFluxPP.majFluxPP(nbMax, self.nbLignes, False)

    # -------------------------------------------------------------------------

    @_waitingCursor
    def tbnFluxPPLAction(self):
        """
        Affichage des flux principaux par ligne
        """

        # Récupération des paramètres d'analyse
        zoneAnal = self.dock.cbxFluxZone.itemData(self.dock.cbxFluxZone.currentIndex())
        cnfZoneAnal = self.qmapodConfig.odtZonageAnalysable[zoneAnal]
        tabZone = cnfZoneAnal["tabZone"]
        colIdZone = cnfZoneAnal["colIdZone"]
        colNomZone = cnfZoneAnal["colNomZone"]
        titleZone = cnfZoneAnal["title"]
        nbMax = self.dock.spxNbFluxPPL.value()

        if self.analFluxPPL is not None:
            self.analFluxPPL.delFluxPP()

        self.analFluxPPL = QMapODFluxPP(
            self.iface,
            self.dock.sender(),
            self.clsFiltrage,
            "ppl",
            tabZone,
            colIdZone,
            colNomZone,
            titleZone,
            self.qmapodConfig,
            self.qmapodSettings,
        )

        if self.dock.sender().isChecked():
            # Affichage des montées/descentes par arrêts
            self.analFluxPPL.addFluxPP(nbMax, self.nbLignes, True)

        else:
            # Suppression de l'affichage des montées/descentes par arrêts
            self.analFluxPPL.delFluxPP()
            self.analFluxPPL = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def spxNbFluxPPLAction(self):
        """
        Mise à jour du nombre de flux principaux par ligne
        """

        nbMax = self.dock.spxNbFluxPPL.value()
        if isinstance(self.analFluxPPL, QMapODFluxPP):
            self.analFluxPPL.majFluxPP(nbMax, self.nbLignes, True)

    # -------------------------------------------------------------------------

    def tbnFluxArretsMDSelect(self, checked):
        """
        Activation de l'outil tbnFluxArretsMD
        TODO : Améliorer l'ergonomie
        (maintenir l'activation de l'outil lorsqu'il n'y a pas de flux)
        """

        self.dock.lblNbArrets.setText("")
        # Si l'outil est coché
        if checked:
            # Initialisation de la couche de zonage pour les flux
            if isinstance(self.analFluxArrets, QMapODFluxArrets):
                self.analFluxArrets.delFluxArrets()
                self.dock.lblNbArrets.setText("")
                self.analFluxArrets = None
            self.analFluxArrets = QMapODFluxArrets(
                self.iface,
                self.dock,
                self.dock.sender(),
                "m",
                self.nbLignes,
                self.qmapodConfig,
                self.qmapodSettings,
            )
            # self.analFluxArrets.setCursor(Qt.ArrowCursor)
            self.analFluxArrets.setButton(self.dock.sender())
            self.analFluxArrets.geomIdentified.connect(self.tbnFluxArretsMDAction)

            # Activation de l'outil
            # self.toggleTbnFlux(self.dock.tbnFluxArretsMD)
            self.iface.mapCanvas().setMapTool(self.analFluxArrets)
        # Si l'outil est décoché
        else:
            # Désactivation de l'outil
            self.iface.mapCanvas().unsetMapTool(self.analFluxArrets)
            if isinstance(self.analFluxArrets, QMapODFluxArrets):
                self.analFluxArrets.delFluxArrets()
                self.dock.lblNbArrets.setText("")
                self.analFluxArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnFluxArretsMDAction(self, layer, feature):
        """
        Action de l'outil tbnFluxArretsMD
        """

        # Traitement de l'arrêt sélectionné
        if isinstance(layer, QgsVectorLayer) and isinstance(feature, QgsFeature):
            self.analFluxArrets.processArret(feature)
            self.dock.lblNbArrets.setText(
                Template("($lst_arrets arrêts)").substitute(
                    lst_arrets=len(self.analFluxArrets.lstArrets)
                )
            )
        else:
            self.analFluxArrets.delFluxArrets()
            self.dock.lblNbArrets.setText("")
            self.analFluxArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnFluxArretsDMSelect(self, checked):
        """
        Gestion de l'activation de l'outil tbnFluxArretsDM
        """

        self.dock.lblNbArrets.setText("")
        # Si l'outil est coché
        if checked:
            # Initialisation de la couche de zonage pour les flux
            if isinstance(self.analFluxArrets, QMapODFluxArrets):
                self.analFluxArrets.delFluxArrets()
                self.dock.lblNbArrets.setText("")
                self.analFluxArrets = None
            self.analFluxArrets = QMapODFluxArrets(
                self.iface,
                self.dock,
                self.dock.sender(),
                "d",
                self.nbLignes,
                self.qmapodConfig,
                self.qmapodSettings,
            )
            # self.analFluxArrets.setLayer(self.analFluxArrets.arretLayer)
            # self.analFluxArrets.setCursor(Qt.ArrowCursor)
            self.analFluxArrets.setButton(self.dock.sender())
            self.analFluxArrets.geomIdentified.connect(self.tbnFluxArretsDMAction)

            # Activation de l'outil
            # self.toggleTbnFlux(self.dock.tbnFluxArretsDM)
            self.iface.mapCanvas().setMapTool(self.analFluxArrets)
        # Si l'outil est décoché
        else:
            # Désactivation de l'outil
            self.iface.mapCanvas().unsetMapTool(self.analFluxArrets)
            if isinstance(self.analFluxArrets, QMapODFluxArrets):
                self.analFluxArrets.delFluxArrets()
                self.dock.lblNbArrets.setText("")
                self.analFluxArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnFluxArretsDMAction(self, layer, feature):
        """
        Action de l'outil tbnFluxArretsDM
        """

        # Traitement de l'arrêt sélectionné
        if isinstance(layer, QgsVectorLayer) and isinstance(feature, QgsFeature):
            self.analFluxArrets.processArret(feature)
            self.dock.lblNbArrets.setText(
                Template("($lst_arrets arrêts)").substitute(
                    lst_arrets=len(self.analFluxArrets.lstArrets)
                )
            )
        else:
            self.analFluxArrets.delFluxArrets()
            self.dock.lblNbArrets.setText("")
            self.analFluxArrets = None
            # self.analFluxArrets.setCursor(Qt.ArrowCursor)
            # self.iface.mapCanvas().setMapTool(self.analFluxArrets)

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def tbnDelFluxArretsAction(self):
        """
        Suppression des flux par arrêt(s)
        """

        if isinstance(self.analFluxArrets, QMapODFluxArrets):
            self.analFluxArrets.delFluxArrets()
            self.dock.lblNbArrets.setText("")
            self.analFluxArrets = None

        # Rafraichissement de toutes les couches
        self.iface.mapCanvas().refreshAllLayers()

    # -------------------------------------------------------------------------

    def _mapToolChanged(self, newTool):
        """
        Désactivation des outils QMapOD lorsqu'un outil Qgis est sélectionné
        """

        if not isinstance(newTool, QMapODFluxZones):
            if isinstance(self.analFluxZones, QMapODFluxZones):
                if self.analFluxZones.button() is not None:
                    self.analFluxZones.button().setChecked(False)
                    self.iface.mapCanvas().unsetMapTool(self.analFluxZones)
        if not isinstance(newTool, QMapODFluxArrets):
            if isinstance(self.analFluxArrets, QMapODFluxArrets):
                if self.analFluxArrets.button() is not None:
                    self.analFluxArrets.button().setChecked(False)
                    self.iface.mapCanvas().unsetMapTool(self.analFluxArrets)

    # -------------------------------------------------------------------------
    # Méthodes de test
    # -------------------------------------------------------------------------

    def help(self):
        webbrowser.open(
            "file://"
            + os.path.realpath(
                os.path.join(self.qmapodConfig.pluginDir, "doc/aide_qmapod.html")
            )
        )

    # -------------------------------------------------------------------------

    def config(self):
        self.dock.show()
        self.dock.tabFiltrage.setCurrentIndex(2)

    # -------------------------------------------------------------------------

    def test(self):
        # show the message box
        QMessageBox.information(
            self.iface.mainWindow(),
            "À propos de MapOD",
            (
                "QMapOD : cartographie d'enquêtes O/D sous Qgis.\n"
                + "---------------------------------------\n"
                + "Développement par SIGéal pour Test-SA 10/2014-01/2015.\n"
                + "GNU General Public License"
            ),
        )

    # -------------------------------------------------------------------------

    def testBtn(self):
        QMessageBox.information(self.iface.mainWindow(), "Test", "testBtn")

    # -------------------------------------------------------------------------

    def btnTest(self):
        QMessageBox.information(self.iface.mainWindow(), "Test", "btnTest")

    # -------------------------------------------------------------------------

    def chkTestAction(self, state):
        """
        # Forcer l'affichage de tous les diagrammes (ne fonctionne pas)
        """

        if state == Qt.Checked:
            # Deprecated QGIS > 2.4 (QgsRenderContext ?)
            self.logger.info("cochée")
            pls = self.analArrets.amdLayer.labeling().settings()
            pls.displayAll = True
            self.analArrets.amdLayer.labeling().setSettings(pls)

        else:
            self.logger.info("décochée")
            pls = self.analArrets.amdLayer.labeling().settings()
            pls.displayAll = False
            self.analArrets.amdLayer.labeling().setSettings(pls)

        # Refresh map
        self.analArrets.amdLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.analArrets.amdLayer.id())
