"""
/***************************************************************************
 QMapODFluxArrets
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

from qgis.core import (
    QgsDataDefinedSizeLegend,
    QgsDataSourceUri,
    QgsLayerTreeGroup,
    QgsLayerTreeLayer,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsProject,
    QgsProperty,
    QgsSimpleMarkerSymbolLayer,
    QgsSingleSymbolRenderer,
    QgsSizeScaleTransformer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)

# Import des librairies QGIS
from qgis.gui import QgsMapToolIdentify, QgsMapToolIdentifyFeature

# Import des librairies PyQt
from qgis.PyQt.QtCore import Qt, pyqtSignal
from qgis.PyQt.QtGui import QColor, QCursor, QFont
from qgis.PyQt.QtWidgets import QApplication

module_logger = logging.getLogger("QMapOD.flux_arrets")


class QMapODFluxArrets(QgsMapToolIdentifyFeature):
    """
    Classe d'analyse des flux par arrêts
    Hérite de QgsMapToolIdentifyFeature
    """

    # Signal nouveau style
    geomIdentified = pyqtSignal(object, object)

    def __init__(
        self, iface, dock, tbn, typAnal, nbLignes, qmapodConfig, qmapodSettings
    ):
        """
        Constructeur
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        self.canvas = iface.mapCanvas()

        # Initialisation de la couche de sélection des arrêts
        self.initFluxArretLayer()

        # Appel du constructeur du parent
        super(QgsMapToolIdentify, self).__init__(self.canvas)
        self.cursor = QCursor(Qt.WhatsThisCursor)

        # Récupération de la référence au panneau ancrable
        self.dock = dock
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Type d'analyse ('m' ou 'd')
        self.typAnal = typAnal
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Nombre de lignes sélectionnées
        self.nbLignes = nbLignes
        # Liste des arrêts sélectionnés
        self.lstArrets = []

        # Couche des arrêts sélectionnés
        self.selLayer = None
        # Couche des flux
        self.faLayer = None
        # Groupe de couche des flux par arrêts
        self.faGroup = None
        # Caractéristiques diagrammes
        self.maxSize = 0
        self.maxValue = 0
        # Titre de la couche
        self.strNom = ""

        self.logger = logging.getLogger("QMapOD.flux_arrets.QMapODFluxArrets")
        self.logger.info("Creating an instance of QMapODFluxArrets")

    def activate(self):
        """
        Activation de l'outil
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Désactivation de l'outil
        """
        pass

    # -------------------------------------------------------------------------

    def canvasReleaseEvent(self, mouseEvent):
        """
        Émission de l'évènement geomIdentified
        """

        # Récupération des entités identifiées
        results = self.identify(
            mouseEvent.x(),
            mouseEvent.y(),
            self.TopDownStopAtFirst,
            [self.arretLayer],
            self.VectorLayer,
        )
        if len(results) > 0:
            self.geomIdentified.emit(results[0].mLayer, results[0].mFeature)
        else:
            self.geomIdentified.emit(None, None)

    # --------------------------------------------------------------------------

    def _addLayerFluxArretsTmp(self):
        """
        Ajout de la couche arrets_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "m":
            self.strNom = "Flux par arrêt(s) de montée"
        elif self.typAnal == "d":
            self.strNom = "Flux par arrêt(s) de descente"

        # Ajout de la couche
        uriFluxArrets = QgsDataSourceUri()
        uriFluxArrets.setDatabase(self.db)
        uriFluxArrets.setDataSource("", "flux_arrets_tmp", "geometry")
        fluxArretsLayer = QgsVectorLayer(uriFluxArrets.uri(), self.strNom, "spatialite")
        if fluxArretsLayer.isValid():
            # Ajout des couches et déplacement au-dessus
            # des couches existantes (api QgsLayerTree)
            root = QgsProject.instance().layerTreeRoot()
            self.faGroup = root.insertGroup(0, "Flux par arrêts")

            self.faLayer = QgsProject.instance().addMapLayer(fluxArretsLayer, False)
            self.faLayer.destroyed.connect(self._deleted)
            fluxArretsTreeLayer = QgsLayerTreeLayer(fluxArretsLayer)
            fluxArretsTreeLayer.setName(fluxArretsLayer.name())
            self.faGroup.insertChildNode(0, fluxArretsTreeLayer)

    # -------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.delFluxArrets()

    # -------------------------------------------------------------------------

    def _getPoids(self, nbLignes):
        """
        Choix du mode de pondération à utiliser
        """

        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # -------------------------------------------------------------------------

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = Template(
                "\nSELECT max($col_) \n\
                    FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            max = cursor.fetchone()[0]
            if max is None:
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _getSumVal(self, tab, col):
        """
        Récupération de la somme des valeurs d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = Template(
                "\nSELECT sum($col_) \n\
                    FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            sum = cursor.fetchone()[0]
            if sum is None:
                return 0
            else:
                return sum

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _genSqlFluxArrets(self, nbLignes, lstArrets, typAnal):
        """
        Génération de la requête de création de la table flux_arrets_tmp
        """

        # Mode de prise en compte des enquêtes
        modeEnq = int(self.qmapodSettings.value("params/buttonGroupFluxArretMode", 1))
        if modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]

        poids = self._getPoids(nbLignes)

        # Requête d'extraction des flux selon le type (montées / descentes)
        # Modifié le 27/07/2015 pour faire apparaître
        # les valeurs inférieures à 0.5
        if typAnal == "m":
            strChampFlux = "cast(e.descentes AS float) descentes"
            strArretJoin = arretFin
            with open(
                os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_8.sql")
            ) as f:
                strSelectEnq = Template(f.read()).substitute(
                    arret_fin=arretFin,
                    poids_=poids,
                    arret_deb=arretDeb,
                    str=", ".join(
                        map(
                            lambda x: str(x) if str(x).isdigit() else repr(str(x)),
                            lstArrets,
                        )
                    ),
                )
        elif typAnal == "d":
            strChampFlux = "cast(e.montees AS float) montees"
            strArretJoin = arretDeb
            with open(
                os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_9.sql")
            ) as f:
                strSelectEnq = Template(f.read()).substitute(
                    arret_deb=arretDeb,
                    poids_=poids,
                    arret_fin=arretFin,
                    # Modifié pour gérer le cas où les id d'arrêts sont des chaînes
                    str=", ".join(
                        map(
                            lambda x: str(x) if str(x).isdigit() else repr(str(x)),
                            lstArrets,
                        )
                    ),
                )

        # Requête de jointure des flux à la couche arrêts
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_10.sql")
        ) as f:
            strSelectAFlux = Template(f.read()).substitute(
                str_champ_flux=strChampFlux,
                str_select_enq=strSelectEnq,
                str_arret_join=strArretJoin,
            )

        # Requête de création de la table flux_arrets_tmp
        strCreateAmdVar = Template(
            "\nCREATE TABLE flux_arrets_tmp AS\n\
            $str_select_a_flux\n"
        ).substitute(str_select_a_flux=strSelectAFlux)
        strSql = strCreateAmdVar

        return strSql

    # -------------------------------------------------------------------------

    def _createTableFluxArretsTmp(self, nbLignes):
        """
        Création de la table flux_arrets_tmp
        """

        # Génération de la requête de création de table

        nbInserted = 0
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)

            # Get a cursor object
            cursor = db.cursor()

            # Suppression de la table flux_arrets_tmp si elle existe
            s = "\nDROP TABLE IF EXISTS flux_arrets_tmp;\n"
            self.logger.debug(s)
            cursor.execute(s)

            # Création de la table flux_arrets_tmp
            strSql = self._genSqlFluxArrets(nbLignes, self.lstArrets, self.typAnal)
            self.logger.debug(strSql)
            cursor.execute(strSql)

            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
               WHERE f_table_name = 'flux_arrets_tmp'"
            self.logger.debug(s)
            cursor.execute(s)

            # Insertion de la déclaration de la colonne geometry
            # (type 1 = point)
            with open(
                os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_11.sql")
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)

            # Récupération du nombre de lignes insérées
            s = "\nSELECT count(*)\n\
               FROM flux_arrets_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            nbInserted = cursor.fetchone()[0]
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e
        finally:
            # Close the db connection
            db.close()

        return nbInserted

    # -------------------------------------------------------------------------

    def _labelFluxArret(self):
        """
        Etiquetage de la couche des flux
        """

        # Paramètres d'étiquetage
        # Pour éliminer les étiquettes si les valeurs sont nulles
        # case when montees=0 and descentes=0 then ''
        # else montees || '/' || descentes end
        intMode = int(self.qmapodSettings.value("params/cbxLabelFluxArret", 1))
        # blnEnabled = True
        if intMode == 0:
            # si aucun aucune étiquette affichée setLabeling(None)
            self.faLayer.setLabelsEnabled(True)
            self.faLayer.setLabeling(None)
            return
        elif intMode == 1:
            if self.typAnal == "m":
                strField = """
                    if("descentes" >= 0.5, round("descentes", 0),
                        round("descentes", 1))
                """
                blnExp = True
            elif self.typAnal == "d":
                strField = """
                    if("montees" >= 0.5, round("montees", 0),
                        round("montees", 1))
                """
                blnExp = True
            else:
                strField = ""
                blnExp = False
        elif intMode == 2:
            qgisVersion = int(self.qmapodSettings.value("version/version", 0))
            if self.typAnal == "m":
                if qgisVersion < 218000:
                    strField = Template(
                        """
                        round(100 * "descentes" / $sum_value, 1) || ' %'
                    """
                    ).substitute(sum_value=self.sumValue)
                    blnExp = True
                else:  # QGIS >= 2.18
                    strField = """
                        round(100 * "descentes" / sum("descentes"), 1) || ' %'
                    """
                    blnExp = True
            elif self.typAnal == "d":
                if qgisVersion < 218000:
                    strField = Template(
                        """
                        round(100 * "montees" / $sum_value, 1) || ' %'
                    """
                    ).substitute(sum_value=self.sumValue)
                    blnExp = True
                else:  # QGIS >= 2.18
                    strField = """
                        round(100 * "montees" / sum("montees"), 1) || ' %'
                    """
                    blnExp = True
            else:
                strField = ""
                blnExp = False
        else:
            return

        palLayer = QgsPalLayerSettings()
        palLayer.bufferDraw = True
        palLayer.textColor = QColor(0, 0, 0, 255)
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp

        # Centrage des étiquettes
        palLayer.placement = QgsPalLayerSettings.AroundPoint
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 7, QFont.Normal))
        txtFormat.setSize(7)
        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.faLayer.setLabelsEnabled(True)
        self.faLayer.setLabeling(labeling)

    # ------------------------------ LEGENDE ---------------------------------

    def _legend(self):
        lgd = QgsDataDefinedSizeLegend()
        lgd.setLegendType(QgsDataDefinedSizeLegend.LegendCollapsed)
        lgd.setTitle("Légende")
        lgds = QgsMarkerSymbol()
        lgds.symbolLayer(0).setColor(QColor("#d5d5d5"))
        lgds.symbolLayer(0).setStrokeColor(QColor("#ffffff"))
        lgd.setClasses(
            [
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue), str(int(self.maxValue))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 2), str(int(self.maxValue / 2))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 10), str(int(self.maxValue / 10))
                ),
            ]
        )
        lgd.setSymbol(lgds)
        return lgd

    # ------------------------------------------------------------------------

    def initFluxArretLayer(self):
        """
        Initialisation de la couche pour les flux par arrêts
        """

        # Nom de la table des arrêts
        tabArret = "arrets"

        # S'il y a une couche active
        boolLayer = False
        if self.iface.activeLayer():
            # Si la source de la couche courante contient
            # le nom de la couche des arrêts
            if tabArret in self.iface.activeLayer().source():
                arretLayer = self.iface.activeLayer()
                self.arretLayer = arretLayer
                boolLayer = True
        # Sinon, on recherche la couche dans les couches chargées
        if not boolLayer:
            root = QgsProject.instance().layerTreeRoot()
            for child in root.findLayers():
                if isinstance(child, QgsLayerTreeLayer):
                    if tabArret in child.layer().source():
                        arretLayer = child.layer()
                        boolLayer = True
                        break
            # Si la couche des arrêts est chargée, on l'active
            if boolLayer:
                self.iface.setActiveLayer(arretLayer)
                self.arretLayer = arretLayer
            # Sinon, on charge la couche des arrêts et on l'active
            else:
                uriArret = QgsDataSourceUri()
                uriArret.setDatabase(self.qmapodConfig.db)
                uriArret.setDataSource("", tabArret, "geometry")
                arretLayer = QgsVectorLayer(uriArret.uri(), tabArret, "spatialite")
                if arretLayer.isValid():
                    QgsProject.instance().addMapLayer(arretLayer)
                self.arretLayer = arretLayer

    # -------------------------------------------------------------------------

    def processArret(self, feature):
        """
        Traitement de l'arret sélectionné
        TODO : gérer le style de la couche des arrêts sélectionnés
        TODO : assurer leur visibilité par rapport aux diagrammes
        """

        # Récupération des attributs de l'arrêt sélectionné
        colIdArret = "id_arret"

        # Ajout/Suppression de l'arrêt sélectionné
        # à la liste d'arrêts de l'outil
        modifiers = QApplication.keyboardModifiers()
        if modifiers == Qt.ShiftModifier or modifiers == Qt.ControlModifier:
            if feature.attribute(colIdArret) in self.lstArrets:
                self.lstArrets.remove(feature.attribute(colIdArret))
            else:
                self.lstArrets.append(feature.attribute(colIdArret))
        else:
            self.lstArrets[:] = []
            self.lstArrets.append(feature.attribute(colIdArret))

        # Détermination du nom de la couche des arrêts sélectionnés
        if self.typAnal == "m":
            strNom = "Arrêt(s) de montées"
        elif self.typAnal == "d":
            strNom = "Arrêt(s) de descentes"

        # Ajout de la visualisation des flux si elle n'existe pas
        if self.faLayer is None:
            self.addFluxArrets(self.nbLignes)
        # Mise à jour de la visualisation existante dans le cas contraire
        else:
            self.majFluxArrets(self.nbLignes)

        # Si le groupe n'existe pas, c'est qu'il n'y a pas
        # de montées/descentes sur l'arrêt sélectionné
        if self.faGroup is None:
            return

        # Ajout de la couche des arrêts sélectionnés si elle n'existe pas
        if self.selLayer is None:
            uri = QgsDataSourceUri()
            uri.setDatabase(self.qmapodConfig.db)
            uri.setDataSource("", "arrets", "geometry")
            self.selLayer = QgsVectorLayer(uri.uri(), strNom, "spatialite")
            self.selLayer.setSubsetString(
                Template("id_arret IN($liste_arrets)").substitute(
                    liste_arrets=", ".join(
                        map(
                            lambda x: str(x) if str(x).isdigit() else repr(str(x)),
                            self.lstArrets,
                        )
                    ),
                )
            )
            if self.selLayer.isValid():
                # Ajout des couches et déplacement au-dessus
                # des couches existantes (api QgsLayerTree)
                self.selLayerId = QgsProject.instance().addMapLayer(
                    self.selLayer, False
                )
                self.selLayer.destroyed.connect(self._deleted)
                selTreeLayer = QgsLayerTreeLayer(self.selLayer)
                selTreeLayer.setName(self.selLayer.name())
                # TODO : self.faGroup.insertChildNode(0, selTreeLayer)
                # AttributeError: 'NoneType' object has no attribute
                # 'insertChildNode' lorsqu'on clique sur un arrêt
                # dans la couche arrets et pas dans la couche du sous-réseau
                self.faGroup.insertChildNode(0, selTreeLayer)

        # Mise à jour du sous-ensemble de la couche dans le cas contraire
        else:
            self.selLayer.setSubsetString(
                Template("id_arret IN($liste_arrets)").substitute(
                    liste_arrets=", ".join(
                        map(
                            lambda x: str(x) if str(x).isdigit() else repr(str(x)),
                            self.lstArrets,
                        )
                    ),
                )
            )

        # Paramétrage du style de la couche des arrêts sélectionnés
        if self.typAnal == "m":
            selCol = self.qmapodSettings.value(
                "params/btnColFluxArretM", QColor(255, 0, 0, 255), QColor
            )
            selAngle = 0
        elif self.typAnal == "d":
            selCol = self.qmapodSettings.value(
                "params/btnColFluxArretD", QColor(255, 0, 0, 255), QColor
            )
            selAngle = 180
        symbol = QgsMarkerSymbol.createSimple(
            {"color": "100, 100, 100", "name": "triangle", "size": "1.6"}
        )
        symbol.setSize(2.4)
        symbol.setColor(selCol)
        symbol.setAngle(selAngle)

        self.selLayer.setRenderer(QgsSingleSymbolRenderer(symbol))

        # Rafraichissement de la carte
        self.selLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())

    # -------------------------------------------------------------------------

    def delFluxArrets(self):
        """
        Suppression de l'affichage des flux par arrêts de montées / descentes
        """
        # Groupe de couche des flux par arrêts
        if isinstance(self.faGroup, QgsLayerTreeGroup):
            root = QgsProject.instance().layerTreeRoot()
            root.removeChildNode(self.faGroup)
        self.faGroup = None

        # Couche des arrêts sélectionnés
        self.selLayer = None

        # Couches des flux par arrêts
        self.faLayer = None

    # -------------------------------------------------------------------------

    def addFluxArrets(self, nbLignes):
        """
        Affichage des flux par arrêts de montées/descentes
        Version symboles proportionnels (DataDefinedSize)
        """
        if len(self.lstArrets) == 0:
            self.delFluxArrets()
            return
        nb = self._createTableFluxArretsTmp(nbLignes)
        if nb <= 0:
            self.delFluxArrets()
            return

        self._addLayerFluxArretsTmp()

        # Transparence / Opacité
        intTransparency = float(
            self.qmapodSettings.value("params/spxTransparencySymbol", 30)
        )
        fltOpacity = (100 - intTransparency) / 100

        # Couleurs du fond des symboles
        colM = self.qmapodSettings.value(
            "params/btnColFluxArretM", QColor(255, 0, 0), QColor
        )
        colD = self.qmapodSettings.value(
            "params/btnColFluxArretD", QColor(255, 0, 0), QColor
        )

        # Champs utilisé pour la proportionnalité
        if self.typAnal == "m":
            strField = "descentes"
            colSymbol = colD
        elif self.typAnal == "d":
            strField = "montees"
            colSymbol = colM

        # Couleur du contour des symboles
        penColor = self.qmapodSettings.value(
            "params/btnColOutlineSymbol", QColor(255, 255, 255, 255), QColor
        )

        # Épaisseur du contour des symboles
        penWidth = float(self.qmapodSettings.value("params/spxWidthOutlineSymbol", 0.4))

        # Valeur maxi
        self.maxValue = self._getMaxVal("flux_arrets_tmp", strField)

        # Taille correspondant à la valeur maximale
        self.maxSize = int(
            self.qmapodSettings.value("params/spxFluxArretsDiagMaxSize", 40)
        )

        # Définition de la symbologie
        symbol = QgsMarkerSymbol.createSimple({})
        symbolLayer = QgsSimpleMarkerSymbolLayer()
        symbolLayer.setShape(QgsSimpleMarkerSymbolLayer.Circle)
        symbolLayer.setSize(2.0)
        symbolLayer.setColor(colSymbol)
        symbolLayer.setStrokeColor(penColor)
        symbolLayer.setStrokeWidth(penWidth)

        symbol.setOpacity(fltOpacity)
        symbol.changeSymbolLayer(0, symbolLayer)

        prop = QgsProperty()
        prop.setField(strField)

        transf = QgsSizeScaleTransformer(
            QgsSizeScaleTransformer.Flannery,
            0.0,
            self.maxValue,
            1.0,
            self.maxSize,
            0.57,
        )
        prop.setTransformer(transf)
        symbol.setDataDefinedSize(prop)

        # Définition de la taille à partir de l'expression
        self.faLayer.renderer().setSymbol(symbol)

        # Affichage de la légende
        # TODO : ajuster la taille de la légende
        self.faLayer.renderer().setDataDefinedSizeLegend(self._legend())

        # Etiquetage de la couche
        self._labelFluxArret()

        # Actualisation de l'affichage
        self.faLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.faLayer.id())

    # -------------------------------------------------------------------------

    def majFluxArrets(self, nbLignes):
        """
        Mise à jour de l'affichage courant des flux par arrêts
        Version symboles proportionnels (DataDefinedSize)
        """

        # Mise à jour de la table flux_arrets_tmp
        if len(self.lstArrets) == 0:
            self.delFluxArrets()
            return
        nb = self._createTableFluxArretsTmp(nbLignes)
        if nb <= 0:
            self.delFluxArrets()
            return

        # Sélection des champs à prendre en compte
        # pour le calcul de la valeur maximale
        if self.typAnal == "m":
            strExp = "descentes"
        elif self.typAnal == "d":
            strExp = "montees"

        # Transparence / Opacité
        intTransparency = float(
            self.qmapodSettings.value("params/spxTransparencySymbol", 30)
        )
        fltOpacity = (100 - intTransparency) / 100

        # Couleurs du fond des symboles
        colM = self.qmapodSettings.value(
            "params/btnColFluxArretM", QColor(255, 0, 0), QColor
        )
        colD = self.qmapodSettings.value(
            "params/btnColFluxArretD", QColor(255, 0, 0), QColor
        )

        # Champs utilisé pour la proportionnalité
        if self.typAnal == "m":
            strField = "descentes"
            colSymbol = colD
            colSel = colM
            angSel = 0
        elif self.typAnal == "d":
            strField = "montees"
            colSymbol = colM
            colSel = colD
            angSel = 180

        # Couleur du contour des symboles
        penColor = self.qmapodSettings.value(
            "params/btnColOutlineSymbol", QColor(255, 255, 255, 255), QColor
        )

        # Épaisseur du contour des symboles
        penWidth = float(self.qmapodSettings.value("params/spxWidthOutlineSymbol", 0.4))

        # Valeur maxi
        self.maxValue = self._getMaxVal("flux_arrets_tmp", strField)

        # Taille correspondant à la valeur maximale
        self.maxSize = int(
            self.qmapodSettings.value("params/spxFluxArretsDiagMaxSize", 40)
        )

        # Définition de la symbologie
        symbol = QgsMarkerSymbol.createSimple({})
        symbolLayer = QgsSimpleMarkerSymbolLayer()
        symbolLayer.setShape(QgsSimpleMarkerSymbolLayer.Circle)
        symbolLayer.setSize(2.0)
        symbolLayer.setColor(colSymbol)
        symbolLayer.setStrokeColor(penColor)
        symbolLayer.setStrokeWidth(penWidth)

        symbol.setOpacity(fltOpacity)
        symbol.changeSymbolLayer(0, symbolLayer)

        prop = QgsProperty()
        prop.setField(strField)

        transf = QgsSizeScaleTransformer(
            QgsSizeScaleTransformer.Flannery,
            0.0,
            self.maxValue,
            1.0,
            self.maxSize,
            0.57,
        )
        prop.setTransformer(transf)
        symbol.setDataDefinedSize(prop)

        # Définition de la taille à partir de l'expression
        self.faLayer.renderer().setSymbol(symbol)

        # Affichage de la légende
        # TODO : ajuster la taille de la légende
        self.faLayer.renderer().setDataDefinedSizeLegend(self._legend())

        # Etiquetage de la couche
        self._labelFluxArret()

        # Mise à jour de la couleur du symbole pour les arrêts sélectionnés
        selSymbol = self.selLayer.renderer().symbol().clone()
        selSymbol.setColor(colSel)
        self.selLayer.renderer().setSymbol(selSymbol)

        # Refresh map
        self.faLayer.triggerRepaint()
        self.selLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.faLayer.id())
        self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())
