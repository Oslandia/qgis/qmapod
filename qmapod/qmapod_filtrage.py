"""
/***************************************************************************
 QMapODFiltrage
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from collections import OrderedDict
from string import Template

# Import des librairies PyQt
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QListWidget, QTreeWidgetItemIterator

module_logger = logging.getLogger("QMapOD.filtrage")


class QMapODFiltrage(object):
    """
    Classe regroupant les fonctions de filtrage et de calcul sur les enquêtes :
    Filtrage des enquêtes
    Calcul des charges par tronçon
    Filtrage sur la cartographie
    """

    def __init__(self, iface, dock, qmapodConfig):
        """
        Constructeur
        :iface : Référence à l'interface QGis
        :dock : Référence au panneau ancrable du plugin
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        self.iface = iface
        self.dock = dock
        self.strRcpSousReseau = ""
        self.strRcpTypeJour = ""
        self.strRcpVoyage = ""
        self.strRcpSignaletique = ""
        self.strRecapEnquetes = "Lignes sélectionnées\r\n{}"
        self.strRecapCarto = "Arrêts supprimés\r\n{}"
        self.strWhereSousReseau = ""
        self.nbLignes = 0
        self.dctWhereCarto = dict()

        self.logger = logging.getLogger("QMapOD.filtrage.QMapODFiltrage")
        self.logger.info("Creating an instance of QMapODFiltrage")

        # Vérification de l'existence de la table courses
        self.blnOffreCourses = True
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table courses
            s = "\nSELECT count(*) \n\
                 FROM sqlite_master \n\
                 WHERE type = 'table' \n\
                   AND name = 'courses';"
            self.logger.debug(s)
            cursor.execute(s)
            if cursor.fetchone()[0] == 0:
                db.close()
                self.blnOffreCourses = False

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

    def _getDictSousReseau(self, typReq):
        """
        Récupération de la liste des types, lignes, sens sélectionnés
        (sous-réseau)
        """

        # Détermination des clés selon le type de requête
        # TODO : À récupérer depuis le fichier de configuration ?
        if typReq == "sous-reseau":
            col_typlign = "cl.typ_ligne"
            col_ligne = "p.id_ligne"
            col_sens = "p.id_sens"
        elif typReq == "enquetes_tmp":
            col_typlign = self.qmapodConfig.dctParam["typlign"]
            col_ligne = self.qmapodConfig.dctParam["ligne"]
            col_sens = self.qmapodConfig.dctParam["sens"]
        elif typReq == "offre":
            col_typlign = "o.typ_ligne"
            col_ligne = "o.id_ligne"
            col_sens = "o.id_sens"
        elif typReq == "offrecourses":
            col_typlign = "c.id_type_ligne"
            col_ligne = "c.id_ligne"
            col_sens = "c.id_sens"
        else:
            return

        # Parcours des éléments cochés
        # (mais ceux qui sont PartiallyChecked sont inclus)
        iterator = QTreeWidgetItemIterator(
            self.dock.twgSousReseau, QTreeWidgetItemIterator.Checked
        )
        item = iterator.value()
        odtSousReseau = OrderedDict()
        odtSousReseau[col_typlign] = list()
        odtSousReseau[col_ligne] = list()
        odtSousReseau[col_sens] = list()
        blnAll = False
        blnType = False
        blnLigne = False
        self.nbLignes = 0
        strRcpSousReseau = "Sous-réseau :\r\n"
        strTypLign = "Types de ligne sélectionnés :\r\n"
        strLigne = "Lignes sélectionnées :\r\n"
        strSens = "Ligne/sens sélectionnés :\r\n"
        while item:

            # Si le réseau est coché on ne fait rien
            # Le data role Qt.UserRole (32) contient le niveau hiérarchique
            # (voir remplissage de l'arbre)
            # Le data role Qt.UserRole + 1 (33) contient le numéro de ligne
            # (voir remplissage de l'arbre)
            if item.data(0, Qt.UserRole) == 0:
                blnAll = False
                # Pour exclure les items PartiallyChecked
                if item.checkState(0) == Qt.Checked:
                    self.nbLignes = self.qmapodConfig.dctParam["nblignes"]
                    blnAll = True
                    strRcpSousReseau = "Sous-réseau : Tout le réseau\r\n\r\n"
                    break

            # Si un type de ligne est coché on l'ajoute à la liste des types
            elif item.data(0, Qt.UserRole) == 1:
                blnType = False
                # Pour exclure les items PartiallyChecked
                if item.checkState(0) == Qt.Checked:
                    odtSousReseau[col_typlign].append(item.data(0, Qt.UserRole + 1))
                    blnType = True
                    strTypLign += Template("- $i\r\n").substitute(i=item.text(0))

            # Si une ligne est cochée on l'ajoute à la liste des lignes
            elif item.data(0, Qt.UserRole) == 2:
                blnLigne = False
                # Pour exclure les items PartiallyChecked
                if item.checkState(0) == Qt.Checked:
                    # On n'ajoute les lignes que si leur type
                    # n'a pas déjà été ajouté
                    self.nbLignes += 1
                    if not blnType:
                        odtSousReseau[col_ligne].append(item.data(0, Qt.UserRole + 1))
                        blnLigne = True
                        strLigne += Template("- $i\r\n").substitute(i=item.text(0))

            # Si un sens est coché on l'ajoute à la liste des ligne/sens
            elif item.data(0, Qt.UserRole) == 3:
                # Pour exclure les items PartiallyChecked
                if item.checkState(0) == Qt.Checked:
                    # On n'ajoute les sens que si leur type et leur ligne
                    # n'ont pas déjà été ajoutés
                    if not blnType and not blnLigne:
                        self.nbLignes += 1
                        odtSousReseau[col_sens].append(item.data(0, Qt.UserRole + 1))
                        strSens += Template("- $i\r\n").substitute(i=item.text(0))

            # TODO : Gestion des courses

            iterator += 1
            item = iterator.value()

        # Récapitulatif sous-réseau
        if not blnAll:
            if strTypLign != "Types de ligne sélectionnés :\r\n":
                strRcpSousReseau += strTypLign + "\r\n"
            if strLigne != "Lignes sélectionnées :\r\n":
                strRcpSousReseau += strLigne + "\r\n"
            if strSens != "Ligne/sens sélectionnés :\r\n":
                strRcpSousReseau += strSens + "\r\n"

        self.strRcpSousReseau = strRcpSousReseau

        return odtSousReseau

    def _getDictSignaletique(self):
        """
        Récupération de la liste des critères signalétiques sélectionnés
        """

        # Construction de la clause where pour les critères signalétique
        odtSignaletique = OrderedDict()
        self.strRcpSignaletique = "Critères signalétique :\r\n"
        for lwgName, dctParam in self.qmapodConfig.odtCritereSignaletique.items():
            lwgObj = self.dock.pageSignaletique.findChild(QListWidget, lwgName)
            odtSignaletique[dctParam["enqfield"]] = list()
            strCritere = dctParam["label"] + " :\r\n"
            for i in range(lwgObj.count()):
                item = lwgObj.item(i)
                if item.checkState() == Qt.Checked:
                    odtSignaletique[dctParam["enqfield"]].append(item.data(Qt.UserRole))
                    strCritere += Template("- $item_\r\n").substitute(item_=item.text())
            # Si toutes les valeurs sont cochées on retire la liste :
            # pas de clause where
            if len(odtSignaletique[dctParam["enqfield"]]) == lwgObj.count():
                odtSignaletique.pop(dctParam["enqfield"], None)
                self.strRcpSignaletique += dctParam["label"] + " : Tous\r\n"
            else:
                self.strRcpSignaletique += strCritere

        return odtSignaletique

    def _getDictVoyage(self):
        """
        Récupération de la liste des critères voyage sélectionnés
        """

        # Construction de la clause where pour les critères voyage
        odtVoyage = OrderedDict()
        self.strRcpVoyage = "Critères voyage :\r\n"
        for lwgName, dctParam in self.qmapodConfig.odtCritereVoyage.items():
            lwgObj = self.dock.pageVoyage.findChild(QListWidget, lwgName)
            odtVoyage[dctParam["enqfield"]] = list()
            strCritere = dctParam["label"] + " :\r\n"
            for i in range(lwgObj.count()):
                item = lwgObj.item(i)
                if item.checkState() == Qt.Checked:
                    odtVoyage[dctParam["enqfield"]].append(item.data(Qt.UserRole))
                    strCritere += Template("- $item_\r\n").substitute(item_=item.text())
            # Si toutes les valeurs sont cochées on retire la liste :
            # pas de clause where
            if len(odtVoyage[dctParam["enqfield"]]) == lwgObj.count():
                odtVoyage.pop(dctParam["enqfield"], None)
                self.strRcpVoyage += dctParam["label"] + " : Tous\r\n"

                # Activation de l'affichage des serpents d'offre/de performance
                # si toutes les tranches horaires sont cochées
                if lwgName == "lwgTrhor":
                    self.dock.tbnSOffre.setEnabled(True)
                    self.dock.tbnSPerf.setEnabled(True)
            else:
                self.strRcpVoyage += strCritere

                # Désactivation de l'affichage des serpents
                # d'offre/de performance si toutes les tranches horaires
                # ne sont pas cochées
                if (lwgName == "lwgTrhor") and not self.blnOffreCourses:
                    self.dock.tbnSOffre.setEnabled(False)
                    self.dock.tbnSPerf.setEnabled(False)

        return odtVoyage

    def _genSqlSousReseau(self):
        """
        Construction de la clause where du sous-réseau
        """

        odtSousReseau = self._getDictSousReseau("sous-reseau")
        odtWhereSousReseau = OrderedDict()
        for strCritere, listValues in odtSousReseau.items():
            if len(listValues) > 0:
                if strCritere == "p.id_sens":
                    # TODO : à faire de façon plus pythonique (format ?) !
                    strW = ""
                    for strLigneSens in listValues:
                        strW += Template(
                            "(p.id_ligne=$ligne_sens_0 and p.id_sens=$ligne_sens_1) or "
                        ).substitute(
                            ligne_sens_0=strLigneSens.split("|")[0],
                            ligne_sens_1=strLigneSens.split("|")[1],
                        )
                    odtWhereSousReseau[strCritere] = strW[:-4]
                else:
                    odtWhereSousReseau[strCritere] = (
                        strCritere + " in(" + ",".join(str(x) for x in listValues) + ")"
                    )

        # Renvoi de la clause where pour le sous-réseau
        strWhere = ""
        if len(odtWhereSousReseau) > 0:
            strWhere += "("
            strWhere += " or ".join(value for key, value in odtWhereSousReseau.items())
            strWhere += ")"
        else:
            strWhere = "1"

        return strWhere

    def _genSqlEnquetesTmp(self):
        """
        Génération du code Sql de création de la table enquetes_tmp
        """

        # Construction de la clause where pour le sous-réseau
        odtSousReseau = self._getDictSousReseau("enquetes_tmp")
        odtWhereSousReseau = OrderedDict()
        for strCritere, listValues in odtSousReseau.items():
            if len(listValues) > 0:
                if strCritere == "sens":
                    strW = ""
                    for strLigneSens in listValues:
                        strW += Template(
                            "(ligne=$str_ligne_sens_0 and sens=$str_ligne_sens_1) or "
                        ).substitute(
                            str_ligne_sens_0=strLigneSens.split("|")[0],
                            str_ligne_sens_1=strLigneSens.split("|")[1],
                        )
                    odtWhereSousReseau[strCritere] = strW[:-4]
                else:
                    odtWhereSousReseau[strCritere] = (
                        strCritere + " in(" + ",".join(str(x) for x in listValues) + ")"
                    )

        # Construction de la clause where pour les critères signalétiques
        odtSignaletique = self._getDictSignaletique()
        odtWhereCritere = OrderedDict()
        for strCritere, listValues in odtSignaletique.items():
            if len(listValues) > 0:
                odtWhereCritere[strCritere] = (
                    strCritere + " in(" + ",".join(str(x) for x in listValues) + ")"
                )

        # Construction de la clause where pour les voyages
        odtVoyage = self._getDictVoyage()
        for strCritere, listValues in odtVoyage.items():
            if len(listValues) > 0:
                odtWhereCritere[strCritere] = (
                    strCritere + " in(" + ",".join(str(x) for x in listValues) + ")"
                )

        # Construction de la clause where pour les autres critères
        cbxTypeJour = self.dock.cbxTypeJour
        strTypeJour = self.qmapodConfig.odtTypeJour["cbxTypeJour"]["enqfield"]
        intTypeJour = cbxTypeJour.itemData(cbxTypeJour.currentIndex())
        strWhereTypeJour = Template("$str_type_jour = $int_type_jour").substitute(
            str_type_jour=strTypeJour, int_type_jour=intTypeJour
        )
        self.strRcpTypeJour = Template("Type de jour : $type_jour\r\n").substitute(
            type_jour=cbxTypeJour.itemText(cbxTypeJour.currentIndex())
        )

        # Assemblage des clauses where
        strWhere = strWhereTypeJour
        if len(odtWhereSousReseau) > 0:
            strWhere += " and "
            strWhere += "("
            strWhere += " or ".join(value for _, value in odtWhereSousReseau.items())
            strWhere += ")"
        if len(odtWhereCritere) > 0:
            strWhere += " and "
            strWhere += " and ".join(value for _, value in odtWhereCritere.items())

        # Assemblage de la requête sql
        # On commence par ajouter la sous-requête permettant d'ajouter
        # le type de ligne à partir de la table code_ligne
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_12.sql")
        ) as f:
            strSqlEnquetesTmp = f.read()
        if len(strWhere) > 0:
            strSqlEnquetesTmp += "WHERE " + strWhere
        strSqlEnquetesTmp += ";"
        return strSqlEnquetesTmp

    def _genSqlOffreTmp(self):
        """
        Génération du code Sql de création de la table offre_tmp.

        Construction de la requête pour la création de la table temporaire
        de l'offre offre_tmp.
        Cette table reprend les parcours de l'ensemble du réseau
        avec l'offre correspondante pour le sous-réseau sélectionné.
        """

        # Table de l'offre
        strSelectPo = "offre"

        # Construction de la clause where pour le sous-réseau
        odtSousReseau = self._getDictSousReseau("offre")
        odtWhereSousReseau = OrderedDict()
        for strCritere, listValues in odtSousReseau.items():
            if len(listValues) > 0:
                if strCritere == "o.id_sens":
                    # TODO : à faire de façon plus pythonique (format ?) !
                    strW = ""
                    for strLigneSens in listValues:
                        strW += Template(
                            "(o.id_ligne=$ligne_sens_0 and o.id_sens=$ligne_sens_1) or "
                        ).substitute(
                            ligne_sens_0=strLigneSens.split("|")[0],
                            ligne_sens_1=strLigneSens.split("|")[1],
                        )
                    odtWhereSousReseau[strCritere] = strW[:-4]
                else:
                    odtWhereSousReseau[strCritere] = (
                        strCritere + " in(" + ",".join(str(x) for x in listValues) + ")"
                    )

        # Construction de la clause where pour les autres critères
        strTypeJour = "typ_jour"
        intTypeJour = self.dock.cbxTypeJour.itemData(
            self.dock.cbxTypeJour.currentIndex()
        )
        strWhereTypeJour = Template("$str_type_jour = $int_type_jour").substitute(
            str_type_jour=strTypeJour, int_type_jour=intTypeJour
        )

        # Assemblage des clauses where
        strWhere = strWhereTypeJour
        if len(odtWhereSousReseau) > 0:
            strWhere += " and "
            strWhere += "("
            strWhere += " or ".join(value for key, value in odtWhereSousReseau.items())
            strWhere += ")"

        # Agrégation de la requête
        strSelectOffreTmp = Template(
            "\n\
            SELECT *\n\
            FROM $str_select_po o WHERE $str_where;\
        "
        ).substitute(str_select_po=strSelectPo, str_where=strWhere)

        self.logger.debug(strSelectOffreTmp)
        return strSelectOffreTmp

    def _genSqlOffreCoursesTmp(self):
        """
        Génération du code Sql de création de la table offre_tmp à partir
        de la table des courses

        Construction de la requête pour la création de la table temporaire
        des charges offre_tmp
        Cette table reprend les parcours de l'ensemble du réseau
        avec l'offre correspondante pour le sous-réseau sélectionné
        """

        # Construction de la clause where pour le sous-réseau
        odtSousReseau = self._getDictSousReseau("offrecourses")
        odtWhereSousReseau = OrderedDict()
        for strCritere, listValues in odtSousReseau.items():
            if len(listValues) > 0:
                if strCritere == "c.id_sens":
                    # TODO : à faire de façon plus pythonique (format ?) !
                    strW = ""
                    for strLigneSens in listValues:
                        strW += Template(
                            "(c.id_ligne=$str_ligne_sens_0 and c.id_sens=$str_ligne_sens_1) or "
                        ).substitute(
                            str_ligne_sens_0=strLigneSens.split("|")[0],
                            str_ligne_sens_1=strLigneSens.split("|")[1],
                        )
                    odtWhereSousReseau[strCritere] = strW[:-4]
                else:
                    odtWhereSousReseau[strCritere] = (
                        strCritere + " in(" + ",".join(str(x) for x in listValues) + ")"
                    )

        # Construction de la clause where pour le type de jour
        strTypeJour = "id_type_jour"
        intTypeJour = self.dock.cbxTypeJour.itemData(
            self.dock.cbxTypeJour.currentIndex()
        )
        strWhereTypeJour = Template("$str_type_jour = $int_type_jour").substitute(
            str_type_jour=strTypeJour, int_type_jour=intTypeJour
        )

        # Construction de la clause where pour les tranches horaires
        lwgObj = self.dock.pageVoyage.findChild(QListWidget, "lwgTrhor")
        odtTrhor = list()
        for i in range(lwgObj.count()):
            item = lwgObj.item(i)
            if item.checkState() == Qt.Checked:
                odtTrhor.append(item.data(Qt.UserRole))

        # Si toutes les valeurs sont cochées on retire la liste :
        # pas de clause where
        strTrhor = "id_trhor"
        if len(odtTrhor) == lwgObj.count():
            strWhereTrhor = ""
        else:
            strWhereTrhor = Template("$str_tr_hor ").substitute(str_tr_hor=strTrhor)
            """
            + " in ("
            + ",".join(str(x) for x in odtTrhor)
            + ")"
            """
            strWhereTrhor = "{} in ({})".format(
                Template("$str_tr_hor ").substitute(str_tr_hor=strTrhor),
                ",".join(str(x) for x in odtTrhor),
            )

        # Assemblage des clauses where
        if len(strWhereTrhor) > 0:
            strWhere = Template("$str_where_type_jour and $str_where_trhor").substitute(
                str_where_type_jour=strWhereTypeJour, str_where_trhor=strWhereTrhor
            )
        else:
            strWhere = strWhereTypeJour
        if len(odtWhereSousReseau) > 0:
            strWhere += " and "
            strWhere += "("
            strWhere += " or ".join(value for key, value in odtWhereSousReseau.items())
            strWhere += ")"

        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_1.sql")
        ) as f:
            strSelectOffreCoursesTmp = Template(f.read()).substitute(
                tab_trhor=self.qmapodConfig.dctParam["tabtrhor"], str_where=strWhere
            )
        return strSelectOffreCoursesTmp

    def _genSqlChargeTmp(self):
        """
        Génération du code Sql de création de la table charge_tmp

        Construction de la requête pour la création de la table temporaire
        des charges charges_tmp
        Cette table reprend les parcours de l'ensemble du réseau
        avec les charges correspondantes pour le sous-réseau sélectionné
        """
        # Choix du poids à prendre en compte
        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if self.nbLignes == 1:
            poids = self.qmapodConfig.dctParam["poidsligne"]
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        elif self.nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            poids = self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            poids = self.qmapodConfig.dctParam["poidsligne"]

        # Calcul des montées
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_2.sql")
        ) as f:
            strSelectMontees = Template(f.read()).substitute(
                ligne=self.qmapodConfig.dctParam["ligne"],
                sens=self.qmapodConfig.dctParam["sens"],
                parcours=self.qmapodConfig.dctParam["parcours"],
                arretdebvoy=self.qmapodConfig.dctParam["arretdebvoy"],
                poids_=poids,
            )

        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_3.sql")
        ) as f:
            strSelectDescentes = Template(f.read()).substitute(
                ligne=self.qmapodConfig.dctParam["ligne"],
                sens=self.qmapodConfig.dctParam["sens"],
                parcours=self.qmapodConfig.dctParam["parcours"],
                arretfinvoy=self.qmapodConfig.dctParam["arretfinvoy"],
                poids_=poids,
            )

        # Extraction des parcours et jointure avec les montées / descentes
        # pour le calcul de la colonne charge
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_4.sql")
        ) as f:
            strSelectPmd1 = Template(f.read()).substitute(
                str_select_montees=strSelectMontees,
                str_select_descentes=strSelectDescentes,
            )

        # Calcul de la colonne charge
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_5.sql")
        ) as f:
            strSelectColonneCharge = Template(f.read()).substitute(
                str_select_pmd1=strSelectPmd1
            )

        # Extraction des parcours et jointure avec les montées / descentes
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_6.sql")
        ) as f:
            strSelectPmd = Template(f.read()).substitute(
                str_select_montees=strSelectMontees,
                str_select_descentes=strSelectDescentes,
            )

        # Agrégation de la requête
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/filtrage_7.sql")
        ) as f:
            strSelectChargeTmp = Template(f.read()).substitute(
                str_select_colonne_charge=strSelectColonneCharge,
                str_select_pmd=strSelectPmd,
            )

        print(strSelectChargeTmp)

        return strSelectChargeTmp

    def majRecap(self, qmapodSettings):
        """
        Mise à jour du récapitulatif de filtrage
        dans le textEdit tedRecap
        """
        # Récupération des paramètres d'analyse
        strModeEnq = {1: "voyage", 2: "déplacement"}
        strArret = strModeEnq[
            int(qmapodSettings.value("params/buttonGroupArretMode", 1))
        ]
        strZone = strModeEnq[int(qmapodSettings.value("params/buttonGroupZoneMode", 1))]
        strFluxZone = strModeEnq[
            int(qmapodSettings.value("params/buttonGroupFluxZoneMode", 1))
        ]
        strFluxArret = strModeEnq[
            int(qmapodSettings.value("params/buttonGroupFluxArretMode", 1))
        ]

        self.dock.tedRecap.setText(
            "--------------------------------------------\r\n"
            + "Mode d'analyse des enquêtes :\r\n"
            + "--------------------------------------------\r\n"
            + "Arrêts : "
            + strArret
            + "\r\n"
            + "Zones : "
            + strZone
            + "\r\n"
            + "Flux par zones : "
            + strFluxZone
            + "\r\n"
            + "Flux par arrêts : "
            + strFluxArret
            + "\r\n"
            + "\r\n"
            + "--------------------------------------------\r\n"
            + "Critères de filtrage sur les enquêtes :\r\n"
            + "--------------------------------------------\r\n"
            + self.strRecapEnquetes
            + "\r\n"
            + "--------------------------------------------\r\n"
            + "Critères de filtrage cartographique  :\r\n"
            + "--------------------------------------------\r\n"
            + self.strRecapCarto
        )

    def sqlSousReseau(self):
        """
        Récupération de la clause where du sous-réseau
        (pour l'affichage du sous-réseau)
        """
        return self.strWhereSousReseau

    def getSousReseau(self):
        """
        Récupération du sous-réseau sélectionné
        Cette fonction permet de récupérer le nombre de lignes du sous-réseau
        de façon à pouvoir spécifier les modes d'analyse (voyage/déplacement)
        avant d'appliquer le filtrage
        """

        # Construction de la clause where pour l'affichage du sous-réseau
        self.strWhereSousReseau = self._genSqlSousReseau()

    def filtreEnquetes(self, qmapodSettings):
        """
        Validation du filtrage des enquêtes
        """
        # Construction de la requête de création de la table enquetes_tmp
        strSqlEnquetesTmp = self._genSqlEnquetesTmp()

        # Création de la table enquetes_tmp
        try:
            # Creates or opens a file called mydb with a SQLite3 DB
            db = sqlite3.connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()
            # Check if table users does not exist and create it
            s = "\nDROP TABLE IF EXISTS enquetes_tmp;"
            self.logger.debug(s)
            cursor.execute(s)
            s = "\nCREATE TABLE enquetes_tmp AS " + strSqlEnquetesTmp
            self.logger.debug(s)
            cursor.execute(s)
            # Commit the change
            db.commit()
        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

        # Construction de la requête de création de la table charge_tmp
        strSqlChargeTmp = self._genSqlChargeTmp()

        # Création de la table charge_tmp
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Création du curseur
            cursor = db.cursor()
            # Suppression de la table charge_tmp si elle existe
            s = "\nDROP TABLE IF EXISTS charge_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            # Création de la table charge_tmp
            s = "\nCREATE TABLE charge_tmp AS " + strSqlChargeTmp
            self.logger.debug(s)
            cursor.execute(s)
            # Commit the change
            db.commit()
        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            raise e
        finally:
            # Close the db connection
            db.close()

        # Construction de la requête de création de la table offre_tmp
        if self.blnOffreCourses:
            # De façon dynamique à partir de la table courses
            strSqlOffreTmp = self._genSqlOffreCoursesTmp()
        else:
            # De façon statique à partir de la table offre
            strSqlOffreTmp = self._genSqlOffreTmp()

        # Création de la table offre_tmp
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Création du curseur
            cursor = db.cursor()
            # Suppression de la table charge_tmp si elle existe
            s = "\nDROP TABLE IF EXISTS offre_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            # Création de la table charge_tmp
            cursor.execute("\nCREATE TABLE offre_tmp AS " + strSqlOffreTmp)
            # Commit the change
            db.commit()
        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            raise e
        finally:
            # Close the db connection
            db.close()

        # Construction du récapitulatif
        self.strRecapEnquetes = (
            self.strRcpTypeJour
            + "\r\n"
            + self.strRcpSousReseau
            + self.strRcpSignaletique
            + "\r\n"
            + self.strRcpVoyage
        )

        # Mise à jour du récapitulatif
        self.majRecap(qmapodSettings)

    def razFiltreEnquetes(self):
        """
        Réinitialisation des critères de filtrage des enquêtes
        """

        # Réinitialisation des critères sous-réseau (tout décoché)
        iterator = QTreeWidgetItemIterator(
            self.dock.twgSousReseau, QTreeWidgetItemIterator.Checked
        )
        item = iterator.value()
        while item:
            item.setCheckState(0, Qt.Unchecked)
            # TODO : Gestion des courses
            iterator += 1
            item = iterator.value()

        # Réinitialisation des critères signalétique (tout coché)
        for lwgName, _ in self.qmapodConfig.odtCritereSignaletique.items():
            lwgObj = self.dock.pageSignaletique.findChild(QListWidget, lwgName)
            for i in range(lwgObj.count()):
                item = lwgObj.item(i)
                item.setCheckState(Qt.Checked)

        # Réinitialisation des critères voyage (tout coché)
        for lwgName, _ in self.qmapodConfig.odtCritereVoyage.items():
            lwgObj = self.dock.pageVoyage.findChild(QListWidget, lwgName)
            for i in range(lwgObj.count()):
                item = lwgObj.item(i)
                item.setCheckState(Qt.Checked)

        # Réinitialisation des autres critères
        self.dock.cbxTypeJour.setCurrentIndex(0)  # Jour de semaine

    def _getExcludedArrets(self):
        """
        Construction de la clause where d'exclusion des arrêts
        TODO : à implémenter pour modulariser la fonction filtreCarto ?
        """

        # Construction de la clause where pour les critères d'arrêt
        # On construit une liste d'inclusion et une liste d'exclusion
        # pour ne traiter que la plus courte
        strArret = "id_arret"
        odtArretIncl = OrderedDict()
        odtArretExcl = OrderedDict()
        odtArretIncl[strArret] = list()
        odtArretExcl[strArret] = list()
        strIncl = ""
        strExcl = ""
        for i in range(self.dock.lwgArret.count()):
            item = self.dock.lwgArret.item(i)
            # Liste d'inclusion
            if item.checkState() == Qt.Checked:
                odtArretIncl[strArret].append(item.data(Qt.UserRole))
                strIncl += Template("- $user_role $item_\r\n").substitute(
                    user_role=item.data(Qt.UserRole), item_=item.text()
                )
            # Liste d'exclusion
            elif item.checkState() == Qt.Unchecked:
                odtArretExcl[strArret].append(item.data(Qt.UserRole))
                strExcl += Template("- $user_role $item_\r\n").substitute(
                    user_role=item.data(Qt.UserRole), item_=item.text()
                )

        self.logger.debug("Critères arrêts : ")
        odtWhereArret = OrderedDict()
        self.strRcpArret = "Critères cartographiques arrêts :\r\n"
        # On construit une clause where IN ou NOT IN à partir
        # de la plus courte des listes
        if len(odtArretIncl[strArret]) <= len(odtArretExcl[strArret]):
            listValues = odtArretIncl[strArret]
            if len(listValues) > 0:
                odtWhereArret[strArret] = (
                    strArret + " in(" + ",".join(str(x) for x in listValues) + ")"
                )
                self.strRcpArret += "Arrêts inclus :\r\n" + strIncl
            else:
                odtWhereArret[strArret] = ""
                self.strRcpArret += "Aucun arrêt inclu."
        else:
            listValues = odtArretExcl[strArret]
            if len(listValues) > 0:
                odtWhereArret[strArret] = (
                    strArret + " not in (" + ",".join(str(x) for x in listValues) + ")"
                )
                self.strRcpArret += "Arrêts exclus :\r\n" + strExcl
            else:
                odtWhereArret[strArret] = ""
                self.strRcpArret += "Aucun arrêt exclu."
        return odtWhereArret

    def _getExcludedZones(self, lwgName):
        """
        Construction de la clause where d'exclusion d'un zonage
        TODO : à implémenter pour modulariser la fonction filtreCarto ?
        """

        # Construction de la clause where pour les critères de zonage
        # Pour chaque zonage, on construit une liste d'inclusion
        # et une liste d'exclusion pour ne traiter que la plus courte
        odtZonages = OrderedDict()
        odtWhereZonage = OrderedDict()

        self.strRcpZonage = "\r\nCritères cartographiques zonages :\r\n"

        dctParam = self.qmapodConfig.odtZonage[lwgName]
        lwgObj = self.dock.pageZonage.findChild(QListWidget, lwgName)
        odtZonages[dctParam["table"]] = dict()
        odtZonages[dctParam["table"]][dctParam["enqfield"]] = dict()
        odtZonages[dctParam["table"]][dctParam["enqfield"]]["incl"] = list()
        odtZonages[dctParam["table"]][dctParam["enqfield"]]["excl"] = list()
        self.strRcpZonage += dctParam["label"] + " :\r\n"
        strIncl = ""
        strExcl = ""
        for i in range(lwgObj.count()):
            item = lwgObj.item(i)
            # Liste d'inclusion
            if item.checkState() == Qt.Checked:
                odtZonages[dctParam["table"]][dctParam["enqfield"]]["incl"].append(
                    item.data(Qt.UserRole)
                )
                strIncl += Template("- $item_data $item_text\r\n").substitute(
                    item_data=item.data(Qt.UserRole), item=item.text()
                )
            # Liste d'exclusion
            if item.checkState() == Qt.Unchecked:
                odtZonages[dctParam["table"]][dctParam["enqfield"]]["excl"].append(
                    item.data(Qt.UserRole)
                )
                strExcl += Template("- $user_role $item_\r\n").substitute(
                    user_role=item.data(Qt.UserRole), item_=item.text()
                )

        # On construit une clause where IN ou NOT IN à partir de la plus courte des listes
        intLenIncl = len(odtZonages[dctParam["table"]][dctParam["enqfield"]]["incl"])
        intLenExcl = len(odtZonages[dctParam["table"]][dctParam["enqfield"]]["excl"])
        if intLenIncl <= intLenExcl:
            lstZonageValues = odtZonages[dctParam["table"]][dctParam["enqfield"]][
                "incl"
            ]
            if len(lstZonageValues) > 0:
                odtWhereZonage[dctParam["table"]] = (
                    "id_zone in(" + ",".join(str(x) for x in lstZonageValues) + ")"
                )
                self.strRcpZonage += dctParam["label"] + " inclues :\r\n" + strIncl
            else:
                odtWhereZonage[dctParam["table"]] = ""
                self.strRcpZonage += "Aucune " + dctParam["label"] + " inclue.\r\n"
        else:
            lstZonageValues = odtZonages[dctParam["table"]][dctParam["enqfield"]][
                "excl"
            ]
            if len(lstZonageValues) > 0:
                odtWhereZonage[dctParam["table"]] = (
                    "id_zone not in(" + ",".join(str(x) for x in lstZonageValues) + ")"
                )
                self.strRcpZonage += dctParam["label"] + " exclues :\r\n" + strExcl
            else:
                odtWhereZonage[dctParam["table"]] = ""
                self.strRcpZonage += "Aucune " + dctParam["label"] + " exclue.\r\n"

        return odtWhereZonage

    def filtreCarto(self, qmapodSettings):
        """
        Validation du filtrage cartographique
        TODO : Filtrage croisé sur les zonages ?
        """

        # Construction de la clause where pour les critères d'arrêt
        # On construit une liste d'inclusion et une liste d"exclusion
        # pour ne traiter que la plus courte
        strArret = "id_arret"
        odtArretIncl = OrderedDict()
        odtArretExcl = OrderedDict()
        odtArretIncl[strArret] = list()
        odtArretExcl[strArret] = list()
        strIncl = ""
        strExcl = ""
        for i in range(self.dock.lwgArret.count()):
            item = self.dock.lwgArret.item(i)
            # Liste d'inclusion
            if item.checkState() == Qt.Checked:
                odtArretIncl[strArret].append(item.data(Qt.UserRole))
                strIncl += Template("- $user_role $item_\r\n").substitute(
                    user_role=item.data(Qt.UserRole), item_=item.text()
                )
            # Liste d'exclusion
            elif item.checkState() == Qt.Unchecked:
                odtArretExcl[strArret].append(item.data(Qt.UserRole))
                strExcl += Template("- $user_role $item_\r\n").substitute(
                    user_role=item.data(Qt.UserRole), item_=item.text()
                )

        odtWhereArret = OrderedDict()
        self.strRcpArret = "Critères cartographiques arrêts :\r\n"
        # On construit une clause where IN ou NOT IN à partir de la plus courte des listes
        if len(odtArretIncl[strArret]) <= len(odtArretExcl[strArret]):
            listValues = odtArretIncl[strArret]
            if len(listValues) > 0:
                odtWhereArret[strArret] = (
                    strArret + " in(" + ",".join(str(x) for x in listValues) + ")"
                )
                self.strRcpArret += "Arrêts inclus :\r\n" + strIncl
            else:
                odtWhereArret[strArret] = ""
                self.strRcpArret += "Aucun arrêt inclus."
        else:
            listValues = odtArretExcl[strArret]
            if len(listValues) > 0:
                odtWhereArret[strArret] = (
                    strArret + " not in(" + ",".join(str(x) for x in listValues) + ")"
                )
                self.strRcpArret += "Arrêts exclus :\r\n" + strExcl
            else:
                odtWhereArret[strArret] = ""
                self.strRcpArret += "Aucun arrêt exclu."

        # Construction de la clause where pour les critères de zonage
        # Pour chaque zonage, on construit une liste d'inclusion
        # et une liste d'exclusion pour ne traiter que la plus courte
        odtZonages = OrderedDict()
        odtWhereZonage = OrderedDict()
        self.strRcpZonage = "\r\nCritères cartographiques zonages :\r\n"
        strExclArret = ""
        for lwgName, dctParam in self.qmapodConfig.odtZonage.items():
            lwgObj = self.dock.pageZonage.findChild(QListWidget, lwgName)
            odtZonages[dctParam["table"]] = dict()
            odtZonages[dctParam["table"]][dctParam["enqfield"]] = dict()
            odtZonages[dctParam["table"]][dctParam["enqfield"]]["incl"] = list()
            odtZonages[dctParam["table"]][dctParam["enqfield"]]["excl"] = list()
            self.strRcpZonage += dctParam["label"] + " :\r\n"
            strIncl = ""
            strExcl = ""
            for i in range(lwgObj.count()):
                item = lwgObj.item(i)
                # Liste d'inclusion
                if item.checkState() == Qt.Checked:
                    idxTab = dctParam["table"]
                    idxEnq = dctParam["enqfield"]
                    odtZonages[idxTab][idxEnq]["incl"].append(item.data(Qt.UserRole))
                    strIncl += Template("- $user_role $item_\r\n").substitute(
                        user_role=item.data(Qt.UserRole), item_=item.text()
                    )
                # Liste d'exclusion
                if item.checkState() == Qt.Unchecked:
                    idxTab = dctParam["table"]
                    idxEnq = dctParam["enqfield"]
                    odtZonages[idxTab][idxEnq]["excl"].append(item.data(Qt.UserRole))
                    strExcl += Template("- $user_role $item_\r\n").substitute(
                        user_role=item.data(Qt.UserRole), item_=item.text()
                    )

            # On construit une clause where IN ou NOT IN à partir de la plus courte des listes
            intLenIncl = len(
                odtZonages[dctParam["table"]][dctParam["enqfield"]]["incl"]
            )
            intLenExcl = len(
                odtZonages[dctParam["table"]][dctParam["enqfield"]]["excl"]
            )
            if intLenIncl <= intLenExcl:
                lstZonageValues = odtZonages[dctParam["table"]][dctParam["enqfield"]][
                    "incl"
                ]
                if len(lstZonageValues) > 0:
                    odtWhereZonage[dctParam["table"]] = (
                        "id_zone in(" + ",".join(str(x) for x in lstZonageValues) + ")"
                    )
                    self.strRcpZonage += dctParam["label"] + " inclues :\r\n" + strIncl
                    strWhereZoneArret = odtWhereZonage[dctParam["table"]].replace(
                        "id_zone in", dctParam["enqfield"] + " not in"
                    )
                else:
                    odtWhereZonage[dctParam["table"]] = ""
                    self.strRcpZonage += "Aucune " + dctParam["label"] + " inclue.\r\n"
            else:
                lstZonageValues = odtZonages[dctParam["table"]][dctParam["enqfield"]][
                    "excl"
                ]
                if len(lstZonageValues) > 0:
                    odtWhereZonage[dctParam["table"]] = (
                        "id_zone not in("
                        + ",".join(str(x) for x in lstZonageValues)
                        + ")"
                    )
                    self.strRcpZonage += dctParam["label"] + " exclues :\r\n" + strExcl
                    strWhereZoneArret = odtWhereZonage[dctParam["table"]].replace(
                        "id_zone not in", dctParam["enqfield"] + " in"
                    )
                else:
                    odtWhereZonage[dctParam["table"]] = ""
                    self.strRcpZonage += "Aucune " + dctParam["label"] + " exclue.\r\n"

            # Construction de la clause d'exclusion d'arrêts correspondant
            # aux zones exclues
            if odtWhereZonage[dctParam["table"]] != "":
                strExclZone = Template(
                    "\n\
                    id_arret NOT IN (\n\
                        SELECT id_arret\n\
                        FROM arrets_zones\n\
                        WHERE $str_where_zone_arret\n\
                    )\n\
                "
                ).substitute(str_where_zone_arret=format(strWhereZoneArret))
                if strExclArret == "":
                    strExclArret = strExclZone
                else:
                    strExclArret = strExclArret + " and " + strExclZone

        # Ajout des clauses d'exclusion d'arrêts correspondant
        # aux zones exclues à la clause d'exclusion d'arrêts
        if strExclArret != "":
            if odtWhereArret[strArret] == "":
                odtWhereArret[strArret] = strExclArret
            else:
                odtWhereArret[strArret] = (
                    odtWhereArret[strArret] + " and " + strExclArret
                )

        # Assemblage des clauses where dans un dictionnaire
        dctWhereCarto = dict()
        dctWhereCarto[strArret] = ""
        if len(odtWhereArret) > 0:
            dctWhereCarto[strArret] = odtWhereArret[strArret]

        for strZonage, listValues in odtWhereZonage.items():
            dctWhereCarto[strZonage] = ""
            if len(odtWhereZonage) > 0:
                dctWhereCarto[strZonage] = odtWhereZonage[strZonage]

        # Mémorisation du filtrage cartographique
        self.dctWhereCarto = dctWhereCarto

        # Construction du récapitulatif
        self.strRecapCarto = self.strRcpArret + "\r\n" + self.strRcpZonage

        # Mise à jour du récapitulatif
        self.majRecap(qmapodSettings)

    def razFiltreCarto(self):
        """
        Réinitialisation des critères de filtrage carto
        """

        # Réinitialisation des critères d'arrêts (tout coché)
        for i in range(self.dock.lwgArret.count()):
            item = self.dock.lwgArret.item(i)
            item.setCheckState(Qt.Checked)

        # Réinitialisation des critères de zonages (tout coché)
        for lwgName, dctParam in self.qmapodConfig.odtZonage.items():
            lwgObj = self.dock.pageZonage.findChild(QListWidget, lwgName)
            for i in range(lwgObj.count()):
                item = lwgObj.item(i)
                item.setCheckState(Qt.Checked)
