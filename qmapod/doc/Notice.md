![QMapOD](img/qmapod2.png)
# **Consultation cartographique d'enquêtes origine destination sous QGIS 3.x**

----------

## ***Notice d'utilisation et de maintenance***

----------

### Table des matières

<!-- Start Document Outline -->

* [1. Généralités :](#1-généralités-)
	* [1.1. Objet de l’application :](#11-objet-de-lapplication--icon-home--icon-bars)
	* [1.2. Architecture de l’application :](#12-architecture-de-lapplication--icon-home--icon-bars)
	* [1.3. Installation de l’application :](#13-installation-de-lapplication--icon-home--icon-bars)
	* [1.4. Lancement de l’application :](#14-lancement-de-lapplication--icon-home--icon-bars)
	* [1.5. Présentation de l’application :](#15-présentation-de-lapplication--icon-home--icon-bars)
* [2. Description des fonctionnalités de l'application :](#2-description-des-fonctionnalités-de-lapplication-)
	* [2.1. Panneau à onglets supérieur :](#21-panneau-à-onglets-supérieur-)
		* [2.1.1. Onglet de filtrage des enquêtes :](#211-onglet-de-filtrage-des-enquêtes--icon-home--icon-bars)
		* [2.1.2. Onglet de filtrage cartographique :](#212-onglet-de-filtrage-cartographique--icon-home--icon-bars)
		* [2.1.3. Onglet des paramètres de l’application :](#213-onglet-des-paramètres-de-lapplication--icon-home--icon-bars)
		* [2.1.4. Onglet des outils de l'application :](#214-onglet-des-outils-de-lapplication--icon-home--icon-bars)
	* [2.2. Panneau à onglets inférieur :](#22-panneau-à-onglets-inférieur-)
		* [2.2.1. Onglet d'analyses cartographiques sur les arrêts :](#221-onglet-danalyses-cartographiques-sur-les-arrêts--icon-home--icon-bars)
		* [2.2.2. Onglet d'analyses cartographiques sur les zonages :](#222-onglet-danalyses-cartographiques-sur-les-zonages--icon-home--icon-bars)
		* [2.2.3. Onglet d'analyses cartographiques sur les tronçons :](#223-onglet-danalyses-cartographiques-sur-les-tronçons--icon-home--icon-bars)
		* [2.2.4. Onglet d'analyses cartographiques de flux :](#224-onglet-danalyses-cartographiques-de-flux--icon-home--icon-bars)
	* [2.3. Autres fonctionnalités :](#23-autres-fonctionnalités-)
		* [2.3.1. Impression de cartes :](#231-impression-de-cartes--icon-home--icon-bars)
		* [2.3.2. Export d'analyses cartographiques :](#232-export-danalyses-cartographiques--icon-home--icon-bars)
		* [2.3.3. Export de couches cartographiques :](#233-export-de-couches-cartographiques--icon-home--icon-bars)
* [3. Rappels sur les enquêtes O/D et leur exploitation :](#3-rappels-sur-les-enquêtes-od-et-leur-exploitation-)
	* [3.1. La pondération des enquêtes :](#31-la-pondération-des-enquêtes--icon-home--icon-bars)
	* [3.2. Les logiques voyage et déplacement :](#32-les-logiques-voyage-et-déplacement--icon-home--icon-bars)
* [4. Structure de la base de données :](#4-structure-de-la-base-de-données-)
	* [4.1. Tables non spatiales :](#41-tables-non-spatiales-)
		* [4.1.1. Tables alphanumériques des enquêtes (SQLite) :](#411-tables-alphanumériques-des-enquêtes-sqlite--icon-home--icon-bars)
		* [4.1.2. Tables alphanumériques temporaires (SQLite) :](#412-tables-alphanumériques-temporaires-sqlite--icon-home--icon-bars)
	* [4.2. Tables spatiales :](#42-tables-spatiales-)
		* [4.2.1. Tables spatiales du réseau (SpatiaLite) :](#421-tables-spatiales-du-réseau-spatialite--icon-home--icon-bars)
		* [4.2.2. Tables spatiales temporaires (SpatiaLite) :](#422-tables-spatiales-temporaires-spatialite--icon-home--icon-bars)
		* [4.2.3. Autres tables :](#423-autres-tables--icon-home--icon-bars)
* [5. Référence du fichier json de configuration :](#5-référence-du-fichier-json-de-configuration-)
	* [5.1. Généralités :](#51-généralités--icon-home--icon-bars)
	* [5.2. dctTypeJour :](#52-dcttypejour--icon-home--icon-bars)
	* [5.3. dctCritereSignaletique :](#53-dctcriteresignaletique--icon-home--icon-bars)
	* [5.4. dctCritereVoyage :](#54-dctcriterevoyage--icon-home--icon-bars)
	* [5.5. dctCritereAutre :](#55-dctcritereautre--icon-home--icon-bars)
	* [5.6. dctZonage :](#56-dctzonage--icon-home--icon-bars)
	* [5.7. dctVariableAnalysable :](#57-dctvariableanalysable--icon-home--icon-bars)
	* [5.8. dctZonageAnalysable :](#58-dctzonageanalysable--icon-home--icon-bars)
	* [5.9. dctParam :](#59-dctparam--icon-home--icon-bars)
	* [5.10. lstColors :](#510-lstcolors--icon-home--icon-bars)
* [Annexe 1 : Liste des fichiers de QMapOD :](#annexe-1--liste-des-fichiers-de-qmapod--icon-home--icon-bars)

<!-- End Document Outline -->

----------

### 1. Généralités \:

#### 1.1. Objet de l’application \: [@icon-home ](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars ](#table-des-matières "Table des matières")

**QMapOD** est la nouvelle version de l'application MapOD, initialement développée sous Access et ArcGis / MapInfo.  
Elle s'appuie sur le système d'information géographique QGIS et la base de données spatiale SQLite / SpatiaLite. La version 2 de QMapOD apporte la compatibilité avec QGIS 3.x.  
Elle se présente sous la forme d'une extension (plugin) QGIS qui permet de cartographier les résultats d'enquêtes origine / destination réalisées sur un réseau de transport en commun, en réalisant des filtrages multicritères sur un jeu de données d’enquêtes, puis en élaborant des analyses cartographiques à partir des données filtrées.

> Le développement initial de QMapOD a été réalisé par [SIGéal](https://www.sigeal.com), et financé par [Test-SA](https://www.test-sa.com).  
> QMapOD est à présent publié [sur GitLab](https://gitlab.com/Oslandia/qgis/qmapod/-/blob/master/README.md?ref_type=heads), sous licence GPL, et principalement maintenu par [Oslandia](https://www.oslandia.com).

QGIS et QMapOD sont diffusés sous licence Open Source (GNU General Public License).

* **Fonctionnalités standard \:**
	* Filtrage par lignes/sens
	* Filtrage par critères signalétiques (catégorie socio-professionnelle, tranche d'âge, sexe, pmr)
	* Filtrage par critères voyage (titre, fréquence, motif, modes amont/aval, tranche horaire)
	* Filtrage cartographique par communes, par zones, par arrêts
	* Affichage des montées/descentes par arrêts
	* Affichage des montées/descentes par variable par arrêts
	* Affichage des montée/descentes par communes, par zones
	* Affichage des montées/descentes par variable par communes/zones
	* Affichage des serpents d'offre, de charge et de performance
	* Affichage des correspondances amont/aval par arrêts
	* Affichage des flux par zones (dégradé, oursins, symboles proportionnels)
	* Affichage des flux par arrêts

* **Fonctionnalités optionnelles \:**
	* Filtrage par type de jour
	* Filtrage par courses
	* Affichage des serpents de charges avec répartition avec/sans correspondance
	* Affichage des serpents de charges avec correspondances amont/aval
	* Affichage des flux par zones avec répartition selon critères voyage et -signalétique (camembert)
	* Affichage des flux principaux (seuil de sélection, flèches intra-zone, flèches inter-zones)

> Les fonctionnalités disponibles peuvent varier selon les variables enquêtées.

#### 1.2. Architecture de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
QMapOD est une extension QGIS développée en python 3.9, intégrant les fonctionnalités de filtrage et les fonctionnalités d'analyse cartographique.
Elle peut fonctionner dans tous les environnements supportés nativement par QGIS \:

* Windows (32 et 64 bits),
* Mac OS X,
* Linux,
* BSD.

#### 1.3. Installation de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
L'extension QMapOD peut-être lancée depuis une installation QGIS existante (***recommandé***), ou  bien depuis une version autonome de QGIS pré-configurée (sous Windows uniquement).

Dans le premier cas, pour installer QGIS, il convient d'installer l'extension à partir du fichier .zip \:
![Barre d'outils](img/dlg_install.png "Barre d'outils")

Dans le second cas, pour installer QGIS, il faut copier le dossier contenant l'installation de QGIS – et donc de QMapOD – sur l'un des disques de la machine.
Pour mettre à jour l'extension QMapOD dans ce second cas, il convient de remplacer le dossier QMapOD situé dans le sous-dossier suivant \: `\QGIS\apps\qgis-ltr\python\plugins`

> Bien qu'il soit théoriquement possible de lancer QGIS et QMapOD depuis un CDROM ou une clé USB, ou un disque réseau, les performances et le confort d'utilisation risquent de s'en ressentir fortement.

#### 1.4. Lancement de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Après le lancement de QGIS, QMapOD doit être activé de la façon suivante \:  
Menu Extension > Gérer les extensions, taper QMapOD dans la case de recherche de la boite de dialogue de gestion des extensions, puis cocher la case QMapOD.  
Si l'extension QMapOD n’apparaît pas après avoir tapé « QMapOD » dans la case de rechercher, vérifier l'installation de l'extension.  
Pour que l'extension QMapOD soit visible, l'option « Afficher les extensions expérimentales » doit être cochée dans l'onglet « Paramètres » du gestionnaire d'extensions.  
Une fois l'extension activée les fonctionnalités générales de QMapOD sont accessibles depuis une barre d'outil \:

![Barre d'outils](img/barre_outils.png "Barre d'outils")

Elle sont également accessibles depuis le menu Extensions > QMapOD 2 \:

![Menu](img/menu.png "Menu extensions")

#### 1.5. Présentation de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Chaque bouton/menu donne accès aux fonctionnalités suivantes \:

![Panneau](img/btn_aide.png "Panneau") \: Affichage du panneau ancrable QMapOD  
![Chargement](img/btn_couches.png "Chargement") \: Chargement des couches géographiques  
![Paramètres](img/btn_parametres.png "Paramètres") \: Activation de l'onglet Paramètres du panneau QMapOD  
![Aide](img/btn_aide.png "Aide") \: Affichage de l'aide en ligne dans le navigateur par défaut  
![À propos](img/btn_apropos.png "À propos") \: Affichage du dialogue À propos de QMapOD  

Les fonctionnalités de QMapOD sont regroupées dans un panneau ancrable, positionné à droite par défaut, comprenant deux panneaux à onglets \:

- Le panneau à onglets supérieur regroupe les fonctionnalités de filtrage, de paramétrage de l'application et les outils complémentaires \:  
![Panneau supérieur](img/filtrage.png "Panneau supérieur")

- Le panneau à onglets inférieur regroupe les outils permettant d'interagir avec la carte \:  
![Panneau inférieur](img/outils.png "Panneau inférieur")

Le panneau ancrable contient également dans sa partie basse un bouton accessible à tout moment permettant d'afficher le sous-réseau courant (lignes/sens sélectionnés) \:  
![Sous-réseau](img/sous_reseau.png "Sous-réseau")

----------

### 2. Description des fonctionnalités de l'application \:
#### 2.1. Panneau à onglets supérieur \:
##### 2.1.1. Onglet de filtrage des enquêtes \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet permet de spécifier les critères de filtrage des enquêtes. Le bouton de validation du filtrage a pour effet la création d'un sous-ensemble d'enquêtes (table *enquetes_tmp*) correspondant aux critères spécifiés. Les analyses cartographiques réalisées s'appuient sur cette table temporaire.

> La validation des critères de filtrage des enquêtes est ***dynamique***, c'est à dire que les éventuelles analyses cartographiques existantes lors de la validation sont automatiquement mises à jour conformément aux critères spécifiés.

L'onglet de filtrage des enquêtes comprend trois rubriques escamotables (accordéon) et une ligne de boutons.  
Dans la vue arborescente du réseau, les catégories de lignes et les lignes peuvent être dépliées/repliées en cliquant sur l'icône >.  
Dans les listes de cases à cocher, il est possible de cocher simultanément plusieurs options en sélectionnant plusieurs lignes, avec les touches MAJ et CTRL du clavier, puis en cochant l'une des cases sélectionnées. Il est également possible de cocher ou de décocher toutes les cases en cochant/décochant celle qui est située à côté du titre de la liste.

- La rubrique sous-réseau contient le cas échéant une liste déroulante permettant de spécifier le type de jour enquêté (jour de semaine (JOB), samedi, dimanche), ainsi qu'une vue arborescente du réseau permettant de cocher/décocher les catégories de lignes, les lignes et les lignes/sens à prendre en compte \:  
![Sous-réseau](img/rub_sous_reseau.png "Sous-réseau")

	> Lorsque plusieurs lignes/sens sont cochés, il convient de s'assurer que les sens de circulation sont cohérents pour éviter un calcul de serpent de charge erroné.

	> Si des tranches horaires différentes sont définies selon le type de jour, la sélection d'un type de jour dans la liste déroulante a pour effet la mise à jour des tranches horaires dans la rubrique Critères Voyage.

- La rubrique critères signalétique contient une liste de cases à cocher pour chaque critère enquêté décrivant la signalétique de l'usager enquêté \: statut socio-professionnel, titre utilisé, tranche d'âge, commune de résidence, pmr, etc. \:  
![Signalétique](img/rub_signaletique.png "Signalétique")

- La rubrique critères voyage contient une liste de cases à cocher pour chaque critère enquêté décrivant le voyage \: tranche horaire, motif de déplacement, mode amont, mode aval, fréquence d'utilisation, etc. \:  
![Voyage](img/rub_voyage.png "Voyage")
	> Lorsque toutes les tranches horaires ne sont pas cochées, les fonctionnalités d'affichage des serpents d'offre et de performance sont désactivées.

- La ligne de boutons comprend \: un bouton de validation du filtrage des enquêtes et un bouton de réinitialisation du formulaire \:  
![Boutons](img/boutons_filtrage_enquetes.png "Boutons")

##### 2.1.2. Onglet de filtrage cartographique \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet permet de spécifier des critères de filtrage cartographique. Le bouton de validation du filtrage a pour effet la spécification d'un filtre d'entités (requête) au niveau des couches utilisées pour les analyses cartographique.

> Le filtrage par zones s'applique également aux arrêts, c'est à dire que les arrêts inclus dans les zones exclues sont également exclus. En revanche, le filtrage cartographique par zonage est appliquée indépendamment des autres zonages.

L'onglet de filtrage cartographique comprend deux rubriques escamotables (accordéon) et une ligne de boutons.

- La rubrique arrêts contient une liste de cases à cocher pour chaque arrêt logique du réseau \:  
![Arrêts](img/rub_arrets.png "Arrêts")  

> Il est possible d'accéder directement à un arrêt particulier en tapant au clavier les premières lettres de son nom.

- La rubrique zonages contient une liste de cases à cocher pour chaque zonage intégré à l'application \: zonage communal, zonage O/D, etc. \:  
![Zonages](img/rub_zonages.png "Zonages")  

> Il est possible d'accéder directement à une zone particulière en tapant au clavier les premières lettres de son nom.

- La ligne de boutons comprend \: un bouton de validation du filtrage cartographique et un bouton de réinitialisation du formulaire \:  
![Boutons](img/boutons_filtrage_carto.png "Boutons")  

##### 2.1.3. Onglet des paramètres de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet permet de régler les paramètres de l'application. Ces paramètres concernent le mode d'affichage et les caractéristiques des analyses cartographiques. Les rubriques « Arrêts », « Zones », « Tronçons », «Flux par zones » et « Flux par arrêts » correspondent aux onglets contenant les outils d'interaction cartographique. Tous les paramètres spécifiés dans cet onglet sont conservés d'une session à une autre.

> La validation des paramètres est ***dynamique***. Les paramètres sélectionnés sont appliqués aux éventuelles analyses cartographiques existantes lorsque l'on clique sur le bouton de validation.

Il comprend six rubriques escamotables (accordéon) et une ligne de boutons \:

* La rubrique ***sous-réseau*** contient les paramètres relatifs à la visualisation cartographique du sous-réseau. Si la table code_ligne contient une colonne « rvb », contenant la couleur à utiliser pour chaque ligne, il est possible de choisir le type de représentation des lignes \:  
	![Paramètres sous-réseau](img/param_sous_reseau_coul.png "Paramètres sous-réseau")

	Dans le cas contraire, seuls les paramètres par défaut sont accessibles \:  
	![Paramètres sous-réseau](img/param_sous_reseau.png "Paramètres sous-réseau")

* La rubrique ***arrêts*** contient les paramètres relatifs aux analyses cartographiques sur les arrêts \:  

	![Paramètres arrêts](img/param_arrets.png "Paramètres arrêts")

* La rubrique ***zones*** contient les paramètres relatifs aux analyses cartographiques sur les zonages \:  

	![Paramètres zonages](img/param_zonages.png "Paramètres zonages")

* La rubrique ***tronçons*** contient les paramètres relatifs aux analyses cartographiques sur les tronçons \:  

	![Paramètres tronçons](img/param_troncons.png "Paramètres tronçons")

* La rubrique ***flux principaux*** contient les paramètres relatifs aux analyses cartographiques des flux principaux et des flux principaux par ligne \:

    ![Paramètres tronçons](img/param_flux_principaux.png "Paramètres tronçons")

* La rubrique ***flux par zones*** contient les paramètres relatifs aux analyses cartographiques de flux par zones \:

	Le choix du type de représentation dans cette rubrique conditionne les paramètres d'affichage et d'analyse disponibles.  

	![Paramètres flux zones](img/param_flux_zones.png "Paramètres flux zones")

	> L'option ***dégradé de couleurs***, présente pour des raisons historiques, n'est pas recommandée car elle n'est pas optimale pour la représentation visuelle des variables quantitatives.

	Paramètres disponibles lorsque la représentation par ***dégradé de couleurs*** est sélectionnée \:  

	![Paramètres flux zones](img/param_flux_zones_degrade.png "Paramètres flux zones")

	Paramètres disponibles lorsque la représentation par ***oursins*** est sélectionnée \:  

	![Paramètres flux zones](img/param_flux_zones_oursins.png "Paramètres flux zones")

	Paramètres disponibles lorsque la représentation par ***diagrammes proportionnels*** est sélectionnée \:  

	![Paramètres flux zones](img/param_flux_zones_diagram.png "Paramètres flux zones")

- La rubrique ***flux par arrêts*** contient les paramètres relatifs aux analyses cartographiques de flux par arrêts \:  

	![Paramètres flux arrêts](img/param_flux_arrets.png "Paramètres flux arrêts")

- La ligne de boutons comprend \: un bouton de validation des paramètres d'affichage et d'analyse et un bouton de chargement des paramètres par défaut \:  
![Boutons](img/boutons_parametres.png "Boutons")
##### 2.1.4. Onglet des outils de l'application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet contient une case à cocher permettant d'activer ou de désactiver l'affichage des diagrammes et des étiquettes superposés.  
Cet onglet comprend aussi une zone de texte dans laquelle est affiché un récapitulatif des paramètres d'affichage, des critères de filtrage des enquêtes et des critères de filtrage cartographique courants. Le contenu de cette zone de texte  peut être utilisé pour avoir une vue d'ensemble des critères sélectionnés, ou pour copier/coller le récapitulatif des critères sélectionnés dans une mise en page destinée à l'impression.

![Récapitulatif](img/rub_recap.png "Récapitulatif")

#### 2.2. Panneau à onglets inférieur \:
##### 2.2.1. Onglet d'analyses cartographiques sur les arrêts \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses sur les arrêts, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:

![Arrêts](img/onglet_arrets.png "Arrêts")

Le fonctionnement des boutons de cet onglet est décrit ci-après \:

* ![Montées / descentes par arrêts](img/btn_md_arrets.png "Montées / descentes par arrêts") **Bouton montées / descentes par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont les secteurs représentent respectivement les montées et les descentes à cet arrêt. La taille du diagramme est proportionnelle à la somme des montées et des descentes à cet arrêt.

* ![Montées par arrêts](img/btn_m_arrets.png "Montées par arrêts") **Bouton montées par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des montées à cet arrêt.

* ![Descentes par arrêts](img/btn_d_arrets.png "Descentes par arrêts") **Bouton Descentes par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des descentes à cet arrêt.

* ![Montées par sens par arrêts](img/btn_ms_arrets.png "Montées par sens par arrêts") **Bouton montées par sens par arrêts \:**
	Ce bouton permet d’afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont les secteurs représentent respectivement les montées dans le sens 1 et le sens 2 à cet arrêt. La taille du diagramme est proportionnelle à la somme des montées dans les deux sens à cet arrêt. Cette analyse est conçue pour visualiser les déséquilibres de montées entre le sens aller et le sens retour, tels qu’ils apparaissent dans la liste des lignes / sens (rubrique sous-réseau). Si un seul sens est sélectionné, elle donne le même résultat que les montées par arrêts. Si plusieurs lignes sont sélectionnées, elle fonctionne mais ses résultats peuvent être difficiles à interpréter.

* ![Descentes par sens par arrêts](img/btn_ds_arrets.png "Descentes par sens par arrêts") **Bouton Descentes par sens par arrêts \:**
		Ce bouton permet d’afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont les secteurs représentent respectivement les descentes dans le sens 1 et le sens 2 à cet arrêt. La taille du diagramme est proportionnelle à la somme des descentes dans les deux sens à cet arrêt. Cette analyse est conçue pour visualiser les déséquilibres de descentes entre le sens aller et le sens retour, tels qu’ils apparaissent dans la liste des lignes / sens (rubrique sous-réseau). Si un seul sens est sélectionné, elle donne le même résultat que les descentes par arrêts. Si plusieurs lignes sont sélectionnées, elle fonctionne mais ses résultats peuvent être difficiles à interpréter.

* ![Montées par variable par arrêts](img/btn_mvar_arrets.png "Montées par variable par arrêts") **Bouton montées par variable par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des montées à cet arrêt et dont les secteurs correspondent à la répartition des montées pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](img/lst_variables.png "Liste des variables").
		Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux montées sur chaque ligne.
	* L'option « **Lignes amont** » pour afficher les lignes de correspondance amont sous la forme de camemberts dont les secteurs sont proportionnels aux montées en provenance de chaque ligne de correspondance amont.
	* Le bouton montées par variable par arrêts est désactivé lorsque l'option « **Lignes aval** » est choisie car cette combinaison n'est pas pertinente.

	> Le changement de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des variables.

* ![Descentes par variable par arrêts](img/btn_dvar_arrets.png "Descentes par variable par arrêts") **Bouton descentes par variable par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des descentes à cet arrêt et dont les secteurs correspondent à la répartition des descentes pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](img/lst_variables.png "Liste des variables").
		Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux descentes sur chaque ligne.
	* Le bouton descentes par variable par arrêts est désactivé lorsque l'option « **Lignes amont** » est choisie car cette combinaison n'est pas pertinente.
	* L'option « **Lignes aval** » pour afficher les lignes de correspondance aval sous la forme de camemberts dont les secteurs sont proportionnels aux descentes à destination de chaque ligne de correspondance aval.

	> Le changement de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des variables.

* ![Effacer analyses par arrêts](img/btn_md_arrets_del.png "Effacer analyses par arrêts") **Bouton effacer les analyses cartographiques par arrêts \:**
		Ce bouton permet de supprimer toutes les analyses cartographiques par arrêts.

##### 2.2.2. Onglet d'analyses cartographiques sur les zonages \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses sur les zonages, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:

![Zonages](img/onglet_zonages.png "Zonages")

Le fonctionnement des boutons de cet onglet est décrit ci-après \:

* ![Montées / descentes par zones](img/btn_md_zones.png "Montées / descentes par zones") **Bouton montées / descentes par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camembert dont les secteurs représentent respectivement les montées et les descentes dans cette zone. La taille du diagramme est proportionnelle à la somme des montées et des descentes dans cette zone.

* ![Montées par zones](img/btn_m_zones.png "Montées par zones") **Bouton montées par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camemberts dont la taille est proportionnelle à la somme des montées dans cette zone.

* ![Descentes par zones](img/btn_d_zones.png "Descentes par zones") **Bouton Descentes par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camemberts dont la taille est proportionnelle à la somme des descentes dans cette zone.

* ![Montées par variable par zones](img/btn_mvar_zones.png "Montées par variable par zones") **Bouton montées par variable par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camembert dont la taille est proportionnelle à la somme des montées dans cette zone et dont les secteurs correspondent à la répartition des montées pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](img/lst_variables.png "Liste des variables").
Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (dans les troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux montées sur chaque ligne dans la zone.
	* L'option « **Lignes amont** » pour afficher les lignes de correspondance amont sous la forme de camemberts dont les secteurs sont proportionnels aux montées en provenance de chaque ligne de correspondance amont dans la zone.
	* Le bouton montées par variable par zones est désactivé lorsque l'option « **Lignes aval** » est choisie car cette combinaison n'est pas pertinente.

> Le changement de zonage ou de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des zonages ou dans la liste déroulante des variables.

* ![Descentes par variable par zones](img/btn_dvar_zones.png "Descentes par variable par zones") **Bouton descentes par variable par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camembert dont la taille est proportionnelle à la somme des descentes dans cette zone et dont les secteurs correspondent à la répartition des descentes pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](img/lst_variables.png "Liste des variables").
		Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (dans les troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux descentes sur chaque ligne dans la zone.
	* Le bouton montées par variable par zones est désactivé lorsque l'option « **Lignes amont** » est choisie car cette combinaison n'est pas pertinente.
	* L'option « **Lignes aval** » pour afficher les lignes de correspondance aval sous la forme de camemberts dont les secteurs sont proportionnels aux descentes à destination de chaque ligne de correspondance aval dans la zone.

> Le changement de zonage ou de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des zonages ou dans la liste déroulante des variables.

* ![Effacer analyses par zones](img/btn_md_zones_del.png "Effacer analyses par zones") **Bouton effacer les analyses cartographiques par zones \:**
		Ce bouton permet de supprimer toutes les analyses cartographiques par zones.

##### 2.2.3. Onglet d'analyses cartographiques sur les tronçons \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses sur les tronçons, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:  

![Tronçons](img/onglet_troncons.png "Tronçons")

Le fonctionnement des boutons de cet onglet est décrit ci-après \:

* ![Serpent d'offre](img/btn_serp_offre.png "Serpent d'offre") **Bouton serpent d'offre \:**
		Ce bouton permet d'afficher le serpent d'offre du sous-réseau sélectionné. Chaque tronçon est affiché avec une épaisseur correspondant à sa classe d'offre exprimée **en nombre de courses**.
		> Si la table des courses existe dans la base de données, le serpent d'offre tient compte de l'éventuel filtrage par tranche horaire. Dans le cas contraire, lorsque toutes les tranches horaires ne sont pas cochées, la fonctionnalité d'affichage du serpent d'offre est désactivée.

* ![Serpent de charge](img/btn_serp_charge.png "Serpent de charge") **Bouton serpent de charge \:**
		Ce bouton permet d'afficher le serpent de charge du sous-réseau sélectionné. Chaque tronçon est affiché avec une épaisseur correspondant à sa classe de charge exprimée **en nombre de passagers**.

* ![Serpent de performance](img/btn_serp_perf.png "Serpent de performance") **Bouton serpent de performance \:**
		Ce bouton permet d'afficher le serpent de performance du sous-réseau sélectionné. Chaque tronçon est affiché avec une épaisseur correspondant à sa classe de performance exprimée **en nombre de passagers rapporté au nombre de courses**.
			> Si la table des courses existe dans la base de données, le serpent de performance tient compte de l'éventuel filtrage par tranche horaire. Dans le cas contraire, lorsque toutes les tranches horaires ne sont pas cochées, la fonctionnalité d'affichage du serpent de performance est désactivée.

* ![Effacer analyses par tronçons](img/btn_serp_del.png "Effacer analyses par tronçons") **Bouton effacer les analyses cartographiques par tronçons \:**
		Ce bouton permet de supprimer toutes les analyses cartographiques par tronçons.

##### 2.2.4. Onglet d'analyses cartographiques de flux \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses de flux sur les zonages et sur les arrêts, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:  

![Flux](img/onglet_fluxpp.png "Flux")

>Les visualisations de flux sont affichées conformément au type de représentation spécifié dans les paramètres (rubrique flux par zones).

Le fonctionnement des outils et des boutons de cet onglet est décrit ci-après \:

- ![Flux principaux](img/btn_fluxpp.png "Flux principaux") **Bouton flux principaux \:**
		Ce bouton permet d'afficher, sous forme de flèches proportionnelles, les N principaux flux de zone à zone, pour le zonage sélectionné dans la liste zonage ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones") et pour le sous ensemble d'enquêtes courant. Le nombre de flux affichés peut-être ajusté au moyen de la boite de saisie incrémentale ![Nb flux principaux](img/spx_nb_fluxpp.png "Nb flux principaux")située à droite de ce bouton.

- ![Flux principaux par ligne](img/btn_fluxppl.png "Flux principaux par ligne") **Bouton flux principaux par ligne \:**
		Ce bouton permet d'afficher, sous forme de flèches proportionnelles, les N principaux flux de zone à zone, distingués par sens, pour le zonage sélectionné dans la liste zonage ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones") et pour le sous ensemble d'enquêtes courant. Le nombre de flux affichés peut-être ajusté au moyen de la boite de saisie incrémentale ![Nb flux principaux](img/spx_nb_fluxpp.png "Nb flux principaux")située à droite de ce bouton. Cette fonctionnalité est conçue pour fonctionner sur la base d'un filtrage sur une seule ligne, afin que la distinction par sens soit cohérente.

> Pour des raisons de lisibilité graphique, les flux réflexifs (d'une zone vers cette même zone) ne sont pas affichés par ces fonctionnalités. Ils sont cependant visibles dans la table attributaire de la couche de visualisation.

- ![Flux par zone(s) de montée](img/tbn_flux_zones_m.png "Flux par zone(s) de montée") **Outil flux par zone(s) de montée \:**
		Cet outil permet de visualiser les descentes pour une ou plusieurs zones de montée du zonage sélectionné dans la liste zonage ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones"). Pour sélectionner la zone de montée, il convient d'activer l'outil, puis de cliquer sur la zone désirée. Pour sélectionner plusieurs zones, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les zones désirées.

* ![Flux par zone(s) de descente](img/tbn_flux_zones_m.png "Flux par zone(s) de descente ") **Outil flux par zone(s) de descente \:**
		Cet outil permet de visualiser les montées pour une ou plusieurs zones de descente du zonage sélectionné dans la liste zonage ![Montées / descentes par zones](img/lst_zonages.png "Montées / descentes par zones"). Pour sélectionner la zone de descente, il convient d'activer l'outil, puis de cliquer sur la zone désirée. Pour sélectionner plusieurs zones, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les zones désirées.

* ![Suppression des flux par zones](img/btn_eff_flux_zones.png "Suppression des flux par zones") **Bouton suppression des flux par zones \:**
		Cet outil permet de supprimer les visualisations de flux par zone(s).

* ![Flux par arrêt(s) de montée](img/tbn_flux_arrets_m.png "Flux par arrêt(s) de montée") **Outil flux par arrêt(s) de montée \:**
		Cet outil permet de visualiser les descentes pour un ou plusieurs arrêts de montée. Pour sélectionner l'arrêt de montée, il convient d'activer l'outil, puis de cliquer sur l'arrêt désiré. Pour sélectionner plusieurs arrêts, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les arrêts désirés.

* ![Flux par arrêt(s) de descente](img/tbn_flux_arrets_m.png "Flux par arrêt(s) de descente ") **Outil flux par arrêt(s) de descente \:**
		Cet outil permet de visualiser les montées pour une ou plusieurs arrêts de descente. Pour sélectionner l'arrêt  de descente, il convient d'activer l'outil, puis de cliquer sur l'arrêt désiré. Pour sélectionner plusieurs arrêts, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les arrêts désirés.

* ![Suppression des flux par zones](img/btn_eff_flux_arrets.png "Suppression des flux par zones") **Bouton suppression des flux par arrêts \:**
		Ce bouton permet de supprimer les visualisations de flux par arrêt(s).

#### 2.3. Autres fonctionnalités \:
##### 2.3.1. Impression de cartes \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Un projet QGIS - *QMapOD.qgs* -  contenant plusieurs modèles de mise en page est fourni avec l'application QMapOD. Situé dans le dossier d'installation de l'extension, il contient six modèles différents permettant de traiter les cas les plus courants \:

- Format A4 vertical ou portrait (A4V)
- Format A4 horizontal ou paysage (A4H)
- Format A3 vertical ou portrait (A3V)
- Format A3 horizontal ou paysage (A3H)
- Format A0 vertical ou portrait (A0V)
- Format A0 horizontal ou paysage (A0H)

Chacune de ces mises en page contient deux éléments de base permettant de construire une carte mise en page avec sa légende \:

- Un cadre carte remplissant la mise en page avec une marge de 10 mm
- Un cadre texte permettant de copier le récapitulatif du filtrage contenu dans l'onglet « **Outils** » de l'application.

> Le projet *QMapOD.qgs* peut donc être utilisé comme un modèle pour
> l'élaboration d'analyses cartographiques destinées à l'impression.

Lors de l'ouverture d'un modèle de mise en page, le cadre carte apparaît vide. Pour y faire apparaître la carte, il convient de le sélectionner, puis de cliquer sur le bouton « **Fixer sur l'emprise courante du canevas de la carte** ». Le bouton « **Déplacer le contenu de l'objet** » peut ensuite être utilisé, conjointement avec la molette et la touche CTRL, pour ajuster le cadrage de la carte.

##### 2.3.2. Export d'analyses cartographiques \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Il est possible de sauvegarder une analyse cartographique complexe en l'exportant sous la forme d'un projet autonome, déconnecté de la base de donnée de l'application dont les données temporaires sont susceptibles de changer à chaque nouveau filtrage. Pour cela, une extension de QGIS doit être utilisée \: ***QConsolidate3***.

Cette extension, fournie avec l'application QMapOD, doit-être activée par le menu Extensions >  Gérer les extensions.

Pour exporter une analyse cartographique, il convient de faire le filtrage et d'appliquer les traitements cartographiques souhaités.

Il faut ensuite lancer l'extension en cliquant sur le bouton![Extension QConsolidate3](img/btn_qconsolidate3.png "Extension QConsolidate3") de la barre d'outils Extensions \:  
![Extension QConsolidate3](img/dlg_qconsolidate3.png "Extension QConsolidate3")

Il convient de spécifier le dossier dans lequel la carte doit être enregistrée, dans un sous-dossier portant le nom de projet spécifié, puis de préciser quel doit être le format d'enregistrement des couches vectorielles (SHP ou GeoPackage).

L'ensemble des fichiers peut être optionnellement être compressé dans une archive .zip.

Enfin, il faut cliquer sur **Ok** pour lancer l'export. Un message de confirmation s'affiche à la fin de l'opération \:  
![Confirmation QPackage](img/msg_qconsolidate3_confirm.png "Confirmation  QPackage")

Le projet QGIS est enregistré dans le dossier spécifié, avec une copie au format sélectionné de toutes les données qu'il référence. On peut donc le ré-ouvrir pour vérifier son bon fonctionnement.

##### 2.3.3. Export de couches cartographiques \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Il est possible de sauvegarder au format shape (.shp) ou GeoPackage (.gpkg) une couche vectorielle correspondant à une analyse cartographique réalisée avec l'application. Il suffit pour cela de cliquer avec le bouton droit sur le nom de la couche dans la liste des couches puis de sélectionner le menu « **Sauvegarder sous** ».

----------

### 3. Rappels sur les enquêtes O/D et leur exploitation \:
#### 3.1. La pondération des enquêtes \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Deux types de pondération sont utilisés pour le traitement des enquêtes \:

- La pondération ligne,
- la pondération réseau.

La première correspond au poids de l’enquête pour la ligne sur laquelle l’enquête a été réalisée. Elle est utilisée pour les analyses sur les voyages (montées/descentes sur la ligne enquêtée).
La seconde correspond au poids de l’enquête pour l’ensemble du réseau de transport en commun, c’est à dire qu’elle tient compte des correspondances réalisées. Elle est donc utilisée pour les analyses sur les déplacements (avec éventuellement des correspondances).
La pondération réseau n’est utilisée que dans le cas où l’ensemble du réseau est sélectionné.
Lorsque plusieurs lignes sont sélectionnées, c’est la pondération ligne qui est utilisée. Dans ce cas, de légères distorsions peuvent être constatées sur les résultats, par exemple si des enquêtes avec correspondances comprennent des lignes ne faisant pas partie du sous-réseau sélectionné (le poids de ces enquêtes peut selon le cas être un peu sur-évalué ou sous-évalué).

#### 3.2. Les logiques voyage et déplacement \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")  
Un voyage correspond à un trajet réalisé sur une seule ligne de transport en commun. On travaille donc en logique voyage lorsque l'on considère les arrêts de début et de fin du trajet effectué **sur la ligne enquêtée**.

Un déplacement correspond à un trajet réalisé avec éventuellement une ou plusieurs correspondances sur le réseau de transport en commun. On travaille donc en logique déplacement lorsque l'on considère les arrêts de début (pour le premier trajet du déplacement) et de fin (pour le dernier trajet du déplacement) du déplacement effectué sur le réseau de transport en commun, c'est à dire **en incluant les correspondances en amont et en aval de la ligne enquêtée**.
Le schéma ci-après illustre les différentes composantes d'une enquête \:  

![Voyage / Déplacement](img/enquetes_voyage_deplacement.png "Voyage / Déplacement")

Les logiques d'analyse utilisées par défaut par QMapOD sont les suivantes \:

* **Logique voyage \:**
	* Affichage des montées/descentes par arrêts (une ou plusieurs lignes)
	* Affichage d'une variable par arrêts (une ou plusieurs lignes)
	* Affichage des serpents de charge et de performance

* **Logique déplacement \:**
	* Affichage d'une variable par zones (toutes les lignes)
	* Affichage des flux par zones (toutes les lignes)
	* Affichage des flux par arrêts (toutes les lignes)

* **Logique paramétrable \:**
	* Affichage des montées/descentes par arrêts (toutes les lignes)
	* Affichage d'une variable par arrêts (toutes les lignes)
	* Affichage des montée/descentes par zones
	* Affichage d'une variable par zones (une ou plusieurs lignes)

Le tableau ci-dessous résume les combinaisons mode d'analyse / pondération utilisées selon les différents cas de figure \:  
![Mode analyse](img/enquetes_mode_analyse.png "Mode analyse")
*Dans les cas de figure correspondant aux cases rouges, la possibilité de choix du mode d'analyse (voyage / déplacement) est verrouillée.*

----------

### 4. Structure de la base de données \:
#### 4.1. Tables non spatiales \:
##### 4.1.1. Tables alphanumériques des enquêtes (SQLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Table parcours \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un élément de parcours de ligne de bus.
	* Champ id_ligne (entier) \: Identifiant de la ligne
	* Champ id_sens (entier court) \: Identifiant du sens de la ligne
	* Champ id_parcours (entier court) \: Identifiant du parcours de la ligne
	* Champ id_arret (entier court) \: Identifiant de l’arrêt
	* Champ ordre (entier court) \: Numéro d’ordre de l’arrêt pour la ligne, le sens et le parcours considéré
	* Champ desserte (booléen) \: Indicateur précisant si l’arrêt est desservi pour le parcours considéré
	* Champ id_tron (chaîne) \: Identifiant du tronçon reliant l’arrêt considéré à l’arrêt suivant

* **Table code_sens \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de sens de ligne.
	* Champ id_sens (entier court) \: Identifiant du sens
	* Champ id_ligne (entier long) \: Identifiant de la ligne
	* Champ lib_sens (chaîne) \: Libellé du sens

* **Table courses \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une course.
	* Champ id_ligne (entier long) \: Identifiant de la ligne
	* Champ num_ligne (chaîne) \: Numéro de la ligne
	* Champ id_sens (entier court) \: Identifiant du sens de la ligne
	* Champ id_parcours (entier court) \: Identifiant du parcours de la ligne
	* Champ id_course (entier long) \: Identifiant de la course
	* Champ id_typ_jour (entier court) \: Code de type de jour
	* Champ id_typ_ligne (entier court) \: Code de type de ligne
	* Champ hdeb (chaîne) \: Heure de départ de la course
	* Champ hfin (chaîne) \: Heure d'arrivée de la course
	* Champ id_arret_deb (entier long) \: Identifiant de l’arrêt de début de la course
	* Champ nom_arret_deb (chaîne) \: Nom de l’arrêt de début de la course
	* Champ id_arret_fin (entier long) \: Identifiant de l’arrêt de fin de la course
	* Champ nom_arret_fin (chaîne) \: Nom de l’arrêt de fin de la course
	* Champ poids (entier long) \: Poids de la course (si saisie par tranche horaire)

* **Table offre \:**
Chaque enregistrement de cette table contient la description alphanumérique de l'offre pour un élément de parcours pour un type de jour.
	* Champ id_ligne(entier long) \: Identifiant de la ligne
	* Champ id_sens (entier court) \: Identifiant du sens de la ligne
	* Champ id_parcours (entier court) \: Identifiant du parcours de la ligne
	* Champ id_arret (entier court) \: Identifiant de l’arrêt
	* Champ ordre (entier court) \: Numéro d’ordre de l’arrêt pour la ligne, le sens et le parcours considéré
	* Champ desserte (booléen) \: Indicateur précisant si l’arrêt est desservi pour le parcours considéré
	* Champ id_tron (chaîne) \: Identifiant du tronçon reliant l’arrêt considéré à l’arrêt suivant
	* Champ typ_jour (entier court) \: Code de type de jour
	* Champ offre (entier) \: Offre globale moyenne

* **Table enquetes \:**
Chaque enregistrement de ces tables contient la description alphanumérique d’un questionnaire d’enquête, pondéré par les comptages.
	* Champ ident (entier) \: Identifiant questionnaire
	* Champ typjour (entier court) \: Identifiant du type de jour
	* Champ ligne (entier court) \: Identifiant de la ligne
	* Champ sens (entier court) \: Identifiant du sens de circulation
	* Champ course (entier) \: Identifiant de la course
	* Champ parcours (entier court) \: Identifiant du parcours
	* Champ tranche (entier court) \: Identifiant de la tranche horaire regroupée
	* Champ arreta2 (entier) \: Identifiant de l’arrêt de montée
	* Champ arretb2 (entier) \: Identifiant de l’arrêt de descente
	* Champ arretdeb2 (entier court) \: Identifiant de l’arrêt de début du déplacement considéré (si correspondance avant)
	* Champ arretfin2 (entier court) \: Identifiant de l’arrêt de fin du déplacement considéré (si correspondance avant)
	* Champ nbav (entier court) \: Nombre de correspondances avant
	* Champ nbap (entier court) \: Nombre de correspondances après
	* Champ lignem1 (entier court) \: Identifiant de la première ligne empruntée avant la ligne courante (si correspondance avant)
	* Champ lignep1 (entier court) \: Identifiant de la première ligne empruntée avant la ligne courante (si correspondance après)
	* Champ motifod (entier court) \: Identifiant du motif agrégé du déplacement
	* Champ sexe (entier court) \: Identifiant du sexe de la personne enquêtée
	* Champ trage (entier court) \: Identifiant de la tranche d’âge regroupée
	* Champ titre (entier court) \: Identifiant du titre de transport utilisé
	* Champ poidsl (réel double) \: Poids du questionnaire considéré sur la ligne
	* Champ poidsr (réel double) \: Poids du questionnaire considéré sur le réseau (inférieur à POIDSL si correspondances)

* **Table code_comres \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une commune de résidence.
	* Champ id_comres (entier court) \: Identifiant du code INSEE de la commune de résidence
	* Champ lib_comres (chaîne) \: Libellé de la commune de résidence
	* Champ comres (chaîne) \: Libellé abrégé du code de la commune de résidence

* **Table code_ligne \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une ligne de bus, de tramway ou de métro.
	* Champ id_ligne (entier) \: Identifiant de la ligne
	* Champ num_ligne (chaîne) \: Numéro de la ligne
	* Champ lib_ligne (chaîne) \: Libellé de la ligne
	* Champ typ_ligne (entier) \: Type de la ligne
	* Champ rvb (chaîne) \: Couleur de la ligne (codes R/V/B)

* **Table code_modav \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un mode de déplacement amont.
	* Champ id_modav (entier court) \: Identifiant du code de mode de déplacement amont
	* Champ lib_modav (chaîne) \: Libellé du code de mode de déplacement amont
	* Champ modav (chaîne) \: Libellé abrégé du code de mode de déplacement amont

* **Table code_modap \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un mode de déplacement aval.
	* Champ id_modap (entier court) \: Identifiant du code de mode de déplacement aval
	* Champ lib_modap (chaîne) \: Libellé du code de mode de déplacement aval
	* Champ modap (chaîne) \: Libellé abrégé du code de mode de déplacement aval

* **Table code_motif \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de motif de déplacement agrégé.
	* Champ id_motif (entier court) \: Identifiant du code de motif de déplacement agrégé
	* Champ lib_motif (chaîne) \: Libellé du code de motif de déplacement agrégé
	* Champ motifod (chaîne) \: Libellé abrégé du code de motif de déplacement agrégé

* **Table code_pmr \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de personne à mobilité réduite.
	* Champ id_pmr (entier court) \: Identifiant du code de personne à mobilité réduite
	* Champ lib_pmr (chaîne) \: Libellé du code de personne à mobilité réduite
	* Champ pmr (chaîne) \: Libellé abrégé du code de personne à mobilité réduite

* **Table code_titre \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de titre de transport.
	* Champ id_titre  entier court) \: Identifiant du code de titre de transport
	* Champ lib_titre (chaîne) \: Libellé du code de titre de transport
	* Champ titre (chaîne) \: Libellé abrégé du code de titre de transport

* **Table code_trage \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une tranche d’âge d’usager du réseau.
	* Champ id_trage (entier court) \: Identifiant de la tranche d’âge d’usager du réseau
	* Champ lib_trage (chaîne) \: Libellé de la tranche d’âge d’usager du réseau
	* Champ trage (chaîne) \: Libellé abrégé de la tranche d’âge d’usager du réseau

* **Table code_trhor \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de tranche horaire.
	* Champ id_trhor (entier court) \: Identifiant du code de tranche horaire
	* Champ lib_trhor (chaîne) \: Libellé du code de tranche horaire
	* Champ trhor (chaîne) \: Libellé abrégé du code de tranche horaire
	* *Champ hdeb (chaîne) \: Heure de début de la tranche horaire (optionnel)*
	* *Champ hfin (chaîne) \: Heure de fin de la tranche horaire (optionnel)*

* **Table code_type_jour \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un type de jour d'enquête.
	* Champ id_type_jour (entier court) \: Identifiant du type de jour d'enquête
	* Champ lib_type_jour (chaîne) \: Libellé du type de jour d'enquête
	* Champ type_jour (chaîne) \: Libellé abrégé du type de jour d'enquête

* **Table code_type_ligne \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un type de ligne enquêtée.
	* Champ id_type_ligne (entier court) \: Identifiant du type de ligne enquêtée
	* Champ lib_type_ligne (chaîne) \: Libellé du type de ligne enquêtée
	* Champ type_ligne (chaîne) \: Libellé abrégé du type de ligne enquêtée

* **Table trhor_typjour \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de tranche horaire pour un type de jour particulier.
	* Champ id_trhor (entier court) \: Identifiant du code de tranche horaire
	* Champ id_type_jour (entier court) \: Identifiant du type de jour d'enquête
	* Champ lib_trhor (chaîne) \: Libellé du code de tranche horaire
	* Champ trhor (chaîne) \: Libellé abrégé du code de tranche horaire
	* Champ hdeb (chaîne) \: Heure de début de la tranche horaire
	* Champ hfin (chaîne) \: Heure de fin de la tranche horaire

##### 4.1.2. Tables alphanumériques temporaires (SQLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Table charge_tmp \:**
Chaque enregistrement de cette table contient un élément de parcours et ses données de charge.

* **Table enquetes_tmp \:**
Chaque enregistrement de cette table contient une enquête filtrée et ses données.

* **Table offre_tmp \:**
Chaque enregistrement de cette table contient un élément de parcours et ses données d'offre.

#### 4.2. Tables spatiales \:
##### 4.2.1. Tables spatiales du réseau (SpatiaLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Table troncons (Polylignes) \:**
Chaque enregistrement de cette table contient la description graphique d’un tronçon de réseau de transport en commun.
	* Champ id_tron (chaîne) \: Identifiant du tronçon
	* Champ geometry (linestring) \: Description géographique du tronçon

* **Table arrets (Points) \:**
Chaque enregistrement de cette table contient la description graphique d’un arrêt logique de transport en commun.
	* Champ id_arret (entier long) \: Identifiant de l’arrêt
	* Champ nom_arret (chaîne) \: Nom de l’arrêt
	* Champ fictif (entier long) \: Indicateur précisant s’il s’agit d’un arrêt ou d’un nœud de réseau (point singulier)
	* Champ geometry (point) \: Description géographique de l'arrêt

* **Table communes (Polygones) \:**
Chaque enregistrement de cette table contient la description graphique d’une commune et ses données attributaires.
	* Champ id_commune (entier long) \: Identifiant INSEE de la commune
	* Champ nom_commun (chaîne) \: Nom de la commune
	* Champ geometry (multipolygon) \: Description géographique de la commune

* **Table zones_xx (Polygones) \:**
Chaque enregistrement de cette table contient la description graphique d’une zone de type xx.
	* Champ id_zonexx (entier long) \: Identifiant de la zone
	* Champ nom_zonexx (chaîne) \: Nom de la zone
	* Champ geometry (multipolygon) \: Description géographique de la zone

##### 4.2.2. Tables spatiales temporaires (SpatiaLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Il s’agit de tables créées lors de l’affichage des flux entre zones.

* **Table arrets_tmp \:**
Chaque enregistrement de cette table contient un arrêt et ses données de montées et de descentes.

* **Table arrets_tmp_var \:**
Chaque enregistrement de cette table contient un arrêt et ses données de montées ou de descentes croisées avec une variable enquêtée.

* **Table flux_arrets_tmp \:**
Chaque enregistrement de cette table contient un arrêt et ses données de flux associées.

* **Table fluxpp_tmp \:**
Chaque enregistrement de cette table contient un flux entre deux zones, utilisé pour l'analyse des flux principaux.

* **Table flux_zones_tmp \:**
Chaque enregistrement de cette table contient une zone et ses données de flux associées.

* **Table sous_reseau_arrets_tmp \:**
Chaque enregistrement de cette table contient un arrêt du sous-réseau filtré.

* **Table sous_reseau_troncons_tmp \:**
Chaque enregistrement de cette table contient un tronçon du sous-réseau filtré.

* **Table troncons_tmp \:**
Chaque enregistrement de cette table contient un tronçon et ses données de charge.

* **Table zones_tmp \:**
Chaque enregistrement de cette table contient une et ses données de montées et de descentes.

* **Table zones_tmp_var \:**
Chaque enregistrement de cette table contient une zone et ses données de montées ou de descentes croisées avec une variable enquêtée.

##### 4.2.3. Autres tables \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Il s’agit de tables interne à SQLite/SpatiaLite.

### 5. Référence du fichier json de configuration \:

#### 5.1. Généralités \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Le fichier json de configuration doit porter le même nom que la base de données SpatiaLite, seule l'extension les différenciant.

Le fichier json permet, dans une certaine mesure, d'adapter la structure de la base de données aux besoins de l'enquête : variables enquêtées, variables analysables sous forme cartographique, nom des colonnes, etc.

Il permet également de donner les informations permettant à l'application de construire l'interface graphique de l'extension à partir des données spécifiques à chaque enquête.

Par exemple, pour une variable enquêtée ***Variable***, la rubrique json précisera :

- "objlbl": "***lblVariable***" -> le nom de l'objet instancié pour la variable dans l'extension
- "title": "***Libellé pour ma variable***" -> le libellé utilisé pour la variable dans l'interface graphique de l'extension
- "sql": "***select id_variable, lib_variable from code_variable order by id_variable;***" -> la requête sql à utiliser pour retrouver les couples clé-valeur de la variable
- "enqfield": "***var***" -> le nom de la colonne contenant les codes de la variable dans la table ***enquetes***.

Les différentes rubriques de configuration sont décrites dans les paragraphes suivants.

#### 5.2. dctTypeJour \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage de la liste déroulante des types de jour :  
 ```
"cbxTypeJour": {
	"objlbl": "lblTypeJour",
	"title": "Type de jour",
	"sql": "select id_type_jour, lib_type_jour from code_type_jour order by id_type_jour;",
	"enqfield": "typjour"
}
```

#### 5.3. dctCritereSignaletique \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage de la liste de cases à cocher pour chaque variable enquêtée relative à l'usager :  

- Motif de déplacement (origine et destination agrégées) :
 ```
"lwgMotif": {
	"objlbl": "lblMotif",
	"label": "Motifs",
	"sql": "select id_motif, lib_motif from code_motif order by id_motif;",
	"enqfield": "motifod"
}
```

- Titre de transport utilisé :
```
"lwgTitre": {
	"objlbl": "lblTitre",
	"label": "Titres",
	"sql": "select id_titre, lib_titre from code_titre order by id_titre;",
	"enqfield": "titre"
}
```

- Tranche d'âge de l'usager :
```
"lwgTrage": {
	"objlbl": "lblTrage",
	"label": "Tranche d'âge",
	"sql": "select id_trage, lib_trage from code_trage order by id_trage;",
	"enqfield": "trage"
}
```

- Lieu de résidence de l'usager :
```
"lwgResidence": {
	"objlbl": "lblResidence",
	"label": "Résidence",
	"sql": "select id_residence, lib_residence from code_residence order by id_residence;",
	"enqfield": "residence"
}
```

- Personne à mobilité réduite :
```
"lwgPmr": {
	"objlbl": "lblPmr",
	"label": "PMR",
	"sql": "select id_pmr, lib_pmr from code_pmr order by id_pmr;",
	"enqfield": "pmr"
}
```

#### 5.4. dctCritereVoyage \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage de la liste de cases à cocher pour chaque variable enquêtée relative au voyage :

- Tranche horaire du voyage :
```
"lwgTrhor": {
	"objlbl": "lblTrhor",
	"label": "Tranches horaires",
	"sql": "select id_trhor, lib_trhor from code_trhor order by id_trhor;",
	"enqfield": "trhor"
}
```

- Mode de déplacement utilisé en amont du voyage (mode avant) :
```
"lwgModAv": {
	"objlbl": "lwgModAv",
	"label": "Mode avant",
	"sql": "select id_modav, lib_modav from code_modav order by id_modav;",
	"enqfield": "modav"
}
```

- Mode de déplacement utilisé en aval du voyage (mode après) :
```
"lwgModAp": {
	"objlbl": "lwgModAp",
	"label": "Mode après",
	"sql": "select id_modap, lib_modap from code_modap order by id_modap;",
	"enqfield": "modap"
}
```

#### 5.5. dctCritereAutre \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet d'ajouter les informations nécessaires pour le remplissage des listes de case à cocher pour d'autres variables.

#### 5.6. dctZonage \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage des listes de cases à cocher pour chacun des zonages intégrés :

- Zonage communal :
```
"lwgCommunes": {
	"table": "communes",
	"objlbl": "lblCommunes",
	"label": "Communes",
	"sql": "select id_commune, nom_commune from communes order by nom_commune;",
	"enqfield": "id_commune"
}
```

- Zonage spécifique à l'analyse de l'enquête OD :
```
"lwgZonesOD2023": {
	"table": "zones_od_2023",
	"objlbl": "lblZonesOD2023",
	"label": "Zones OD 2023",
	"sql": "select id_zoneod, nom_zoneod from zones_od_2023 order by nom_zoneod;",
	"enqfield": "id_zoneod"
}
```

#### 5.7. dctVariableAnalysable \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet d'ajouter les informations nécessaires à la construction de l'interface utilisateur pour les variables qui peuvent faire l'objet d'une analyse croisée.

Par exemple, pour une variable enquêtée ***Variable***, la rubrique json précisera :

- "tabVar": "***code_variable***" -> le nom de la table contenant les couples clé-valeur pour la variable
- "title": "***Description de la variable***" -> le libellé utilisé pour la variable dans la légende des analyses croisées
- "colVar": "***variable***" -> le nom de la colonne de la table ***enquetes*** contenant les code pour la variable
- "colIdVar": "***id_variable***" -> le nom de la colonne de la table de codage de la variable contenant les clés
- "colLibVar": "***lib_variable***" -> le nom de la colonne de la table de codage de la variable contenant les valeurs

- Motif de déplacement (origine et destination agrégées) :
```
"motifod": {
	"tabVar": "code_motif",
	"title": "Motifs agrégés",
	"colVar": "motifod",
	"colIdVar": "id_motif",
	"colLibVar": "lib_motif"
}
```

- Titre de transport utilisé par l'usager :
```
"titre": {
	"tabVar": "code_titre",
	"title": "Titres de transport",
	"colVar": "titre",
	"colIdVar": "id_titre",
	"colLibVar": "lib_titre"
}
```

- Statut socio-professionnel de l'usager :
```
"statut": {
	"tabVar": "code_statut",
	"title": "Statut",
	"colVar": "statut",
	"colIdVar": "id_statut",
	"colLibVar": "lib_statut"
}
```

- Tranche horaire du voyage :
```
"trhor": {
	"tabVar": "code_trhor",
	"title": "Tranches horaires",
	"colVar": "trhor",
	"colIdVar": "id_trhor",
	"colLibVar": "lib_trhor"
}
```

- Mode de déplacement utilisé en amont du voyage (mode avant) :
```
"modav": {
	"tabVar": "code_modav",
	"title": "Mode avant",
	"colVar": "modav",
	"colIdVar": "id_modav",
	"colLibVar": "lib_modav"
}
```

- Mode de déplacement utilisé en aval du voyage (mode après) :
```
"modap": {
	"tabVar": "code_modap",
	"title": "Mode après",
	"colVar": "modap",
	"colIdVar": "id_modap",
	"colLibVar": "lib_modap"
}
```

En plus des variables enquêtées analysables des analyses croisées peuvent être faite pour la ligne enquêtée, la première ligne amont du voyage, et la première ligne aval du voyage.

- Ligne enquêtée :
```
"ligne":  {
	"tabVar": "code_ligne_var",
	"title": "Lignes",
	"colVar": "ligne",
	"colIdVar": "id_ligne",
	"colLibVar": "lib_ligne"
}
```

- Première ligne empruntée en amont du voyage :
```
"lignem1":  {
	"tabVar": "code_ligne_coram",
	"title": "Lignes amont",
	"colVar": "lignem1",
	"colIdVar": "id_ligne",
	"colLibVar": "lib_ligne"
}
```

- Première ligne empruntée en aval du voyage :
```
"lignep1":  {
	"tabVar": "code_ligne_corav",
	"title": "Lignes aval",
	"colVar": "lignep1",
	"colIdVar": "id_ligne",
	"colLibVar": "lib_ligne"
}
```

#### 5.8. dctZonageAnalysable \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet d'ajouter les informations nécessaires à la construction de l'interface utilisateur pour les zonages qui peuvent faire l'objet d'une analyse cartographique.

Par exemple, pour une variable enquêtée ***Variable***, la rubrique json précisera :

- "tabVar": "***code_variable***" -> le nom de la table contenant les couples clé-valeur pour la variable
- "title": "***Description de la variable***" -> le libellé utilisé pour la variable dans la légende des analyses croisées
- "colVar": "***variable***" -> le nom de la colonne de la table ***enquetes*** contenant les code pour la variable
- "colIdVar": "***id_variable***" -> le nom de la colonne de la table de codage de la variable contenant les clés
- "colLibVar": "***lib_variable***" -> le nom de la colonne de la table de codage de la variable contenant les valeurs

- Zonage communal :
```
"communes": {
	"tabZone": "communes",
	"title": "Communes",
	"colIdZone": "id_commune",
	"colNomZone": "nom_commune"
}
```

- Zonage spécifique à l'analyse de l'enquête OD :
```
"zones_od_2023": {
	"tabZone": "zones_od_2023",
	"title": "Zones OD 2023",
	"colIdZone": "id_zoneod",
	"colNomZone": "nom_zoneod"
}
```

#### 5.9. dctParam \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient des informations de paramétrage de l'application.

- Système de coordonnées et de projection géographiques utilisé pour les couches géographiques :
```
"epsg": 2154,
```

- Nom du réseau affiché dans la liste des couches de QGIS :
```
"nomreseau": "Réseau RANDOM 2023",
```

- Nombre total de lignes enquêtées
```
"nblignes": 6,
```

- Noms des colonnes de la table ***enquetes*** :
```
"typlign": "typlign",
"typjour": "typjour",
"ligne": "ligne",
"sens": "sens",
"parcours": "parcours",
"course": "course",
"poidsligne": "poidsl",
"poidsreseau": "poidsr",
"arretdebvoy": "arreta2",
"arretfinvoy": "arretb2",
"arretdebdep": "arretdeb2",
"arretfindep": "arretfin2",
"ligneamont": "lignem1",
"ligneaval": "lignep1",
```

- Ligne sélectionnée par défaut pour le filtrage des enquêtes :
```
"defaultligne": "1001",
```

- Possibilité d'affichage du sous-réseau avec les couleurs de lignes utilisées pour les document commerciaux :
```
"couleurslignes": "True",
```

- Nom de la table de codage des tranches horaires :
```
"tabtrhor": "code_trhor"
```


#### 5.10. lstColors \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet de préciser la liste des couleurs utilisées pour les secteurs dans les analyses par diagrammes.

```
"lstColors": [
	[232,92,99]
	[R, V, B],
	...
]
```

### Annexe 1 \: Liste des fichiers de QMapOD \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Dossier /QMapOD \:**  
\__init__.py  
metadata.txt  
plugin_upload.py  
pylintrc  
QMapOD.ini  
qmapod.py  
QMapOD.qgs  
qmapod\_anal_arrets.py  
qmapod\_anal_arrets_var.py  
qmapod\_anal_serpent.py  
qmapod\_anal_zones.py  
qmapod\_anal_zones_var.py  
qmapod\_config.py  
qmapod\_dialog.py  
qmapod\_dialog_base.py  
qmapod\_dialog_base.ui  
qmapod_dock.py  
qmapod\_dock_base.py  
qmapod\_dock_base.ui  
qmapod_filtrage.py  
qmapod\_flux_arrets.py  
qmapod\_flux_zones.py  
qmapod\_sous_reseau.py  
QMapODDefaut.ini  
resources.qrc  
resources_rc.py  

* **Dossier /QMapOD/data \:**  
qmapod_xxx.sqlite  
qmapod_xxx.json  

* **Dossier /QMapOD/doc \:**  
aide_qmapod.html  
aide_qmapod.pdf  
stackedit.css  

* **Dossier /QMapOD/doc/fonts \:**  
cursive_standard-webfont.woff  
dir.txt  
fontello.eot  
fontello.svg  
fontello.ttf  
fontello.woff  
fontface-fontello.css.ejs  
glyphicons-halflings-regular.eot  
glyphicons-halflings-regular.svg  
glyphicons-halflings-regular.ttf  
glyphicons-halflings-regular.woff  
glyphicons-halflings-regular.woff2  
PTSans-BoldItalic-webfont.woff  
PTSans-Bold-webfont.woff  
PTSans-Italic-webfont.woff  
PTSans-Regular-webfont.woff  
SourceCodePro-Bold-webfont.woff  
SourceCodePro-Regular-webfont.woff  
SourceSansPro-BoldItalic-webfont.woff  
SourceSansPro-Bold-webfont.woff  
SourceSansPro-Italic-webfont.woff  
SourceSansPro-LightItalic-webfont.woff  
SourceSansPro-Light-webfont.woff  
SourceSansPro-Regular-webfont.woff  

* **Dossier /QMapOD/doc/img \:**  
barre_outils.png  
boutons\_filtrage_carto.png  
boutons\_filtrage_enquetes.png  
boutons\_parametres.png  
btn_aide.png  
btn_apropos.png  
btn_couches.png  
btn\_d_arrets.png  
btn\_d_zones.png  
btn\_dvar_arrets.png  
btn\_dvar_zones.png  
btn\_eff_flux_arrets.png  
btn\_eff_flux_zones.png  
btn\_m_arrets.png  
btn\_m_zones.png  
btn\_md_arrets.png  
btn\_md_zones.png  
btn\_mvar_arrets.pn  g
btn\_mvar_zones.png  
btn_panneau.png  
btn_parametres.png
btn_qconsolidate.png  
btn\_serp_charge.png  
btn\_serp_offre.png  
btn\_serp_perf.png  
diagrammes_etiquettes.png  
dlg_qconsolidate.png  
enquetes_mode_analyse.png  
enquetes_voyage_deplacement.png  
filtrage.png  
lst_variables.png  
lst_zonages.png  
menu.png  
onglet_arrets.png  
onglet_flux.png  
onglet_troncons.png  
onglet_zonages.png  
outils.png  
param_arrets.png  
param\_flux_arrets.png  
param\_flux_zones.png  
param\_flux_zones_degrade.png  
param\_flux_zones_diagram.png  
param\_flux_zones_oursins.png  
param\_sous_reseau.png  
param_troncons.png  
param_zonages.png  
qmapod.png  
rub_arrets.png  
rub_autres.png  
rub_recap.png  
rub_signaletique.png  
rub\_sous_reseau.png  
rub_voyage.png  
rub_zonages.png  
sous\_reseau_diagrammes_etiquettes.png  
tbn\_flux_arrets_d.png  
tbn\_flux_arrets_m.png  
tbn\_flux_zones_d.png  
tbn\_flux_zones_m.png  

* **Dossier /QMapOD/icons \:**  
aide.svg  
apropos.svg  
bhns.svg  
bus.svg  
bus_bleu.svg  
bus_jaune.svg  
bus_mauve.svg  
bus_orange.svg  
bussub.svg  
busurb.svg  
busway.svg  
chronobus.svg
config.svg  
d_arret.svg  
d_zone.svg  
dvar_arret.svg  
dvar_zone.svg  
fleche_circulaire.svg  
fleche_droite.svg  
fleche_gauche.svg  
flux_arret_dm.svg  
flux_arret_md.svg  
flux_zone_d.svg  
flux_zone_dm.svg  
flux_zone_m.svg  
flux_zone_md.svg  
layers.svg  
m_arret.svg  
m_zone.svg  
mapod.svg  
md_arret.svg  
md_arret_del.svg  
md_zone.svg  
md_zone_del.svg  
mdvar_arret_del.svg  
mdvar_zone_del.svg  
metro.svg  
mvar_arret.svg  
mvar_zone.svg  
navibus.svg  
qmapod.svg  
reseaux.svg  
s_charge.svg  
s_del.svg  
s_offre.svg  
s_perf.svg  
sous_reseau.svg  
tram.svg  
tramway.svg  

----------

[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
