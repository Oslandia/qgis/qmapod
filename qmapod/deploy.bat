rem Emplacement des fichiers sources
set PLUGIN_SOURCE=D:\QMapOD\QMapOD2_git

rem Paramétrage de l'environnement d'exécution pour QGIS 3.34.9
@echo off
set OSGEO4W_ROOT=C:\Program Files\QGIS 3.34.9
call "%OSGEO4W_ROOT%\bin\o4w_env.bat"

@echo on

rem Déploiement du plugin
set PLUGIN_DIR=%userprofile%\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\QMapOD2

rem xcopy /S /Y %PLUGIN_SOURCE%\*.* %PLUGIN_DIR%
xcopy /S /Y %PLUGIN_SOURCE%\icon.png %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\icons\*.* %PLUGIN_DIR%\icons\
xcopy /S /Y %PLUGIN_SOURCE%\doc\*.* %PLUGIN_DIR%\doc\
xcopy /S /Y %PLUGIN_SOURCE%\data\*.* %PLUGIN_DIR%\data\
xcopy /S /Y %PLUGIN_SOURCE%\composers\*.* %PLUGIN_DIR%\composers\

xcopy /S /Y %PLUGIN_SOURCE%\__init__.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\metadata.txt %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\QMapOD.ini %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\QMapOD.qgs %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_anal_arrets.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_anal_arrets_var.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_anal_serpent.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_anal_zones.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_anal_zones_var.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_config.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_dialog.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_dialog_base.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_dialog_base.ui %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_dock.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_dock_base.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_dock_base.ui %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_filtrage.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_flux_arrets.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_flux_zones.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_flux_principaux.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_sous_reseau.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_test_tool.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\qmapod_lab.py %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\QMapODDefault.ini %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\resources.qrc %PLUGIN_DIR%\
xcopy /S /Y %PLUGIN_SOURCE%\resources_rc.py %PLUGIN_DIR%\

exit /B
