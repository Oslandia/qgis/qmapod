"""
/***************************************************************************
 QMapODAnalSerpent
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

from qgis.core import (
    QgsDataSourceUri,
    QgsGraduatedSymbolRenderer,
    QgsLayerTreeLayer,
    QgsLineSymbol,
    QgsPalLayerSettings,
    QgsProject,
    QgsRandomColorRamp,
    QgsRendererRange,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)

# Import des librairies PyQt
from qgis.PyQt.QtGui import QColor, QFont
from qgis.PyQt.QtWidgets import QApplication

# Import des librairies QGIS


module_logger = logging.getLogger("QMapOD.anal_serpent")


class QMapODAnalSerpent(object):
    """
    Analyses cartographiques par serpents
    """

    def __init__(self, iface, tbn, clsFiltrage, typAnal, qmapodConfig, qmapodSettings):
        """
        Constructor
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Récupération de l'instance de la classe filtrage
        self.currentFiltrage = clsFiltrage
        # Type d'analyse ('c' = charge, 'o' = offre, 'p' = performance)
        self.typAnal = typAnal
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Couche générée
        self.serpLayer = None

        self.logger = logging.getLogger("QMapOD.anal_serpent.QMapODAnalSerpent")
        self.logger.info("Creating an instance of QMapODAnalSerpent")

    def _addLayerTronconsTmp(self):
        """
        Ajout de la couche troncons_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "c":
            strSerpent = "Charge"
        elif self.typAnal == "o":
            strSerpent = "Offre"
        elif self.typAnal == "p":
            strSerpent = "Performance"

        strNom = Template("Serpent de $str_serpent").substitute(str_serpent=strSerpent)

        # Ajout de la couche
        uriSerpent = QgsDataSourceUri()
        uriSerpent.setDatabase(self.db)
        uriSerpent.setDataSource("", "troncons_tmp", "geometry")
        serpentLayer = QgsVectorLayer(uriSerpent.uri(), strNom, "spatialite")
        if serpentLayer.isValid():
            # Ajout des couches et déplacement au-dessus des couches existantes
            # (api QgsLayerTree)
            root = QgsProject.instance().layerTreeRoot()

            self.serpLayer = QgsProject.instance().addMapLayer(serpentLayer, False)
            self.serpLayer.destroyed.connect(self._deleted)
            serpentTreeLayer = QgsLayerTreeLayer(serpentLayer)
            serpentTreeLayer.setName(serpentLayer.name())
            root.insertChildNode(0, serpentTreeLayer)

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis
        Désactivation de l'outil
        """

        self.serpLayer = None
        self.tbn.setChecked(False)

    # -------------------------------------------------------------------------

    def _getPoids(self, nbLignes):
        """
        Choix du mode de pondération à utiliser
        TODO : à supprimer (plus utilisé) ?
        """
        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # --------------------------------------------------------------------------

    def _genSqlOffre(self):

        # Récupération de la clause where de filtrage sur le sous-réseau
        strWhere = self.currentFiltrage.sqlSousReseau()
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/anal_serpent_1.sql")
        ) as f:
            strSql = Template(f.read()).substitute(str_where=strWhere)
        return strSql

    def _genSqlCharge(self):
        """
        Génération de la requête de création de la table troncons_tmp pour la charge
        """
        # Récupération de la clause where de filtrage sur le sous-réseau
        strWhere = self.currentFiltrage.sqlSousReseau()
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/anal_serpent_2.sql")
        ) as f:
            strSql = Template(f.read()).substitute(str_where=strWhere)
        return strSql

    def _genSqlPerf(self):
        """
        Génération de la requête de création de la table troncons_tmp
        pour la performance
        """
        # Récupération de la clause where de filtrage sur le sous-réseau
        strWhere = self.currentFiltrage.sqlSousReseau()
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/anal_serpent_4.sql")
        ) as f:
            strSql = Template(f.read()).substitute(str_where=strWhere)
        return strSql

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """
        # Récupération de la valeur maximale d'une colonne d'une table
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Récupération de la valeur maximale d'une colonne d'une table
            s = Template(
                "\nSELECT max($col_) \n\
                     FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            max = cursor.fetchone()[0]
            if max is None:
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _createTableTronconsTmp(self, strSql):
        """
        Création de la table troncons_tmp
        """

        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table troncons_tmp si elle existe
            s = "DROP TABLE IF EXISTS troncons_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            # Création de la table troncons_tmp
            self.logger.debug(strSql)
            cursor.execute(strSql)
            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns\n\
               WHERE f_table_name = 'troncons_tmp'"
            self.logger.debug(s)
            cursor.execute(s)
            # Insertion de la déclaration de la colonne geometry
            # (type 2 = polyligne)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_serpent_3.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _labelSerpent(self, exp):
        """
        Etiquetage de la couche
        """

        # Paramètres d'étiquetage
        intMode = int(self.qmapodSettings.value("params/cbxLabelSerpent", 4))
        # blnEnabled = True
        if intMode == 0:
            self.serpLayer.setLabelsEnabled(True)
            self.serpLayer.setLabeling(None)
            return
        elif intMode == 1:
            strField = "id_tron"
            blnExp = False
        elif intMode == 2:
            strField = exp
            blnExp = False
        else:
            return

        palLayer = QgsPalLayerSettings()
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        palLayer.placement = QgsPalLayerSettings.Line
        palLayer.placementFlags = QgsPalLayerSettings.AboveLine
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 7, QFont.Normal))
        txtFormat.setSize(8)
        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.serpLayer.setLabelsEnabled(True)
        self.serpLayer.setLabeling(labeling)

    # -------------------------------------------------------------------------

    def delSerpent(self):
        """
        Suppression de l'analyse en serpents
        """

        if isinstance(self.serpLayer, QgsVectorLayer):
            QgsProject.instance().removeMapLayer(self.serpLayer.id())
        self.serpLayer = None

    # -------------------------------------------------------------------------

    def addSerpent(self):
        """
        Choix de la méthode d'analyse selon le type de serpent
        """

        # Sélection des champs à prendre en compte pour le calcul
        # de la valeur maximale et de la couleur de représentation
        if self.typAnal == "o":
            exp = "offre"
            colSerp = self.qmapodSettings.value(
                "params/btnColSOffre", QColor(255, 0, 0, 255), QColor
            )
            strSql = self._genSqlOffre()
            self._createTableTronconsTmp(strSql)
        elif self.typAnal == "c":
            exp = "charge"
            colSerp = self.qmapodSettings.value(
                "params/btnColSCharge", QColor(255, 0, 0, 255), QColor
            )
            strSql = self._genSqlCharge()
            self._createTableTronconsTmp(strSql)
        elif self.typAnal == "p":
            exp = "perf"
            colSerp = self.qmapodSettings.value(
                "params/btnColSPerf", QColor(255, 0, 0, 255), QColor
            )
            strSql = self._genSqlPerf()
            self._createTableTronconsTmp(strSql)

        self._addLayerTronconsTmp()

        # Récupération du paramètre de transparence
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencySerpent", 30)
        )
        fltOpacity = (100 - intTransparency) / 100

        gr = QgsGraduatedSymbolRenderer()
        # Création d'un GraduatedSymbolRenderer temporaire pour obtenir
        # une classification
        symbol = QgsLineSymbol()

        colorRamp = QgsRandomColorRamp()
        intNbClasses = int(self.qmapodSettings.value("params/spxSerpNbClasses", 5))

        # Méthodes de classification disponibles :
        # EqualInterval : Intervalles égaux
        # Quantile : Méthode des quantiles (effectifs égaux)
        # Jenks : Intervalles naturels de Jenks
        # StdDev : Déviation standard (écart-type)
        # Pretty : Intervalles jolis (arrondis)

        dctMode = {
            0: QgsGraduatedSymbolRenderer.EqualInterval,
            1: QgsGraduatedSymbolRenderer.Quantile,
            2: QgsGraduatedSymbolRenderer.Jenks,
            3: QgsGraduatedSymbolRenderer.StdDev,
            4: QgsGraduatedSymbolRenderer.Pretty,
        }
        objMode = dctMode[
            int(self.qmapodSettings.value("params/cbxSerpClassification", 2))
        ]

        grtmp = QgsGraduatedSymbolRenderer.createRenderer(
            self.serpLayer, exp, intNbClasses, objMode, symbol, colorRamp
        )

        # Récupération de la classification et création
        # du GraduatedSymbolRenderer définitif
        fltMinWidth = float(self.qmapodSettings.value("params/spxSerpMinWidth", 0.2))
        fltMaxWidth = float(self.qmapodSettings.value("params/spxSerpMaxWidth", 2.0))
        fltWidth = fltMinWidth
        fltIncWidth = (fltMaxWidth - fltMinWidth) / intNbClasses
        rangeList = []

        for r in grtmp.ranges():
            symLine = QgsLineSymbol.createSimple(
                {
                    "width": str(fltWidth),
                    "color": Template(
                        "$col_serp_red, $col_serp_green, $col_serp_blue"
                    ).substitute(
                        col_serp_red=colSerp.red(),
                        col_serp_green=colSerp.green(),
                        col_serp_blue=colSerp.blue(),
                    ),
                    "capstyle": "round",
                    "joinstyle": "round",
                }
            )
            strLabel = Template("De $lower_value à $upper_value").substitute(
                lower_value=int(round((max(1, r.lowerValue())))),
                upper_value=int(round((r.upperValue()))),
            )
            rangeList.append(
                QgsRendererRange(
                    max(1, r.lowerValue()), r.upperValue(), symLine, strLabel
                )
            )
            fltWidth += fltIncWidth

        gr = QgsGraduatedSymbolRenderer(exp, rangeList)
        gr.setMode(QgsGraduatedSymbolRenderer.Custom)
        grtmp = None

        # Application du GraduatedSymbolRenderer à la couche
        self.serpLayer.setRenderer(gr)
        self.serpLayer.setOpacity(fltOpacity)

        # Etiquetage de la couche
        self._labelSerpent(exp)

        # Pour contournement du bug sur majArretMD
        self.tbn.setChecked(True)

    # -------------------------------------------------------------------------

    def majSerpent(self):
        """
        Choix de la méthode de mise à jour selon le type de serpent
        """

        # Sélection des champs à prendre en compte pour le calcul
        # de la valeur maximale et de la couleur de représentation
        if self.typAnal == "o":
            exp = "offre"
            colSerp = self.qmapodSettings.value(
                "params/btnColSOffre", QColor(255, 0, 0, 255), QColor
            )
            strSql = self._genSqlOffre()
            self._createTableTronconsTmp(strSql)
        elif self.typAnal == "c":
            exp = "charge"
            colSerp = self.qmapodSettings.value(
                "params/btnColSCharge", QColor(255, 0, 0, 255), QColor
            )
            strSql = self._genSqlCharge()
            self._createTableTronconsTmp(strSql)
        elif self.typAnal == "p":
            exp = "perf"
            colSerp = self.qmapodSettings.value(
                "params/btnColSPerf", QColor(255, 0, 0, 255), QColor
            )
            strSql = self._genSqlPerf()
            self._createTableTronconsTmp(strSql)

        # Récupération du paramètre de transparence
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencySerpent", 30)
        )
        fltOpacity = (100 - intTransparency) / 100

        grtmp = self.serpLayer.renderer()
        intNbClasses = int(self.qmapodSettings.value("params/spxSerpNbClasses", 5))

        grtmp.updateClasses(
            self.serpLayer, QgsGraduatedSymbolRenderer.Jenks, intNbClasses
        )

        # Récupération de la classification et création
        # du GraduatedSymbolRenderer définitif
        fltMinWidth = float(self.qmapodSettings.value("params/spxSerpMinWidth", 0.2))
        fltMaxWidth = float(self.qmapodSettings.value("params/spxSerpMaxWidth", 2.0))
        fltWidth = fltMinWidth
        fltIncWidth = (fltMaxWidth - fltMinWidth) / intNbClasses
        rangeList = []

        for r in grtmp.ranges():
            symLine = QgsLineSymbol.createSimple(
                {
                    "width": str(fltWidth),
                    "color": Template(
                        "$col_serp_red, $col_serp_green, $col_serp_blue"
                    ).substitute(
                        col_serp_red=colSerp.red(),
                        col_serp_green=colSerp.green(),
                        col_serp_blue=colSerp.blue(),
                    ),
                    "capstyle": "round",
                    "joinstyle": "round",
                }
            )
            strLabel = Template("De $lower_value à $upper_value").substitute(
                lower_value=int(round((max(1, r.lowerValue())))),
                upper_value=int(round((r.upperValue()))),
            )
            rangeList.append(
                QgsRendererRange(
                    max(1, r.lowerValue()), r.upperValue(), symLine, strLabel
                )
            )
            fltWidth += fltIncWidth

        gr = QgsGraduatedSymbolRenderer(exp, rangeList)

        gr.setGraduatedMethod(QgsGraduatedSymbolRenderer.GraduatedSize)
        gr.setMode(QgsGraduatedSymbolRenderer.Custom)
        grtmp = None

        self.serpLayer.setRenderer(gr)
        self.serpLayer.setOpacity(fltOpacity)
        self.serpLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.serpLayer.id())
