"""
/***************************************************************************
 QMapODTestTool
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2014 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from string import Template

# Import des librairies QGIS
from qgis.core import (
    QgsDataSourceUri,
    QgsFeature,
    QgsLayerTreeLayer,
    QgsProject,
    QgsRenderContext,
    QgsSimpleFillSymbolLayer,
    QgsVectorLayer,
)
from qgis.gui import QgsMapToolIdentify

# Import des librairies PyQt
from qgis.PyQt.QtCore import Qt, pyqtSignal
from qgis.PyQt.QtGui import QCursor
from qgis.PyQt.QtWidgets import QApplication


class QMapODTestTool(QgsMapToolIdentify):
    geomIdentified = pyqtSignal(QgsVectorLayer, QgsFeature)
    geomUnidentified = pyqtSignal()

    def __init__(self, iface, dock, tbn, typAnal, qmapodConfig, qmapodSettings):
        """
        Outil de test pour amélioration de l'ergonomie
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        self.dock = dock
        self.canvas = iface.mapCanvas()

        super(QgsMapToolIdentify, self).__init__(self.canvas)
        self.cursor = QCursor(Qt.CrossCursor)

        # Récupération de la référence au panneau ancrable
        self.dock = dock
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        self.typAnal = typAnal
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings

        # Liste des zones sélectionnées
        self.lstZones = []
        self.selLayer = None
        self.fzGroup = None

    def activate(self):
        self.canvas.setCursor(self.cursor)

    def canvasReleaseEvent(self, mouseEvent):
        results = self.identify(
            mouseEvent.x(), mouseEvent.y(), self.ActiveLayer, self.VectorLayer
        )

        if len(results) > 0:
            self.geomIdentified.emit(results[0].mLayer, QgsFeature(results[0].mFeature))
            self.logger.info("Identified")
        else:
            self.geomUnidentified.emit()
            # self.geomIdentified.emit(None, None)
            self.delFluxZones()
            self.logger.info("UnIdentified")

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGIS
        Désactivation de l'outil
        """
        pass

    # -------------------------------------------------------------------------

    def processFeature(self, feature):
        tabZone = "zones_od"
        colIdZone = "id_zoneod"
        strNom = "Zones OD"
        # Ajout/Suppression de la zone sélectionnée
        # à la liste de zones de l'outil
        modifiers = QApplication.keyboardModifiers()
        if modifiers == Qt.ShiftModifier or modifiers == Qt.ControlModifier:
            if feature.attribute(colIdZone) in self.lstZones:
                self.lstZones.remove(feature.attribute(colIdZone))
            else:
                self.lstZones.append(feature.attribute(colIdZone))
        else:
            self.lstZones[:] = []
            self.lstZones.append(feature.attribute(colIdZone))

        self.logger.info(self.lstZones)

        # Ajout de la couche des zones sélectionnés si elle n'existe pas
        if self.selLayer is None:
            uri = QgsDataSourceUri()
            uri.setDatabase(self.qmapodConfig.db)
            uri.setDataSource("", tabZone, "geometry")
            self.selLayer = QgsVectorLayer(uri.uri(), strNom, "spatialite")
            self.selLayer.setSubsetString(
                Template("$col_id_zone IN($str)").substitute(
                    col_id_zone=colIdZone, str=", ".join(map(str, self.lstZones))
                )
            )
            if self.selLayer.isValid():
                # Ajout des couches et déplacement au-dessus
                # des couches existantes (api QgsLayerTree)
                root = QgsProject.instance().layerTreeRoot()
                self.fzGroup = root.insertGroup(0, "Flux par zones")
                self.selLayerId = QgsProject.instance().addMapLayer(
                    self.selLayer, False
                )
                self.selLayer.destroyed.connect(self._deleted)
                selTreeLayer = QgsLayerTreeLayer(self.selLayer)
                selTreeLayer.setName(self.selLayer.name())
                self.fzGroup.insertChildNode(0, selTreeLayer)

                symbolLayer = QgsSimpleFillSymbolLayer()

                self.selLayer.renderer().symbols(QgsRenderContext())[
                    0
                ].changeSymbolLayer(0, symbolLayer)
                self.selLayer.triggerRepaint()
                self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())
        # Mise à jour du sous-ensemble de la couche dans le cas contraire
        else:
            self.selLayer.setSubsetString(
                Template("$col_id_zone IN($str)").substitute(
                    col_id_zone=colIdZone, str=", ".join(map(str, self.lstZones))
                )
            )

    # -------------------------------------------------------------------------

    def delFluxZones(self):
        """
        Suppression de l'affichage des flux par zones de montées / descentes
        """
        pass

    # -------------------------------------------------------------------------
