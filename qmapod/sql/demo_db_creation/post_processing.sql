INSERT INTO arrets (id_arret, nom_arret, fictif, geometry)
SELECT id_arret, nom_arret, fictif, geometry
FROM arrets_
WHERE id_arret in (
    SELECT id_arret
    FROM parcours
    WHERE id_ligne = 1001
        OR id_ligne = 4
        OR id_ligne = 10
        OR id_ligne = 11
        OR id_ligne = 12
        OR id_ligne = 52
);
SELECT DiscardGeometryColumn('arrets_','geometry');

CREATE INDEX idx_arrets_geometry
ON arrets (geometry);
DROP TABLE arrets_;


INSERT INTO troncons (id_tron, geometry)
SELECT id_tron, geometry
FROM troncons_
WHERE substr(id_tron, 1, instr(id_tron, '\')-1) in (
    SELECT id_arret
    FROM parcours
    WHERE id_ligne = 1001
        OR id_ligne = 4
        OR id_ligne = 10
        OR id_ligne = 11
        OR id_ligne = 12
        OR id_ligne = 52
)
AND substr(id_tron, instr(id_tron, '\')+1, length(id_tron)) in (
    SELECT id_arret
    FROM parcours
    WHERE id_ligne = 1001
        OR id_ligne = 4
        OR id_ligne = 10
        OR id_ligne = 11
        OR id_ligne = 12
        OR id_ligne = 52
);
SELECT DiscardGeometryColumn('troncons_','geometry');
DROP TABLE troncons_;

CREATE INDEX idx_troncons_geometry
ON troncons (geometry);


INSERT INTO communes (id_commune, nom_commune, geometry)
SELECT id_commune, nom_commu0, st_multi(geometry)
FROM communes_
WHERE id_commune in (
    SELECT id_commune
    FROM communes_
    INNER JOIN arrets ON st_within(arrets.geometry, communes_.geometry)
);
SELECT DiscardGeometryColumn('communes_','geometry');

CREATE INDEX idx_communes_geometry
ON communes (geometry);

DROP TABLE communes_;


INSERT INTO zones_od (id_zoneod, nom_zoneod, geometry)
SELECT id_zoneod, nom_zoneod, geometry
FROM zones_od_
WHERE id_zoneod in (
    SELECT id_zoneod
    FROM zones_od_
    INNER JOIN arrets ON st_within(arrets.geometry, zones_od_.geometry)
);
SELECT DiscardGeometryColumn('zones_od_','geometry');

CREATE INDEX idx_zones_od_geometry
ON zones_od (geometry);

DROP TABLE zones_od_;


INSERT INTO arrets_zones
SELECT
    a.id_arret,
    a.nom_arret,
    c.id_commune,
    c.nom_commune,
    z.id_zoneod,
    z.nom_zoneod
FROM arrets a
INNER JOIN communes c ON st_within(a.geometry, c.geometry)
INNER JOIN zones_od z ON st_within(a.geometry, z.geometry)
WHERE fictif = 0;

CREATE INDEX idx_id_arret
ON arrets_zones(id_arret);

CREATE INDEX idx_id_commune
ON arrets_zones(id_commune);

CREATE INDEX idx_id_zoneod
ON arrets_zones(id_zoneod);
