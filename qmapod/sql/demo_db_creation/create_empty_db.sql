/*
 *
 * NB : la table offre est facultative
 * la table offre_tmp est générée :
 * - + à partir de la table offre si celle-ci est présente (enquêtes les plus anciennes) ;
 * - à partir de la table courses si la table offre n'est pas présente (permet de définir
 *   l'offre en fonction des périodes horaires, appliqué sur les enquêtes les plus récentes).
 *
 */


DROP TABLE IF EXISTS code_type_jour;
CREATE TABLE code_type_jour (
    id_type_jour INTEGER PRIMARY KEY, -- Identifiant du type de jour d'enquête
    lib_type_jour TEXT, -- Libellé du type de jour d'enquête
    type_jour TEXT -- Libellé abrégé du type de jour d'enquête
);

DROP TABLE IF EXISTS code_ligne;
CREATE TABLE code_ligne (
    id_ligne INTEGER PRIMARY KEY, -- Identifiant de la ligne
    num_ligne TEXT, -- Numéro de la ligne
    lib_ligne TEXT, -- Libellé de la ligne
    typ_ligne INTEGER, -- Type de la ligne
    rvb TEXT -- Couleur de la ligne (codes R/V/B)
);

DROP TABLE IF EXISTS code_sens;
CREATE TABLE code_sens (
    id_sens INTEGER, -- Identifiant du sens
    id_ligne INTEGER, -- Identifiant de la ligne
    lib_sens TEXT -- Libellé du sens
);

DROP TABLE IF EXISTS parcours;
CREATE TABLE parcours (
    id_ligne INTEGER, -- Identifiant de la ligne
    id_sens INTEGER, -- Identifiant de la ligne
    id_parcours INTEGER, -- Identifiant du parcours de la ligne
    id_arret INTEGER, -- Identifiant de l’arrêt
    ordre INTEGER, -- Numéro d’ordre de l’arrêt pour la ligne, le sens et le parcours considéré
    desserte INTEGER, -- Indicateur précisant si l’arrêt est desservi pour le parcours considéré
    id_tron TEXT -- Identifiant du tronçon reliant l’arrêt considéré à l’arrêt suivant
);

DROP TABLE IF EXISTS courses;
CREATE TABLE courses (
    id_ligne INTEGER, -- Identifiant de la ligne
    num_ligne TEXT, -- Numéro de la ligne
    id_sens INTEGER, -- Identifiant du sens de la ligne
    id_parcours INTEGER, -- Identifiant du parcours de la ligne
    id_course INTEGER, -- Identifiant de la course
    id_type_jour INTEGER, -- Identifiant du jour de validité de la course
    id_type_ligne INTEGER, -- Identifiant de la ligne
    hdeb TEXT, -- Heure de départ de la course
    hfin TEXT, -- Heure d'arrivée de la course
    id_arret_deb INTEGER, -- Identifiant de l’arrêt de début de la course
    nom_arret_deb TEXT, -- Nom de l’arrêt de début de la course
    id_arret_fin INTEGER, -- Identifiant de l’arrêt de fin de la course
    nom_arret_fin TEXT, -- Nom de l’arrêt de fin de la course
    poids INTEGER -- Poids de la course (si saisie par tranche horaire)
);

DROP TABLE IF EXISTS code_residence;
CREATE TABLE code_residence (
    id_residence INTEGER PRIMARY KEY, -- Identifiant du code INSEE de la commune de résidence
    lib_residence TEXT, -- Libellé de la commune de résidence
    residence TEXT -- Libellé abrégé du code de la commune de résidence
);

DROP TABLE IF EXISTS code_modav;
CREATE TABLE code_modav (
    id_modav INTEGER PRIMARY KEY, -- Identifiant du code de mode de déplacement amont
    lib_modav TEXT, -- Libellé du code de mode de déplacement amont
    modav TEXT -- Libellé abrégé du code de mode de déplacement amont
);

DROP TABLE IF EXISTS code_modap;
CREATE TABLE code_modap (
    id_modap INTEGER PRIMARY KEY, -- Identifiant du code de mode de déplacement aval
    lib_modap TEXT, -- Libellé du code de mode de déplacement aval
    modap TEXT -- Libellé abrégé du code de mode de déplacement aval
);

DROP TABLE IF EXISTS code_motif;
CREATE TABLE code_motif (
    id_motif INTEGER PRIMARY KEY, -- Identifiant du code de motif de déplacement agrégé
    lib_motif TEXT, -- Libellé du code de motif de déplacement agrégé
    motifod TEXT -- Libellé abrégé du code de motif de déplacement agrégé
);

DROP TABLE IF EXISTS code_pmr;
CREATE TABLE code_pmr (
    id_pmr INTEGER, -- Identifiant du code de personne à mobilité réduite
    lib_pmr TEXT, -- Libellé du code de personne à mobilité réduite
    pmr TEXT -- Libellé abrégé du code de personne à mobilité réduite
);

DROP TABLE IF EXISTS code_titre;
CREATE TABLE code_titre (
    id_titre INTEGER PRIMARY KEY, -- Identifiant du code de titre de transport
    lib_titre TEXT, -- Libellé du code de titre de transport
    titre TEXT -- Libellé abrégé du code de titre de transport
);

DROP TABLE IF EXISTS code_trage;
CREATE TABLE code_trage (
    id_trage INTEGER PRIMARY KEY, -- Identifiant de la tranche d’âge d’usager du réseau
    lib_trage TEXT, -- Libellé de la tranche d’âge d’usager du réseau
    trage TEXT -- Libellé abrégé de la tranche d’âge d’usager du réseau
);

DROP TABLE IF EXISTS code_trhor;
CREATE TABLE code_trhor (
    id_trhor INTEGER PRIMARY KEY, -- Identifiant du code de tranche horaire
    lib_trhor TEXT, -- Libellé du code de tranche horaire
    trhor TEXT, -- Libellé abrégé du code de tranche horaire
    hdeb TEXT, -- Heure de début de la tranche horaire (optionnel)
    hfin TEXT -- Heure de fin de la tranche horaire (optionnel)
);

DROP TABLE IF EXISTS code_trhor_typjour;
CREATE TABLE code_trhor_typjour (
    id_trhor INTEGER, -- Identifiant du code de tranche horaire
    id_typjour INTEGER, -- Identifiant du type de jour d'enquête
    lib_trhor TEXT, -- Libellé du code de tranche horaire
    trhor TEXT, -- Libellé abrégé du code de tranche horaire
    hdeb TEXT, -- Heure de début de la tranche horaire
    hfin TEXT -- Heure de fin de la tranche horaire
);

DROP TABLE IF EXISTS code_type_ligne;
CREATE TABLE code_type_ligne (
    id_typlign INTEGER PRIMARY KEY, -- Identifiant du type de ligne enquêtée
    lib_typlign TEXT, -- Libellé du type de ligne enquêtée
    icon_typlign TEXT -- Libellé abrégé du type de ligne enquêtée
);

DROP TABLE IF EXISTS enquetes;
CREATE TABLE enquetes (
    ident INTEGER PRIMARY KEY, -- Identifiant questionnaire
    typjour INTEGER, -- Identifiant du type de jour
    ligne INTEGER, -- Identifiant de la ligne
    sens INTEGER, -- Identifiant du sens de circulation
    course INTEGER, -- Identifiant de la course
    parcours INTEGER, -- Identifiant du parcours
    tranche INTEGER, -- Identifiant de la tranche horaire regroupée
    arreta2 INTEGER, -- Identifiant de l’arrêt de montée
    arretb2 INTEGER, -- Identifiant de l’arrêt de descente
    arretdeb2 INTEGER, -- Identifiant de l’arrêt de début du déplacement considéré (si correspondance avant)
    arretfin2 INTEGER, -- Identifiant de l’arrêt de fin du déplacement considéré (si correspondance avant)
    nbav INTEGER, -- Nombre de correspondances avant
    nbap INTEGER, -- Nombre de correspondances après
    lignem1 INTEGER, -- Identifiant de la première ligne empruntée avant la ligne courante (si correspondance avant)
    lignep1 INTEGER, -- Identifiant de la première ligne empruntée avant la ligne courante (si correspondance après)
    motifod INTEGER, -- Identifiant du motif agrégé du déplacement
    sexe INTEGER, -- Identifiant du sexe de la personne enquêtée
    trage INTEGER, -- Identifiant de la tranche d’âge regroupée
    titre INTEGER, -- Identifiant du titre de transport utilisé
    trhor INTEGER,
    modav INTEGER,
    modap INTEGER,
    poidsl REAL, -- Poids du questionnaire considéré sur la ligne
    poidsr REAL -- Poids du questionnaire considéré sur le réseau (inférieur à POIDSL si correspondances)
);

DROP TABLE IF EXISTS communes;
CREATE TABLE communes (
    id_commune integer PRIMARY KEY,
    nom_commune text
);
select addGeometryColumn('communes', 'geometry', 2154, 'MULTIPOLYGON');

DROP TABLE IF EXISTS arrets;
CREATE TABLE arrets (
    id_arret integer PRIMARY KEY,
    nom_arret text,
    fictif bigint
);
select addGeometryColumn('arrets', 'geometry', 2154, 'POINT');

DROP TABLE IF EXISTS troncons;
CREATE TABLE troncons (
    id_tron text PRIMARY KEY
);
select addGeometryColumn('troncons', 'geometry', 2154, 'LINESTRING');

DROP TABLE IF EXISTS zones_od;
CREATE TABLE zones_od (
    id_zoneod PRIMARY KEY,
    nom_zoneod TEXT
);
select addGeometryColumn('zones_od', 'geometry', 2154, 'POLYGON');

DROP TABLE IF EXISTS arrets_zones;
CREATE TABLE arrets_zones (
    id_arret,
    nom_arret,
    id_commune,
    nom_commune,
    id_zoneod,
    nom_zoneod
);
