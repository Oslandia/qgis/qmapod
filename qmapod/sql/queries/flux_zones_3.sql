
-- Requête "flux_zones_3.sql"
SELECT
    geometry,
    c.$col_id_zone $col_id_zone,
    c.$col_nom_zone $col_nom_zone,
    $str_champ_flux
FROM $tab_zone c
INNER JOIN $str_select_enq
    ON (c.$col_id_zone = e.$col_id_zone);
