
-- Requête "anal_zones_var_2.sql"
CREATE TABLE code_ligne_coram AS
    SELECT DISTINCT
        l.ligne_amont id_ligne,
        'L' || num_ligne || ' ' || cl.lib_ligne lib_ligne,
        'L' || num_ligne lignem1
    FROM (
        SELECT $ligne_amont ligne_amont
        FROM enquetes_tmp
        WHERE $ligne_amont IS NOT NULL
    ) l
    INNER JOIN code_ligne cl
    ON (l.ligne_amont = cl.id_ligne)
    ORDER BY ligne_amont;
