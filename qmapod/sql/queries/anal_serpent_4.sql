
-- Requête "anal_serpent_4.sql"
-- Récupération de la clause where de filtrage sur le sous-réseau
-- cast(round(tcg.charge / tog.offre, 1) AS float) perf
CREATE TABLE troncons_tmp AS
    SELECT
        geometry, t.id_tron id_tron,
        round(cast(tcg.charge AS float) / tog.offre, 1) perf
    FROM troncons t
    -- Jointure de la couche tronçons avec les requêtes de calcul
    -- de la charge et de l'offre
    LEFT JOIN
    -- Requête d'aggrégation des offres par rapport aux tronçons (tog)
    (
        SELECT ot.id_tron id_tron, sum(ot.offre) offre
        FROM offre_tmp ot
        GROUP BY id_tron
    ) tog
    ON (t.id_tron = tog.id_tron)
    LEFT JOIN
    -- Requête d'aggrégation des charges par rapport aux tronçons (tcg)
    (
        SELECT ct.id_tron id_tron, sum(ct.charge) charge
        FROM charge_tmp ct
        GROUP BY id_tron
    ) tcg
    ON (t.id_tron = tcg.id_tron)
    WHERE t.id_tron IN (
        SELECT id_tron
        FROM parcours p
        INNER JOIN code_ligne cl ON (p.id_ligne = cl.id_ligne)
        WHERE $str_where
    );
