
-- Requête "anal_arrets_2.sql"
-- Montées par sens
CREATE TABLE arrets_tmp AS
SELECT geometry, a.id_arret, a.nom_arret,
    cast(coalesce(tmp1.ms1, 0) AS float) ms1,
    cast(coalesce(tmp1.ms2, 0) AS float) ms2,
    1 etiq, st_x(geometry) x_etiq, st_y(geometry) y_etiq, 0 a_etiq
FROM arrets a
LEFT JOIN (
    SELECT arreta2 id_arret, ligne, sum(ms1) ms1, sum(ms2) ms2
    FROM (
        SELECT e.arreta2, e.ligne, sum(e.poidsl) ms1, 0 ms2
        FROM enquetes_tmp e WHERE e.sens = 1
        GROUP BY e.arreta2, e.ligne, e.sens
        UNION
        SELECT e.arreta2, e.ligne, 0 ms1, sum(e.poidsl) ms2
        FROM enquetes_tmp e WHERE e.sens = 2
    GROUP BY e.arreta2, e.ligne, e.sens
    ) tmp
    GROUP BY arreta2
) tmp1 ON a.id_arret = tmp1.id_arret
WHERE ms1 IS NOT NULL OR ms2 IS NOT NULL;
