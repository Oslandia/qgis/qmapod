
-- Requête "filtrage_1.sql"
-- Agrégation de la requête
-- Patch pour calcul de l'offre à partir de l'heure de début ET de l'heure de fin des courses
-- à copier à la place des lignes 51 à 55
-- Mais la convention adoptée par Test-SA consiste à prendre en compte uniquement l'heure de début
/*
    (100 * CAST(SUBSTR(c.hdeb, 1, 2) as integer) + CAST(SUBSTR(c.hdeb, 3, 2) as integer) >= 100 * CAST(SUBSTR(t.hdeb, 1, 2) as integer) + CAST(SUBSTR(t.hdeb, 4, 2) as integer) AND
     100 * CAST(SUBSTR(c.hdeb, 1, 2) as integer) + CAST(SUBSTR(c.hdeb, 3, 2) as integer) <= 100 * CAST(SUBSTR(t.hfin, 1, 2) as integer) + CAST(SUBSTR(t.hfin, 4, 2) as integer)) OR
    (100 * CAST(SUBSTR(c.hfin, 1, 2) as integer) + CAST(SUBSTR(c.hfin, 3, 2) as integer) <= 100 * CAST(SUBSTR(t.hfin, 1, 2) as integer) + CAST(SUBSTR(t.hfin, 4, 2) as integer) AND
     100 * CAST(SUBSTR(c.hfin, 1, 2) as integer) + CAST(SUBSTR(c.hfin, 3, 2) as integer) >= 100 * CAST(SUBSTR(t.hdeb, 1, 2) as integer) + CAST(SUBSTR(t.hdeb, 4, 2) as integer))
*/
-- TODO : rendre code_trhorg paramétrable !!! self.qmapodConfig.dctParam['tabtrhor']
SELECT
    p.id_ligne id_ligne,
    p.id_sens id_sens,
    p.id_parcours id_parcours,
    p.ordre ordre,
    p.id_arret id_arret,
    p.desserte desserte,
    p.id_tron id_tron,
    coalesce(sum(o.offre), 0) offre
FROM parcours p
LEFT JOIN (
    SELECT
        c.id_ligne,
        c.id_sens,
        c.id_parcours,
        c.id_arret_deb,
        (
            SELECT ordre
            FROM parcours p
            WHERE c.id_ligne = p.id_ligne
              AND c.id_sens = p.id_sens
              AND c.id_parcours = p.id_parcours
              AND c.id_arret_deb = p.id_arret
            ORDER BY ordre asc
            LIMIT 1
        ) odeb,
        c.id_arret_fin,
        (
            SELECT ordre
            FROM parcours p
            WHERE c.id_ligne = p.id_ligne
              AND c.id_sens = p.id_sens
              AND c.id_parcours = p.id_parcours
              AND c.id_arret_fin = p.id_arret
            ORDER BY ordre DESC LIMIT 1
        ) ofin,
        sum(poids) offre, t.id_trhor
    FROM courses c
    INNER JOIN $tab_trhor t ON (
        (100 * cast(substr(c.hdeb, 1, 2) as integer) + cast(substr(c.hdeb, 3, 2) as integer)
        >= 100 * cast(substr(t.hdeb, 1, 2) as integer) + cast(substr(t.hdeb, 4, 2) as integer))
        AND
        (100 * cast(substr(c.hdeb, 1, 2) as integer) + cast(substr(c.hdeb, 3, 2) as integer)
        <= 100 * cast(substr(t.hfin, 1, 2) as integer) + cast(substr(t.hfin, 4, 2) as integer))
    )
    WHERE $str_where
    GROUP BY
        c.id_ligne,
        c.id_sens,
        c.id_parcours,
        c.id_arret_deb,
        c.id_arret_fin,
        t.id_trhor
    ORDER BY
        c.id_ligne,
        c.id_sens,
        c.id_parcours,
        t.id_trhor,
        offre DESC,
        c.id_arret_deb,
        c.id_arret_fin
) o
ON ((
        p.id_ligne = o.id_ligne
        AND p.id_sens = o.id_sens
        AND p.id_parcours = o.id_parcours
    )
    AND (
        p.ordre >= o.odeb
        AND p.ordre < o.ofin
    )
)
GROUP BY
    p.id_ligne,
    p.id_sens,
    p.id_parcours,
    p.ordre,
    p.id_arret,
    p.desserte,
    p.id_tron
ORDER BY
    p.id_ligne,
    p.id_sens,
    p.id_parcours,
    p.ordre;
