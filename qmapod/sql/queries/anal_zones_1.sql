
-- Requête "anal_zones_1.sql"
CREATE TABLE zones_tmp AS
    SELECT
        z.geometry,
        z.$col_id_zone id_zone,
        z.$col_nom_zone nom_zone,
        cast(coalesce(zm.montees, 0) AS float) montees,
        cast(coalesce(zd.descentes, 0) AS float) descentes
    FROM $tab_zone z
    INNER JOIN
        (
            SELECT $col_id_zone, cast(coalesce(sum(em.$poids_), 0) AS float) montees
            FROM arrets_zones azm
            LEFT JOIN enquetes_tmp em ON azm.id_arret = em.$arret_deb
            GROUP BY $col_id_zone
        ) zm ON (zm.$col_id_zone = z.$col_id_zone)
    INNER JOIN
        (
            SELECT
                $col_id_zone,
                cast(coalesce(sum(ed.$poids_), 0) AS float) descentes
            FROM arrets_zones azd
            LEFT JOIN enquetes_tmp ed ON azd.id_arret = ed.$arret_fin
            GROUP BY $col_id_zone
        ) zd ON (zm.$col_id_zone = zd.$col_id_zone);
