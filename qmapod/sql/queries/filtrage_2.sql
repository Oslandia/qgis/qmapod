
-- Requête "filtrage_2.sql"
(
    SELECT
        $ligne,
        $sens,
        $parcours,
        $arretdebvoy arret,
        sum($poids_) montees
    FROM enquetes_tmp
    GROUP BY
        $ligne,
        $sens,
        $parcours,
        $arretdebvoy
) a
