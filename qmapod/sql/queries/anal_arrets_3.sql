
-- Requête "anal_arrets_3.sql"
-- Descentes par sens
CREATE TABLE arrets_tmp AS
SELECT
    geometry,
    a.id_arret,
    a.nom_arret,
    cast(coalesce(tmp1.ds1, 0) AS float) ds1,
    cast(coalesce(tmp1.ds2, 0) AS float) ds2,
    1 etiq,
    st_x(geometry) x_etiq,
    st_y(geometry) y_etiq,
    0 a_etiq
FROM arrets a
LEFT JOIN (
    SELECT
        arretb2 id_arret,
        ligne,
        sum(ds1) ds1,
        sum(ds2) ds2
    FROM (
        SELECT
            e.arretb2,
            e.ligne,
            sum(e.poidsl) ds1,
            0 ds2
        FROM enquetes_tmp e
        WHERE e.sens = 1
        GROUP BY e.arretb2, e.ligne, e.sens
        UNION
        SELECT
            e.arretb2,
            e.ligne,
            0 ds1,
            sum(e.poidsl) ds2
        FROM enquetes_tmp e
        WHERE e.sens = 2
        GROUP BY e.arretb2, e.ligne, e.sens
    ) tmp
    GROUP BY arretb2
) tmp1
ON a.id_arret = tmp1.id_arret
WHERE ds1 IS NOT NULL
   OR ds2 IS NOT NULL;
