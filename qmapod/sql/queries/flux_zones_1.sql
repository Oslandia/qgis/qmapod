
-- Requête "flux_zones_1.sql"
(
    SELECT
        azd.$col_id_zone $col_id_zone,
        azd.$col_nom_zone $col_nom_zone,
        cast(coalesce(sum($poids_), 0) AS float) descentes
    FROM enquetes_tmp
    INNER JOIN arrets_zones azm ON $arret_deb = azm.id_arret
    INNER JOIN arrets_zones azd ON $arret_fin = azd.id_arret
    WHERE azm.$col_id_zone IN($str)
    GROUP BY azd.$col_id_zone
) e
