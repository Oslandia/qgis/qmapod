
-- Requête "anal_serpent_1.sql"
CREATE TABLE troncons_tmp AS
    SELECT geometry, t.id_tron id_tron, cast(tg.offre AS int) offre
    FROM troncons t
    -- Jointure de la couche tronçons avec la requête de calcul de l'offre
    LEFT JOIN (
    -- Requête d'aggrégation des offres par rapport aux tronçons (tg)
        SELECT ot.id_tron id_tron, sum(ot.offre) offre
        FROM offre_tmp ot
        GROUP BY id_tron
    ) tg ON t.id_tron = tg.id_tron
    WHERE t.id_tron IN (
        SELECT id_tron
        FROM parcours p
        INNER JOIN code_ligne cl ON (p.id_ligne = cl.id_ligne)
        WHERE $str_where
    );
