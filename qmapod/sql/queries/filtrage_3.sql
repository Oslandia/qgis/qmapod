
-- Requête "filtrage_3.sql"
(
    SELECT
        $ligne,
        $sens,
        $parcours,
        $arretfinvoy arret,
        sum($poids_) descentes
    FROM enquetes_tmp
    GROUP BY
        $ligne,
        $sens,
        $parcours,
        $arretfinvoy
) b
