
-- Requête "filtrage_7.sql"
SELECT
    pmd.ligne ligne,
    pmd.sens sens,
    pmd.parcours parcours,
    pmd.ordre ordre,
    pmd.desserte desserte,
    pmd.arret arret,
    pmd.montees montees,
    pmd.descentes descentes,
    pmd.id_tron id_tron,
    $str_select_colonne_charge
FROM $str_select_pmd
