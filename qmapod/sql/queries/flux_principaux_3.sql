
-- Requête "flux_principaux_3.sql"
SELECT $col_id_zone
FROM $tab_zone
WHERE $col_id_zone IN (
    SELECT DISTINCT id_zm
    FROM fluxpp_tmp
    UNION
    SELECT DISTINCT id_zd
    FROM fluxpp_tmp
);
