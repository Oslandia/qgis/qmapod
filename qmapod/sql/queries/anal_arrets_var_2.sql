
-- Requête "anal_arrets_var_2.sql"
CREATE TABLE code_ligne_coram AS
    SELECT DISTINCT
        l.ligne_amont AS id_ligne,
        'L' || num_ligne || ' ' || cl.lib_ligne AS lib_ligne,
        'L' || num_ligne AS lignem1
    FROM (
        SELECT $id_ligne_amont ligne_amont
        FROM enquetes_tmp
        WHERE $id_ligne_amont IS NOT NULL
    ) l
    INNER JOIN code_ligne cl ON l.ligne_amont = cl.id_ligne
    ORDER BY ligne_amont
;
