
-- Requête "anal_serpent_2.sql"
CREATE TABLE troncons_tmp AS
    SELECT
        geometry,
        t.id_tron id_tron,
        cast(tg.charge AS int) charge
    FROM troncons t
    -- Jointure de la couche tronçons avec la requête de calcul de la charge
    LEFT JOIN
    -- Requête d'aggrégation des charges par rapport aux tronçons (tg)
    (
        SELECT
            ct.id_tron id_tron,
            sum(ct.charge) charge
        FROM charge_tmp ct
        GROUP BY id_tron
    ) tg
    ON (t.id_tron = tg.id_tron)
    WHERE t.id_tron IN (
        SELECT id_tron
        FROM parcours p
        INNER JOIN code_ligne cl ON p.id_ligne = cl.id_ligne
        WHERE $str_where
    );

-- Exemple de requête complète (y compris celle qui permet de créer la table charge_tmp
-- qui est calculée lors de la création du sous-ensemble d'enquêtes
/*CREATE TABLE troncons_tmp AS
    SELECT
        geometry,
        t.id_tron id_tron,
        cast(tg.charge AS int) charge
    FROM troncons t
    -- Jointure de la couche tronçons avec la requête de calcul de la charge
    LEFT JOIN
    -- Requête d'aggrégation des charges par rapport aux tronçons (tg)
    (
        SELECT
            pmdc.id_tron id_tron,
            sum(pmdc.charge) charge
        FROM (
            -- Requête d'extraction des parcours et de jointure de la charge (pmdc)
            SELECT pmd.ligne ligne,
                    pmd.sens sens,
                    pmd.parcours parcours,
                    pmd.ordre ordre,
                    pmd.arret arret,
                    pmd.montees montees,
                    pmd.descentes descentes,
                    pmd.id_tron id_tron,
                    -- Sous-requête colonne pour le calcul de la charge pour le tronçon courant
                    (
                        SELECT sum(coalesce(pmd1.montees, 0)) - sum(coalesce(pmd1.descentes, 0))
                        FROM (
                            -- Sous-requête d'extraction des parcours et de jointure des montées / descentes (pmd1)
                            (
                                SELECT p.id_ligne ligne, p.id_sens sens, p.id_parcours parcours, p.ordre ordre, p.id_arret arret,
                                    p.desserte desserte, a.montees montees, b.descentes descentes, p.id_tron id_tron
                                FROM
                                -- Extraction des parcours
                                    (
                                        SELECT id_ligne, id_sens, id_parcours, ordre, id_arret, id_tron, desserte
                                        FROM parcours
                                    ) p
                                    LEFT JOIN
                                    -- Calcul et jointure des montées
                                    (
                                        SELECT ligne, sens, parcours, arreta2 arret, sum(poids) montees
                                        FROM enquetes_tmp
                                        GROUP BY ligne, sens, parcours, arreta2
                                    ) a
                                    ON (p.id_ligne = a.ligne AND p.id_sens = a.sens AND p.id_parcours = a.parcours AND p.id_arret = a.arret)
                                    LEFT JOIN
                                    -- Calcul et jointure des descentes
                                    (
                                        SELECT ligne, sens, parcours, arretb2 arret, sum(poids) descentes
                                        FROM enquetes_tmp
                                        GROUP BY ligne, sens, parcours, arretb2
                                    ) b
                                    ON (p.id_ligne = b.ligne AND p.id_sens = b.sens AND p.id_parcours = b.parcours AND p.id_arret = b.arret)
                            ) pmd1
                            -- La jointure décalée sur l'ordre permet le calcul de charge (pmd1.ordre <= pmd.ordre)
                        WHERE pmd1.ligne = pmd.ligne AND pmd1.sens = pmd.sens AND pmd1.parcours = pmd.parcours
                            AND pmd1.ordre <= pmd.ordre AND pmd1.desserte = 1
                        ORDER BY pmd1.ligne, pmd1.sens, pmd1.parcours, pmd1.ordre
                    ) charge
            FROM
                -- Sous-requête d'extraction des parcours et de jointure des montées / descentes (pmd1)
                (
                    SELECT p.id_ligne ligne, p.id_sens sens,
                            p.id_parcours parcours, p.id_arret arret,
                            p.ordre ordre, a.montees montees,
                            b.descentes descentes, p.id_tron id_tron
                    FROM
                    -- Extraction des parcours
                    (
                        SELECT id_ligne, id_sens, id_parcours, ordre, id_arret, id_tron, desserte
                        FROM parcours
                    ) p
                    LEFT JOIN
                    -- Calcul et jointure des montées
                    (
                        SELECT ligne, sens, parcours, arreta2 arret, sum(poids) montees
                        FROM enquetes_tmp
                        GROUP BY ligne, sens, parcours, arreta2
                    ) a
                    ON p.id_ligne = a.ligne AND p.id_sens = a.sens AND p.id_parcours = a.parcours AND p.id_arret = a.arret
                    LEFT JOIN
                    -- Calcul et jointure des descentes
                    (
                        SELECT ligne, sens, parcours, arretb2 arret, sum(poids) descentes
                        FROM enquetes_tmp
                        GROUP BY ligne, sens, parcours, arretb2
                    ) b
                    ON p.id_ligne = b.ligne AND p.id_sens = b.sens AND p.id_parcours = b.parcours AND p.id_arret = b.arret
                    ORDER BY ligne, sens, parcours, ordre
                ) pmd
        ) pmdc
        GROUP BY pmdc.id_tron
    ) tg ON t.id_tron = tg.id_tron;*/
