
-- Requête "anal_arrets_var_1.sql"
CREATE TABLE code_ligne_var AS
SELECT DISTINCT
    e.$id_ligne AS id_ligne,
    'L' || num_ligne || ' ' || cl.lib_ligne lib_ligne,
    'L' || cl.num_ligne ligne
FROM enquetes_tmp e
INNER JOIN code_ligne cl ON e.$id_ligne = cl.id_ligne
ORDER BY id_ligne;
