
-- Requête "sous_reseau_2.sql"
CREATE TABLE sous_reseau_arrets_tmp AS
    SELECT
        a.id_arret id_arret,
        a.nom_arret nom_arret,
        1 etiq,
        st_x(geometry) x_etiq,
        st_y(geometry) y_etiq,
        0 a_etiq,
        geometry
    FROM arrets a
    INNER JOIN parcours p ON (a.id_arret = p.id_arret)
    INNER JOIN code_ligne cl ON p.id_ligne = cl.id_ligne
    WHERE $str_where AND p.desserte
    GROUP BY a.id_arret;
