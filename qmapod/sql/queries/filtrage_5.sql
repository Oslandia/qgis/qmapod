
-- Requête "filtrage_5.sql"
(
    SELECT
        sum(coalesce(pmd1.montees, 0))
        - sum(coalesce(pmd1.descentes, 0))
    FROM $str_select_pmd1
    WHERE pmd1.ligne = pmd.ligne
        AND pmd1.sens = pmd.sens
        AND pmd1.parcours = pmd.parcours
        AND pmd1.ordre <= pmd.ordre
        AND pmd1.desserte = 1
    ORDER BY pmd1.ligne, pmd1.sens, pmd1.parcours, pmd1.ordre
) charge
