
-- Requête "flux_zones_7.sql"
(
    SELECT
        azd.$col_id_zone $col_id_zone_d,
        azd.$col_nom_zone $col_nom_zone_d,
        azm.$col_id_zone $col_id_zone_m,
        azm.$col_nom_zone $col_nom_zone_m,
        cast(coalesce(sum($poids_), 0) AS float) montees
    FROM enquetes_tmp
    INNER JOIN arrets_zones azd ON $arret_fin = azd.id_arret
    INNER JOIN arrets_zones azm ON $arret_deb = azm.id_arret
    WHERE azd.$col_id_zone IN($lst_zones)
    GROUP BY azm.$col_id_zone
) e
