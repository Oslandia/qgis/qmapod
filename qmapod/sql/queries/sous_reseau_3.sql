
-- Requête "sous_reseau_3.sql"
INSERT INTO geometry_columns (
    f_table_name, f_geometry_column,
    geometry_type, coord_dimension, srid,
    spatial_index_enabled
)
VALUES ('sous_reseau_arrets_tmp', 'geometry', 1, 2, $epsg, 0);
