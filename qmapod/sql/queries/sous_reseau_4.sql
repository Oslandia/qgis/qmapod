
-- Requête "sous_reseau_4.sql"
CREATE TABLE sous_reseau_troncons_tmp AS
    SELECT
        t.id_tron id_tron,
        count(distinct cl.id_ligne)
        nb,
        rvb,
        geometry
    FROM troncons t
    INNER JOIN parcours p ON t.id_tron = p.id_tron
    INNER JOIN code_ligne cl ON p.id_ligne = cl.id_ligne
    WHERE $str_where
    GROUP BY t.id_tron
    ORDER BY nb;
