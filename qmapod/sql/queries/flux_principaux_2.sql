
-- Requête "flux_principaux_2.sql"
INSERT INTO geometry_columns (
    f_table_name,
    f_geometry_column,
    geometry_type,
    coord_dimension, srid,
    spatial_index_enabled
)
VALUES ('fluxpp_tmp', 'geometry', 2, 2, $epsg, 0);
