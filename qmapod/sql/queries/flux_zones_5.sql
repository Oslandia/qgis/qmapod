
-- Requête "flux_zones_5.sql"
(
    SELECT
        azm.$col_id_zone $col_id_zone_m,
        azm.$col_nom_zone $col_nom_zone_m,
        azd.$col_id_zone $col_id_zone_d,
        azd.$col_nom_zone $col_nom_zone_d,
        cast(coalesce(sum($poids), 0) AS float) descentes
    FROM enquetes_tmp
    INNER JOIN arrets_zones azm ON $arret_deb = azm.id_arret
    INNER JOIN arrets_zones azd ON $arret_fin = azd.id_arret
    WHERE azm.$col_id_zone IN($lst_zones)
    GROUP BY azm.$col_id_zone, azd.$col_id_zone
) e
