
-- Requête "anal_arrets_var_3.sql"
CREATE TABLE code_ligne_corav AS
    SELECT DISTINCT
        l.ligne_aval id_ligne,
        'L' || num_ligne || ' ' || cl.lib_ligne lib_ligne,
        'L' || num_ligne lignep1
    FROM (SELECT $id_ligne_aval ligne_aval FROM enquetes_tmp
            WHERE $id_ligne_aval IS NOT NULL
            ) l
    INNER JOIN code_ligne cl ON l.ligne_aval = cl.id_ligne
    ORDER BY ligne_aval
;
