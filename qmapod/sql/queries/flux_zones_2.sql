
-- Requête "flux_zones_2.sql"
(
    SELECT
        azm.$col_id_zone $col_id_zone,
        azm.$col_nom_zone $col_nom_zone,
        cast(coalesce(sum($poids_), 0) AS float) montees
    FROM enquetes_tmp
    INNER JOIN arrets_zones azd ON $arret_fin = azd.id_arret
    INNER JOIN arrets_zones azm ON $arret_deb = azm.id_arret
    WHERE azd.$col_id_zone IN($str)
    GROUP BY azm.$col_id_zone
) e
