
-- Requête "anal_zones_2.sql"
INSERT INTO geometry_columns (
    f_table_name,
    f_geometry_column,
    geometry_type,
    coord_dimension,
    srid,
    spatial_index_enabled
)
VALUES (
    'zones_tmp',
    'geometry',
    3,
    2,
    $epsg,
    0
);
