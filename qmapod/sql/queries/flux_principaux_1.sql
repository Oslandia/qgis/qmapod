
-- Requête "flux_principaux_1.sql"
WITH flux AS (
    SELECT
        $str_grp azm.$col_id_zone id_zm,
        azm.$col_nom_zone nom_zm,
        azd.$col_id_zone id_zd,
        azd.$col_nom_zone nom_zd,
        cast(coalesce(sum($poids_), 0) AS float) flux
    FROM enquetes_tmp
    INNER JOIN arrets_zones azm ON $arret_deb = azm.id_arret
    INNER JOIN arrets_zones azd ON $arret_fin = azd.id_arret
    GROUP BY $str_grp azm.$col_id_zone, azm.$col_nom_zone, azd.$col_id_zone, azd.$col_nom_zone
    ORDER BY $str_grp flux DESC
)
SELECT
    row_number() over() id,
    *
FROM (
    SELECT
        LineFromText(
            'LINESTRING(' ||
            ST_X(ST_Centroid(zm.geometry)) || ' ' || ST_Y(ST_Centroid(zm.geometry)) || ',' ||
            (((ST_X(ST_Centroid(zm.geometry)) + ST_X(ST_Centroid(zd.geometry))) / 2) -
            (ST_Distance(ST_Centroid(zd.geometry), ST_Centroid(zm.geometry)) / 3
                * sin(ST_Azimuth(ST_Centroid(zm.geometry), ST_Centroid(zd.geometry))))) || ' ' ||
            (((ST_Y(ST_Centroid(zm.geometry)) + ST_Y(ST_Centroid(zd.geometry))) / 2) +
            (ST_Distance(ST_Centroid(zd.geometry), ST_Centroid(zm.geometry)) / 3
                * cos(ST_Azimuth(ST_Centroid(zm.geometry), ST_Centroid(zd.geometry))))) || ',' ||
            ST_X(ST_Centroid(zd.geometry)) || ' ' || ST_Y(ST_Centroid(zd.geometry)) || ')'
        , 2154) geometry,
        row_number() OVER ($str_part ORDER BY flux DESC) AS rang,
        flux.*
    FROM flux
    INNER JOIN $tab_zone zm ON (zm.$col_id_zone = flux.id_zm)
    INNER JOIN $tab_zone zd ON (zd.$col_id_zone = flux.id_zd)
) x
WHERE x.rang <= $nb_max;
