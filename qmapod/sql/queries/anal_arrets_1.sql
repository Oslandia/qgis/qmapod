
-- Requête "anal_arrets_1.sql"

-- Modifié le 05/03/2015 : supprimé la colonne ROWID as rowid
-- pour permettre l'importation sous MapInfo
-- Modifié le 05/05/2015 : supprimé la jointure sur les parcours
-- pour conserver les montées/descentes en dehors du réseau sélectionné
-- La clause where est devenue inutile puisqu'on travaille
-- sur les données filtrées
-- Requête sans arrondis

CREATE TABLE arrets_tmp AS
    SELECT
        geometry,
        a.id_arret,
        a.nom_arret,
        cast(coalesce(m.montees, 0) AS float) montees,
        cast(coalesce(d.descentes, 0) AS float) descentes
    FROM arrets a
    LEFT JOIN
    (
        SELECT
            e.$arret_deb id_arret,
            cast((coalesce(sum(e.$poids_), 0)) AS float) AS montees
        FROM enquetes_tmp e
        GROUP BY e.$arret_deb
    ) m ON m.id_arret = a.id_arret
    LEFT JOIN (
        SELECT
            e.$arret_fin id_arret,
            cast((coalesce(sum(e.$poids_), 0)) AS float) AS descentes
        FROM enquetes_tmp e
        GROUP BY e.$arret_fin
    ) d ON d.id_arret = a.id_arret
    WHERE montees IS NOT NULL OR descentes IS NOT NULL;
