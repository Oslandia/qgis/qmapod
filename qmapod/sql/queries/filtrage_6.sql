
-- Requête "filtrage_6.sql"
(
    SELECT
        p.id_ligne ligne,
        p.id_sens sens,
        p.id_parcours parcours,
        p.ordre ordre,
        p.id_arret arret,
        p.desserte desserte,
        a.montees montees,
        b.descentes descentes,
        p.id_tron id_tron
        FROM (
            SELECT
                id_ligne,
                id_sens,
                id_parcours,
                ordre,
                id_arret,
                id_tron,
                desserte
            FROM parcours
        ) p
    LEFT JOIN $str_select_montees ON (
        p.id_ligne = a.ligne AND
        p.id_sens = a.sens AND
        p.id_parcours = a.parcours AND
        p.id_arret = a.arret
    )
    LEFT JOIN $str_select_descentes ON (
        p.id_ligne = b.ligne AND
        p.id_sens = b.sens AND
        p.id_parcours = b.parcours AND
        p.id_arret = b.arret
    )
ORDER BY ligne, sens, parcours, ordre
) pmd
