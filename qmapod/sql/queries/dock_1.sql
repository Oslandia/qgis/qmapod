select
    l.typ_ligne,
    t.lib_typlign,
    l.id_ligne,
    l.num_ligne,
    l.lib_ligne,
    s.id_sens,
    s.lib_sens,
    t.icon_typlign
from code_type_ligne t
inner join code_ligne l on t.id_typlign = l.typ_ligne
inner join code_sens s on l.id_ligne = s.id_ligne
order by l.typ_ligne, replace(l.num_ligne, '_', ''), s.id_sens;
