
-- Requête "filtrage_11.sql"
INSERT INTO geometry_columns (
    f_table_name,
    f_geometry_column,
    geometry_type,
    coord_dimension,
    srid,spatial_index_enabled
)
VALUES (
    'flux_arrets_tmp',
    'geometry',
    1,
    2,
    $epsg,
    0
);
