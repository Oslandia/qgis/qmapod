
-- Requête "anal_zones_var_1.sql"
CREATE TABLE code_ligne_var AS
    SELECT DISTINCT
        e.$ligne id_ligne,
        'L' || num_ligne || ' ' || cl.lib_ligne lib_ligne,
        'L' || cl.num_ligne ligne
    FROM enquetes_tmp e
    INNER JOIN code_ligne cl ON (e.$ligne = cl.id_ligne)
    ORDER BY id_ligne;
