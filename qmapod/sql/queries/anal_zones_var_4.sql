
-- Requête "anal_zones_var_4.sql"
CREATE TABLE zones_tmp_var AS
    SELECT
        z.geometry,
        z.$col_id_zone id_zone,
        z.$col_nom_zone nom_zone,
        $str_col1
    FROM $tab_zone z
    INNER JOIN (
        SELECT $col_id_zone$str_col_2
        FROM arrets_zones azm
        LEFT JOIN (
            SELECT
                $col_arret,
                v.$col_var,
                cast(sum(e.$poids_) AS float) poids
            FROM enquetes_tmp e
            LEFT JOIN $tab_var v ON e.$col_var = v.$col_id_var
            GROUP BY $col_arret, e.$col_var
        ) ev
        ON (azm.id_arret = ev.$col_arret)
        GROUP BY $col_id_zone
    ) var
    ON z.$col_id_zone = var.$col_id_zone;
