
-- Requête "flux_zones_6.sql"
SELECT
    linefromtext('LINESTRING(' ||
        x(centroid(zm.geometry)) || ' ' ||
        y((centroid(zm.geometry))) || ', ' ||
        cast((x(centroid(zd.geometry)) + x(centroid(zm.geometry))) / 2
        + distance(centroid(zm.geometry), centroid(zd.geometry))
        * sin(azimuth(centroid(zd.geometry), centroid(zm.geometry))) / 5 as string) || ' ' ||
        cast((y(centroid(zd.geometry)) + y(centroid(zm.geometry))) / 2
        - distance(centroid(zm.geometry), centroid(zd.geometry))
        * cos(azimuth(centroid(zd.geometry), centroid(zm.geometry))) / 5 as string) || ', ' ||
        x(centroid(zd.geometry)) || ' ' || y(centroid(zd.geometry)) || ')', $epsg
    ) as geometry,
    e.$col_id_zone_m,
    e.$col_nom_zone_m,
    e.$col_id_zone_d,
    e.$col_nom_zone_d,
    $str_champ_flux
FROM $str_select_enq
INNER JOIN $tab_zone zm ON e.$col_id_zone_m = zm.$col_id_zone
INNER JOIN $tab_zone zd ON e.$col_id_zone_d = zd.$col_id_zone;
