"""
/***************************************************************************
 QMapODAnalArret
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

import qgis.utils

# Import des librairies QGIS
from qgis.core import (
    Qgis,
    QgsDataDefinedSizeLegend,
    QgsDataSourceUri,
    QgsDiagramLayerSettings,
    QgsDiagramSettings,
    QgsLayerTreeLayer,
    QgsLinearlyInterpolatedDiagramRenderer,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsPieDiagram,
    QgsProject,
    QgsRenderContext,
    QgsSimpleMarkerSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)

# Import des librairies PyQt
from qgis.PyQt.QtCore import QSizeF
from qgis.PyQt.QtGui import QColor, QFont
from qgis.PyQt.QtWidgets import QApplication

from .qmapod_lab import QMapOdLab

module_logger = logging.getLogger("QMapOD.anal_arrets")


class QMapODAnalArret(object):
    """
    Analyses cartographiques par arrêts
    """

    def __init__(
        self,
        iface,
        dock,
        tbn,
        clsFiltrage,
        typAnal,
        qmapodConfig,
        qmapodSettings,
    ):
        """
        Constructor
        """

        # Version de Qgis
        self.qgisVersionInt = Qgis.QGIS_VERSION_INT
        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        # Récupération de la référence au panneau ancrable
        self.dock = dock
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Type d'analyse ('md', 'm', 'd', 'ms' ou 'ds')
        self.typAnal = typAnal
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        self.currentFiltrage = clsFiltrage
        # Couche générée
        self.amdLayer = None
        # Caractéristiques diagrammes
        self.maxSize = 0
        self.maxValue = 0
        # Titre de la couche
        self.strNom = ""

        self.logger = logging.getLogger("QMapOD.anal_arrets.QMapODAnalArret")
        self.logger.info("Creating an instance of QMapODAnalArret")

    def _addLayerArretsTmp(self):
        """
        Ajout de la couche arrets_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "md":
            strArret = "Montées/descentes"
        elif self.typAnal == "m":
            strArret = "Montées"
        elif self.typAnal == "d":
            strArret = "Descentes"
        elif self.typAnal == "ms":
            strArret = "Montées par sens"
        elif self.typAnal == "ds":
            strArret = "Descentes par sens"

        self.strNom = Template("$str_arret par arrêts").substitute(str_arret=strArret)

        # Ajout de la couche
        uriArrets = QgsDataSourceUri()
        uriArrets.setDatabase(self.db)
        uriArrets.setDataSource("", "arrets_tmp", "geometry")
        arretsLayer = QgsVectorLayer(uriArrets.uri(), self.strNom, "spatialite")
        if arretsLayer.isValid():
            # Ajout des couches et déplacement au-dessus
            # des couches existantes (api QgsLayerTree)
            root = QgsProject.instance().layerTreeRoot()

            self.amdLayer = QgsProject.instance().addMapLayer(arretsLayer, False)
            arretsTreeLayer = QgsLayerTreeLayer(arretsLayer)
            arretsTreeLayer.setName(arretsLayer.name())
            self.amdLayer.destroyed.connect(self._deleted)

            root.insertChildNode(0, arretsTreeLayer)

    # -------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.amdLayer = None
        self.tbn.setChecked(False)

    # -------------------------------------------------------------------------

    def _getPoids(self, nbLignes, modeEnq):
        """
        Choix du mode de pondération à utiliser
        """

        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        # ou sur la ligne selon la logique d'analyse
        # Voyage : poids sur la ligne - Déplacement : poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            if modeEnq == 1:
                return self.qmapodConfig.dctParam["poidsligne"]
            elif modeEnq == 2:
                return self.qmapodConfig.dctParam["poidsreseau"]
            else:
                return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # -------------------------------------------------------------------------

    def _genSql(self, nbLignes):
        """
        Génération de la requête de création de la table arrets_tmp
        """

        # Mode de prise en compte des enquêtes
        modeEnq = int(self.qmapodSettings.value("params/buttonGroupArretMode", 1))
        if modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]

        poids = self._getPoids(nbLignes, modeEnq)

        if self.typAnal in ["md", "m", "d"]:
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_1.sql"
                )
            ) as f:
                strSql = Template(f.read()).substitute(
                    arret_deb=arretDeb, poids_=poids, arret_fin=arretFin
                )
        elif self.typAnal in ["ms"]:
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_2.sql"
                )
            ) as f:
                strSql = f.read()
        elif self.typAnal in ["ds"]:
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_3.sql"
                )
            ) as f:
                strSql = f.read()

        return strSql

    # ------------------------------------------------------------------------

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            s = Template(
                "\nSELECT max($col_) \n\
                     FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            max = cursor.fetchone()[0]
            if max is None:
                self.logger.info(
                    Template("No values in column $col_ of table $tab_").substitute(
                        col_=col, tab_=tab
                    )
                )
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _createTableArretsTmp(self, nbLignes):
        """
        Création de la table arrets_tmp
        """

        try:
            # Connexion à la base de données
            db = qgis.utils.spatialite_connect(self.db)

            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table arrets_tmp si elle existe
            s = "\nDROP TABLE IF EXISTS arrets_tmp;"
            self.logger.debug(s)
            cursor.execute(s)
            # Création de la table arrets_tmp
            strSql = self._genSql(nbLignes)
            self.logger.debug(strSql)
            cursor.execute(strSql)
            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
                 WHERE f_table_name = 'arrets_tmp';"
            self.logger.debug(s)
            cursor.execute(s)
            # Insertion de la déclaration de la colonne geometry
            # (type 1 = point)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/anal_arrets_4.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

    # ------------------------------------------------------------------------

    def _labelArretMD(self):
        """
        Etiquetage simplifié de la couche

        Options d'étiquetage de la couche: intMode
        0  : Aucun
        1  : Selon analyse
        2  : Nom - Selon analyse
        3  : Montees + Descentes
        4  : Nom Montées + Descentes
        5  : Nom
        6  : ID

        Modes d'étiquetage: lstMode
        0  : Aucun
        1  : Concaténation des valeurs
        2  : Somme des valeurs
        3  : Nom - Somme des valeurs
        4  : Nom - Concaténation des valeurs
        5  : Nom
        6  : ID
        """
        # on instancie la class de génération des expressions d'étiquetage
        lab = QMapOdLab()
        # on récupère le type d'étiquetage demandé
        intMode = int(self.qmapodSettings.value("params/cbxArretLabel", 4))
        # on définit où le nom à afficher dans l'étiquette doit être récupéré
        nomCol = "nom_arret"
        idCol = "id_arret"
        # affichage par défaut Selon Analyse
        lstMode = 1
        lstColumns = []
        affMessage = False
        nomColManq = ""

        # selection du nom des données manquantes en fonction de l'analyse,
        # validation de l'affichage du message d'avertissement
        # suppression de l'étiquetage
        if intMode in [3, 4]:
            if self.typAnal in ["m", "ms"]:
                nomColManq = "Descentes"
                affMessage = True
                lstMode = 0
            elif self.typAnal in ["d", "ds"]:
                nomColManq = "Montées"
                affMessage = True
                lstMode = 0
        # affichages ne dépendant pas de l'analyse
        if intMode in (0, 5, 6):
            lstMode = intMode

        # pour analyse montées descentes tous les affichages sont possibles
        elif self.typAnal == "md":
            lstColumns = ["montees", "descentes"]
            if intMode == 4:
                lstMode = 3
            elif intMode == 3:
                lstMode = 2
            elif intMode == 2:
                lstMode = 4

        # pour analyse montées seules les données de montées sont affichées
        # on ne sélectionne que le champ montees
        elif self.typAnal == "m":
            lstColumns = ["montees"]
            if intMode == 2:
                lstMode = 3

        # fonctionnement identique pour l'analyse descente
        elif self.typAnal == "d":
            lstColumns = ["descentes"]
            if intMode == 2:
                lstMode = 3

        # fonctionnement identique pour l'analyse montées par sens
        elif self.typAnal == "ms":
            lstColumns = ["ms1", "ms2"]  # Liste des champs
            if intMode == 2:
                lstMode = 4

        # fonctionnement identique pour l'analyse decentes par sens
        elif self.typAnal == "ds":
            lstColumns = ["ds1", "ds2"]  # Liste des champs
            if intMode == 2:
                lstMode = 4

        if affMessage:
            # affichage message d'avertissement
            msgBar = self.iface.messageBar()
            msg = msgBar.createMessage(
                Template(
                    "\nAffichage demandé non significatif \n\
                      données de $nom_col_manq absentes"
                ).substitute(nomColManq)
            )
            # premier chiffre niveau deuxième chiffre durée d'affichage
            msgBar.pushWidget(msg, 1, 5)

        blnExp = True

        # calcul de l'expression d'étiquetage
        strField = lab.lbl(idCol, nomCol, lstMode, lstColumns)

        palLayer = QgsPalLayerSettings()
        # palLayer.enabled = blnEnabled
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        palLayer.placement = QgsPalLayerSettings.AroundPoint
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderUnit.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 8, QFont.Normal))
        txtFormat.setSize(8)
        palLayer.setFormat(txtFormat)

        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.amdLayer.setLabelsEnabled(True)
        self.amdLayer.setLabeling(labeling)

    # -------------------- LEGENDE -------------------------------------------

    def _legend(self):
        """
        Définition du type de légende et de l'échelle
        """
        lgd = QgsDataDefinedSizeLegend()
        # on définit le type de légende
        lgd.setLegendType(QgsDataDefinedSizeLegend.LegendCollapsed)
        lgd.setTitle("Légende")
        lgds = QgsMarkerSymbol()
        # couleur de remplissage de la légende
        lgds.symbolLayer(0).setColor(QColor("#d5d5d5"))
        # couleur de bordure de la légende
        lgds.symbolLayer(0).setStrokeColor(QColor("#ffffff"))
        # on définit 3 tailles de légendes: max, 1/2 et 1/10
        lgd.setClasses(
            [
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue), str(int(self.maxValue))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 2), str(int(self.maxValue / 2))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 10), str(int(self.maxValue / 10))
                ),
            ]
        )
        lgd.setSymbol(lgds)
        return lgd

    # ------------------------------------------------------------------------

    def delArretMD(self):
        """
        Suppression de l'analyse en montées / descentes par arrêts
        """

        if isinstance(self.amdLayer, QgsVectorLayer):
            QgsProject.instance().removeMapLayer(self.amdLayer.id())
        self.amdLayer = None

    # -------------------------------------------------------------------------

    def addArretMD(self, nbLignes):
        """
        Affichage de l'analyse en montées / descentes par arrêts
        """

        self._createTableArretsTmp(nbLignes)
        self._addLayerArretsTmp()

        # Suppression de la symbologie par défaut (entités ponctuelles)
        symbolLayer = QgsSimpleMarkerSymbolLayer()
        symbolLayer.setSize(0.0)
        symbolLayer.setColor(QColor(255, 255, 255, 255))

        self.amdLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayer
        )

        # line numbers in comments refer to qgsdiagramproperties.cpp
        diagram = QgsPieDiagram()  # 500 - Type de diagramme

        ds = QgsDiagramSettings()  # 507

        # On récupère la transparence dans paramètres
        paramTransp = float(self.qmapodSettings.value("params/spxArretTranspDiag", 30))
        fltAlpha = 1.0 - (paramTransp / 100)
        # on remplace transparency par 1 - transparency pour l'opacité
        ds.opacity = fltAlpha

        colM = self.qmapodSettings.value(
            "params/btnArretColMontees", QColor(255, 0, 0, 255), QColor
        )
        colM.setAlphaF(fltAlpha)
        colD = self.qmapodSettings.value(
            "params/btnArretColDescentes", QColor(255, 0, 0, 255), QColor
        )
        colD.setAlphaF(fltAlpha)
        colMS1 = self.qmapodSettings.value(
            "params/btnArretColMonteesS1", QColor(255, 0, 0, 255), QColor
        )
        colMS1.setAlphaF(fltAlpha)
        colMS2 = self.qmapodSettings.value(
            "params/btnArretColMonteesS2", QColor(255, 0, 0, 255), QColor
        )
        colMS2.setAlphaF(fltAlpha)
        colDS1 = self.qmapodSettings.value(
            "params/btnArretColDescentesS1", QColor(255, 0, 0, 255), QColor
        )
        colDS1.setAlphaF(fltAlpha)
        colDS2 = self.qmapodSettings.value(
            "params/btnArretColDescentesS2", QColor(255, 0, 0, 255), QColor
        )
        colDS2.setAlphaF(fltAlpha)

        # Sélection des champs à prendre en compte pour les diagrammes
        if self.typAnal == "md":
            lstColumns = ["montees", "descentes"]  # Liste des champs
            lstColors = [colM, colD]  # Liste des couleurs
            lstLabels = ["Montées", "Descentes"]  # Liste des libellés
            exp = "montees + descentes"
        elif self.typAnal == "m":
            lstColumns = ["montees"]  # Liste des champs
            lstColors = [colM]  # Liste des couleurs
            lstLabels = ["Montées"]  # Liste des libellés
            exp = "montees"
        elif self.typAnal == "d":
            lstColumns = ["descentes"]  # Liste des champs
            lstColors = [colD]  # Liste des couleurs
            lstLabels = ["Descentes"]  # Liste des libellés
            exp = "descentes"
        elif self.typAnal == "ms":
            lstColumns = ["ms1", "ms2"]  # Liste des champs
            lstColors = [colMS1, colMS2]  # Liste des couleurs
            lstLabels = ["Montées sens 1", "Montées sens 2"]  # Liste des libellés
            exp = "ms1 + ms2"
        elif self.typAnal == "ds":
            lstColumns = ["ds1", "ds2"]  # Liste des champs
            lstColors = [colDS1, colDS2]  # Liste des couleurs
            lstLabels = ["Descentes sens 1", "Descentes sens 2"]  # Liste des libellés
            exp = "ds1 + ds2"

        ds.categoryAttributes = lstColumns
        ds.categoryColors = lstColors
        ds.categoryLabels = lstLabels  # QGis 2.10

        ds.size = QSizeF(100.0, 100.0)  # 522 - Si taille fixe ?
        # ds.sizeType = 0  # 523 - mm(0), map units (1)
        ds.sizeType = QgsUnitTypes.RenderUnit.RenderMillimeters
        ds.labelPlacementMethod = 1  # 524 - from an existing example...
        ds.scaleByArea = True  # 525
        ds.minimumSize = 0.0  # 533

        # Couleur du contour des diagrammes
        # ds.backgroundColor = QColor(255, 255, 255, 0)  # 536 Transp White
        # ds.penColor = QColor(255, 255, 255, 255)  # 537
        outlineColor = self.qmapodSettings.value(
            "params/btnArretColOutline", QColor(255, 255, 255, 255), QColor
        )
        penColor = outlineColor
        penColor.setAlphaF(fltAlpha)
        ds.penColor = penColor
        # ds.penWidth = 0.4  # 538
        outlineWidth = float(
            self.qmapodSettings.value("params/spxArretOutlineWidth", 0.4)
        )
        ds.penWidth = outlineWidth
        # ds.minScaleDenominator = -1;  # 546
        # ds.maxScaleDenominator = -1;  # 547
        ds.diagramOrientation = 2  # 551 - May only be required for histograms
        # ds.barWidth = 5.0  # 553
        # We want a linear size interpolated version
        # so that Total_BA = 0 means they disappear
        # 564-572

        dr = QgsLinearlyInterpolatedDiagramRenderer()
        dr.setLowerValue(0.0)
        dr.setLowerSize(QSizeF(0.0, 0.0))

        # Pour la légende
        self.maxValue = self._getMaxVal("arrets_tmp", exp)
        dr.setUpperValue(self.maxValue)

        # Expression SQL spécifiant la taille
        dr.setClassificationAttributeExpression(exp)
        # La taille est spécifiée par une expression
        dr.setClassificationAttributeIsExpression(True)

        # Pour la légende
        # Taille de diagramme correspondant à la valeur maximale
        self.maxSize = int(self.qmapodSettings.value("params/spxArretDiagMaxSize", 40))
        # Taille de diagramme correspondant à la valeur maximale
        dr.setUpperSize(QSizeF(self.maxSize, self.maxSize))

        # dr.setClassificationAttribute(4)  # Colonne spécifiant la taille
        dr.setDiagram(diagram)
        dr.setDiagramSettings(ds)

        # And now finally put it into the layer... :-)
        self.amdLayer.setDiagramRenderer(dr)  # 572

        dls = QgsDiagramLayerSettings()  # 575
        dls.setDistance(0)
        dls.setIsObstacle(False)
        dls.setPriority(1)
        dls.setPlacement(QgsDiagramLayerSettings.OverPoint)  # 588
        dls.setShowAllDiagrams(True)
        dls.setZIndex(-1.0)
        self.amdLayer.setDiagramLayerSettings(dls)  # 593

        # affichage légende dans boite Couches
        dr.setAttributeLegend(True)
        dr.setDataDefinedSizeLegend(self._legend())

        # Application du filtrage carto pour les arrêts
        self.amdLayer.setSubsetString(self.currentFiltrage.dctWhereCarto["id_arret"])

        # Refresh map
        self.amdLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.amdLayer.id())

        # Étiquetage de la couche
        self._labelArretMD()

        # Pour contournement du bug sur majArretMD
        self.tbn.setChecked(True)

    # -------------------------------------------------------------------------
