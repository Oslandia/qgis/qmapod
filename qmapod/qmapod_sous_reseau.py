"""
/***************************************************************************
 QMapODSousReseau
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

import qgis.utils

# Import des librairies QGIS
from qgis.core import (
    QgsCategorizedSymbolRenderer,
    QgsDataSourceUri,
    QgsLayerTreeGroup,
    QgsLayerTreeLayer,
    QgsLineSymbol,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsProject,
    QgsProperty,
    QgsRendererCategory,
    QgsSimpleLineSymbolLayer,
    QgsSimpleMarkerSymbolLayer,
    QgsSingleSymbolRenderer,
    QgsSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)

# Import des librairies PyQt
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QColor, QFont
from qgis.PyQt.QtWidgets import QApplication

module_logger = logging.getLogger("QMapOD.sous_reseau")


class QMapODSousReseau(object):
    """
    Affichage du sous-réseau
    """

    def __init__(self, iface, dock, tbn, clsFiltrage, qmapodConfig, qmapodSettings):
        """
        Constructor
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        self.logger = logging.getLogger("QMapOD.flux_zones.QMapODSousReseau")
        self.logger.info("Creating an instance of QMapODSousReseau")

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        # Récupération de la référence au panneau ancrable
        self.dock = dock
        # Récupération de la référence à l'outil qui crée l'analyse
        # (2 outils pour le sous-réseau)
        self.tbn = tbn
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Récupération de l'instance de la classe filtrage
        self.currentFiltrage = clsFiltrage
        # Couches générées
        self.srArretLayer = None
        self.srTronconLayer = None
        self.srGroup = None

        # Vérification de l'existence de la colonne rvb
        # dans la table code_ligne
        self.blnRvb = True
        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la colonne rvb
            # dans la table code_ligne
            s = "\nSELECT * \n\
                 FROM sqlite_master \n\
                 WHERE name = 'code_ligne' AND sql LIKE('%rvb%')"
            self.logger.debug(s)
            cursor.execute(s)
            if cursor.fetchone() is None:
                db.close()
                self.blnRvb = False

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e
        finally:
            # Close the db connection
            db.close()

    # ------------------------------------------------------------------------

    def _addLayerArretsTmp(self):
        """
        Ajout des couches du sous-réseau
        """

        # Affichage de la couche des tronçons du sous-réseau
        # Ajout de la couche des tronçons du sous-réseau si elle n'existe pas
        uri = QgsDataSourceUri()
        uri.setDatabase(self.qmapodConfig.db)
        uri.setDataSource("", "sous_reseau_troncons_tmp", "geometry")
        srTronconLayer = QgsVectorLayer(uri.uri(), "Tronçons sous-réseau", "spatialite")
        if srTronconLayer.isValid():
            self.srTronconLayer = QgsProject.instance().addMapLayer(
                srTronconLayer, False
            )
            self.srTronconLayer.destroyed.connect(self._deleted)
            self.srTronconLayer.updateExtents()

        # Affichage de la couche des arrêts du sous-réseau
        # Ajout de la couche des arrêts du sous-réseau si elle n'existe pas
        uri = QgsDataSourceUri()
        uri.setDatabase(self.qmapodConfig.db)
        uri.setDataSource("", "sous_reseau_arrets_tmp", "geometry")
        srArretLayer = QgsVectorLayer(uri.uri(), "Arrêts sous-réseau", "spatialite")
        if srArretLayer.isValid():

            self.srArretLayer = QgsProject.instance().addMapLayer(srArretLayer, False)
            self.srArretLayer.destroyed.connect(self._deleted)

            self.srArretLayer.updateExtents()

    # ------------------------------------------------------------------------

    def _createTablesSousReseauTmp(self):
        """
        Création des tables sous_reseau_arrets_tmp et sous_reseau_troncons_tmp
        """

        try:
            # Connexion à la base de données
            db = qgis.utils.spatialite_connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table sous_reseau_arrets_tmp si elle existe
            s = "DROP TABLE IF EXISTS sous_reseau_arrets_tmp"
            self.logger.debug(s)
            cursor.execute(s)

            # Attention : pas de parenthèses pour faire fonctionner
            # la requête sur plusieurs lignes
            # Création de la table sous_reseau_arrets_tmp
            # Récupération de la clause where pour le sous-réseau
            strWhere = self.currentFiltrage.sqlSousReseau()

            # Création de la table temporaire des arrêts du sous-réseau
            # Modifié le 27/07/2015 pour n'afficher que les arrêts
            # desservis par le sous-réseau
            if self.blnRvb:
                # Version avec couleurs de lignes
                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/sous_reseau_1.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(str_where=strWhere)
            else:
                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/sous_reseau_2.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(str_where=strWhere)
            self.logger.debug(strSql)
            cursor.execute(strSql)

            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
                 WHERE f_table_name = 'sous_reseau_arrets_tmp'"
            self.logger.debug(s)
            cursor.execute(s)

            # Insertion de la déclaration de la colonne geometry
            # (type 1 = point)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/sous_reseau_3.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)

            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.qmapodConfig.db)
            # Get a cursor object
            cursor = db.cursor()

            # Suppression de la table sous_reseau_troncons_tmp si elle existe
            s = "DROP TABLE IF EXISTS sous_reseau_troncons_tmp;"
            self.logger.debug(s)
            cursor.execute(s)

            # Attention : pas de parenthèses pour faire fonctionner
            # la requête sur plusieurs lignes
            # Création de la table sous_reseau_troncons_tmp
            # Création de la table temporaire des tronçons du sous-réseau
            if self.blnRvb:
                # Version avec couleurs de lignes
                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/sous_reseau_4.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(str_where=strWhere)
            else:
                with open(
                    os.path.join(
                        self.qmapodConfig.pluginDir, "sql/queries/sous_reseau_5.sql"
                    )
                ) as f:
                    strSql = Template(f.read()).substitute(str_where=strWhere)
            self.logger.debug(strSql)
            cursor.execute(strSql)

            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
                WHERE f_table_name = 'sous_reseau_troncons_tmp'"
            self.logger.debug(s)
            cursor.execute(s)

            # Insertion de la déclaration de la colonne geometry
            # (type 2 = polyligne)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/sous_reseau_6.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e
        finally:
            # Close the db connection
            db.close()

    # ------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation des couches si elles sont supprimées
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.srTronconLayer = None
        self.srArretLayer = None
        self.srGroup = None
        self.tbn.setChecked(False)

    # ------------------------------------------------------------------------

    def _labelArretSousReseau(self):
        """
        ### maj QGIS 3.x
        Étiquetage de la couche des arrêts
        """

        # Paramètres d'étiquetage
        intMode = int(self.qmapodSettings.value("params/cbxArretSousReseauLabel", 4))

        if intMode == 0:
            self.srArretLayer.setLabelsEnabled(True)
            self.srArretLayer.setLabeling(None)
            return
        elif intMode == 1:
            strField = "id_arret"
            blnExp = False
        elif intMode == 2:
            strField = "nom_arret"
            blnExp = False
        elif intMode == 3:
            strField = '"id_arret" || \'\\n\' || "nom_arret"'
            blnExp = True
        else:
            return

        palLayer = QgsPalLayerSettings()
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        palLayer.placement = QgsPalLayerSettings.AroundPoint
        palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter
        palLayer.dist = 1
        palLayer.bufferDraw = True

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 7, QFont.Normal))
        txtFormat.setSize(7)
        palLayer.setFormat(txtFormat)

        # Validation des propriétés d'étiquetage
        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.srArretLayer.setLabelsEnabled(True)
        self.srArretLayer.setLabeling(labeling)

    # ------------------------------------------------------------------------

    def addSousReseau(self):
        """
        Choix du mode d'affichage du sous-réseau
        """

        if (
            int(self.qmapodSettings.value("params/buttonGroupSousReseauMode", 1)) == 1
        ):  # Couleur unique
            self.addSousReseauRaw()
        else:
            self.addSousReseauColor()

    # ------------------------------------------------------------------------

    def addSousReseauColor(self):
        """
        Affichage du sous-réseau avec les couleurs de lignes
        """

        # Création des tables temporaires
        self._createTablesSousReseauTmp()
        self._addLayerArretsTmp()

        # Récupération du paramètre de transparence
        intTransparency = int(
            self.qmapodSettings.value("params/spxTranspSousReseau", 30)
        )

        # Paramétrage du style de la couche des arrêts du sous-réseau
        categories = []

        # Style de point pour les troncs communs
        symbol = QgsMarkerSymbol.createSimple(
            {"color": "100, 100, 100", "name": "circle", "size": "2.0"}
        )
        color = self.qmapodSettings.value(
            "params/btnColTronCom", QColor(100, 100, 100, 255), QColor
        )
        size = float(self.qmapodSettings.value("params/spxArretsSize", 1.2))
        symbol.setSize(size)
        symbol.setColor(color)
        category = QgsRendererCategory("", symbol, "Tronc commun")
        categories.append(category)

        # Style de point pour les lignes isolées
        # (couleur définie par la colonne rvb)
        symbol = QgsMarkerSymbol.createSimple(
            {"color": "255, 100, 100", "name": "circle", "size": "2.0"}
        )

        symbolMarker = QgsSimpleMarkerSymbolLayer()
        symbolMarker.setSize(size)
        symbolMarker.setColor(QColor(255, 0, 0, 255))

        symbolMarker.setDataDefinedProperty(
            QgsSymbolLayer.PropertyFillColor, QgsProperty.fromExpression("rvb")
        )
        symbol.changeSymbolLayer(0, symbolMarker)

        category = QgsRendererCategory(1, symbol, "Arrêts")
        categories.append(category)

        self.srArretLayer.setRenderer(QgsCategorizedSymbolRenderer("nb", categories))
        self.srArretLayer.setOpacity(intTransparency / 50)

        # Paramétrage du style de la couche des tronçons du sous-réseau
        categories = []

        # Style de ligne pour les troncs communs
        width = float(self.qmapodSettings.value("params/spxTronconsWidth", 1.0))
        color = self.qmapodSettings.value(
            "params/btnColTronCom", QColor(100, 100, 100, 255), QColor
        )

        symbol = QgsLineSymbol.createSimple("")
        symbolLine = QgsSimpleLineSymbolLayer()
        symbolLine.setWidth(width)
        symbolLine.setColor(color)
        symbolLine.setPenCapStyle(Qt.RoundCap)
        symbolLine.setPenJoinStyle(Qt.RoundJoin)
        symbol.changeSymbolLayer(0, symbolLine)

        category = QgsRendererCategory("", symbol, "Tronc commun")
        categories.append(category)

        # Style de ligne pour les lignes isolées
        # (couleur définie par la colonne rvb)
        symbol = QgsLineSymbol.createSimple({"color": "255, 100, 100", "width": "1.0"})
        symbolLine = QgsSimpleLineSymbolLayer()
        symbolLine.setWidth(width)
        symbolLine.setColor(QColor(255, 0, 0, 255))
        symbolLine.setPenCapStyle(Qt.RoundCap)
        symbolLine.setPenJoinStyle(Qt.RoundJoin)
        symbolLine.setDataDefinedProperty(
            QgsSymbolLayer.PropertyStrokeColor, QgsProperty.fromExpression("rvb")
        )
        symbol.changeSymbolLayer(0, symbolLine)
        category = QgsRendererCategory(1, symbol, "Lignes")
        categories.append(category)

        self.srTronconLayer.setRenderer(QgsCategorizedSymbolRenderer("nb", categories))
        self.srTronconLayer.setOpacity(intTransparency / 50)

        # Ajout des couches et déplacement dans un groupe (api QgsLayerTree)
        root = QgsProject.instance().layerTreeRoot()
        self.srGroup = root.insertGroup(0, "Sous-réseau courant")

        # Couche des arrêts
        QgsProject.instance().addMapLayer(self.srArretLayer, False)
        srArretTreeLayer = QgsLayerTreeLayer(self.srArretLayer)
        srArretTreeLayer.setName(self.srArretLayer.name())

        self.srGroup.insertChildNode(0, srArretTreeLayer)

        # Couche des tronçons
        QgsProject.instance().addMapLayer(self.srTronconLayer, False)
        srTronconTreeLayer = QgsLayerTreeLayer(self.srTronconLayer)
        srTronconTreeLayer.setName(self.srTronconLayer.name())

        self.srGroup.insertChildNode(1, srTronconTreeLayer)

        # Étiquetage de la couche
        self._labelArretSousReseau()

        # Refresh map
        self.srArretLayer.triggerRepaint()

        self.iface.layerTreeView().refreshLayerSymbology(self.srArretLayer.id())

        self.srTronconLayer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(self.srTronconLayer.id())

        self.iface.mapCanvas().refresh()

        # Décochage du bouton (au cas où la méthode
        # est lancée par programmation)
        self.tbn.setChecked(True)

    # ------------------------------------------------------------------------

    def addSousReseauRaw(self):
        """
        Affichage du sous-réseau
        """

        # Création des tables temporaires
        self._createTablesSousReseauTmp()
        self._addLayerArretsTmp()

        # Récupération du paramètre de transparence
        intTransparency = int(
            self.qmapodSettings.value("params/spxTranspSousReseau", 30)
        )
        fltOpacity = (100.0 - intTransparency) / 100

        # Paramétrage du style de la couche des tronçons du sous-réseau
        color = self.qmapodSettings.value(
            "params/btnColTroncons", QColor(255, 0, 0, 255), QColor
        )
        width = float(self.qmapodSettings.value("params/spxTronconsWidth", 0.4))

        symbol = QgsLineSymbol.createSimple({})

        symbolLine = QgsSimpleLineSymbolLayer()
        symbolLine.setWidth(width)
        symbolLine.setColor(color)
        symbolLine.setPenCapStyle(Qt.RoundCap)
        symbolLine.setPenJoinStyle(Qt.RoundJoin)
        symbol.changeSymbolLayer(0, symbolLine)

        self.srTronconLayer.setRenderer(QgsSingleSymbolRenderer(symbol))

        self.srTronconLayer.setOpacity(fltOpacity)

        # Paramétrage du style de la couche des arrêts du sous-réseau
        color = self.qmapodSettings.value(
            "params/btnColArrets", QColor(255, 0, 0, 255), QColor
        )
        size = float(self.qmapodSettings.value("params/spxArretsSize", 1.2))

        symbol = QgsMarkerSymbol.createSimple(
            {"color": "255, 100, 100", "name": "circle", "size": "1.2"}
        )
        symbol.setColor(color)
        symbol.setSize(size)

        self.srArretLayer.setRenderer(QgsSingleSymbolRenderer(symbol))

        self.srArretLayer.setOpacity(fltOpacity)

        # Ajout des couches et déplacement dans un groupe (api QgsLayerTree)
        root = QgsProject.instance().layerTreeRoot()
        self.srGroup = root.insertGroup(0, "Sous-réseau courant")

        # Couche des arrêts
        QgsProject.instance().addMapLayer(self.srArretLayer, False)
        srArretTreeLayer = QgsLayerTreeLayer(self.srArretLayer)
        srArretTreeLayer.setName(self.srArretLayer.name())

        self.srGroup.insertChildNode(0, srArretTreeLayer)

        QgsProject.instance().addMapLayer(self.srTronconLayer, False)
        srTronconTreeLayer = QgsLayerTreeLayer(self.srTronconLayer)
        srTronconTreeLayer.setName(self.srTronconLayer.name())

        self.srGroup.insertChildNode(1, srTronconTreeLayer)

        # Étiquetage de la couche
        self._labelArretSousReseau()

        # Refresh map
        self.srArretLayer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(self.srArretLayer.id())

        self.srTronconLayer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(self.srTronconLayer.id())

        self.iface.mapCanvas().refresh()

        # Décochage du bouton (au cas où la méthode
        # est lancée par programmation)
        self.tbn.setChecked(True)

    # -------------------------------------------------------------------------

    def delSousReseau(self):
        """
        Suppression de l'affichage du sous-réseau
        """

        # Suppression du groupe de couches sous-réseau
        if isinstance(self.srGroup, QgsLayerTreeGroup):
            root = QgsProject.instance().layerTreeRoot()
            root.removeChildNode(self.srGroup)
        self.srGroup = None
        self.srArretLayer = None
        self.srTronconLayer = None

    # -------------------------------------------------------------------------

    def majSpxTronconsWidth(self, width):
        if isinstance(self.srTronconLayer, QgsVectorLayer):
            self.srTronconLayer.renderer().symbol().setWidth(width)
            self.iface.mapCanvas().refresh()

    # -------------------------------------------------------------------------

    def majSousReseau(self):
        """
        Choix du mode d'affichage du sous-réseau
        """

        if (
            int(self.qmapodSettings.value("params/buttonGroupSousReseauMode", 1)) == 1
        ):  # Couleur unique
            self.majSousReseauRaw()
        else:
            self.majSousReseauColor()

    # -------------------------------------------------------------------------

    def majSousReseauColor(self):
        """
        Mise à jour de l'affichage du sous-réseau avec couleurs de lignes
        """

        # Mise à jour des tables sous_reseau_arrets_tmp
        # et sous_reseau_troncons_tmp
        self._createTablesSousReseauTmp()
        if isinstance(self.srTronconLayer, QgsVectorLayer):
            self.srTronconLayer.updateExtents()
        if isinstance(self.srArretLayer, QgsVectorLayer):
            self.srArretLayer.updateExtents()

        # Récupération du paramètre de transparence
        intTransparency = int(
            self.qmapodSettings.value("params/spxTranspSousReseau", 30)
        )
        fltOpacity = (100.0 - intTransparency) / 100

        # Paramétrage du style de la couche des arrêts du sous-réseau
        categories = []

        # Style de point pour les troncs communs
        symbol = QgsMarkerSymbol.createSimple(
            {"color": "100, 100, 100", "name": "circle", "size": "2.0"}
        )
        color = self.qmapodSettings.value(
            "params/btnColTronCom", QColor(100, 100, 100, 255), QColor
        )
        size = float(self.qmapodSettings.value("params/spxArretsSize", 1.2))
        symbol.setSize(size)
        symbol.setColor(color)
        category = QgsRendererCategory("", symbol, "Tronc commun")
        categories.append(category)

        # Style de point pour les lignes isolées
        # (couleur définie par la colonne rvb)
        symbol = QgsMarkerSymbol.createSimple(
            {"color": "255, 100, 100", "name": "circle", "size": "2.0"}
        )
        symbolMarker = QgsSimpleMarkerSymbolLayer()
        symbolMarker.setSize(size)
        symbolMarker.setColor(QColor(255, 0, 0, 255))
        symbolMarker.setDataDefinedProperty(
            QgsSymbolLayer.PropertyFillColor, QgsProperty.fromExpression("rvb")
        )
        symbol.changeSymbolLayer(0, symbolMarker)
        category = QgsRendererCategory(1, symbol, "Arrêts")
        categories.append(category)

        self.srArretLayer.setRenderer(QgsCategorizedSymbolRenderer("nb", categories))
        self.srArretLayer.setOpacity(fltOpacity)

        # Paramétrage du style de la couche des tronçons du sous-réseau
        categories = []

        # Style de ligne pour les troncs communs
        color = self.qmapodSettings.value(
            "params/btnColTronCom", QColor(100, 100, 100, 255), QColor
        )
        width = float(self.qmapodSettings.value("params/spxTronconsWidth", 1.0))

        symbol = QgsLineSymbol.createSimple({})
        symbolLine = QgsSimpleLineSymbolLayer()
        symbolLine.setWidth(width)
        symbolLine.setColor(color)
        symbolLine.setPenCapStyle(Qt.RoundCap)
        symbolLine.setPenJoinStyle(Qt.RoundJoin)
        symbol.changeSymbolLayer(0, symbolLine)

        category = QgsRendererCategory("", symbol, "Tronc commun")
        categories.append(category)

        # Style de ligne pour les lignes isolées
        # (couleur définie par la colonne rvb)
        symbol = QgsLineSymbol.createSimple({"color": "255, 100, 100", "width": "1.0"})
        symbolLine = QgsSimpleLineSymbolLayer()
        symbolLine.setWidth(width)
        symbolLine.setColor(QColor(255, 0, 0, 255))
        symbolLine.setPenCapStyle(Qt.RoundCap)
        symbolLine.setPenJoinStyle(Qt.RoundJoin)
        symbolLine.setDataDefinedProperty(
            QgsSymbolLayer.PropertyStrokeColor, QgsProperty.fromExpression("rvb")
        )
        symbol.changeSymbolLayer(0, symbolLine)

        category = QgsRendererCategory(1, symbol, "Lignes")
        categories.append(category)

        self.srTronconLayer.setRenderer(QgsCategorizedSymbolRenderer("nb", categories))
        self.srTronconLayer.setOpacity(fltOpacity)

        # Étiquetage de la couche
        self._labelArretSousReseau()

        # Refresh map
        self.srArretLayer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(self.srArretLayer.id())

        self.srTronconLayer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(self.srTronconLayer.id())

        self.iface.mapCanvas().refresh()

    # -------------------------------------------------------------------------

    def majSousReseauRaw(self):
        """
        Mise à jour de l'affichage du sous-réseau
        """

        # Mise à jour des tables sous_reseau_arrets_tmp
        # et sous_reseau_troncons_tmp
        self._createTablesSousReseauTmp()
        if isinstance(self.srTronconLayer, QgsVectorLayer):
            self.srTronconLayer.updateExtents()
        if isinstance(self.srArretLayer, QgsVectorLayer):
            self.srArretLayer.updateExtents()

        # Récupération du paramètre de transparence
        intTransparency = int(
            self.qmapodSettings.value("params/spxTranspSousReseau", 30)
        )
        fltOpacity = (100.0 - intTransparency) / 100

        # Paramétrage du style de la couche des arrêts du sous-réseau
        color = self.qmapodSettings.value(
            "params/btnColTroncons", QColor(255, 0, 0, 255), QColor
        )
        width = float(self.qmapodSettings.value("params/spxTronconsWidth", 0.4))

        symbol = QgsLineSymbol.createSimple({})
        symbolLine = QgsSimpleLineSymbolLayer()
        symbolLine.setWidth(width)
        symbolLine.setColor(color)
        symbolLine.setPenCapStyle(Qt.RoundCap)
        symbolLine.setPenJoinStyle(Qt.RoundJoin)
        symbol.changeSymbolLayer(0, symbolLine)

        self.srTronconLayer.setRenderer(QgsSingleSymbolRenderer(symbol))
        self.srTronconLayer.setOpacity(fltOpacity)

        # Paramétrage du style de la couche des arrêts du sous-réseau
        color = self.qmapodSettings.value(
            "params/btnColArrets", QColor(255, 0, 0, 255), QColor
        )
        size = float(self.qmapodSettings.value("params/spxArretsSize", 1.2))

        symbol = QgsMarkerSymbol.createSimple(
            {"color": "255, 100, 100", "name": "circle", "size": "1.2"}
        )
        symbol.setColor(color)
        symbol.setSize(size)

        self.srArretLayer.setRenderer(QgsSingleSymbolRenderer(symbol))
        self.srArretLayer.setOpacity(fltOpacity)

        # Étiquetage de la couche
        self._labelArretSousReseau()

        # Refresh map
        self.srArretLayer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(self.srArretLayer.id())

        self.srTronconLayer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(self.srTronconLayer.id())
        self.iface.mapCanvas().refresh()
