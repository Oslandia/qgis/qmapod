"""
/***************************************************************************
 QMapODDock
                                 A QGIS plugin
 Plugin qmapod
                             -------------------
        begin                : 2014-10-02
        git sha              : $Format:%H$
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import getpass
import logging
import os
import sqlite3
import sys
from collections import OrderedDict

# Import des librairies QGIS
from qgis.gui import QgsColorButton

# Import des librairies PyQt
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QSize, Qt
from qgis.PyQt.QtGui import QColor, QFont, QIcon
from qgis.PyQt.QtWidgets import (
    QAbstractItemView,
    QCheckBox,
    QDockWidget,
    QFormLayout,
    QHBoxLayout,
    QLabel,
    QListWidget,
    QListWidgetItem,
    QSizePolicy,
    QSpacerItem,
    QTreeWidgetItem,
)

# Chargement de la configuration de l'application
# from qmapod_config import QMapODConfig

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "qmapod_dock_base.ui")
)

module_logger = logging.getLogger("QMapOD.qmapod_dock")


class QMapODDock(QDockWidget, FORM_CLASS):

    def __init__(self, qmapodConfig, qmapodSettings, parent=None):
        """
        Constructor.
        :parent : instance de l'interface générée par QTDesigner
        """

        super(QMapODDock, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Chemin d'accès à la base de données
        self.pluginDir = self.qmapodConfig.pluginDir
        self.db = self.qmapodConfig.db

        # Récupération du paramétrage du plugin
        self.qmapodSettings = qmapodSettings

        self.logger = logging.getLogger("QMapOD.qmapod_dock.QMapODDock")
        self.logger.info("Creating an instance of QMapODDock")

        # Masquage de l'onglet test
        if getpass.getuser() not in ["SIGEAL", "Jean-Luc", "abousquet"]:
            self.tabFiltrage.removeTab(4)

        # Paramétrage du textEdit récapitulatif
        self.tedRecap.setReadOnly(True)

        # Chargement de la liste des types de jour
        dctTypesJour = self.qmapodConfig.odtTypeJour["cbxTypeJour"]
        con = None
        try:
            con = sqlite3.connect(self.db)
            cur = con.cursor()
            cur.execute(dctTypesJour["sql"])
            data = cur.fetchall()

        except sqlite3.Error as e:
            self.logger.error("Error %s:" % e.args[0])
            sys.exit(1)

        finally:
            if con:
                con.close()

        for item in data:
            self.cbxTypeJour.addItem((item[1]), item[0])

        # Chargement du sous-réseau dans le treeWidget twgSousReseau
        self.nomReseau = self.qmapodConfig.dctParam["nomreseau"]
        self._twgSousReseauPopulate()

        """
        Création/remplissage des listes de critères à l'exécution
        car leur présence et leur contenu sont paramétrables
        """
        # Liste des QListWidget / QCheckBox associés
        self.lstListWidget = {}
        self.lstCheckboxAll = {}

        # Critères Signalétique
        for lwgName, dctParam in self.qmapodConfig.odtCritereSignaletique.items():
            self.lwgObj = self._addListWidget(
                dctParam["objlbl"],
                " ".join([dctParam["label"], ":"]),
                lwgName,
                self.verticalLayout_11,
                self.tabFiltreEnquete,
            )
            self._populateListWidget(self.lwgObj, dctParam["sql"])

        # Critères Voyage
        for lwgName, dctParam in self.qmapodConfig.odtCritereVoyage.items():
            self.lwgObj = self._addListWidget(
                dctParam["objlbl"],
                " ".join([dctParam["label"], ":"]),
                lwgName,
                self.verticalLayout_12,
                self.tabFiltreEnquete,
            )
            self._populateListWidget(self.lwgObj, dctParam["sql"])

        # Autres critères
        for lwgName, dctParam in self.qmapodConfig.odtCritereAutre.items():
            self.lwgObj = self._addListWidget(
                dctParam["objlbl"],
                " ".join([dctParam["label"], ":"]),
                lwgName,
                self.verticalLayout_13,
                self.tabFiltreEnquete,
            )
            self._populateListWidget(self.lwgObj, dctParam["sql"])

        # Critères Carto
        # Liste des arrêts
        # TODO : Remplissage dynamique de la liste en fonction
        # du sous-réseau sélectionné
        self.lwgArret = self._addListWidget(
            "lblArret",
            "Arrêts :",
            "lwgArret",
            self.verticalLayout_14,
            self.tabFiltreEnquete,
        )
        self._populateListWidget(
            self.lwgArret,
            "\n\
            SELECT id_arret, nom_arret \n\
            FROM arrets\n\
            WHERE fictif = 0 \n\
            ORDER BY nom_arret;\n\
        ",
        )

        # Critères zonages
        for lwgName, dctParam in self.qmapodConfig.odtZonage.items():
            self.lwgObj = self._addListWidget(
                dctParam["objlbl"],
                " ".join([dctParam["label"], ":"]),
                lwgName,
                self.verticalLayout_15,
                self.tabFiltreEnquete,
            )
            self._populateListWidget(self.lwgObj, dctParam["sql"])

        # Ajout d'un bouton sélecteur de couleur
        self.btnColor = QgsColorButton(
            self.gpbFlux,
            "Couleur des montées",
        )
        self.btnColor.setObjectName("btnColor")
        self.formLayout.setWidget(
            self.formLayout.count() + 1, QFormLayout.LabelRole, self.btnColor
        )

        # Liste des critères analysables (arrêts)
        self._populateComboBox(
            self.cbxVarArret, self.qmapodConfig.odtVariableAnalysable
        )

        # Liste des zonages disponibles
        self._populateComboBox(self.cbxMDZone, self.qmapodConfig.odtZonageAnalysable)
        self._populateComboBox(self.cbxFluxZone, self.qmapodConfig.odtZonageAnalysable)

        # Liste des critères analysables (zones)
        self._populateComboBox(self.cbxVarZone, self.qmapodConfig.odtVariableAnalysable)

        # ----------------------------------------------------------------
        # Initialisation des valeurs de contrôles
        # ----------------------------------------------------------------
        self._initParams(self.qmapodSettings)

    def _initParams(self, qmapodSettings):
        """
        Initialisation des contrôles de l'onglet Paramètres
        """

        # ----------------------------------------------------------------
        # Rubrique Sous-réseau
        # ----------------------------------------------------------------
        # Type de représentation
        self.buttonGroupSousReseauMode.setId(self.rbnSingleColor, 1)
        self.buttonGroupSousReseauMode.setId(self.rbnLineColors, 2)
        self.buttonGroupSousReseauMode.button(
            int(qmapodSettings.value("params/buttonGroupSousReseauMode", 1))
        ).setChecked(True)
        # Couleur des troncs communs
        self.btnColTronCom.setColor(
            qmapodSettings.value(
                "params/btnColTronCom", QColor(100, 100, 100, 255), QColor
            )
        )
        # Couleur des tronçons
        self.btnColTroncons.setColor(
            qmapodSettings.value(
                "params/btnColTroncons", QColor(255, 0, 0, 255), QColor
            )
        )
        # Épaisseur des tronçons
        self.spxTronconsWidth.setRange(0, 10)
        self.spxTronconsWidth.setSingleStep(0.1)
        self.spxTronconsWidth.setValue(
            float(qmapodSettings.value("params/spxTronconsWidth", 0.4))
        )
        # Couleur des arrêts
        self.btnColArrets.setColor(
            qmapodSettings.value("params/btnColArrets", QColor(255, 0, 0, 255), QColor)
        )
        # Taille des arrêts
        self.spxArretsSize.setRange(0, 10)
        self.spxArretsSize.setSingleStep(0.1)
        self.spxArretsSize.setValue(
            float(qmapodSettings.value("params/spxArretsSize", 1.4))
        )
        # Transparence du sous-réseau
        self.spxTranspSousReseau.setRange(0, 100)
        self.spxTranspSousReseau.setValue(
            int(qmapodSettings.value("params/spxTranspSousReseau", 30))
        )
        # Étiquetage des entités
        self.cbxArretSousReseauLabel.clear()
        self.cbxArretSousReseauLabel.addItems(
            ["Aucun", "ID arrêt", "Nom arrêt", "ID/Nom arrêt"]
        )
        self.cbxArretSousReseauLabel.setCurrentIndex(
            int(qmapodSettings.value("params/cbxArretSousReseauLabel", 0))
        )

        # ----------------------------------------------------------------
        # Rubrique Arrêts
        # ----------------------------------------------------------------
        # Diamètre maxi des diagrammes de montées/descentes (QSpinbox)
        self.spxArretDiagMaxSize.setRange(0, 100)
        self.spxArretDiagMaxSize.setSingleStep(1)
        self.spxArretDiagMaxSize.setValue(
            int(qmapodSettings.value("params/spxArretDiagMaxSize", 40))
        )
        # Couleur du secteur Montées des diagrammes
        self.btnArretColMontees.setColor(
            qmapodSettings.value(
                "params/btnArretColMontees", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur du secteur Descentes des diagrammes
        self.btnArretColDescentes.setColor(
            qmapodSettings.value(
                "params/btnArretColDescentes", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur du secteur Montées sens 1 des diagrammes
        self.btnArretColMonteesS1.setColor(
            qmapodSettings.value(
                "params/btnArretColMonteesS1", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur du secteur Montées sens 2 des diagrammes
        self.btnArretColMonteesS2.setColor(
            qmapodSettings.value(
                "params/btnArretColMonteesS2", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur du secteur Descentes sens 1 des diagrammes
        self.btnArretColDescentesS1.setColor(
            qmapodSettings.value(
                "params/btnArretColDescentesS1", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur du secteur Descentes sens 2 des diagrammes
        self.btnArretColDescentesS2.setColor(
            qmapodSettings.value(
                "params/btnArretColDescentesS2", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur des contours de diagrammes
        self.btnArretColOutline.setColor(
            qmapodSettings.value(
                "params/btnArretColOutline", QColor(255, 255, 255, 255), QColor
            )
        )
        # Épaisseur des contours de diagrammes
        self.spxArretOutlineWidth.setRange(0.0, 5.0)
        self.spxArretOutlineWidth.setSingleStep(0.1)
        self.spxArretOutlineWidth.setValue(
            float(qmapodSettings.value("params/spxArretOutlineWidth", 0.4))
        )
        # Transparence des diagrammes
        self.spxArretTranspDiag.setRange(0, 100)
        self.spxArretTranspDiag.setValue(
            int(qmapodSettings.value("params/spxArretTranspDiag", 30))
        )
        # Étiquetage des entités
        self.cbxArretLabel.clear()
        self.cbxArretLabel.addItems(
            [
                "Aucun",
                "Selon analyse",
                "Nom - Selon analyse",
                "Montées+Descentes",
                "Nom - Montées + Descentes",
                "Nom entité",
                "ID entité",
            ]
        )
        self.cbxArretLabel.setCurrentIndex(
            int(qmapodSettings.value("params/cbxArretLabel", 1))
        )
        # Mode de prise en compte des enquêtes
        self.buttonGroupArretMode.setId(self.rbnArretVoyage, 1)
        self.buttonGroupArretMode.setId(self.rbnArretDeplacement, 2)
        self.buttonGroupArretMode.button(
            int(qmapodSettings.value("params/buttonGroupArretMode", 1))
        ).setChecked(True)
        # Paramétrage des boutons radio voyage / déplacement
        # (une seule ligne sélectionnée par défaut)
        self.gbxArretVD.setDisabled(True)
        self.rbnArretVoyage.setDisabled(True)
        self.rbnArretVoyage.setChecked(True)
        self.rbnArretDeplacement.setDisabled(True)
        self.rbnArretDeplacement.setChecked(False)

        # ----------------------------------------------------------------
        # Rubrique Zones
        # ----------------------------------------------------------------
        # Diamètre maxi des diagrammes de montées/descentes (QSpinbox)
        self.spxZoneDiagMaxSize.setRange(0, 100)
        self.spxZoneDiagMaxSize.setSingleStep(1)
        self.spxZoneDiagMaxSize.setValue(
            int(qmapodSettings.value("params/spxZoneDiagMaxSize", 40))
        )
        # Couleur du secteur Montées des diagrammes
        self.btnZoneColMontees.setColor(
            qmapodSettings.value(
                "params/btnZoneColMontees", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur du secteur Descentes des diagrammes
        self.btnZoneColDescentes.setColor(
            qmapodSettings.value(
                "params/btnZoneColDescentes", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur des contours de diagrammes
        self.btnZoneColOutline.setColor(
            qmapodSettings.value(
                "params/btnZoneColOutline", QColor(255, 255, 255, 255), QColor
            )
        )
        # Épaisseur des contours de diagrammes
        self.spxZoneOutlineWidth.setRange(0.0, 5.0)
        self.spxZoneOutlineWidth.setSingleStep(0.1)
        self.spxZoneOutlineWidth.setValue(
            float(qmapodSettings.value("params/spxZoneOutlineWidth", 0.4))
        )
        # Transparence des diagrammes
        self.spxZoneTranspDiag.setRange(0, 100)
        self.spxZoneTranspDiag.setValue(
            int(qmapodSettings.value("params/spxZoneTranspDiag", 30))
        )
        # Étiquetage des entités
        self.cbxZoneLabel.clear()
        self.cbxZoneLabel.addItems(
            [
                "Aucun",
                "Selon analyse",
                "Nom - Selon analyse",
                "Montées+Descentes",
                "Nom - Montées + Descentes",
                "Nom entité",
                "ID entité",
            ]
        )
        self.cbxZoneLabel.setCurrentIndex(
            int(qmapodSettings.value("params/cbxZoneLabel", 1))
        )
        # Mode de prise en compte des enquêtes
        self.buttonGroupZoneMode.setId(self.rbnZoneVoyage, 1)
        self.buttonGroupZoneMode.setId(self.rbnZoneDeplacement, 2)
        self.buttonGroupZoneMode.button(
            int(qmapodSettings.value("params/buttonGroupZoneMode", 1))
        ).setChecked(True)
        # Paramétrage des boutons radio voyage / déplacement
        # (une seule ligne sélectionnée par défaut)
        self.gbxZoneVD.setDisabled(False)
        self.rbnZoneVoyage.setDisabled(False)
        self.rbnZoneDeplacement.setDisabled(False)

        # ----------------------------------------------------------------
        # Rubrique Tronçons
        # ----------------------------------------------------------------
        # Épaisseur mini des serpents
        self.spxSerpMinWidth.setRange(0.0, 5.0)
        self.spxSerpMinWidth.setSingleStep(0.1)
        self.spxSerpMinWidth.setValue(
            float(qmapodSettings.value("params/spxSerpMinWidth", 0.2))
        )
        # Épaisseur maxi des serpents
        self.spxSerpMaxWidth.setRange(0.0, 20.0)
        self.spxSerpMaxWidth.setSingleStep(0.1)
        self.spxSerpMaxWidth.setValue(
            float(qmapodSettings.value("params/spxSerpMaxWidth", 2))
        )
        # Nombre maximum de classes de valeurs (offre, charge, performance)
        self.spxSerpNbClasses.setRange(0, 10)
        self.spxSerpNbClasses.setValue(
            int(qmapodSettings.value("params/spxSerpNbClasses", 5))
        )
        # Méthode de classification des épaisseurs
        self.cbxSerpClassification.addItem("Intervalles égaux", 1)
        self.cbxSerpClassification.addItem("Quantile(effectifs égaux", 2)
        self.cbxSerpClassification.addItem("Ruptures naturelles (Jenks)", 3)
        self.cbxSerpClassification.addItem("Écart- type", 4)
        self.cbxSerpClassification.addItem("Jolies ruptures", 5)
        self.cbxSerpClassification.setCurrentIndex(
            int(qmapodSettings.value("params/cbxSerpClassification", 3))
        )
        # Couleur du serpent d'offre
        self.btnColSOffre.setColor(
            qmapodSettings.value("params/btnColSOffre", QColor(255, 0, 0, 255), QColor)
        )
        # Couleur du serpent de charge
        self.btnColSCharge.setColor(
            qmapodSettings.value("params/btnColSCharge", QColor(255, 0, 0, 255), QColor)
        )
        # Couleur du serpent de performance
        self.btnColSPerf.setColor(
            qmapodSettings.value("params/btnColSPerf", QColor(255, 0, 0, 255), QColor)
        )
        # Transparence des serpents
        self.spxTransparencySerpent.setRange(0, 100)
        self.spxTransparencySerpent.setValue(
            int(qmapodSettings.value("params/spxTransparencySerpent", 30))
        )
        # Étiquetage des entités
        self.cbxLabelSerpent.clear()
        self.cbxLabelSerpent.addItems(["Aucun", "ID entité", "Selon analyse"])
        self.cbxLabelSerpent.setCurrentIndex(
            int(qmapodSettings.value("params/cbxLabelSerpent", 2))
        )

        # ----------------------------------------------------------------
        # Rubrique flux principaux
        # ----------------------------------------------------------------
        self.spxFluxPPLargMaxFleches.setValue(
            int(qmapodSettings.value("params/spxFluxPPLargMaxFleches", 12))
        )
        self.btnFluxPPCol.setColor(
            qmapodSettings.value(
                "params/btnFluxPPCol", QColor(154, 125, 238, 255), QColor
            )
        )
        self.btnFluxPPColOutline.setColor(
            qmapodSettings.value(
                "params/btnFluxPPColOutline", QColor(154, 125, 238, 255), QColor
            )
        )
        self.btnFluxPPLColSens1.setColor(
            qmapodSettings.value(
                "params/btnFluxPPLColSens1", QColor(51, 160, 44, 255), QColor
            )
        )
        self.btnFluxPPLColOutlineSens1.setColor(
            qmapodSettings.value(
                "params/btnFluxPPLColOutlineSens1", QColor(51, 160, 44, 255), QColor
            )
        )
        self.btnFluxPPLColSens2.setColor(
            qmapodSettings.value(
                "params/btnFluxPPLColSens2", QColor(255, 127, 0, 255), QColor
            )
        )
        self.btnFluxPPLColOutlineSens2.setColor(
            qmapodSettings.value(
                "params/btnFluxPPLColOutlineSens2", QColor(255, 127, 0, 255), QColor
            )
        )
        self.spxFluxPPOutlineWidth.setValue(
            float(qmapodSettings.value("params/spxFluxPPOutlineWidth", 0.4))
        )
        self.spxFluxPPTranspFleches.setValue(
            int(qmapodSettings.value("params/spxFluxPPTranspFleches", 50))
        )
        self.btnFluxPPColZones.setColor(
            qmapodSettings.value(
                "params/btnFluxPPColZones", QColor(100, 100, 100, 255), QColor
            )
        )
        self.btnFluxPPColOutlineZones.setColor(
            qmapodSettings.value(
                "params/btnFluxPPColOutlineZones", QColor(0, 0, 0, 255), QColor
            )
        )
        self.spxFluxPPOutlineWidthZones.setValue(
            float(qmapodSettings.value("params/spxFluxPPOutlineWidthZones", 1.0))
        )
        self.spxFluxPPTranspZones.setValue(
            int(qmapodSettings.value("params/spxFluxPPTranspZones", 50))
        )

        # ----------------------------------------------------------------
        # Rubrique flux par zones
        # ----------------------------------------------------------------
        # Type d'analyse de flux
        self.buttonGroupVisuFluxZone.setId(self.rbnGraduated, 1)
        self.buttonGroupVisuFluxZone.setId(self.rbnUrchin, 2)
        self.buttonGroupVisuFluxZone.setId(self.rbnDiagram, 3)
        self.buttonGroupVisuFluxZone.button(
            int(qmapodSettings.value("params/buttonGroupVisuFluxZone", 1))
        ).setChecked(True)
        # Étiquetage des entités
        self.cbxLabelFluxZone.clear()
        self.cbxLabelFluxZone.addItems(
            ["Aucun", "Nb Montées ou Descentes", "% Montées ou Descentes"]
        )
        self.cbxLabelFluxZone.setCurrentIndex(
            int(qmapodSettings.value("params/cbxLabelFluxZone", 1))
        )
        # Mode de prise en compte des enquêtes
        self.buttonGroupFluxZoneMode.setId(self.rbnFluxZoneVoyage, 1)
        self.buttonGroupFluxZoneMode.setId(self.rbnFluxZoneDeplacement, 2)
        self.buttonGroupFluxZoneMode.button(
            int(qmapodSettings.value("params/buttonGroupFluxZoneMode", 1))
        ).setChecked(True)

        # ******* Paramètres flux dégradé ****** #
        # Couleur mini du dégradé (montées)
        self.btnColMiniGraduatedM.setColor(
            qmapodSettings.value(
                "params/btnColMiniGraduatedM", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur maxi du dégradé (montées)
        self.btnColMaxiGraduatedM.setColor(
            qmapodSettings.value(
                "params/btnColMaxiGraduatedM", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur mini du dégradé (descentes)
        self.btnColMiniGraduatedD.setColor(
            qmapodSettings.value(
                "params/btnColMiniGraduatedD", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur maxi du dégradé (descentes)
        self.btnColMaxiGraduatedD.setColor(
            qmapodSettings.value(
                "params/btnColMaxiGraduatedD", QColor(255, 0, 0, 255), QColor
            )
        )
        # Transparence du dégradé
        self.spxTransparencyGraduated.setRange(0, 100)
        self.spxTransparencyGraduated.setValue(
            int(qmapodSettings.value("params/spxTransparencyGraduated", 30))
        )
        # Nombre de classes du dégradé
        self.spxNbClassesGraduated.setRange(0, 10)
        self.spxNbClassesGraduated.setValue(
            int(qmapodSettings.value("params/spxNbClassesGraduated", 5))
        )
        # Méthode de classification du dégradé
        self.cbxClassificationGraduated.addItem("Intervalles égaux", 1)
        self.cbxClassificationGraduated.addItem("Quantile(effectifs égaux", 2)
        self.cbxClassificationGraduated.addItem("Ruptures naturelles (Jenks)", 3)
        self.cbxClassificationGraduated.addItem("Écart-type", 4)
        self.cbxClassificationGraduated.addItem("Jolies ruptures", 5)
        self.cbxClassificationGraduated.setCurrentIndex(
            int(qmapodSettings.value("params/cbxClassificationGraduated", 3))
        )

        # ******* Paramètres flux oursins ****** #
        # Épaisseur des rayons des oursins
        self.spxUrchinWidth.setRange(0.0, 10.0)
        self.spxUrchinWidth.setSingleStep(0.1)
        self.spxUrchinWidth.setValue(
            float(qmapodSettings.value("params/spxUrchinWidth", 2.0))
        )
        # Couleur mini du dégradé (montées)
        self.btnColMiniUrchinM.setColor(
            qmapodSettings.value(
                "params/btnColMiniUrchinM", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur maxi du dégradé (montées)
        self.btnColMaxiUrchinM.setColor(
            qmapodSettings.value(
                "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur mini du dégradé (descentes)
        self.btnColMiniUrchinD.setColor(
            qmapodSettings.value(
                "params/btnColMiniUrchinD", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur maxi du dégradé (descentes)
        self.btnColMaxiUrchinD.setColor(
            qmapodSettings.value(
                "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
            )
        )
        # Transparence du dégradé
        self.spxTransparencyUrchin.setRange(0, 100)
        self.spxTransparencyUrchin.setValue(
            int(qmapodSettings.value("params/spxTransparencyUrchin", 30))
        )
        # Nombre de classes du dégradé
        self.spxNbClassesUrchin.setRange(0, 10)
        self.spxNbClassesUrchin.setValue(
            int(qmapodSettings.value("params/spxNbClassesUrchin", 5))
        )
        # Méthode de classification du dégradé
        self.cbxClassificationUrchin.addItem("Intervalles égaux", 1)
        self.cbxClassificationUrchin.addItem("Quantile(effectifs égaux", 2)
        self.cbxClassificationUrchin.addItem("Ruptures naturelles (Jenks)", 3)
        self.cbxClassificationUrchin.addItem("Écart- type", 4)
        self.cbxClassificationUrchin.addItem("Jolies ruptures", 5)
        self.cbxClassificationUrchin.setCurrentIndex(
            int(qmapodSettings.value("params/cbxClassificationUrchin", 3))
        )

        # ******* Paramètres flux diagrammes ****** #
        # Diamètre maxi des diagrammes de flux zones (QSpinbox)
        self.spxFluxZoneDiagMaxSize.setRange(0, 100)
        self.spxFluxZoneDiagMaxSize.setSingleStep(1)
        self.spxFluxZoneDiagMaxSize.setValue(
            int(qmapodSettings.value("params/spxFluxZoneDiagMaxSize", 40))
        )
        # Couleur de fond des diagrammes de montées
        self.btnColBgDiagramM.setColor(
            qmapodSettings.value(
                "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur de fond des diagrammes de descentes
        self.btnColBgDiagramD.setColor(
            qmapodSettings.value(
                "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
            )
        )
        # Transparence des diagrammes
        self.spxTransparencyDiagram.setRange(0, 100)
        self.spxTransparencyDiagram.setValue(
            int(qmapodSettings.value("params/spxTransparencyDiagram", 30))
        )
        # Couleur des contours des diagrammes
        self.btnColOutlineDiagram.setColor(
            qmapodSettings.value(
                "params/btnColOutlineDiagram", QColor(255, 0, 0, 255), QColor
            )
        )
        # Épaisseur des contours des diagrammes
        self.spxWidthOutlineDiagram.setRange(0.0, 5.0)
        self.spxWidthOutlineDiagram.setSingleStep(0.1)
        self.spxWidthOutlineDiagram.setValue(
            float(qmapodSettings.value("params/spxWidthOutlineDiagram", 0.4))
        )
        # Affichage uniquement des paramètres correspondant
        # au type de visualisation sélectionné
        self.gpbGraduated.setVisible(False)
        self.gpbUrchin.setVisible(False)
        self.gpbDiagram.setVisible(False)
        if int(qmapodSettings.value("params/buttonGroupVisuFluxZone", 1)) == 1:
            self.gpbGraduated.setVisible(True)
        elif int(qmapodSettings.value("params/buttonGroupVisuFluxZone", 1)) == 2:
            self.gpbUrchin.setVisible(True)
        elif int(qmapodSettings.value("params/buttonGroupVisuFluxZone", 1)) == 3:
            self.gpbDiagram.setVisible(True)
        else:
            self.gpbGraduated.setVisible(True)
        # Paramétrage des boutons radio voyage / déplacement
        # (une seule ligne sélectionnée par défaut)
        self.gbxFluxZoneVD.setDisabled(False)
        self.rbnFluxZoneVoyage.setDisabled(False)
        self.rbnFluxZoneDeplacement.setDisabled(False)

        # ----------------------------------------------------------------
        # Rubrique flux par arrêts
        # ----------------------------------------------------------------
        # Mode de prise en compte des enquêtes
        self.buttonGroupFluxArretMode.setId(self.rbnFluxArretVoyage, 1)
        self.buttonGroupFluxArretMode.setId(self.rbnFluxArretDeplacement, 2)
        self.buttonGroupFluxArretMode.button(
            int(qmapodSettings.value("params/buttonGroupFluxArretMode", 1))
        ).setChecked(True)
        # Diamètre maxi des diagrammes de flux arrêts (QSpinbox)
        self.spxFluxArretsDiagMaxSize.setRange(0, 100)
        self.spxFluxArretsDiagMaxSize.setSingleStep(1)
        self.spxFluxArretsDiagMaxSize.setValue(
            int(qmapodSettings.value("params/spxFluxArretsDiagMaxSize", 40))
        )
        # Couleur des montées
        self.btnColFluxArretM.setColor(
            qmapodSettings.value(
                "params/btnColFluxArretM", QColor(255, 0, 0, 255), QColor
            )
        )
        # Couleur des descentes
        self.btnColFluxArretD.setColor(
            qmapodSettings.value(
                "params/btnColFluxArretD", QColor(255, 0, 0, 255), QColor
            )
        )
        # Transparence des symboles
        self.spxTransparencySymbol.setRange(0, 100)
        self.spxTransparencySymbol.setValue(
            int(qmapodSettings.value("params/spxTransparencySymbol", 30))
        )
        # Couleur des contours des symboles
        self.btnColOutlineSymbol.setColor(
            qmapodSettings.value(
                "params/btnColOutlineSymbol", QColor(255, 0, 0, 255), QColor
            )
        )
        # Épaisseur des contours des symboles
        self.spxWidthOutlineSymbol.setRange(0.0, 5.0)
        self.spxWidthOutlineSymbol.setSingleStep(0.1)
        self.spxWidthOutlineSymbol.setValue(
            float(qmapodSettings.value("params/spxWidthOutlineSymbol", 0.4))
        )
        # Étiquetage des entités
        self.cbxLabelFluxArret.clear()
        self.cbxLabelFluxArret.addItems(
            ["Aucun", "Nb Montées ou Descentes", "% Montées ou Descentes"]
        )
        self.cbxLabelFluxArret.setCurrentIndex(
            int(qmapodSettings.value("params/cbxLabelFluxArret", 1))
        )
        # Paramétrage des boutons radio voyage / déplacement
        # (une seule ligne sélectionnée par défaut)
        self.gbxFluxArretVD.setDisabled(False)
        self.rbnFluxArretVoyage.setDisabled(False)
        self.rbnFluxArretDeplacement.setDisabled(False)

        self.btnExportLayer.setVisible(False)
        self.btnExportProject.setVisible(False)

    def _populateComboBox(self, comboBox, variantData):
        """
        Chargement des données dans une QComboBox
        :comboBox: la liste à remplir
        :variantData: le dictionnaire ordonné ou la requête sql
                      récupérant les données
        """

        # Si variantData est un OrderedDict, on remplit la comboBox directement
        if isinstance(variantData, OrderedDict):
            for key, item in variantData.items():
                comboBox.addItem(item["title"], key)

        # Si variantData est une chaîne, on remplit la liste à partir
        # de la requête qu'elle contient
        elif isinstance(variantData, str):
            # Chargement de la requête strSql
            con = None
            try:
                con = sqlite3.connect(self.db)
                cur = con.cursor()
                cur.execute(variantData)
                data = cur.fetchall()

            except sqlite3.Error as e:
                self.logger.error("Error %s:" % e.args[0])
                sys.exit(1)

            finally:
                if con:
                    con.close()

            # Remplissage de la QComboBox
            for item in data:
                comboBox.addItem((item[1]), item[0])

    """
    Gestion des QListWidget
    """

    def _addListWidget(self, strLbl, strLblText, strLwg, layout, container):
        """
        Ajout d'une liste de critères
        :strLbl : Objet QLabel
        :strLblText : Contenu de l'étiquette
        :strLwg : Nom de l'objet QListWidget à créer
        :layout : Objet QLayout dans lequel sont insérés le label et la liste
        :container : Objet dans lequel sont insérés le label et la liste
        """

        hbxTitre = QHBoxLayout(container)
        lblObj = QLabel(container)
        lblObj.setObjectName(strLbl)
        lblObj.setText(strLblText)
        hbxTitre.addWidget(lblObj)
        chkBoxAll = QCheckBox(container)
        chkBoxAll.setMinimumSize(QSize(20, 20))
        chkBoxAll.setMaximumSize(QSize(20, 20))
        chkBoxAll.setObjectName("chkAll" + strLbl)
        chkBoxAll.setCheckState(Qt.Checked)
        hbxTitre.addWidget(chkBoxAll)
        spcTitre = QSpacerItem(10, 10, QSizePolicy.Expanding)
        hbxTitre.addItem(spcTitre)
        layout.addLayout(hbxTitre)
        lwgObj = QListWidget(container)
        font = QFont()
        font.setFamily("MS Shell Dlg 2")
        font.setPointSize(10)
        font.setItalic(True)
        lwgObj.setFont(font)
        lwgObj.setSelectionMode(QAbstractItemView.ExtendedSelection)
        lwgObj.setSpacing(2)
        lwgObj.setObjectName(strLwg)
        layout.addWidget(lwgObj)

        # Connection de chkBoxAll à une fonction
        self.lstListWidget[strLwg] = lwgObj
        self.lstCheckboxAll[strLwg] = chkBoxAll

        chkBoxAll.stateChanged.connect(
            lambda: self._listWidgetChangeAll(
                strListWidgetName=strLwg, chkBox=chkBoxAll
            )
        )

        return lwgObj

    def _populateListWidget(self, listWidget, strSql):
        """
        Chargement des données dans un QListWidget
        :listWidget: la liste à remplir
        :strSql: la requête sql récupérant les données
        """

        # Chargement de la table dataTable
        con = None
        try:
            con = sqlite3.connect(self.db)
            cur = con.cursor()
            self.logger.debug(strSql)
            cur.execute(strSql)
            data = cur.fetchall()

        except sqlite3.Error as e:
            self.logger.error("Error %s:" % e.args[0])
            sys.exit(1)

        finally:
            if con:
                con.close()

        """
        Pour les listView et treeView, paramétrer dans QTDesigner :
        - SelectionMode : ExtendedSelection
        - SelectionBehavior : SelectItems
        - EditTriggers : aucun
        !!! Impossible d'avoir plusieurs colonnes dans une listView !!!
        Essayer tableView...
        """
        # Remplissage du QListWidget
        for item in data:
            lib_item = QListWidgetItem(str(item[0]))
            lib_item.setText(item[1])
            lib_item.setData(Qt.UserRole, item[0])
            lib_item.setFlags(
                Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled
            )
            lib_item.setCheckState(Qt.Checked)
            listWidget.addItem(lib_item)

        # Connection du QListWidget à une fonction
        listWidget.itemChanged.connect(self._listWidgetItemChanged)

    def _listWidgetChangeAll(self, strListWidgetName, chkBox):
        """
        Mise à jour des cases à cocher d'une liste critère
        lorsque chkBoxAll est cochée/décochée
        """

        # Cocher/décocher tous les items
        lstWidget = self.lstListWidget[strListWidgetName]
        for index in range(lstWidget.count()):
            lstItem = lstWidget.item(index)
            lstItem.setCheckState(chkBox.checkState())

    def _listWidgetItemChanged(self, item):
        """
        Mise à jour des cases à cocher d'une liste critère
        lorsqu'un item de la liste est coché/décoché
        """

        # Récupération des items sélectionnés
        lstSel = item.listWidget().selectedItems()
        for selItem in lstSel:
            selItem.setCheckState(item.checkState())
            # Aurait été intéressant pour restaurer la sélection initiale,
            # mais ne fonctionne pas... TODO
            # selItem.setSelected(True)

    """
    Gestion du QTreeWidget twgSousReseau
    """

    def _twgSousReseauPopulate(self):
        """
        Pour les listView et treeView, paramétrer dans QTDesigner :
        - SelectionMode : ExtendedSelection
        - SelectionBehavior : SelectItems
        - EditTriggers : aucun
        !!! Impossible d'avoir plusieurs colonnes dans une listView !!!
        Essayer tableView...
        """

        # Chargement des lignes et des sens (code_ligne et code_sens)
        con = None
        try:
            con = sqlite3.connect(self.db)
            # Pour pouvoir accéder aux colonnes par leur nom
            con.row_factory = sqlite3.Row
            cur = con.cursor()

            with open(
                os.path.join(self.qmapodConfig.pluginDir, "sql/queries/dock_1.sql")
            ) as f:
                s = f.read()
                cur.execute(s)
                data = cur.fetchall()

        except sqlite3.Error as e:
            self.logger.error("Error %s:" % e.args[0])
            sys.exit(1)

        finally:
            if con:
                con.close()

        # Remplissage du QTreeWidget twgSousReseau
        defaultCheckedLigne = int(self.qmapodConfig.dctParam["defaultligne"])
        iconDroite = QIcon(os.path.join(self.pluginDir, "icons/fleche_droite.svg"))
        iconGauche = QIcon(os.path.join(self.pluginDir, "icons/fleche_gauche.svg"))
        iconCirc = QIcon(os.path.join(self.pluginDir, "icons/fleche_circulaire.svg"))
        dctIconsSens = {1: iconDroite, 2: iconGauche, 3: iconCirc}
        itemSize = QSize(0, 20)
        id_network = QTreeWidgetItem(0)
        # id_network.setIcon(0, dctIcons[item['typ_ligne']])
        id_network.setSizeHint(0, itemSize)
        id_network.setText(0, self.nomReseau)
        id_network.setFlags(
            Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled
        )
        # Le rôle 32 est l'index de stockage de données utilisateur
        # Qt.UserRole renvoie 32
        id_network.setData(0, Qt.UserRole, 0)
        id_network.setCheckState(0, Qt.PartiallyChecked)
        sav_type = ""
        for item in data:
            if item["lib_typlign"] != sav_type:
                id_typlign = QTreeWidgetItem(item["typ_ligne"])
                id_typlign.setText(0, item["lib_typlign"])
                id_typlign.setIcon(
                    0,
                    QIcon(
                        os.path.join(self.pluginDir, "icons/" + item["icon_typlign"])
                    ),
                )
                id_typlign.setSizeHint(0, itemSize)
                id_typlign.setFlags(
                    Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled
                )
                id_typlign.setData(0, Qt.UserRole, 1)
                id_typlign.setData(0, Qt.UserRole + 1, item["typ_ligne"])
                id_typlign.setCheckState(0, Qt.Unchecked)
                id_network.addChild(id_typlign)
            sav_type = item["lib_typlign"]
            if item["id_sens"] == 1 or item["id_sens"] == 3:
                id_ligne = QTreeWidgetItem(item["id_ligne"])
                id_ligne.setText(0, "L" + item["num_ligne"] + " " + item["lib_ligne"])
                id_ligne.setIcon(
                    0,
                    QIcon(
                        os.path.join(self.pluginDir, "icons/" + item["icon_typlign"])
                    ),
                )
                id_ligne.setSizeHint(0, itemSize)
                id_ligne.setFlags(
                    Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled
                )
                id_ligne.setData(0, Qt.UserRole, 2)
                id_ligne.setData(0, Qt.UserRole + 1, item["id_ligne"])
                # Pour développement
                if item["id_ligne"] == defaultCheckedLigne:
                    id_ligne.setCheckState(0, Qt.Checked)
                else:
                    id_ligne.setCheckState(0, Qt.Unchecked)
                id_typlign.addChild(id_ligne)
                id_sens = QTreeWidgetItem(item["id_sens"])
                id_sens.setText(0, "S" + str(item["id_sens"]) + " " + item["lib_sens"])
                id_sens.setIcon(0, dctIconsSens[item["id_sens"]])
                id_sens.setSizeHint(0, itemSize)
                id_sens.setFlags(
                    Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled
                )
                id_sens.setData(0, Qt.UserRole, 3)
                id_sens.setData(
                    0,
                    Qt.UserRole + 1,
                    str(item["id_ligne"]) + "|" + str(item["id_sens"]),
                )
                # Pour développement
                if item["id_ligne"] == defaultCheckedLigne:
                    id_sens.setCheckState(0, Qt.Checked)
                    id_ligne.parent().setCheckState(0, Qt.PartiallyChecked)
                else:
                    id_sens.setCheckState(0, Qt.Unchecked)
                id_ligne.addChild(id_sens)
            elif item["id_sens"] == 2:
                id_sens = QTreeWidgetItem(item["id_sens"])
                id_sens.setText(0, "S" + str(item["id_sens"]) + " " + item["lib_sens"])
                id_sens.setIcon(0, dctIconsSens[item["id_sens"]])
                id_sens.setSizeHint(0, itemSize)
                id_sens.setFlags(
                    Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled
                )
                id_sens.setData(0, Qt.UserRole, 3)
                id_sens.setData(
                    0,
                    Qt.UserRole + 1,
                    str(item["id_ligne"]) + "|" + str(item["id_sens"]),
                )
                # Pour développement
                if item["id_ligne"] == defaultCheckedLigne:
                    id_sens.setCheckState(0, Qt.Checked)
                    id_ligne.parent().setCheckState(0, Qt.PartiallyChecked)
                else:
                    id_sens.setCheckState(0, Qt.Unchecked)
                id_ligne.addChild(id_sens)
        # TODO : Ajout des courses le cas échéant

        self.twgSousReseau.setAlternatingRowColors(True)
        self.twgSousReseau.addTopLevelItems([id_network])
        self.twgSousReseau.expandToDepth(2)

        self.twgSousReseau.itemChanged.connect(self._twgSousReseauItemChanged)

    def _twgSousReseauCheckParents(self, item, column, itemCheckState):
        """
        Fonction récursive : mise à jour des checkboxes des items parents
        """

        # Vérification de l'état des items de même niveau
        blnAllChecked = True
        blnAllUnchecked = True
        for i in range(item.childCount()):
            if (
                item.child(i).checkState(column) == Qt.Unchecked
                or item.child(i).checkState(column) == Qt.PartiallyChecked
            ):
                blnAllChecked = False
                break
        for i in range(item.childCount()):
            if (
                item.child(i).checkState(column) == Qt.Checked
                or item.child(i).checkState(column) == Qt.PartiallyChecked
            ):
                blnAllUnchecked = False
                break

        # Si tous les items de même niveau sont cochés, on coche le parent
        if blnAllChecked:
            item.setCheckState(column, Qt.Checked)
        elif blnAllUnchecked:
            item.setCheckState(column, Qt.Unchecked)
        else:
            item.setCheckState(column, Qt.PartiallyChecked)

        # Si l'item a un parent, on relance la fonction sur celui-ci (récursion)
        if isinstance(item.parent(), QTreeWidgetItem):
            self._twgSousReseauCheckParents(item.parent(), column, itemCheckState)

    def _twgSousReseauCheckChildren(self, item, column, itemCheckState):
        """
        Fonction récursive : mise à jour des checkboxes des items parents
        """

        # On coche/décoche tous les enfants de l'item coché/décoché
        for i in range(item.childCount()):
            item.child(i).setCheckState(column, itemCheckState)
            # Si l'enfant a lui-même des enfants, on relance la fonction (récursion)
            if item.child(i).childCount() > 0:
                self._twgSousReseauCheckChildren(item.child(i), column, itemCheckState)

    def _twgSousReseauItemChanged(self, item, column):
        """
        Mise à jour des cases à cocher de l'arbre des lignes/sens
        lorsqu'un item de l'arborescence est coché/décoché
        """

        # Blocage de la propagation des évènements
        self.twgSousReseau.blockSignals(True)

        # Mise à jour des parents/enfants de l'item modifié (coché ou décoché)
        if item.childCount() > 0:
            # Mise à jour des cases à cocher des enfants
            self._twgSousReseauCheckChildren(item, column, item.checkState(column))
        if isinstance(item.parent(), QTreeWidgetItem):
            # Mise à jour des cases à cocher des parents
            self._twgSousReseauCheckParents(
                item.parent(), column, item.checkState(column)
            )

        # Récupération des items sélectionnés (en surbrillance)
        lstSel = self.twgSousReseau.selectedItems()
        # Mise à jour des parents/enfants des items sélectionnés
        # uniquement s'il y en a plus d'un
        if len(lstSel) > 1:
            for selItem in lstSel:
                selItem.setCheckState(column, item.checkState(column))
                if selItem.childCount() > 0:
                    # Mise à jour des cases à cocher des enfants
                    self._twgSousReseauCheckChildren(
                        selItem, column, item.checkState(column)
                    )
                if isinstance(item.parent(), QTreeWidgetItem):
                    # Mise à jour des cases à cocher des parents
                    self._twgSousReseauCheckParents(
                        selItem.parent(), column, item.checkState(column)
                    )
                # Aurait été intéressant pour restaurer la sélection initiale,
                # mais ne fonctionne pas...
                # selItem.setSelected(True)

        # Restauration de la propagation des évènements
        self.twgSousReseau.blockSignals(False)
