"""
/***************************************************************************
 QMapOD
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                             -------------------
        begin                : 2014-10-15
        copyright            : (C) 2017 by SIGéal
        email                : sigeal@sigeal.fr
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load QMapOD class from file QMapOD.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .qmapod import QMapOD

    return QMapOD(iface)
