"""
/***************************************************************************
 QMapODFluxZones
                                 A QGIS plugin
 Cartographie d'enquêtes O/D
                              -------------------
        begin                : 2014-10-15
        git sha              : $Format:%H$
        copyright            : (C) 2019 by SIGéal
        email                : sigeal@sigeal.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import logging
import os.path
import sqlite3
from string import Template

import qgis.utils

# Import des librairies QGIS
from qgis.core import (
    QgsArrowSymbolLayer,
    QgsDataDefinedSizeLegend,
    QgsDataSourceUri,
    QgsDiagramLayerSettings,
    QgsDiagramSettings,
    QgsFeatureRequest,
    QgsFillSymbol,
    QgsGradientColorRamp,
    QgsGraduatedSymbolRenderer,
    QgsLayerTreeGroup,
    QgsLayerTreeLayer,
    QgsLinearlyInterpolatedDiagramRenderer,
    QgsLineSymbol,
    QgsMarkerLineSymbolLayer,
    QgsMarkerSymbol,
    QgsPalLayerSettings,
    QgsPieDiagram,
    QgsProject,
    QgsRenderContext,
    QgsSimpleFillSymbolLayer,
    QgsSimpleLineSymbolLayer,
    QgsSimpleMarkerSymbolLayer,
    QgsTextBufferSettings,
    QgsUnitTypes,
    QgsVectorLayer,
    QgsVectorLayerSimpleLabeling,
)
from qgis.gui import QgsMapToolIdentify, QgsMapToolIdentifyFeature

# Import des librairies PyQt
from qgis.PyQt.QtCore import QSizeF, Qt, pyqtSignal
from qgis.PyQt.QtGui import QColor, QCursor, QFont
from qgis.PyQt.QtWidgets import QApplication

module_logger = logging.getLogger("QMapOD.flux_zones")


class QMapODFluxZones(QgsMapToolIdentifyFeature):
    """
    Classe d'analyse des flux par zones
    """

    # Signal nouveau style
    geomIdentified = pyqtSignal(object, object)

    def __init__(
        self,
        iface,
        dock,
        tbn,
        typAnal,
        nbLignes,
        zoneIndex,
        qmapodConfig,
        qmapodSettings,
    ):
        """
        Constructeur
        """

        # Récupération de la configuration du plugin
        self.qmapodConfig = qmapodConfig

        # Récupération de la référence à l'interface QGis
        self.iface = iface
        self.dock = dock
        self.canvas = iface.mapCanvas()

        # Initiation de la couche de sélection du zonage sélectionné
        self.initFluxZoneLayer(zoneIndex)

        # Appel du constructeur du parent
        super(QgsMapToolIdentify, self).__init__(self.canvas)
        self.cursor = QCursor(Qt.WhatsThisCursor)

        # Récupération de la référence au panneau ancrable
        self.dock = dock
        # Récupération de la référence à l'outil qui crée l'analyse
        self.tbn = tbn
        # Récupération de la référence à la base de données
        self.db = self.qmapodConfig.db
        # Type d'analyse ("m" ou "d")
        self.typAnal = typAnal
        # Paramètres de visualisation
        self.qmapodSettings = qmapodSettings
        # Type de visualisation (1:"graduated", 2:"urchin" ou 3:"diagram")
        self.typVisu = int(
            self.qmapodSettings.value("params/buttonGroupVisuFluxZone", 3)
        )
        # Nombre de lignes sélectionnées
        self.nbLignes = nbLignes
        # Liste des zones sélectionnées
        self.lstZones = []

        # Couche des zones sélectionnées
        self.selLayer = None
        # Couche des flux
        self.fzLayer = None
        # Groupe de couche des flux par zones
        self.fzGroup = None
        # Caractéristiques diagrammes
        self.maxSize = 0
        self.maxValue = 0
        # Titre de la couche
        self.strNom = ""

        self.logger = logging.getLogger("QMapOD.flux_zones.QMapODFluxZones")
        self.logger.info("Creating an instance of QMapODFluxZones")

    def activate(self):
        """
        Activation de l'outil
        """
        self.canvas.setCursor(self.cursor)

    # -------------------------------------------------------------------------

    def deactivate(self):
        """
        Désactivation de l'outil
        """
        pass

    # -------------------------------------------------------------------------

    def canvasReleaseEvent(self, mouseEvent):
        """
        Émission de l'événement geomIdentified
        """
        # Récupération des entités identifiées
        results = self.identify(
            mouseEvent.x(),
            mouseEvent.y(),
            self.TopDownStopAtFirst,
            [self.zoneLayer],
            self.VectorLayer,
        )
        if len(results) > 0:
            # Signal ancien / nouveau style
            self.geomIdentified.emit(results[0].mLayer, results[0].mFeature)
        else:
            self.geomIdentified.emit(None, None)

    # -------------------------------------------------------------------------

    def _addLayerFluxZonesTmp(self, intPos):
        """
        Ajout de la couche zones_tmp
        """

        # Détermination du nom de la couche
        if self.typAnal == "m":
            self.strNom = "Flux par zone(s) de montée"
        elif self.typAnal == "d":
            self.strNom = "Flux par zone(s) de descente"

        # Ajout de la couche
        uriFluxZones = QgsDataSourceUri()
        uriFluxZones.setDatabase(self.db)
        uriFluxZones.setDataSource("", "flux_zones_tmp", "geometry")
        fluxZonesLayer = QgsVectorLayer(uriFluxZones.uri(), self.strNom, "spatialite")
        if fluxZonesLayer.isValid():
            # Ajout des couches et déplacement au-dessus
            # des couches existantes (api QgsLayerTree)
            self.fzLayer = QgsProject.instance().addMapLayer(fluxZonesLayer, False)
            self.fzLayer.destroyed.connect(self._deleted)
            fluxZonesTreeLayer = QgsLayerTreeLayer(fluxZonesLayer)
            fluxZonesTreeLayer.setName(fluxZonesLayer.name())
            self.fzGroup.insertChildNode(intPos, fluxZonesTreeLayer)

    # -------------------------------------------------------------------------

    def _deleted(self):
        """
        Réinitialisation de la couche si elle est supprimée
        depuis l'interface QGis - Désactivation de l'outil
        """

        self.delFluxZones()
        self.tbn.setChecked(False)
        # self.iface.mapCanvas().unsetMapTool(self)

    # -------------------------------------------------------------------------

    def _getPoids(self, nbLignes):
        """
        Choix du mode de pondération à utiliser
        """

        # Si une seule ligne est sélectionnée, on prend le poids sur la ligne
        if nbLignes == 1:
            return self.qmapodConfig.dctParam["poidsligne"]
        # Si tout le réseau est sélectionné, on prend le poids sur le réseau
        elif nbLignes == self.qmapodConfig.dctParam["nblignes"]:
            return self.qmapodConfig.dctParam["poidsreseau"]
        # Sinon, on prend le poids sur la ligne
        else:
            return self.qmapodConfig.dctParam["poidsligne"]

    # -------------------------------------------------------------------------

    def _getMaxVal(self, tab, col):
        """
        Récupération de la valeur maximale d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = Template(
                "\nSELECT max($col_) \n\
                 FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            max = cursor.fetchone()[0]
            if max is None:
                return 0
            else:
                return max

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _getSumVal(self, tab, col):
        """
        Récupération de la somme des valeurs d'une colonne d'une table
        """

        # Vérification de l'existence d'un jeu de données filtrées
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Vérification de l'existence de la table enquetes_tmp
            s = Template(
                "\nSELECT sum($col_) \n\
                     FROM $tab_;"
            ).substitute(col_=col, tab_=tab)
            self.logger.debug(s)
            cursor.execute(s)
            sum = cursor.fetchone()[0]
            if sum is None:
                return 0
            else:
                return sum

        # Catch the exception
        except Exception as e:
            self.logger.error("Error %s:" % e.args[0])
            raise e

        # Executed in any case
        finally:
            # Close the db connection
            if db:
                db.close()

    # -------------------------------------------------------------------------

    def _genSqlFluxZones(self, nbLignes, lstZones, typAnal):
        """
        Génération de la requête de création de la table flux_zones_tmp
        """

        # Récupération des paramètres pour le type de zone sélectionné
        typeZone = self.qmapodConfig.odtZonageAnalysable[self.typeZone]
        tabZone = typeZone["tabZone"]
        colNomZone = typeZone["colNomZone"]
        colIdZone = typeZone["colIdZone"]

        # Mode de prise en compte des enquêtes
        modeEnq = int(self.qmapodSettings.value("params/buttonGroupFluxZoneMode", 1))
        if modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]

        poids = self._getPoids(nbLignes)

        # Requête d'extraction des flux selon le type (montées / descentes)
        # Modifié le 27/07/2015 pour faire apparaître
        # les valeurs inférieures à 0.5
        if typAnal == "m":
            strChampFlux = "cast(descentes AS float) descentes"
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_1.sql"
                )
            ) as f:
                strSelectEnq = Template(f.read()).substitute(
                    col_id_zone=colIdZone,
                    col_nom_zone=colNomZone,
                    poids_=poids,
                    arret_deb=arretDeb,
                    arret_fin=arretFin,
                    str=", ".join(map(str, lstZones)),
                )
        elif typAnal == "d":
            strChampFlux = "cast(montees AS float) montees"
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_2.sql"
                )
            ) as f:
                strSelectEnq = Template(f.read()).substitute(
                    col_id_zone=colIdZone,
                    col_nom_zone=colNomZone,
                    poids_=poids,
                    arret_fin=arretFin,
                    arret_deb=arretDeb,
                    str=", ".join(map(str, lstZones)),
                )

        # Requête de jointure des flux à la couche du zonage sélectionné
        with open(
            os.path.join(self.qmapodConfig.pluginDir, "sql/queries/flux_zones_3.sql")
        ) as f:
            strSelectZFlux = Template(f.read()).substitute(
                col_id_zone=colIdZone,
                col_nom_zone=colNomZone,
                str_champ_flux=strChampFlux,
                tab_zone=tabZone,
                str_select_enq=strSelectEnq,
            )

        # Requête de création de la table flux_zones_tmp
        strCreateZmdVar = Template(
            "CREATE TABLE flux_zones_tmp AS $str_selectz_flux"
        ).substitute(str_selectz_flux=strSelectZFlux)
        strSql = strCreateZmdVar

        return strSql

    # -------------------------------------------------------------------------

    def _createTableFluxZonesTmp(self, nbLignes):
        """
        Création de la table flux_zones_tmp
        """

        nbInserted = 0
        try:
            # Connexion à la base de données
            db = sqlite3.connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table flux_zones_tmp si elle existe
            s = "DROP TABLE IF EXISTS flux_zones_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            # Création de la table flux_zones_tmp
            strSql = self._genSqlFluxZones(nbLignes, self.lstZones, self.typAnal)
            self.logger.debug(strSql)
            cursor.execute(strSql)
            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
                 WHERE f_table_name = 'flux_zones_tmp'"
            self.logger.debug(s)
            cursor.execute(s)
            # Insertion de la déclaration de la colonne geometry
            # (type 3 = polygone)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_4.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(strInsert)
                cursor.execute(strInsert)
            # Récupération du nombre de lignes insérées
            s = "\nSELECT count(*) \n\
                FROM flux_zones_tmp;"
            self.logger.debug(s)
            cursor.execute(s)
            nbInserted = cursor.fetchone()[0]
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

        return nbInserted

    # -------------------------------------------------------------------------

    def _genSqlFluxZonesUrchin(self, nbLignes, lstZones, typAnal):
        """
        Génération de la requête de création de la table flux_zones_tmp
        """

        # Récupération des paramètres pour le type de zone sélectionné
        typeZone = self.qmapodConfig.odtZonageAnalysable[self.typeZone]
        tabZone = typeZone["tabZone"]
        colNomZone = typeZone["colNomZone"]
        colIdZone = typeZone["colIdZone"]

        # Mode de prise en compte des enquêtes
        modeEnq = int(self.qmapodSettings.value("params/buttonGroupFluxZoneMode", 1))
        if modeEnq == 1:  # Logique voyage
            arretDeb = self.qmapodConfig.dctParam["arretdebvoy"]
            arretFin = self.qmapodConfig.dctParam["arretfinvoy"]
        elif modeEnq == 2:  # Logique déplacement
            arretDeb = self.qmapodConfig.dctParam["arretdebdep"]
            arretFin = self.qmapodConfig.dctParam["arretfindep"]

        poids = self._getPoids(nbLignes)

        # Requête d'extraction des flux selon le type (montées / descentes)
        # Modifié le 27/07/2015 pour faire apparaître
        # les valeurs inférieures à 0.5
        if typAnal == "m":
            strChampFlux = "cast(descentes AS float) descentes"
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_5.sql"
                )
            ) as f:
                strSelectEnq = Template(f.read()).substitute(
                    col_id_zone=colIdZone,
                    col_nom_zone=colNomZone,
                    col_id_zone_m=colIdZone + "_m",
                    col_nom_zone_m=colNomZone + "_m",
                    col_id_zone_d=colIdZone + "_d",
                    col_nom_zone_d=colNomZone + "_d",
                    poids_=poids,
                    arret_deb=arretDeb,
                    arret_fin=arretFin,
                    lst_zones=", ".join(map(str, lstZones)),
                )

            # Requête de jointure des flux à la couche du zonage sélectionné
            # Génération de branche à trois points pour rendu courbe
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_6.sql"
                )
            ) as f:
                strSelectZFlux = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"],
                    col_id_zone=colIdZone,
                    col_nom_zone=colNomZone,
                    col_id_zone_m=colIdZone + "_m",
                    col_nom_zone_m=colNomZone + "_m",
                    col_id_zone_d=colIdZone + "_d",
                    col_nom_zone_d=colNomZone + "_d",
                    str_champ_flux=strChampFlux,
                    str_select_enq=strSelectEnq,
                    tab_zone=tabZone,
                )
        elif typAnal == "d":
            strChampFlux = "cast(montees AS float) montees"
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_7.sql"
                )
            ) as f:
                strSelectEnq = Template(f.read()).substitute(
                    col_id_zone=colIdZone,
                    col_nom_zone=colNomZone,
                    poids_=poids,
                    arret_fin=arretFin,
                    arret_deb=arretDeb,
                    lst_zones=", ".join(map(str, lstZones)),
                )
            # Requête de jointure des flux à la couche du zonage sélectionné
            # Génération de branche à trois points pour rendu courbe
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_8.sql"
                )
            ) as f:
                strSelectZFlux = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"],
                    col_id_zone=colIdZone,
                    col_nom_zone=colNomZone,
                    col_id_zone_m=colIdZone + "_m",
                    col_nom_zone_m=colNomZone + "_m",
                    col_id_zone_d=colIdZone + "_d",
                    col_nom_zone_d=colNomZone + "_d",
                    str_champ_flux=strChampFlux,
                    str_select_enq=strSelectEnq,
                    tab_zone=tabZone,
                )

        # Requête de création de la table flux_zones_tmp
        strCreateZmdVar = Template(
            "CREATE TABLE flux_zones_tmp AS $str_select_zflux;"
        ).substitute(str_select_zflux=strSelectZFlux)
        strSql = strCreateZmdVar

        return strSql

    # -------------------------------------------------------------------------

    def _createTableFluxZonesUrchinTmp(self, nbLignes):
        """
        Création de la table flux_zones_tmp
        """

        # Génération de la requête de création de table

        nbInserted = 0
        try:
            # Connexion à la base de données
            db = qgis.utils.spatialite_connect(self.db)
            # Get a cursor object
            cursor = db.cursor()
            # Suppression de la table flux_zones_tmp si elle existe
            s = "DROP TABLE IF EXISTS flux_zones_tmp"
            self.logger.debug(s)
            cursor.execute(s)
            # Création de la table flux_zones_tmp
            strSql = self._genSqlFluxZonesUrchin(nbLignes, self.lstZones, self.typAnal)
            self.logger.debug(strSql)
            cursor.execute(strSql)
            # Suppression de la déclaration de la colonne geometry
            s = "\nDELETE FROM geometry_columns \n\
                 WHERE f_table_name = 'flux_zones_tmp'"
            self.logger.debug(s)
            cursor.execute(s)
            # Insertion de la déclaration de la colonne geometry (type 2 = polyligne)
            with open(
                os.path.join(
                    self.qmapodConfig.pluginDir, "sql/queries/flux_zones_9.sql"
                )
            ) as f:
                strInsert = Template(f.read()).substitute(
                    epsg=self.qmapodConfig.dctParam["epsg"]
                )
                self.logger.debug(s)
                cursor.execute(strInsert)
            # Récupération du nombre de lignes insérées
            s = "\nSELECT count(*) \n\
                FROM flux_zones_tmp;"
            self.logger.debug(s)
            cursor.execute(s)
            nbInserted = cursor.fetchone()[0]
            # Commit the change
            db.commit()

        # Catch the exception
        except Exception as e:
            # Roll back any change if something goes wrong
            db.rollback()
            # Restauration du curseur
            QApplication.restoreOverrideCursor()
            raise e

        finally:
            # Close the db connection
            if db:
                db.close()

        return nbInserted

    def _labelFluxZone(self, blnUrchin):
        """
        Etiquetage de la couche des flux
        """

        # Paramètres d'étiquetage
        # Pour éliminer les étiquettes si les valeurs sont nulles
        intMode = int(self.qmapodSettings.value("params/cbxLabelFluxZone", 1))
        # blnEnabled = True
        if intMode == 0:
            # si aucun aucune étiquette affichée setLabeling(None)
            self.fzLayer.setLabelsEnabled(True)
            self.fzLayer.setLabeling(None)
            return
        elif intMode == 1:
            if self.typAnal == "m":
                strField = """
                    if("descentes" >= 0.5, round("descentes", 0),
                        round("descentes", 1))
                """
                blnExp = True
            elif self.typAnal == "d":
                strField = """
                    if("montees" >= 0.5, round("montees", 0),
                        round("montees", 1))
                """
                blnExp = True
            else:
                strField = ""
                blnExp = False
        elif intMode == 2:
            if self.typAnal == "m":
                strField = """
                    round(100 * "descentes" / sum("descentes"), 1) || ' %'
                    """
                blnExp = True
            elif self.typAnal == "d":
                strField = """
                    round(100 * "montees" / sum("montees"), 1) || ' %'
                """
                blnExp = True
            else:
                strField = ""
                blnExp = False

        else:
            return

        palLayer = QgsPalLayerSettings()
        palLayer.fieldName = strField
        palLayer.isExpression = blnExp
        if blnUrchin:
            palLayer.placement = QgsPalLayerSettings.Line
            palLayer.placementFlags = QgsPalLayerSettings.AboveLine
            palLayer.multilineAlign = QgsPalLayerSettings.MultiCenter
            palLayer.dist = (
                float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0)) / 2.0
            )
        else:
            palLayer.placement = QgsPalLayerSettings.OverPoint
        palLayer.bufferDraw = True

        # Formatage du tampon
        bufferFormat = QgsTextBufferSettings()
        bufferFormat.setEnabled(True)

        # Formatage du texte
        txtFormat = palLayer.format()
        txtFormat.setBuffer(bufferFormat)
        txtFormat.setColor(QColor(0, 0, 0, 255))
        txtFormat.setSizeUnit(QgsUnitTypes.RenderUnit.RenderPoints)  # Points
        txtFormat.setFont(QFont("Arial Narrow", 8, QFont.Normal))
        txtFormat.setSize(8)
        palLayer.setFormat(txtFormat)

        # Etiquetage
        labeling = QgsVectorLayerSimpleLabeling(palLayer)
        self.fzLayer.setLabelsEnabled(True)
        self.fzLayer.setLabeling(labeling)

    # -------------------- LEGENDE -------------------------------------------

    def _legend(self):
        lgd = QgsDataDefinedSizeLegend()
        lgd.setLegendType(QgsDataDefinedSizeLegend.LegendCollapsed)
        lgd.setTitle("Légende")
        lgds = QgsMarkerSymbol()
        lgds.symbolLayer(0).setColor(QColor("#d5d5d5"))
        lgds.symbolLayer(0).setStrokeColor(QColor("#ffffff"))
        lgds.symbolLayer(0).setStrokeWidth(0.4)
        lgd.setClasses(
            [
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue), str(int(self.maxValue))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 2), str(int(self.maxValue / 2))
                ),
                QgsDataDefinedSizeLegend.SizeClass(
                    int(self.maxValue / 10), str(int(self.maxValue / 10))
                ),
            ]
        )
        lgd.setSymbol(lgds)
        return lgd

    # ----------------------- FIN LEGENDE --------------------------------

    def initFluxZoneLayer(self, zoneIndex):
        """
        Initialisation de la couche zonage pour les flux par zones
        """

        # Récupération du type de zonage pour les flux
        self.typeZone = self.dock.cbxFluxZone.itemData(zoneIndex)
        typeZone = self.qmapodConfig.odtZonageAnalysable[self.typeZone]
        tabZone = typeZone["tabZone"]

        # S'il y a une couche active
        boolLayer = False
        if self.iface.activeLayer():
            # Si la source de la couche courante contient
            # le nom du zonage sélectionné
            if 'table="' + tabZone + '"' in self.iface.activeLayer().source():
                zoneLayer = self.iface.activeLayer()
                # self.logger.debug('type zone : ' + tabZone + ' source : ' + zoneLayer.source())
                self.zoneLayer = zoneLayer
                boolLayer = True
        # Sinon, on recherche la couche dans les couches chargées
        if not boolLayer:
            root = QgsProject.instance().layerTreeRoot()
            for child in root.findLayers():
                if isinstance(child, QgsLayerTreeLayer):
                    # self.logger.debug("- layer: " + child.layerName() + "  ID: " + child.layerId())
                    if 'table="' + tabZone + '"' in child.layer().source():
                        # self.logger.debug("type zone : " + tabZone + " source : " + child.layer().source())
                        zoneLayer = child.layer()
                        boolLayer = True
                        break
            # Si la couche du zonage sélectionné est chargée, on l'active
            if boolLayer:
                self.iface.setActiveLayer(zoneLayer)
                self.zoneLayer = zoneLayer
            # Sinon, on charge la couche du zonage sélectionné et on l'active
            else:
                uriZone = QgsDataSourceUri()
                uriZone.setDatabase(self.qmapodConfig.db)
                uriZone.setDataSource("", tabZone, "geometry")
                zoneLayer = QgsVectorLayer(
                    uriZone.uri(), "Flux zonage " + tabZone, "spatialite"
                )
                if zoneLayer.isValid():
                    QgsProject.instance().addMapLayer(zoneLayer)
                self.zoneLayer = zoneLayer

        # Au cas où la couche ne serait pas visible
        QgsProject.instance().layerTreeRoot().findLayer(
            zoneLayer.id()
        ).setItemVisibilityChecked(True)

    # -------------------------------------------------------------------------

    def processZone(self, feature):
        """
        Traitement de la zone sélectionnée
        """

        # Récupération des attributs de la zone sélectionnée
        typeZone = self.qmapodConfig.odtZonageAnalysable[self.typeZone]
        tabZone = typeZone["tabZone"]
        title = typeZone["title"]
        colIdZone = typeZone["colIdZone"]

        # Ajout/Suppression de la zone sélectionnée
        # à la liste de zones de l'outil
        modifiers = QApplication.keyboardModifiers()
        if modifiers == Qt.ShiftModifier or modifiers == Qt.ControlModifier:
            if feature.attribute(colIdZone) in self.lstZones:
                self.lstZones.remove(feature.attribute(colIdZone))
            else:
                self.lstZones.append(feature.attribute(colIdZone))
        else:
            self.lstZones[:] = []
            self.lstZones.append(feature.attribute(colIdZone))

        # Récupération de la transparence
        paramTransp = float(
            self.qmapodSettings.value("params/spxTransparencyDiagram", 30)
        )
        fltAlpha = 1.0 - (paramTransp / 100)

        # Détermination du nom de la couche et de la couleur des zones sélectionnés
        if self.typAnal == "m":
            strNom = Template("$title_ de montées").substitute(title_=title)
            if self.typVisu == 1:  # Couleurs graduées
                selColFill = QColor(255, 255, 255, 0)
                selBorderCol = self.qmapodSettings.value(
                    "params/btnColMaxiGraduatedM", QColor(255, 0, 0, 255), QColor
                )
                selBorderWidth = 0.6
            elif self.typVisu == 2:  # Oursins
                selColFill = QColor(255, 255, 255, 0)
                selBorderCol = self.qmapodSettings.value(
                    "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
                )
                selBorderWidth = 0.6
            elif self.typVisu == 3:  # Diagrammes proportionnels
                selColFill = self.qmapodSettings.value(
                    "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
                )
                selBorderCol = self.qmapodSettings.value(
                    "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
                )
                selColFill.setAlphaF(fltAlpha)
                selBorderWidth = 0.6

        elif self.typAnal == "d":
            strNom = Template("$title_ de descentes").substitute(title_=title)
            if self.typVisu == 1:  # Couleurs graduées
                selColFill = QColor(255, 255, 255, 0)
                selBorderCol = self.qmapodSettings.value(
                    "params/btnColMaxiGraduatedD", QColor(255, 0, 0, 255), QColor
                )
                selBorderWidth = 0.6
            elif self.typVisu == 2:  # Oursins
                selColFill = QColor(255, 255, 255, 0)
                selBorderCol = self.qmapodSettings.value(
                    "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
                )
                selBorderWidth = 0.6
            elif self.typVisu == 3:  # Diagrammes proportionnels
                selColFill = self.qmapodSettings.value(
                    "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
                )
                selBorderCol = self.qmapodSettings.value(
                    "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
                )
                selColFill.setAlphaF(fltAlpha)
                selBorderWidth = 0.6

        # Ajout de la couche des zones sélectionnés si elle n'existe pas
        if self.selLayer is None:
            uri = QgsDataSourceUri()
            uri.setDatabase(self.qmapodConfig.db)
            uri.setDataSource("", tabZone, "geometry")
            self.selLayer = QgsVectorLayer(uri.uri(), strNom, "spatialite")
            self.selLayer.setSubsetString(
                Template("$col_id_zone IN($lst_zones)").substitute(
                    col_id_zone=colIdZone, lst_zones=", ".join(map(str, self.lstZones))
                )
            )
            if self.selLayer.isValid():
                # Ajout des couches et déplacement au-dessus
                # des couches existantes (api QgsLayerTree)
                root = QgsProject.instance().layerTreeRoot()
                self.fzGroup = root.insertGroup(0, "Flux par zones")
                self.selLayerId = QgsProject.instance().addMapLayer(
                    self.selLayer, False
                )
                self.selLayer.destroyed.connect(self._deleted)
                selTreeLayer = QgsLayerTreeLayer(self.selLayer)
                selTreeLayer.setName(self.selLayer.name())
                self.fzGroup.insertChildNode(0, selTreeLayer)

                symbolLayer = QgsSimpleFillSymbolLayer()
                symbolLayer.setStrokeWidth(selBorderWidth)
                symbolLayer.setStrokeColor(selBorderCol)
                symbolLayer.setFillColor(selColFill)

                self.selLayer.renderer().symbols(QgsRenderContext())[
                    0
                ].changeSymbolLayer(0, symbolLayer)
                self.selLayer.triggerRepaint()
                self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())

        # Mise à jour du sous-ensemble de la couche dans le cas contraire
        else:
            self.selLayer.setSubsetString(
                Template("$col_id_zone IN($str)").substitute(
                    col_id_zone=colIdZone, str=", ".join(map(str, self.lstZones))
                )
            )

        # Ajout de la visualisation des flux si elle n'existe pas
        if self.fzLayer is None:
            self.addFluxZones(self.nbLignes, self.typVisu)
        # Mise à jour de la visualisation existante dans le cas contraire
        else:
            self.majFluxZones(self.nbLignes, self.typVisu)

        # Rafraichissement de la carte
        self.iface.mapCanvas().refresh()

    # -------------------------------------------------------------------------

    def delFluxZones(self):
        """
        Suppression de l'affichage des flux par zones de montées / descentes
        """

        # Groupe de couche des flux par zones
        if isinstance(self.fzGroup, QgsLayerTreeGroup):
            root = QgsProject.instance().layerTreeRoot()
            root.removeChildNode(self.fzGroup)
        self.fzGroup = None
        # Activer la couche de sélection ?

        """
        # Couche des zones sélectionnés
        if isinstance(self.selLayer, QgsVectorLayer):
            QgsMapLayerRegistry.instance().removeMapLayer(self.selLayer.id())
        """
        self.selLayer = None

        """
        # Couches des flux par zones
        if isinstance(self.fzLayer, QgsVectorLayer):
            QgsMapLayerRegistry.instance().removeMapLayer(self.fzLayer.id())
        """
        self.fzLayer = None

    # -------------------------------------------------------------------------

    def addFluxZones(self, nbLignes, typVisu):
        """
        Affichage des flux par Zones de montées/descentes
        Selon type de visualisation
        """

        self.logger.debug("visu" + str(typVisu))
        if typVisu == 1:  # Couleurs graduées
            self.addFluxZonesGraduated(nbLignes)
        elif typVisu == 2:  # Oursins
            self.addFluxZonesUrchin(nbLignes)
        elif typVisu == 3:  # Diagrammes proportionnels
            self.addFluxZonesDiagram(nbLignes)

    # -------------------------------------------------------------------------

    def addFluxZonesGraduated(self, nbLignes):
        """
        Affichage des flux par Zones de montées/descentes
        Visualisation de type dégradé de couleur
        """

        self.logger.debug("zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesTmp(nbLignes)

        self.logger.debug("insertions : " + str(nb))
        if nb <= 0:
            self.delFluxZones()
            return

        self._addLayerFluxZonesTmp(1)

        # Définition du dégradé de couleurs
        if self.typAnal == "m":
            strExp = "descentes"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniGraduatedD", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiGraduatedD", QColor(255, 0, 0, 255), QColor
            )
        elif self.typAnal == "d":
            strExp = "montees"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniGraduatedM", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiGraduatedM", QColor(255, 0, 0, 255), QColor
            )

        # Paramètres des classes de valeurs
        colorRamp = QgsGradientColorRamp(colMin, colMax)

        intNbClasses = int(self.qmapodSettings.value("params/spxNbClassesGraduated", 5))
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencyGraduated", 30)
        )
        fltOpacity = (100 - intTransparency) / 100

        symbol = QgsFillSymbol()
        symbol.setColor(colMax)

        # Méthodes de classification disponibles :
        # EqualInterval : Intervalles égaux
        # Quantile : Méthode des quantiles (effectifs égaux)
        # Jenks : Intervalles naturels de Jenks
        # StdDev : Déviation standard (écart-type)
        # Pretty : Intervalles jolis (arrondis)

        dctMode = {
            0: QgsGraduatedSymbolRenderer.EqualInterval,
            1: QgsGraduatedSymbolRenderer.Quantile,
            2: QgsGraduatedSymbolRenderer.Jenks,
            3: QgsGraduatedSymbolRenderer.StdDev,
            4: QgsGraduatedSymbolRenderer.Pretty,
        }

        objMode = dctMode[
            int(self.qmapodSettings.value("params/cbxClassificationGraduated", 2))
        ]

        gr = QgsGraduatedSymbolRenderer.createRenderer(
            self.fzLayer, strExp, intNbClasses, objMode, symbol, colorRamp
        )
        # Application du GraduatedSymbolRenderer à la couche
        self.fzLayer.setRenderer(gr)
        self.fzLayer.setOpacity(fltOpacity)  # Pour essai

        # Etiquetage de la couche
        self.sumValue = self._getSumVal("flux_zones_tmp", strExp)  # QGIS < 2.18
        self._labelFluxZone(False)

        # Refresh map
        self.fzLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())

    # -------------------------------------------------------------------------

    def addFluxZonesUrchinLines(self, nbLignes):
        """
        Affichage des flux par Zones de montées/descentes
        Visualisation de type oursins
        """

        nb = self._createTableFluxZonesUrchinTmp(nbLignes)
        self.logger.debug("zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesUrchinTmp(nbLignes)
        self.logger.debug("insertions : " + str(nb))
        if nb <= 0:
            self.delFluxZones()
            return

        self._addLayerFluxZonesTmp(1)

        # Récupération des paramètres en fonction du type d'analyse
        intNbClasses = int(self.qmapodSettings.value("params/spxNbClassesUrchin", 5))
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencyUrchin", 30)
        )
        fltOpacity = (100 - intTransparency) / 100
        # Définition du dégradé de couleurs
        if self.typAnal == "m":
            strExp = "descentes"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinD", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
            )
            posArrow = QgsMarkerLineSymbolLayer.LastVertex
            angArrow = 90
        elif self.typAnal == "d":
            strExp = "montees"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinM", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
            )
            posArrow = QgsMarkerLineSymbolLayer.FirstVertex
            angArrow = 270

        # Construction d'un symbole de ligne avec une flèche à l'extrémité.

        symbol = QgsLineSymbol.createSimple("")
        symbol.deleteSymbolLayer(0)  # Remove default symbol layer.
        symbolLine = QgsSimpleLineSymbolLayer()
        symbolLine.setWidth(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0))
        )
        symbolLine.setColor(colMax)
        symbolLine.setPenCapStyle(Qt.RoundCap)
        symbol.appendSymbolLayer(symbolLine)
        symbolArrow = QgsMarkerSymbol.createSimple("")
        symbolArrow.deleteSymbolLayer(0)  # Remove default symbol layer.

        symbolArrowLayer = QgsSimpleMarkerSymbolLayer()
        symbolArrowLayer.setShape(QgsSimpleMarkerSymbolLayer.Triangle)
        symbolArrowLayer.setSize(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0)) * 2
        )
        symbolArrowLayer.setColor(colMax)
        symbolArrowLayer.setStrokeStyle(Qt.NoPen)
        symbolArrowLayer.setAngle(angArrow)
        symbolArrow.appendSymbolLayer(symbolArrowLayer)

        symbolLineArrow = QgsMarkerLineSymbolLayer()
        symbolLineArrow.setPlacement(posArrow)
        symbolLineArrow.setSubSymbol(symbolArrow)
        symbol.appendSymbolLayer(symbolLineArrow)

        colorRamp = QgsGradientColorRamp(colMin, colMax)

        # Méthodes de classification disponibles :
        # EqualInterval : Intervalles égaux
        # Quantile : Méthode des quantiles (effectifs égaux)
        # Jenks : Intervalles naturels de Jenks
        # StdDev : Déviation standard (écart-type)
        # Pretty : Intervalles jolis (arrondis)

        dctMode = {
            0: QgsGraduatedSymbolRenderer.EqualInterval,
            1: QgsGraduatedSymbolRenderer.Quantile,
            2: QgsGraduatedSymbolRenderer.Jenks,
            3: QgsGraduatedSymbolRenderer.StdDev,
            4: QgsGraduatedSymbolRenderer.Pretty,
        }
        objMode = dctMode[
            int(self.qmapodSettings.value("params/cbxClassificationUrchin", 2))
        ]
        gr = QgsGraduatedSymbolRenderer.createRenderer(
            self.fzLayer, strExp, intNbClasses, objMode, symbol, colorRamp
        )

        # Application du GraduatedSymbolRenderer à la couche
        self.fzLayer.setRenderer(gr)
        self.fzLayer.setOpacity(fltOpacity)

        # Etiquetage de la couche
        self.sumValue = self._getSumVal("flux_zones_tmp", strExp)

        self._labelFluxZone(True)

        # Refresh map
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())

    # -------------------------------------------------------------------------

    def addFluxZonesUrchin(self, nbLignes):
        """
        Affichage des flux par Zones de montées/descentes
        Visualisation de type oursins
        """

        nb = self._createTableFluxZonesUrchinTmp(nbLignes)
        self.logger.debug("zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesUrchinTmp(nbLignes)
        self.logger.debug("insertions : " + str(nb))
        if nb <= 0:
            self.delFluxZones()
            return

        self._addLayerFluxZonesTmp(1)

        # Récupération des paramètres en fonction du type d'analyse
        intNbClasses = int(self.qmapodSettings.value("params/spxNbClassesUrchin", 5))
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencyUrchin", 30)
        )
        fltOpacity = (100 - intTransparency) / 100
        # Définition du dégradé de couleurs
        if self.typAnal == "m":
            strExp = "descentes"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinD", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
            )
            headtype = QgsArrowSymbolLayer.HeadSingle

        elif self.typAnal == "d":
            strExp = "montees"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinM", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
            )
            headtype = QgsArrowSymbolLayer.HeadReversed

        # Construction d'un symbole de ligne avec une flèche à l'extrémité.
        symbol = QgsLineSymbol.createSimple("")
        symbolArrow = QgsArrowSymbolLayer.create()
        symbolArrow.setHeadType(headtype)
        symbolArrow.setArrowType(QgsArrowSymbolLayer.ArrowPlain)
        symbolArrow.setIsCurved(True)
        symbolArrow.setArrowStartWidth(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0))
        )
        symbolArrow.setArrowWidth(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0))
        )
        symbolArrow.setHeadLength(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0)) * 1.25
        )
        symbolArrow.setHeadThickness(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0)) * 1.25
        )
        symbolArrow.subSymbol().setOpacity(fltOpacity)

        symbol.changeSymbolLayer(0, symbolArrow)
        # application du gradient de couleur
        colorRamp = QgsGradientColorRamp(colMin, colMax)

        # Méthodes de classification disponibles :
        # EqualInterval : Intervalles égaux
        # Quantile : Méthode des quantiles (effectifs égaux)
        # Jenks : Intervalles naturels de Jenks
        # StdDev : Déviation standard (écart-type)
        # Pretty : Intervalles jolis (arrondis)

        dctMode = {
            0: QgsGraduatedSymbolRenderer.EqualInterval,
            1: QgsGraduatedSymbolRenderer.Quantile,
            2: QgsGraduatedSymbolRenderer.Jenks,
            3: QgsGraduatedSymbolRenderer.StdDev,
            4: QgsGraduatedSymbolRenderer.Pretty,
        }
        objMode = dctMode[
            int(self.qmapodSettings.value("params/cbxClassificationUrchin", 2))
        ]

        gr = QgsGraduatedSymbolRenderer.createRenderer(
            self.fzLayer, strExp, intNbClasses, objMode, symbol, colorRamp
        )

        # Résultats les plus élevés au-dessus
        clause = QgsFeatureRequest.OrderByClause(strExp)
        orderBy = QgsFeatureRequest.OrderBy([clause])
        gr.setOrderBy(orderBy)
        gr.setOrderByEnabled(True)

        # Application du GraduatedSymbolRenderer à la couche
        self.fzLayer.setRenderer(gr)

        # Etiquetage de la couche
        self.sumValue = self._getSumVal("flux_zones_tmp", strExp)

        self._labelFluxZone(True)

        # Refresh map
        self.fzLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())

    # -------------------------------------------------------------------------

    def addFluxZonesDiagram(self, nbLignes):
        """
        Affichage des flux par Zones de montées/descentes
        Visualisation de type diagrammes proportionnels
        """

        self.logger.debug("zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesTmp(nbLignes)
        self.logger.debug("insertions : " + str(nb))
        if nb <= 0:
            self.delFluxZones()
            return

        self._addLayerFluxZonesTmp(0)

        # Suppression de la symbologie par défaut (entités polygonales)
        symbolLayer = QgsSimpleFillSymbolLayer()
        symbolLayer.setStrokeWidth(0.6)
        symbolLayer.setStrokeColor(QColor(0, 0, 0, 255))
        symbolLayer.setFillColor(QColor(255, 255, 255, 0))
        self.fzLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayer
        )

        # line numbers in comments refer to qgsdiagramproperties.cpp
        diagram = QgsPieDiagram()  # Type de diagramme

        ds = QgsDiagramSettings()

        # Le paramètre transparency ne fonctionne pas directement,
        # sauf après avoir ouvert les propriétés de la couche et revalidé...
        # Il faut donc spécifier la transparence directement
        # dans la définition des couleurs (canal alpha)
        paramTransp = float(
            self.qmapodSettings.value("params/spxTransparencyDiagram", 30)
        )
        fltAlpha = 1.0 - (paramTransp / 100)
        intTransp = 257 * paramTransp / 100  # Pourquoi 257 ?
        ds.transparency = intTransp
        colM = self.qmapodSettings.value(
            "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
        )
        colM.setAlphaF(fltAlpha)
        colD = self.qmapodSettings.value(
            "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
        )
        colD.setAlphaF(fltAlpha)

        # Spécification des paramètres pour les diagrammes
        if self.typAnal == "m":
            lstColumns = ["descentes"]
            lstColors = [colD]
            symbolLayer.setStrokeColor(
                self.qmapodSettings.value(
                    "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
                )
            )
            lstLabels = ["Descentes"]
            strExp = "descentes"
        elif self.typAnal == "d":
            lstColumns = ["montees"]
            lstColors = [colM]
            symbolLayer.setStrokeColor(
                self.qmapodSettings.value(
                    "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
                )
            )
            lstLabels = ["Montées"]
            strExp = "montees"

        ds.categoryAttributes = lstColumns
        ds.categoryColors = lstColors
        ds.categoryLabels = lstLabels  # QGis 2.10

        # ds.sizeType = 0  # 523 - mm(0), map units (1)
        ds.sizeType = QgsUnitTypes.RenderUnit.RenderMillimeters
        ds.labelPlacementMethod = 1  # 524 - from an existing example...
        ds.scaleByArea = True  # 525
        ds.minimumSize = 0.0  # 533

        # Couleur du contour des diagrammes
        penColor = self.qmapodSettings.value(
            "params/btnColOutlineDiagram", QColor(255, 255, 255, 255), QColor
        )
        penColor.setAlphaF(fltAlpha)
        ds.penColor = penColor
        ds.penWidth = float(
            self.qmapodSettings.value("params/spxWidthOutlineDiagram", 0.4)
        )
        ds.diagramOrientation = 2  # 551 - May only be required for histograms
        # Voir aussi : QgsSingleCategoryDiagramRenderer
        dr = QgsLinearlyInterpolatedDiagramRenderer()
        dr.setLowerValue(0.0)
        dr.setLowerSize(QSizeF(0.0, 0.0))

        # Pour la légende
        self.maxValue = self._getMaxVal("flux_zones_tmp", strExp)
        dr.setUpperValue(self.maxValue)

        # Expression SQL spécifiant la taille
        dr.setClassificationAttributeExpression(strExp)
        # La taille est spécifiée par une expression
        dr.setClassificationAttributeIsExpression(True)

        # Pour la légende
        # Taille de diagramme correspondant à la valeur maximale
        self.maxSize = int(
            self.qmapodSettings.value("params/spxFluxZoneDiagMaxSize", 40)
        )
        dr.setUpperSize(QSizeF(self.maxSize, self.maxSize))

        # Colonne spécifiant la taille (si fixe)
        # dr.setClassificationAttribute(4)
        dr.setDiagram(diagram)
        dr.setDiagramSettings(ds)

        # And now finally put it into the layer... :-)
        self.fzLayer.setDiagramRenderer(dr)  # 572

        # affichage légende dans boite Couches
        dr.setAttributeLegend(True)
        dr.setDataDefinedSizeLegend(self._legend())

        dls = QgsDiagramLayerSettings()  # 575
        dls.setDistance(0)
        dls.setIsObstacle(False)
        dls.setPriority(1)
        dls.setPlacement(QgsDiagramLayerSettings.OverPoint)  # 588
        dls.setShowAllDiagrams(True)
        dls.setZIndex(-1.0)
        self.fzLayer.setDiagramLayerSettings(dls)  # 593

        # Etiquetage de la couche
        self.sumValue = self._getSumVal("flux_zones_tmp", strExp)  # QGIS < 2.18

        self._labelFluxZone(False)

        # Rafraichissement de la carte
        self.fzLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())

    # -------------------------------------------------------------------------

    def majFluxZones(self, nbLignes, typVisu):
        """
        Mise à jour de l'affichage courant des flux par zones
        Selon type de visualisation
        """

        self.logger.debug("maj visu" + str(typVisu))
        if typVisu == 1:  # Couleurs graduées
            self.majFluxZonesGraduated(nbLignes)
        elif typVisu == 2:  # Oursins
            self.majFluxZonesUrchin(nbLignes)
        elif typVisu == 3:  # Diagrammes proportionnels
            self.majFluxZonesDiagram(nbLignes)

    # -------------------------------------------------------------------------

    def majFluxZonesGraduated(self, nbLignes):
        """
        Mise à jour de l'affichage courant des flux par zones
        Visualisation de type dégradé de couleur
        """

        # Mise à jour de la table flux_zones_tmp
        self.logger.debug("zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesTmp(nbLignes)
        self.logger.debug("insertions : " + str(nb))
        if nb <= 0:
            self.delFluxZones()
            return

        # Sélection des paramètres à prendre en compte
        # pour le dégradé de couleurs
        if self.typAnal == "m":

            strExp = "descentes"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniGraduatedD", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiGraduatedD", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColMaxiGraduatedM", QColor(255, 0, 0, 255), QColor
            )
        elif self.typAnal == "d":
            strExp = "montees"
            colMin = self.qmapodSettings.value(
                "params/btnColMiniGraduatedM", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiGraduatedM", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColMaxiGraduatedD", QColor(255, 0, 0, 255), QColor
            )

        # Paramètres des classes de valeurs
        intNbClasses = int(self.qmapodSettings.value("params/spxNbClassesGraduated", 5))
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencyGraduated", 30)
        )

        colorRamp = QgsGradientColorRamp(colMin, colMax)

        # Méthodes de classification disponibles :
        # EqualInterval : Intervalles égaux
        # Quantile : Méthode des quantiles (effectifs égaux)
        # Jenks : Intervalles naturels de Jenks
        # StdDev : Déviation standard (écart-type)
        # Pretty : Intervalles jolis (arrondis)

        dctMode = {
            0: QgsGraduatedSymbolRenderer.EqualInterval,
            1: QgsGraduatedSymbolRenderer.Quantile,
            2: QgsGraduatedSymbolRenderer.Jenks,
            3: QgsGraduatedSymbolRenderer.StdDev,
            4: QgsGraduatedSymbolRenderer.Pretty,
        }
        objMode = dctMode[
            int(self.qmapodSettings.value("params/cbxClassificationGraduated", 2))
        ]

        # Mise à jour de la valeur maximum des variables analysées
        # Erreur si on ouvre et valide les propriétés de la couche des flux :
        # AttributeError: 'QgsDiagramRendererV2'
        # object has no attribute 'setUpperValue'
        # gr.updateClasses(
        #    self.fzLayer, QgsGraduatedSymbolRendererV2.EqualInterval,
        #    intNbClasses)

        gr = self.fzLayer.renderer()

        gr.updateClasses(self.fzLayer, objMode, intNbClasses)
        # gr.updateRangeUpperValue(
        #    rangeIndex, self._getMaxVal('flux_zones_tmp', exp))
        gr.updateColorRamp(colorRamp)

        # Application du GraduatedSymbolRenderer à la couche
        self.fzLayer.setRenderer(gr)

        # Etiquetage de la couche
        self.sumValue = self._getSumVal("flux_zones_tmp", strExp)  # QGIS < 2.18

        self._labelFluxZone(False)

        # Récupération du renderer de la couche de sélection

        grSel = self.selLayer.renderer()

        # Mise à jour des paramètre d'affichage de la couche de sélection
        symbolLayer = grSel.symbol().symbolLayer(0).clone()
        symbolLayer.setStrokeColor(selBorderCol)
        grSel.symbols(QgsRenderContext())[0].changeSymbolLayer(0, symbolLayer)

        # Rafraichissement de la carte
        self.fzLayer.triggerRepaint()
        self.selLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())
        self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())

    # -------------------------------------------------------------------------

    def majFluxZonesUrchin(self, nbLignes):
        """
        Mise à jour de l'affichage courant des flux par zones
        Visualisation de type oursins
        """

        # Mise à jour de la table flux_zones_tmp
        self.logger.debug("zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesUrchinTmp(nbLignes)
        self.logger.debug("insertions : " + str(nb))
        if nb <= 0:
            self.delFluxZones()
            return

        # Sélection des paramètres à prendre en compte
        # pour le calcul de la valeur maximale
        if self.typAnal == "m":
            # strExp = 'descentes'
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinD", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
            )
            # posArrow = QgsMarkerLineSymbolLayerV2.LastVertex
            # angArrow = 90
        elif self.typAnal == "d":
            # strExp = 'montees'
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinM", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
            )
            # posArrow = QgsMarkerLineSymbolLayerV2.FirstVertex
            # angArrow = 270

        # Récupération du renderer de la couche
        gr = self.fzLayer.renderer()

        # Mise à jour de la classification des valeurs
        intNbClasses = int(self.qmapodSettings.value("params/spxNbClassesUrchin", 5))

        dctMode = {
            0: QgsGraduatedSymbolRenderer.EqualInterval,
            1: QgsGraduatedSymbolRenderer.Quantile,
            2: QgsGraduatedSymbolRenderer.Jenks,
            3: QgsGraduatedSymbolRenderer.StdDev,
            4: QgsGraduatedSymbolRenderer.Pretty,
        }
        objMode = dctMode[
            int(self.qmapodSettings.value("params/cbxClassificationUrchin", 2))
        ]
        gr.updateClasses(self.fzLayer, objMode, intNbClasses)

        # Mise à jour des couleurs du dégradé
        colorRamp = QgsGradientColorRamp(colMin, colMax)
        gr.updateColorRamp(colorRamp)

        # Mise à jour de l'épaisseur des rayons de l'oursin
        urchinSymbol = gr.sourceSymbol().clone()
        subSymbol = urchinSymbol.symbolLayer(0).subSymbol()

        urchinSymbol.symbolLayer(0).setArrowStartWidth(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0))
        )
        urchinSymbol.symbolLayer(0).setArrowWidth(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0))
        )
        urchinSymbol.symbolLayer(0).setHeadLength(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0)) * 1.25
        )
        urchinSymbol.symbolLayer(0).setHeadThickness(
            float(self.qmapodSettings.value("params/spxUrchinWidth", 2.0)) * 1.25
        )

        # Mise à jour de la transparence de la couche
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencyUrchin", 30)
        )
        fltOpacity = (100 - intTransparency) / 100
        subSymbol.setOpacity(fltOpacity)

        gr.updateSymbols(urchinSymbol)

        # Récupération du renderer de la couche de sélection
        grSel = self.selLayer.renderer()

        # Mise à jour des paramètre d'affichage de la couche de sélection
        symbolLayer = grSel.symbol().symbolLayer(0).clone()

        self._labelFluxZone(True)

        symbolLayer.setStrokeColor(selBorderCol)

        grSel.symbols(QgsRenderContext())[0].changeSymbolLayer(0, symbolLayer)

        # Rafraichissement de la carte
        self.fzLayer.triggerRepaint()
        self.selLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())
        self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())

    # ------------------------------------------------------------------------

    def majFluxZonesUrchinLines(self, nbLignes):
        """
        Mise à jour de l'affichage courant des flux par zones
        Visualisation de type oursins
        """

        # Mise à jour de la table flux_zones_tmp
        self.logger.debug("zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesUrchinTmp(nbLignes)
        self.logger.debug("insertions : " + str(nb))
        if nb <= 0:
            self.delFluxZones()
            return

        # Sélection des paramètres à prendre en compte
        # pour le calcul de la valeur maximale
        if self.typAnal == "m":
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinD", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
            )
        elif self.typAnal == "d":
            colMin = self.qmapodSettings.value(
                "params/btnColMiniUrchinM", QColor(255, 0, 0, 255), QColor
            )
            colMax = self.qmapodSettings.value(
                "params/btnColMaxiUrchinM", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColMaxiUrchinD", QColor(255, 0, 0, 255), QColor
            )

        # Récupération du renderer de la couche
        gr = self.fzLayer.renderer()

        # Mise à jour de la classification des valeurs
        intNbClasses = int(self.qmapodSettings.value("params/spxNbClassesUrchin", 5))

        dctMode = {
            0: QgsGraduatedSymbolRenderer.EqualInterval,
            1: QgsGraduatedSymbolRenderer.Quantile,
            2: QgsGraduatedSymbolRenderer.Jenks,
            3: QgsGraduatedSymbolRenderer.StdDev,
            4: QgsGraduatedSymbolRenderer.Pretty,
        }
        objMode = dctMode[
            int(self.qmapodSettings.value("params/cbxClassificationUrchin", 2))
        ]
        gr.updateClasses(self.fzLayer, objMode, intNbClasses)

        colorRamp = QgsGradientColorRamp(colMin, colMax)
        gr.updateColorRamp(colorRamp)

        # Mise à jour de l'épaisseur des rayons de l'oursin
        urchinSymbol = gr.sourceSymbol().clone()
        subSymbol = urchinSymbol.symbolLayer(0).subSymbol()

        # Mise à jour de la transparence de la couche
        intTransparency = int(
            self.qmapodSettings.value("params/spxTransparencyUrchin", 30)
        )
        fltOpacity = (100 - intTransparency) / 100
        subSymbol.setOpacity(fltOpacity)
        gr.updateSymbols(urchinSymbol)

        grSel = self.selLayer.renderer()

        # Mise à jour des paramètre d'affichage de la couche de sélection
        symbolLayer = grSel.symbol().symbolLayer(0).clone()

        self._labelFluxZone(True)

        symbolLayer.setStrokeColor(selBorderCol)
        grSel.symbols(QgsRenderContext())[0].changeSymbolLayer(0, symbolLayer)

        # Rafraichissement de la carte
        self.fzLayer.triggerRepaint()
        self.selLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())
        self.iface.layerTreeView().refreshLayerSymbology(self.selLayer.id())

    # -------------------------------------------------------------------------

    def majFluxZonesDiagram(self, nbLignes):
        """
        Mise à jour de l'affichage courant des flux par zones
        Visualisation de type diagrammes proportionnels
        diagramLayerSettings()
        diagramRenderer()
        diagram()
        """

        # Mise à jour de la table flux_zones_tmp
        self.logger.debug("maj zones : " + str(len(self.lstZones)))
        if len(self.lstZones) == 0:
            self.delFluxZones()
            return
        nb = self._createTableFluxZonesTmp(nbLignes)
        if nb <= 0:
            self.delFluxZones()
            return

        # Le paramètre transparency ne fonctionne pas directement,
        # sauf après avoir ouvert les propriétés de la couche et revalidé...
        # Il faut donc spécifier la transparence directement
        # dans la définition des couleurs (canal alpha)
        paramTransp = float(
            self.qmapodSettings.value("params/spxTransparencyDiagram", 30)
        )
        fltAlpha = 1.0 - (paramTransp / 100)
        intTransp = 257 * paramTransp / 100  # Pourquoi 257 ?
        colM = self.qmapodSettings.value(
            "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
        )
        colM.setAlphaF(fltAlpha)
        colD = self.qmapodSettings.value(
            "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
        )
        colD.setAlphaF(fltAlpha)

        # Sélection des paramètres à prendre en compte pour les diagrammes
        if self.typAnal == "m":
            strExp = "descentes"
            lstColumns = ["descentes"]
            lstLabels = ["Descentes"]
            lstColors = [colD]
            diagBorderColor = self.qmapodSettings.value(
                "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
            )
            selColFill = self.qmapodSettings.value(
                "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
            )
            selColFill.setAlphaF(fltAlpha)
            selBorderWidth = 0.6
        elif self.typAnal == "d":
            strExp = "montees"
            lstColumns = ["montees"]
            lstLabels = ["Montées"]
            lstColors = [colM]
            diagBorderColor = self.qmapodSettings.value(
                "params/btnColBgDiagramM", QColor(255, 0, 0, 255), QColor
            )
            selBorderCol = self.qmapodSettings.value(
                "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
            )
            selColFill = self.qmapodSettings.value(
                "params/btnColBgDiagramD", QColor(255, 0, 0, 255), QColor
            )
            selColFill.setAlphaF(fltAlpha)
            selBorderWidth = 0.6

        # Mise à jour de la valeur maximum des variables analysées
        # Erreur si on ouvre et valide les propriétés de la couche des flux :
        dr = self.fzLayer.diagramRenderer()
        dr.setUpperValue(self._getMaxVal("flux_zones_tmp", strExp))

        symbolLayerSel = QgsSimpleFillSymbolLayer()
        symbolLayerSel.setStrokeWidth(selBorderWidth)
        symbolLayerSel.setStrokeColor(selBorderCol)
        symbolLayerSel.setFillColor(selColFill)

        self.selLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayerSel
        )
        self.selLayer.triggerRepaint()

        # Suppression de la symbologie par défaut (entités polygonales)
        symbolLayer = QgsSimpleFillSymbolLayer()
        symbolLayer.setStrokeWidth(0.6)
        symbolLayer.setStrokeColor(QColor(0, 0, 0, 255))
        symbolLayer.setFillColor(QColor(255, 255, 255, 0))
        symbolLayer.setStrokeColor(diagBorderColor)

        self.fzLayer.renderer().symbols(QgsRenderContext())[0].changeSymbolLayer(
            0, symbolLayer
        )
        diagram = QgsPieDiagram()  # 500 - Type de diagramme

        ds = QgsDiagramSettings()  # 507

        ds.transparency = intTransp

        ds.categoryAttributes = lstColumns
        ds.categoryColors = lstColors
        ds.categoryLabels = lstLabels  # QGis 2.10

        # ds.sizeType = 0  # 523 - mm(0), map units (1)
        ds.sizeType = QgsUnitTypes.RenderUnit.RenderMillimeters
        ds.labelPlacementMethod = 1  # 524 - from an existing example...
        ds.scaleByArea = True  # 525
        ds.minimumSize = 0.0  # 533

        # Couleur du contour des diagrammes
        penColor = self.qmapodSettings.value(
            "params/btnColOutlineDiagram", QColor(255, 255, 255, 255), QColor
        )
        penColor.setAlphaF(fltAlpha)
        ds.penColor = penColor
        ds.penWidth = float(
            self.qmapodSettings.value("params/spxWidthOutlineDiagram", 0.4)
        )
        ds.diagramOrientation = 2  # 551 - May only be required for histograms
        # Voir aussi : QgsSingleCategoryDiagramRenderer
        dr = QgsLinearlyInterpolatedDiagramRenderer()
        dr.setLowerValue(0.0)
        dr.setLowerSize(QSizeF(0.0, 0.0))

        # Pour la légende
        self.maxValue = self._getMaxVal("flux_zones_tmp", strExp)
        dr.setUpperValue(self.maxValue)

        # Expression SQL spécifiant la taille
        dr.setClassificationAttributeExpression(strExp)
        # La taille est spécifiée par une expression
        dr.setClassificationAttributeIsExpression(True)

        # Pour la légende
        # Taille de diagramme correspondant à la valeur maximale
        self.maxSize = int(
            self.qmapodSettings.value("params/spxFluxZoneDiagMaxSize", 40)
        )
        dr.setUpperSize(QSizeF(self.maxSize, self.maxSize))
        dr.setDiagram(diagram)
        dr.setDiagramSettings(ds)

        # And now finally put it into the layer... :-)
        self.fzLayer.setDiagramRenderer(dr)  # 572

        # affichage légende dans boite Couches
        dr.setAttributeLegend(True)
        dr.setDataDefinedSizeLegend(self._legend())

        dls = QgsDiagramLayerSettings()  # 575
        dls.setDistance(0)
        dls.setIsObstacle(False)
        dls.setPriority(1)
        dls.setPlacement(QgsDiagramLayerSettings.OverPoint)  # 588
        dls.setShowAllDiagrams(True)
        dls.setZIndex(-1.0)
        self.fzLayer.setDiagramLayerSettings(dls)  # 593

        # Etiquetage de la couche

        self.sumValue = self._getSumVal("flux_zones_tmp", strExp)
        self._labelFluxZone(False)

        # Rafraichissement de la carte
        self.fzLayer.triggerRepaint()
        self.iface.mapCanvas().refresh()
        self.iface.layerTreeView().refreshLayerSymbology(self.fzLayer.id())
