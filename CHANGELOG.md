Version 2.7 -> 3.0

- Strings are templated with the `Template` python module;
- Main SQL queries are extracted into autonomous SQL files, in the `sql` folder;
- A logger is added, to report information, warnings and errors;
- Each SQL query sent to Spatialite is logged from the DEBUG level of the logger;
- A `changelog` file is created;
- The `README` file is filled;
- The `metadata.txt` file is enriched;
- The generated files are no longer followed by Git (`qmapod_dock_base.py`, `QMapOD.ini`, `data/demo.json` et `data/demo.sqlite`);
- References to the resources files are removed, since they have become useless;
- Demonstration data are stored in a `demo_data` folder and copied when needed into the `data` folder, the `data` folder is no longer followed by Git;
- A deployment script `deploy.sh` has been added for Linux and the Windows script was reviewed.
