# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

```{toctree}
---
caption: Utilisation
maxdepth: 1
---
Installation <utilisation/installation>
Utilisation <utilisation/utilisation>
```

```{toctree}
---
caption: Développement
maxdepth: 1
---
development/contribute
development/environment
development/architecture
development/documentation
development/packaging
development/history
```

```{toctree}
---
caption: Divers
maxdepth: 1
---
misc/financer
```
