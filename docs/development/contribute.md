# Pour contribuer au plugin

## Sous Linux

1. Cloner le [dépôt Gitlab](https://gitlab.com/Oslandia/qgis/qmapod) dans un dossier local. Par exemple, en SSH :
```shell
git clone git@gitlab.com:Oslandia/qgis/qmapod.git
```

ou en HTTP :
```shell
git clone https://gitlab.com/Oslandia/qgis/qmapod.git
```

2. Installer les outils de développement de Qt :
```shell
sudo apt install pyqt5-dev-tools
```

3. Exécuter le script de déploiement :

* Sous Linux :
```shell
bash deploy.sh
```

* Sous Windows :
```shell
deploy.bat
```

ou dérouler manuellement les étapes suivantes en cas de besoin :

- créer un fichier de paramètres `QMapOD.ini` pour le plugin, à partir du modèle fourni `QMapODDefault.ini`. Ce fichier `QMapOD.ini` est mis à jour lorsqu'on modifie le paramétrage du plugin dans QGIS afin de conserver ce paramétrage pour les futures exécutions.

```shell
cp QMapODDefault.ini QMapOD.ini
```

- recopier les données d'exemple dans le dossier `data` du plugin :
```shell
mkdir -p data
cp demo_db_creation/* data
```

- le cas échéant, changer le niveau de log dans le fichier `data/demo.json`, à la ligne 207 :
```json
"loglevel": "DEBUG"
```

à adapter en utilisant un des niveaux de log suivants : `"DEBUG", "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"`.


4. Ajouter une variable d'environnement dans QGIS pour ajouter le dossier contenant le plugin dans le gestionnaire d'extensions : dans `Préférences / Options / Système / Environnement`, ajouter une variable `QGIS_PLUGINPATH` valant `path/to/dir`, où `dir` est le dossier parent du plugin. Activer l'extension depuis le gestionnaire d'extension


Remarque : pour recharger le plugin après l'avoir modifié, vous pouvez :
- fermer et rouvrir QGIS ;
ou
- utiliser le plugin "Plugin reloader" pour recharger la configuration.

## Données

Une base de données de test `demo.sqlite`, avec un fichier de paramètres associé `demo.json` sont mis à disposition dans le dossier `data`. Ils servent de démonstration pour le plugin, ils sont basés sur un réseau inspiré de celui de l'agglomération de Rennes en 2023, mais réduit en termes de nombre de lignes. Les données d'enquête associées sont fictives, générées aléatoirement.

### Pour générer une base de données vierge pour héberger vos propres données d'enquête

#### Sous Linux

- Installer Spatialite :

```shell
sudo apt install spatialite-bin
```

### Pour créer une base de données

#### Base vierge dans laquelle saisir les données via un SIG

```shell
spatialite data/my_db.sqlite < sql/demo_db_creation/create_empty_db.sql
```
Pour manipuler, requêter les données, vous pouvez utiliser notamment `spatialite-gui`, sous Linux :

```shell
sudo apt install spatialite-gui
```

Lancer Spatialite-GUI :
```shell
spatialite-gui
```

QGIS permet également de requêter et de visualiser les données géométriques.

#### Base vierge dans laquelle importer des données shape existantes

```shell
spatialite data/my_db.sqlite
```

Pour importer des données au format `.shp`, utiliser des commandes telles que celles ci-dessous :
```shell
spatialite_tool -i -shp myfolder/arrets -d data/my_db.sqlite -t arrets -s 2154 -c UTF-8 --type POINT
spatialite_tool -i -shp myfolder/communes -d data/my_db.sqlite -t communes -s 2154 -c UTF-8 --type MULTIPOLYGON
spatialite_tool -i -shp myfolder/troncons -d data/my_db.sqlite -t troncons -s 2154 -c UTF-8 --type LINESTRING
spatialite_tool -i -shp myfolder/zones_od -d data/my_db.sqlite -t zones_od -s 2154 -c UTF-8 --type POLYGON
spatialite_tool -i -dbf myfolder/arrets_zones.dbf -d data/my_db.sqlite -t arrets_zones -c UTF-8
spatialite_tool -i -dbf myfolder/code_freq.dbf -d data/my_db.sqlite -t code_freq -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne.dbf -d data/my_db.sqlite -t code_ligne -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne_coram.dbf -d data/my_db.sqlite -t code_ligne_coram -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne_corav.dbf -d data/my_db.sqlite -t code_ligne_corav -c UTF-8
spatialite_tool -i -dbf myfolder/code_ligne_var.dbf -d data/my_db.sqlite -t code_ligne_var -c UTF-8
spatialite_tool -i -dbf myfolder/code_modap.dbf -d data/my_db.sqlite -t code_modap -c UTF-8
spatialite_tool -i -dbf myfolder/code_motif.dbf -d data/my_db.sqlite -t code_motif -c UTF-8
spatialite_tool -i -dbf myfolder/code_pmr.dbf -d data/my_db.sqlite -t code_pmr -c UTF-8
spatialite_tool -i -dbf myfolder/code_residence.dbf -d data/my_db.sqlite -t code_residence -c UTF-8
spatialite_tool -i -dbf myfolder/code_sens.dbf -d data/my_db.sqlite -t code_sens -c UTF-8
spatialite_tool -i -dbf myfolder/code_statut.dbf -d data/my_db.sqlite -t code_statut -c UTF-8
spatialite_tool -i -dbf myfolder/code_titre.dbf -d data/my_db.sqlite -t code_titre -c UTF-8
spatialite_tool -i -dbf myfolder/code_trage.dbf -d data/my_db.sqlite -t code_trage -c UTF-8
spatialite_tool -i -dbf myfolder/code_trhor.dbf -d data/my_db.sqlite -t code_trhor -c UTF-8
spatialite_tool -i -dbf myfolder/code_trhor_typjour.dbf -d data/my_db.sqlite -t code_trhor_typjour -c UTF-8
spatialite_tool -i -dbf myfolder/code_type_jour.dbf -d data/my_db.sqlite -t code_type_jour -c UTF-8
spatialite_tool -i -dbf myfolder/code_type_ligne.dbf -d data/my_db.sqlite -t code_type_ligne -c UTF-8
spatialite_tool -i -dbf myfolder/comptages.dbf -d data/my_db.sqlite -t comptages -c UTF-8
spatialite_tool -i -dbf myfolder/data_licenses.dbf -d data/my_db.sqlite -t data_licenses -c UTF-8
spatialite_tool -i -dbf myfolder/enquetes.dbf -d data/my_db.sqlite -t enquetes -c UTF-8
spatialite_tool -i -dbf myfolder/parcours.dbf -d data/my_db.sqlite -t parcours -c UTF-8
spatialite_tool -i -dbf myfolder/schemas.dbf -d data/my_db.sqlite -t schemas -c UTF-8
```

## Auteurs et licence

L'application MapOD a été initialement développée par la société SIGéal, sous Access et ArcGis / MapInfo.  
Il a ensuite été réécrit sous la forme d'un plugin QGIS, intégrant une base de données Spatialite, pour devenir QMapOD.  
Les développements ont été financés à l'origine par la société Test SA.  
SIGéal ayant cessé son activité depuis janvier 2024, c'est Oslandia qui assure aujourd'hui la maintenance du plugin.  

QGIS et QMapOD sont diffusés sous licence Open Source (GNU General Public License).

Contact : contact@oslandia.com
