# Architecture
## 1. Tables non spatiales \:
### 1.1. Tables alphanumériques des enquêtes (SQLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Table parcours \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un élément de parcours de ligne de bus.
	* Champ id_ligne (entier) \: Identifiant de la ligne
	* Champ id_sens (entier court) \: Identifiant du sens de la ligne
	* Champ id_parcours (entier court) \: Identifiant du parcours de la ligne
	* Champ id_arret (entier court) \: Identifiant de l’arrêt
	* Champ ordre (entier court) \: Numéro d’ordre de l’arrêt pour la ligne, le sens et le parcours considéré
	* Champ desserte (booléen) \: Indicateur précisant si l’arrêt est desservi pour le parcours considéré
	* Champ id_tron (chaîne) \: Identifiant du tronçon reliant l’arrêt considéré à l’arrêt suivant

* **Table code_sens \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de sens de ligne.
	* Champ id_sens (entier court) \: Identifiant du sens
	* Champ id_ligne (entier long) \: Identifiant de la ligne
	* Champ lib_sens (chaîne) \: Libellé du sens

* **Table courses \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une course.
	* Champ id_ligne (entier long) \: Identifiant de la ligne
	* Champ num_ligne (chaîne) \: Numéro de la ligne
	* Champ id_sens (entier court) \: Identifiant du sens de la ligne
	* Champ id_parcours (entier court) \: Identifiant du parcours de la ligne
	* Champ id_course (entier long) \: Identifiant de la course
	* Champ id_typ_jour (entier court) \: Code de type de jour
	* Champ id_typ_ligne (entier court) \: Code de type de ligne
	* Champ hdeb (chaîne) \: Heure de départ de la course
	* Champ hfin (chaîne) \: Heure d'arrivée de la course
	* Champ id_arret_deb (entier long) \: Identifiant de l’arrêt de début de la course
	* Champ nom_arret_deb (chaîne) \: Nom de l’arrêt de début de la course
	* Champ id_arret_fin (entier long) \: Identifiant de l’arrêt de fin de la course
	* Champ nom_arret_fin (chaîne) \: Nom de l’arrêt de fin de la course
	* Champ poids (entier long) \: Poids de la course (si saisie par tranche horaire)

* **Table offre \:**
Chaque enregistrement de cette table contient la description alphanumérique de l'offre pour un élément de parcours pour un type de jour.
	* Champ id_ligne(entier long) \: Identifiant de la ligne
	* Champ id_sens (entier court) \: Identifiant du sens de la ligne
	* Champ id_parcours (entier court) \: Identifiant du parcours de la ligne
	* Champ id_arret (entier court) \: Identifiant de l’arrêt
	* Champ ordre (entier court) \: Numéro d’ordre de l’arrêt pour la ligne, le sens et le parcours considéré
	* Champ desserte (booléen) \: Indicateur précisant si l’arrêt est desservi pour le parcours considéré
	* Champ id_tron (chaîne) \: Identifiant du tronçon reliant l’arrêt considéré à l’arrêt suivant
	* Champ typ_jour (entier court) \: Code de type de jour
	* Champ offre (entier) \: Offre globale moyenne

* **Table enquetes \:**
Chaque enregistrement de ces tables contient la description alphanumérique d’un questionnaire d’enquête, pondéré par les comptages.
	* Champ ident (entier) \: Identifiant questionnaire
	* Champ typjour (entier court) \: Identifiant du type de jour
	* Champ ligne (entier court) \: Identifiant de la ligne
	* Champ sens (entier court) \: Identifiant du sens de circulation
	* Champ course (entier) \: Identifiant de la course
	* Champ parcours (entier court) \: Identifiant du parcours
	* Champ tranche (entier court) \: Identifiant de la tranche horaire regroupée
	* Champ arreta2 (entier) \: Identifiant de l’arrêt de montée
	* Champ arretb2 (entier) \: Identifiant de l’arrêt de descente
	* Champ arretdeb2 (entier court) \: Identifiant de l’arrêt de début du déplacement considéré (si correspondance avant)
	* Champ arretfin2 (entier court) \: Identifiant de l’arrêt de fin du déplacement considéré (si correspondance avant)
	* Champ nbav (entier court) \: Nombre de correspondances avant
	* Champ nbap (entier court) \: Nombre de correspondances après
	* Champ lignem1 (entier court) \: Identifiant de la première ligne empruntée avant la ligne courante (si correspondance avant)
	* Champ lignep1 (entier court) \: Identifiant de la première ligne empruntée avant la ligne courante (si correspondance après)
	* Champ motifod (entier court) \: Identifiant du motif agrégé du déplacement
	* Champ sexe (entier court) \: Identifiant du sexe de la personne enquêtée
	* Champ trage (entier court) \: Identifiant de la tranche d’âge regroupée
	* Champ titre (entier court) \: Identifiant du titre de transport utilisé
	* Champ poidsl (réel double) \: Poids du questionnaire considéré sur la ligne
	* Champ poidsr (réel double) \: Poids du questionnaire considéré sur le réseau (inférieur à POIDSL si correspondances)

* **Table code_comres \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une commune de résidence.
	* Champ id_comres (entier court) \: Identifiant du code INSEE de la commune de résidence
	* Champ lib_comres (chaîne) \: Libellé de la commune de résidence
	* Champ comres (chaîne) \: Libellé abrégé du code de la commune de résidence

* **Table code_ligne \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une ligne de bus, de tramway ou de métro.
	* Champ id_ligne (entier) \: Identifiant de la ligne
	* Champ num_ligne (chaîne) \: Numéro de la ligne
	* Champ lib_ligne (chaîne) \: Libellé de la ligne
	* Champ typ_ligne (entier) \: Type de la ligne
	* Champ rvb (chaîne) \: Couleur de la ligne (codes R/V/B)

* **Table code_modav \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un mode de déplacement amont.
	* Champ id_modav (entier court) \: Identifiant du code de mode de déplacement amont
	* Champ lib_modav (chaîne) \: Libellé du code de mode de déplacement amont
	* Champ modav (chaîne) \: Libellé abrégé du code de mode de déplacement amont

* **Table code_modap \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un mode de déplacement aval.
	* Champ id_modap (entier court) \: Identifiant du code de mode de déplacement aval
	* Champ lib_modap (chaîne) \: Libellé du code de mode de déplacement aval
	* Champ modap (chaîne) \: Libellé abrégé du code de mode de déplacement aval

* **Table code_motif \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de motif de déplacement agrégé.
	* Champ id_motif (entier court) \: Identifiant du code de motif de déplacement agrégé
	* Champ lib_motif (chaîne) \: Libellé du code de motif de déplacement agrégé
	* Champ motifod (chaîne) \: Libellé abrégé du code de motif de déplacement agrégé

* **Table code_pmr \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de personne à mobilité réduite.
	* Champ id_pmr (entier court) \: Identifiant du code de personne à mobilité réduite
	* Champ lib_pmr (chaîne) \: Libellé du code de personne à mobilité réduite
	* Champ pmr (chaîne) \: Libellé abrégé du code de personne à mobilité réduite

* **Table code_titre \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de titre de transport.
	* Champ id_titre  entier court) \: Identifiant du code de titre de transport
	* Champ lib_titre (chaîne) \: Libellé du code de titre de transport
	* Champ titre (chaîne) \: Libellé abrégé du code de titre de transport

* **Table code_trage \:**
Chaque enregistrement de cette table contient la description alphanumérique d’une tranche d’âge d’usager du réseau.
	* Champ id_trage (entier court) \: Identifiant de la tranche d’âge d’usager du réseau
	* Champ lib_trage (chaîne) \: Libellé de la tranche d’âge d’usager du réseau
	* Champ trage (chaîne) \: Libellé abrégé de la tranche d’âge d’usager du réseau

* **Table code_trhor \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de tranche horaire.
	* Champ id_trhor (entier court) \: Identifiant du code de tranche horaire
	* Champ lib_trhor (chaîne) \: Libellé du code de tranche horaire
	* Champ trhor (chaîne) \: Libellé abrégé du code de tranche horaire
	* *Champ hdeb (chaîne) \: Heure de début de la tranche horaire (optionnel)*
	* *Champ hfin (chaîne) \: Heure de fin de la tranche horaire (optionnel)*

* **Table code_type_jour \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un type de jour d'enquête.
	* Champ id_type_jour (entier court) \: Identifiant du type de jour d'enquête
	* Champ lib_type_jour (chaîne) \: Libellé du type de jour d'enquête
	* Champ type_jour (chaîne) \: Libellé abrégé du type de jour d'enquête

* **Table code_type_ligne \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un type de ligne enquêtée.
	* Champ id_type_ligne (entier court) \: Identifiant du type de ligne enquêtée
	* Champ lib_type_ligne (chaîne) \: Libellé du type de ligne enquêtée
	* Champ type_ligne (chaîne) \: Libellé abrégé du type de ligne enquêtée

* **Table trhor_typjour \:**
Chaque enregistrement de cette table contient la description alphanumérique d’un code de tranche horaire pour un type de jour particulier.
	* Champ id_trhor (entier court) \: Identifiant du code de tranche horaire
	* Champ id_type_jour (entier court) \: Identifiant du type de jour d'enquête
	* Champ lib_trhor (chaîne) \: Libellé du code de tranche horaire
	* Champ trhor (chaîne) \: Libellé abrégé du code de tranche horaire
	* Champ hdeb (chaîne) \: Heure de début de la tranche horaire
	* Champ hfin (chaîne) \: Heure de fin de la tranche horaire

### 1.1.2. Tables alphanumériques temporaires (SQLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Table charge_tmp \:**
Chaque enregistrement de cette table contient un élément de parcours et ses données de charge.

* **Table enquetes_tmp \:**
Chaque enregistrement de cette table contient une enquête filtrée et ses données.

* **Table offre_tmp \:**
Chaque enregistrement de cette table contient un élément de parcours et ses données d'offre.

## 2. Tables spatiales \:
### 2.1. Tables spatiales du réseau (SpatiaLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Table troncons (Polylignes) \:**
Chaque enregistrement de cette table contient la description graphique d’un tronçon de réseau de transport en commun.
	* Champ id_tron (chaîne) \: Identifiant du tronçon
	* Champ geometry (linestring) \: Description géographique du tronçon

* **Table arrets (Points) \:**
Chaque enregistrement de cette table contient la description graphique d’un arrêt logique de transport en commun.
	* Champ id_arret (entier long) \: Identifiant de l’arrêt
	* Champ nom_arret (chaîne) \: Nom de l’arrêt
	* Champ fictif (entier long) \: Indicateur précisant s’il s’agit d’un arrêt ou d’un nœud de réseau (point singulier)
	* Champ geometry (point) \: Description géographique de l'arrêt

* **Table communes (Polygones) \:**
Chaque enregistrement de cette table contient la description graphique d’une commune et ses données attributaires.
	* Champ id_commune (entier long) \: Identifiant INSEE de la commune
	* Champ nom_commun (chaîne) \: Nom de la commune
	* Champ geometry (multipolygon) \: Description géographique de la commune

* **Table zones_xx (Polygones) \:**
Chaque enregistrement de cette table contient la description graphique d’une zone de type xx.
	* Champ id_zonexx (entier long) \: Identifiant de la zone
	* Champ nom_zonexx (chaîne) \: Nom de la zone
	* Champ geometry (multipolygon) \: Description géographique de la zone

### 2.2. Tables spatiales temporaires (SpatiaLite) \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Il s’agit de tables créées lors de l’affichage des flux entre zones.

* **Table arrets_tmp \:**
Chaque enregistrement de cette table contient un arrêt et ses données de montées et de descentes.

* **Table arrets_tmp_var \:**
Chaque enregistrement de cette table contient un arrêt et ses données de montées ou de descentes croisées avec une variable enquêtée.

* **Table flux_arrets_tmp \:**
Chaque enregistrement de cette table contient un arrêt et ses données de flux associées.

* **Table fluxpp_tmp \:**
Chaque enregistrement de cette table contient un flux entre deux zones, utilisé pour l'analyse des flux principaux.

* **Table flux_zones_tmp \:**
Chaque enregistrement de cette table contient une zone et ses données de flux associées.

* **Table sous_reseau_arrets_tmp \:**
Chaque enregistrement de cette table contient un arrêt du sous-réseau filtré.

* **Table sous_reseau_troncons_tmp \:**
Chaque enregistrement de cette table contient un tronçon du sous-réseau filtré.

* **Table troncons_tmp \:**
Chaque enregistrement de cette table contient un tronçon et ses données de charge.

* **Table zones_tmp \:**
Chaque enregistrement de cette table contient une et ses données de montées et de descentes.

* **Table zones_tmp_var \:**
Chaque enregistrement de cette table contient une zone et ses données de montées ou de descentes croisées avec une variable enquêtée.

### 2.3. Autres tables \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Il s’agit de tables interne à SQLite/SpatiaLite.

## 3. Référence du fichier json de configuration \:

### 3.1. Généralités \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Le fichier json de configuration doit porter le même nom que la base de données SpatiaLite, seule l'extension les différenciant.

Le fichier json permet, dans une certaine mesure, d'adapter la structure de la base de données aux besoins de l'enquête : variables enquêtées, variables analysables sous forme cartographique, nom des colonnes, etc.

Il permet également de donner les informations permettant à l'application de construire l'interface graphique de l'extension à partir des données spécifiques à chaque enquête.

Par exemple, pour une variable enquêtée ***Variable***, la rubrique json précisera :

- "objlbl": "***lblVariable***" -> le nom de l'objet instancié pour la variable dans l'extension
- "title": "***Libellé pour ma variable***" -> le libellé utilisé pour la variable dans l'interface graphique de l'extension
- "sql": "***select id_variable, lib_variable from code_variable order by id_variable;***" -> la requête sql à utiliser pour retrouver les couples clé-valeur de la variable
- "enqfield": "***var***" -> le nom de la colonne contenant les codes de la variable dans la table ***enquetes***.

Les différentes rubriques de configuration sont décrites dans les paragraphes suivants.

### 3.2. dctTypeJour \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage de la liste déroulante des types de jour :  
 ```
"cbxTypeJour": {
	"objlbl": "lblTypeJour",
	"title": "Type de jour",
	"sql": "select id_type_jour, lib_type_jour from code_type_jour order by id_type_jour;",
	"enqfield": "typjour"
}
```

### 3.3. dctCritereSignaletique \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage de la liste de cases à cocher pour chaque variable enquêtée relative à l'usager :  

- Motif de déplacement (origine et destination agrégées) :
 ```
"lwgMotif": {
	"objlbl": "lblMotif",
	"label": "Motifs",
	"sql": "select id_motif, lib_motif from code_motif order by id_motif;",
	"enqfield": "motifod"
}
```

- Titre de transport utilisé :
```
"lwgTitre": {
	"objlbl": "lblTitre",
	"label": "Titres",
	"sql": "select id_titre, lib_titre from code_titre order by id_titre;",
	"enqfield": "titre"
}
```

- Tranche d'âge de l'usager :
```
"lwgTrage": {
	"objlbl": "lblTrage",
	"label": "Tranche d'âge",
	"sql": "select id_trage, lib_trage from code_trage order by id_trage;",
	"enqfield": "trage"
}
```

- Lieu de résidence de l'usager :
```
"lwgResidence": {
	"objlbl": "lblResidence",
	"label": "Résidence",
	"sql": "select id_residence, lib_residence from code_residence order by id_residence;",
	"enqfield": "residence"
}
```

- Personne à mobilité réduite :
```
"lwgPmr": {
	"objlbl": "lblPmr",
	"label": "PMR",
	"sql": "select id_pmr, lib_pmr from code_pmr order by id_pmr;",
	"enqfield": "pmr"
}
```

### 3.4. dctCritereVoyage \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage de la liste de cases à cocher pour chaque variable enquêtée relative au voyage :

- Tranche horaire du voyage :
```
"lwgTrhor": {
	"objlbl": "lblTrhor",
	"label": "Tranches horaires",
	"sql": "select id_trhor, lib_trhor from code_trhor order by id_trhor;",
	"enqfield": "trhor"
}
```

- Mode de déplacement utilisé en amont du voyage (mode avant) :
```
"lwgModAv": {
	"objlbl": "lwgModAv",
	"label": "Mode avant",
	"sql": "select id_modav, lib_modav from code_modav order by id_modav;",
	"enqfield": "modav"
}
```

- Mode de déplacement utilisé en aval du voyage (mode après) :
```
"lwgModAp": {
	"objlbl": "lwgModAp",
	"label": "Mode après",
	"sql": "select id_modap, lib_modap from code_modap order by id_modap;",
	"enqfield": "modap"
}
```

### 3.5. dctCritereAutre \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet d'ajouter les informations nécessaires pour le remplissage des listes de case à cocher pour d'autres variables.

### 3.6. dctZonage \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient les informations nécessaires pour le remplissage des listes de cases à cocher pour chacun des zonages intégrés :

- Zonage communal :
```
"lwgCommunes": {
	"table": "communes",
	"objlbl": "lblCommunes",
	"label": "Communes",
	"sql": "select id_commune, nom_commune from communes order by nom_commune;",
	"enqfield": "id_commune"
}
```

- Zonage spécifique à l'analyse de l'enquête OD :
```
"lwgZonesOD2023": {
	"table": "zones_od_2023",
	"objlbl": "lblZonesOD2023",
	"label": "Zones OD 2023",
	"sql": "select id_zoneod, nom_zoneod from zones_od_2023 order by nom_zoneod;",
	"enqfield": "id_zoneod"
}
```

### 3.7. dctVariableAnalysable \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet d'ajouter les informations nécessaires à la construction de l'interface utilisateur pour les variables qui peuvent faire l'objet d'une analyse croisée.

Par exemple, pour une variable enquêtée ***Variable***, la rubrique json précisera :

- "tabVar": "***code_variable***" -> le nom de la table contenant les couples clé-valeur pour la variable
- "title": "***Description de la variable***" -> le libellé utilisé pour la variable dans la légende des analyses croisées
- "colVar": "***variable***" -> le nom de la colonne de la table ***enquetes*** contenant les code pour la variable
- "colIdVar": "***id_variable***" -> le nom de la colonne de la table de codage de la variable contenant les clés
- "colLibVar": "***lib_variable***" -> le nom de la colonne de la table de codage de la variable contenant les valeurs

- Motif de déplacement (origine et destination agrégées) :
```
"motifod": {
	"tabVar": "code_motif",
	"title": "Motifs agrégés",
	"colVar": "motifod",
	"colIdVar": "id_motif",
	"colLibVar": "lib_motif"
}
```

- Titre de transport utilisé par l'usager :
```
"titre": {
	"tabVar": "code_titre",
	"title": "Titres de transport",
	"colVar": "titre",
	"colIdVar": "id_titre",
	"colLibVar": "lib_titre"
}
```

- Statut socio-professionnel de l'usager :
```
"statut": {
	"tabVar": "code_statut",
	"title": "Statut",
	"colVar": "statut",
	"colIdVar": "id_statut",
	"colLibVar": "lib_statut"
}
```

- Tranche horaire du voyage :
```
"trhor": {
	"tabVar": "code_trhor",
	"title": "Tranches horaires",
	"colVar": "trhor",
	"colIdVar": "id_trhor",
	"colLibVar": "lib_trhor"
}
```

- Mode de déplacement utilisé en amont du voyage (mode avant) :
```
"modav": {
	"tabVar": "code_modav",
	"title": "Mode avant",
	"colVar": "modav",
	"colIdVar": "id_modav",
	"colLibVar": "lib_modav"
}
```

- Mode de déplacement utilisé en aval du voyage (mode après) :
```
"modap": {
	"tabVar": "code_modap",
	"title": "Mode après",
	"colVar": "modap",
	"colIdVar": "id_modap",
	"colLibVar": "lib_modap"
}
```

En plus des variables enquêtées analysables des analyses croisées peuvent être faite pour la ligne enquêtée, la première ligne amont du voyage, et la première ligne aval du voyage.

- Ligne enquêtée :
```
"ligne":  {
	"tabVar": "code_ligne_var",
	"title": "Lignes",
	"colVar": "ligne",
	"colIdVar": "id_ligne",
	"colLibVar": "lib_ligne"
}
```

- Première ligne empruntée en amont du voyage :
```
"lignem1":  {
	"tabVar": "code_ligne_coram",
	"title": "Lignes amont",
	"colVar": "lignem1",
	"colIdVar": "id_ligne",
	"colLibVar": "lib_ligne"
}
```

- Première ligne empruntée en aval du voyage :
```
"lignep1":  {
	"tabVar": "code_ligne_corav",
	"title": "Lignes aval",
	"colVar": "lignep1",
	"colIdVar": "id_ligne",
	"colLibVar": "lib_ligne"
}
```

### 3.8. dctZonageAnalysable \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet d'ajouter les informations nécessaires à la construction de l'interface utilisateur pour les zonages qui peuvent faire l'objet d'une analyse cartographique.

Par exemple, pour une variable enquêtée ***Variable***, la rubrique json précisera :

- "tabVar": "***code_variable***" -> le nom de la table contenant les couples clé-valeur pour la variable
- "title": "***Description de la variable***" -> le libellé utilisé pour la variable dans la légende des analyses croisées
- "colVar": "***variable***" -> le nom de la colonne de la table ***enquetes*** contenant les code pour la variable
- "colIdVar": "***id_variable***" -> le nom de la colonne de la table de codage de la variable contenant les clés
- "colLibVar": "***lib_variable***" -> le nom de la colonne de la table de codage de la variable contenant les valeurs

- Zonage communal :
```
"communes": {
	"tabZone": "communes",
	"title": "Communes",
	"colIdZone": "id_commune",
	"colNomZone": "nom_commune"
}
```

- Zonage spécifique à l'analyse de l'enquête OD :
```
"zones_od_2023": {
	"tabZone": "zones_od_2023",
	"title": "Zones OD 2023",
	"colIdZone": "id_zoneod",
	"colNomZone": "nom_zoneod"
}
```

### 3.9. dctParam \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique contient des informations de paramétrage de l'application.

- Système de coordonnées et de projection géographiques utilisé pour les couches géographiques :
```
"epsg": 2154,
```

- Nom du réseau affiché dans la liste des couches de QGIS :
```
"nomreseau": "Réseau RANDOM 2023",
```

- Nombre total de lignes enquêtées
```
"nblignes": 6,
```

- Noms des colonnes de la table ***enquetes*** :
```
"typlign": "typlign",
"typjour": "typjour",
"ligne": "ligne",
"sens": "sens",
"parcours": "parcours",
"course": "course",
"poidsligne": "poidsl",
"poidsreseau": "poidsr",
"arretdebvoy": "arreta2",
"arretfinvoy": "arretb2",
"arretdebdep": "arretdeb2",
"arretfindep": "arretfin2",
"ligneamont": "lignem1",
"ligneaval": "lignep1",
```

- Ligne sélectionnée par défaut pour le filtrage des enquêtes :
```
"defaultligne": "1001",
```

- Possibilité d'affichage du sous-réseau avec les couleurs de lignes utilisées pour les document commerciaux :
```
"couleurslignes": "True",
```

- Nom de la table de codage des tranches horaires :
```
"tabtrhor": "code_trhor"
```


### 3.10. lstColors \:
[@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cette rubrique permet de préciser la liste des couleurs utilisées pour les secteurs dans les analyses par diagrammes.

```
"lstColors": [
	[232,92,99]
	[R, V, B],
	...
]
```

## Annexe 1 \: Liste des fichiers de QMapOD \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

* **Dossier /QMapOD \:**  
\__init__.py  
metadata.txt  
plugin_upload.py  
pylintrc  
QMapOD.ini  
qmapod.py  
QMapOD.qgs  
qmapod\_anal_arrets.py  
qmapod\_anal_arrets_var.py  
qmapod\_anal_serpent.py  
qmapod\_anal_zones.py  
qmapod\_anal_zones_var.py  
qmapod\_config.py  
qmapod\_dialog.py  
qmapod\_dialog_base.py  
qmapod\_dialog_base.ui  
qmapod_dock.py  
qmapod\_dock_base.py  
qmapod\_dock_base.ui  
qmapod_filtrage.py  
qmapod\_flux_arrets.py  
qmapod\_flux_zones.py  
qmapod\_sous_reseau.py  
QMapODDefaut.ini  
resources.qrc  
resources_rc.py  

* **Dossier /QMapOD/data \:**  
qmapod_xxx.sqlite  
qmapod_xxx.json  

* **Dossier /QMapOD/doc \:**  
aide_qmapod.html  
aide_qmapod.pdf  
stackedit.css  

* **Dossier /QMapOD/doc/fonts \:**  
cursive_standard-webfont.woff  
dir.txt  
fontello.eot  
fontello.svg  
fontello.ttf  
fontello.woff  
fontface-fontello.css.ejs  
glyphicons-halflings-regular.eot  
glyphicons-halflings-regular.svg  
glyphicons-halflings-regular.ttf  
glyphicons-halflings-regular.woff  
glyphicons-halflings-regular.woff2  
PTSans-BoldItalic-webfont.woff  
PTSans-Bold-webfont.woff  
PTSans-Italic-webfont.woff  
PTSans-Regular-webfont.woff  
SourceCodePro-Bold-webfont.woff  
SourceCodePro-Regular-webfont.woff  
SourceSansPro-BoldItalic-webfont.woff  
SourceSansPro-Bold-webfont.woff  
SourceSansPro-Italic-webfont.woff  
SourceSansPro-LightItalic-webfont.woff  
SourceSansPro-Light-webfont.woff  
SourceSansPro-Regular-webfont.woff  

* **Dossier /QMapOD/doc/img \:**  
barre_outils.png  
boutons\_filtrage_carto.png  
boutons\_filtrage_enquetes.png  
boutons\_parametres.png  
btn_aide.png  
btn_apropos.png  
btn_couches.png  
btn\_d_arrets.png  
btn\_d_zones.png  
btn\_dvar_arrets.png  
btn\_dvar_zones.png  
btn\_eff_flux_arrets.png  
btn\_eff_flux_zones.png  
btn\_m_arrets.png  
btn\_m_zones.png  
btn\_md_arrets.png  
btn\_md_zones.png  
btn\_mvar_arrets.pn  g
btn\_mvar_zones.png  
btn_panneau.png  
btn_parametres.png
btn_qconsolidate.png  
btn\_serp_charge.png  
btn\_serp_offre.png  
btn\_serp_perf.png  
diagrammes_etiquettes.png  
dlg_qconsolidate.png  
enquetes_mode_analyse.png  
enquetes_voyage_deplacement.png  
filtrage.png  
lst_variables.png  
lst_zonages.png  
menu.png  
onglet_arrets.png  
onglet_flux.png  
onglet_troncons.png  
onglet_zonages.png  
outils.png  
param_arrets.png  
param\_flux_arrets.png  
param\_flux_zones.png  
param\_flux_zones_degrade.png  
param\_flux_zones_diagram.png  
param\_flux_zones_oursins.png  
param\_sous_reseau.png  
param_troncons.png  
param_zonages.png  
qmapod.png  
rub_arrets.png  
rub_autres.png  
rub_recap.png  
rub_signaletique.png  
rub\_sous_reseau.png  
rub_voyage.png  
rub_zonages.png  
sous\_reseau_diagrammes_etiquettes.png  
tbn\_flux_arrets_d.png  
tbn\_flux_arrets_m.png  
tbn\_flux_zones_d.png  
tbn\_flux_zones_m.png  

* **Dossier /QMapOD/icons \:**  
aide.svg  
apropos.svg  
bhns.svg  
bus.svg  
bus_bleu.svg  
bus_jaune.svg  
bus_mauve.svg  
bus_orange.svg  
bussub.svg  
busurb.svg  
busway.svg  
chronobus.svg
config.svg  
d_arret.svg  
d_zone.svg  
dvar_arret.svg  
dvar_zone.svg  
fleche_circulaire.svg  
fleche_droite.svg  
fleche_gauche.svg  
flux_arret_dm.svg  
flux_arret_md.svg  
flux_zone_d.svg  
flux_zone_dm.svg  
flux_zone_m.svg  
flux_zone_md.svg  
layers.svg  
m_arret.svg  
m_zone.svg  
mapod.svg  
md_arret.svg  
md_arret_del.svg  
md_zone.svg  
md_zone_del.svg  
mdvar_arret_del.svg  
mdvar_zone_del.svg  
metro.svg  
mvar_arret.svg  
mvar_zone.svg  
navibus.svg  
qmapod.svg  
reseaux.svg  
s_charge.svg  
s_del.svg  
s_offre.svg  
s_perf.svg  
sous_reseau.svg  
tram.svg  
tramway.svg  
