# Financer

Please consider to contribute to its development by funding a feature or sponsoring the project:

<!-- markdownlint-disable MD034 -->

```{button-link} mailto:qgis@oslandia.com
:color: info
:expand:
:tooltip: qgis@oslandia.com
Contact us by email {material-regular}`mail;1.5em`
```

## Sponsors

:::::{grid} 3

::::{grid-item-card} Oslandia
:img-top: /static/logo_oslandia.png
:link: https://oslandia.com/

* Hosts core developer and main maintainers
::::

::::{grid-item-card} SIGéal
:img-top: /static/logo_sigeal.png
:link: https://www.sigeal.com

* Conception and initial development
* SIGéal ceased operations in January 2024
::::

::::{grid-item-card} TEST-SA
:img-top: /static/logo_test-sa.png
:link: https://www.test-sa.com

* Idea and initial financing of development
::::

:::::
