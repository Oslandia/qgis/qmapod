![QMapOD](../static/qmapod2.png)
# **Consultation cartographique d'enquêtes origine destination sous QGIS 3.x**

## 1. Objet de l’application \: [@icon-home ](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars ](#table-des-matières "Table des matières")

**QMapOD** est la version QGIS de l'application MapOD, initialement développée sous Access et ArcGis / MapInfo.  
Elle s'appuie sur le système d'information géographique QGIS et la base de données spatiale SQLite / SpatiaLite. La version 2 de QMapOD apporte la compatibilité avec QGIS 3.x.  
Elle se présente sous la forme d'une extension (plugin) QGIS qui permet de cartographier les résultats d'enquêtes origine / destination réalisées sur un réseau de transport en commun, en réalisant des filtrages multicritères sur un jeu de données d’enquêtes, puis en élaborant des analyses cartographiques à partir des données filtrées.

> Le développement initial de QMapOD a été réalisé par [SIGéal](https://www.sigeal.com), et financé par [Test-SA](https://www.test-sa.com).  
> QMapOD est à présent publié [sur GitLab](https://gitlab.com/Oslandia/qgis/qmapod/-/blob/master/README.md?ref_type=heads), sous licence GPL, et principalement maintenu par [Oslandia](https://www.oslandia.com).

QGIS et QMapOD sont diffusés sous licence Open Source (GNU General Public License).

* **Fonctionnalités standard \:**
	* Filtrage par lignes/sens
	* Filtrage par critères signalétiques (catégorie socio-professionnelle, tranche d'âge, sexe, pmr)
	* Filtrage par critères voyage (titre, fréquence, motif, modes amont/aval, tranche horaire)
	* Filtrage cartographique par communes, par zones, par arrêts
	* Affichage des montées/descentes par arrêts
	* Affichage des montées/descentes par variable par arrêts
	* Affichage des montée/descentes par communes, par zones
	* Affichage des montées/descentes par variable par communes/zones
	* Affichage des serpents d'offre, de charge et de performance
	* Affichage des correspondances amont/aval par arrêts
	* Affichage des flux par zones (dégradé, oursins, symboles proportionnels)
	* Affichage des flux par arrêts

* **Fonctionnalités optionnelles \:**
	* Filtrage par type de jour
	* Filtrage par courses
	* Affichage des serpents de charges avec répartition avec/sans correspondance
	* Affichage des serpents de charges avec correspondances amont/aval
	* Affichage des flux par zones avec répartition selon critères voyage et -signalétique (camembert)
	* Affichage des flux principaux (seuil de sélection, flèches intra-zone, flèches inter-zones)

> Les fonctionnalités disponibles peuvent varier selon les variables enquêtées.

## 2. Architecture de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
QMapOD est une extension QGIS développée en python 3.9, intégrant les fonctionnalités de filtrage et les fonctionnalités d'analyse cartographique.
Elle peut fonctionner dans tous les environnements supportés nativement par QGIS \:

* Windows (32 et 64 bits),
* Mac OS X,
* Linux,
* BSD.

## 3. Installation de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
L'extension QMapOD peut-être lancée depuis une installation QGIS existante (***recommandé***), ou  bien depuis une version autonome de QGIS pré-configurée (sous Windows uniquement).

Dans le premier cas, pour installer QGIS, il convient d'installer l'extension à partir du fichier .zip \:
![Barre d'outils](../static/dlg_install.png "Barre d'outils")

Dans le second cas, pour installer QGIS, il faut copier le dossier contenant l'installation de QGIS – et donc de QMapOD – sur l'un des disques de la machine.
Pour mettre à jour l'extension QMapOD dans ce second cas, il convient de remplacer le dossier QMapOD situé dans le sous-dossier suivant \: `\QGIS\apps\qgis-ltr\python\plugins`

> Bien qu'il soit théoriquement possible de lancer QGIS et QMapOD depuis un CDROM ou une clé USB, ou un disque réseau, les performances et le confort d'utilisation risquent de s'en ressentir fortement.

## 4. Lancement de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Après le lancement de QGIS, QMapOD doit être activé de la façon suivante \:  
Menu Extension > Gérer les extensions, taper QMapOD dans la case de recherche de la boite de dialogue de gestion des extensions, puis cocher la case QMapOD.  
Si l'extension QMapOD n’apparaît pas après avoir tapé « QMapOD » dans la case de rechercher, vérifier l'installation de l'extension.  
Pour que l'extension QMapOD soit visible, l'option « Afficher les extensions expérimentales » doit être cochée dans l'onglet « Paramètres » du gestionnaire d'extensions.  
Une fois l'extension activée les fonctionnalités générales de QMapOD sont accessibles depuis une barre d'outil \:

![Barre d'outils](../static/barre_outils.png "Barre d'outils")

Elle sont également accessibles depuis le menu Extensions > QMapOD 2 \:

![Menu](../static/menu.png "Menu extensions")

## 5. Présentation de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Chaque bouton/menu donne accès aux fonctionnalités suivantes \:

![Panneau](../static/btn_aide.png "Panneau") \: Affichage du panneau ancrable QMapOD  
![Chargement](../static/btn_couches.png "Chargement") \: Chargement des couches géographiques  
![Paramètres](../static/btn_parametres.png "Paramètres") \: Activation de l'onglet Paramètres du panneau QMapOD  
![Aide](../static/btn_aide.png "Aide") \: Affichage de l'aide en ligne dans le navigateur par défaut  
![À propos](../static/btn_apropos.png "À propos") \: Affichage du dialogue À propos de QMapOD  

Les fonctionnalités de QMapOD sont regroupées dans un panneau ancrable, positionné à droite par défaut, comprenant deux panneaux à onglets \:

- Le panneau à onglets supérieur regroupe les fonctionnalités de filtrage, de paramétrage de l'application et les outils complémentaires \:  
![Panneau supérieur](../static/filtrage.png "Panneau supérieur")

- Le panneau à onglets inférieur regroupe les outils permettant d'interagir avec la carte \:  
![Panneau inférieur](../static/outils.png "Panneau inférieur")

Le panneau ancrable contient également dans sa partie basse un bouton accessible à tout moment permettant d'afficher le sous-réseau courant (lignes/sens sélectionnés) \:  
![Sous-réseau](../static/sous_reseau.png "Sous-réseau")
