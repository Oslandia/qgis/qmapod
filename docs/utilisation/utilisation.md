# **Consultation cartographique d'enquêtes origine destination sous QGIS 3.x**

![QMapOD](../static/qmapod2.png)

## 1. Description des fonctionnalités de l'application \:
### 1.1. Panneau à onglets supérieur \:
#### 1.1.1. Onglet de filtrage des enquêtes \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet permet de spécifier les critères de filtrage des enquêtes. Le bouton de validation du filtrage a pour effet la création d'un sous-ensemble d'enquêtes (table *enquetes_tmp*) correspondant aux critères spécifiés. Les analyses cartographiques réalisées s'appuient sur cette table temporaire.

> La validation des critères de filtrage des enquêtes est ***dynamique***, c'est à dire que les éventuelles analyses cartographiques existantes lors de la validation sont automatiquement mises à jour conformément aux critères spécifiés.

L'onglet de filtrage des enquêtes comprend trois rubriques escamotables (accordéon) et une ligne de boutons.  
Dans la vue arborescente du réseau, les catégories de lignes et les lignes peuvent être dépliées/repliées en cliquant sur l'icône >.  
Dans les listes de cases à cocher, il est possible de cocher simultanément plusieurs options en sélectionnant plusieurs lignes, avec les touches MAJ et CTRL du clavier, puis en cochant l'une des cases sélectionnées. Il est également possible de cocher ou de décocher toutes les cases en cochant/décochant celle qui est située à côté du titre de la liste.

- La rubrique sous-réseau contient le cas échéant une liste déroulante permettant de spécifier le type de jour enquêté (jour de semaine (JOB), samedi, dimanche), ainsi qu'une vue arborescente du réseau permettant de cocher/décocher les catégories de lignes, les lignes et les lignes/sens à prendre en compte \:  
![Sous-réseau](../static/rub_sous_reseau.png "Sous-réseau")

	> Lorsque plusieurs lignes/sens sont cochés, il convient de s'assurer que les sens de circulation sont cohérents pour éviter un calcul de serpent de charge erroné.

	> Si des tranches horaires différentes sont définies selon le type de jour, la sélection d'un type de jour dans la liste déroulante a pour effet la mise à jour des tranches horaires dans la rubrique Critères Voyage.

- La rubrique critères signalétique contient une liste de cases à cocher pour chaque critère enquêté décrivant la signalétique de l'usager enquêté \: statut socio-professionnel, titre utilisé, tranche d'âge, commune de résidence, pmr, etc. \:  
![Signalétique](../static/rub_signaletique.png "Signalétique")

- La rubrique critères voyage contient une liste de cases à cocher pour chaque critère enquêté décrivant le voyage \: tranche horaire, motif de déplacement, mode amont, mode aval, fréquence d'utilisation, etc. \:  
![Voyage](../static/rub_voyage.png "Voyage")
	> Lorsque toutes les tranches horaires ne sont pas cochées, les fonctionnalités d'affichage des serpents d'offre et de performance sont désactivées.

- La ligne de boutons comprend \: un bouton de validation du filtrage des enquêtes et un bouton de réinitialisation du formulaire \:  
![Boutons](../static/boutons_filtrage_enquetes.png "Boutons")

#### 1.1.2. Onglet de filtrage cartographique \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet permet de spécifier des critères de filtrage cartographique. Le bouton de validation du filtrage a pour effet la spécification d'un filtre d'entités (requête) au niveau des couches utilisées pour les analyses cartographique.

> Le filtrage par zones s'applique également aux arrêts, c'est à dire que les arrêts inclus dans les zones exclues sont également exclus. En revanche, le filtrage cartographique par zonage est appliquée indépendamment des autres zonages.

L'onglet de filtrage cartographique comprend deux rubriques escamotables (accordéon) et une ligne de boutons.

- La rubrique arrêts contient une liste de cases à cocher pour chaque arrêt logique du réseau \:  
![Arrêts](../static/rub_arrets.png "Arrêts")  

> Il est possible d'accéder directement à un arrêt particulier en tapant au clavier les premières lettres de son nom.

- La rubrique zonages contient une liste de cases à cocher pour chaque zonage intégré à l'application \: zonage communal, zonage O/D, etc. \:  
![Zonages](../static/rub_zonages.png "Zonages")  

> Il est possible d'accéder directement à une zone particulière en tapant au clavier les premières lettres de son nom.

- La ligne de boutons comprend \: un bouton de validation du filtrage cartographique et un bouton de réinitialisation du formulaire \:  
![Boutons](../static/boutons_filtrage_carto.png "Boutons")  

#### 1.1.3. Onglet des paramètres de l’application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet permet de régler les paramètres de l'application. Ces paramètres concernent le mode d'affichage et les caractéristiques des analyses cartographiques. Les rubriques « Arrêts », « Zones », « Tronçons », «Flux par zones » et « Flux par arrêts » correspondent aux onglets contenant les outils d'interaction cartographique. Tous les paramètres spécifiés dans cet onglet sont conservés d'une session à une autre.

> La validation des paramètres est ***dynamique***. Les paramètres sélectionnés sont appliqués aux éventuelles analyses cartographiques existantes lorsque l'on clique sur le bouton de validation.

Il comprend six rubriques escamotables (accordéon) et une ligne de boutons \:

* La rubrique ***sous-réseau*** contient les paramètres relatifs à la visualisation cartographique du sous-réseau. Si la table code_ligne contient une colonne « rvb », contenant la couleur à utiliser pour chaque ligne, il est possible de choisir le type de représentation des lignes \:  
	![Paramètres sous-réseau](../static/param_sous_reseau_coul.png "Paramètres sous-réseau")

	Dans le cas contraire, seuls les paramètres par défaut sont accessibles \:  
	![Paramètres sous-réseau](../static/param_sous_reseau.png "Paramètres sous-réseau")

* La rubrique ***arrêts*** contient les paramètres relatifs aux analyses cartographiques sur les arrêts \:  

	![Paramètres arrêts](../static/param_arrets.png "Paramètres arrêts")

* La rubrique ***zones*** contient les paramètres relatifs aux analyses cartographiques sur les zonages \:  

	![Paramètres zonages](../static/param_zonages.png "Paramètres zonages")

* La rubrique ***tronçons*** contient les paramètres relatifs aux analyses cartographiques sur les tronçons \:  

	![Paramètres tronçons](../static/param_troncons.png "Paramètres tronçons")

* La rubrique ***flux principaux*** contient les paramètres relatifs aux analyses cartographiques des flux principaux et des flux principaux par ligne \:

    ![Paramètres tronçons](../static/param_flux_principaux.png "Paramètres tronçons")

* La rubrique ***flux par zones*** contient les paramètres relatifs aux analyses cartographiques de flux par zones \:

	Le choix du type de représentation dans cette rubrique conditionne les paramètres d'affichage et d'analyse disponibles.  

	![Paramètres flux zones](../static/param_flux_zones.png "Paramètres flux zones")

	> L'option ***dégradé de couleurs***, présente pour des raisons historiques, n'est pas recommandée car elle n'est pas optimale pour la représentation visuelle des variables quantitatives.

	Paramètres disponibles lorsque la représentation par ***dégradé de couleurs*** est sélectionnée \:  

	![Paramètres flux zones](../static/param_flux_zones_degrade.png "Paramètres flux zones")

	Paramètres disponibles lorsque la représentation par ***oursins*** est sélectionnée \:  

	![Paramètres flux zones](../static/param_flux_zones_oursins.png "Paramètres flux zones")

	Paramètres disponibles lorsque la représentation par ***diagrammes proportionnels*** est sélectionnée \:  

	![Paramètres flux zones](../static/param_flux_zones_diagram.png "Paramètres flux zones")

- La rubrique ***flux par arrêts*** contient les paramètres relatifs aux analyses cartographiques de flux par arrêts \:  

	![Paramètres flux arrêts](../static/param_flux_arrets.png "Paramètres flux arrêts")

- La ligne de boutons comprend \: un bouton de validation des paramètres d'affichage et d'analyse et un bouton de chargement des paramètres par défaut \:  
![Boutons](../static/boutons_parametres.png "Boutons")
#### 1.1.4. Onglet des outils de l'application \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Cet onglet contient une case à cocher permettant d'activer ou de désactiver l'affichage des diagrammes et des étiquettes superposés.  
Cet onglet comprend aussi une zone de texte dans laquelle est affiché un récapitulatif des paramètres d'affichage, des critères de filtrage des enquêtes et des critères de filtrage cartographique courants. Le contenu de cette zone de texte  peut être utilisé pour avoir une vue d'ensemble des critères sélectionnés, ou pour copier/coller le récapitulatif des critères sélectionnés dans une mise en page destinée à l'impression.

![Récapitulatif](../static/rub_recap.png "Récapitulatif")

### 1.2. Panneau à onglets inférieur \:
#### 1.2.1. Onglet d'analyses cartographiques sur les arrêts \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses sur les arrêts, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:

![Arrêts](../static/onglet_arrets.png "Arrêts")

Le fonctionnement des boutons de cet onglet est décrit ci-après \:

* ![Montées / descentes par arrêts](../static/btn_md_arrets.png "Montées / descentes par arrêts") **Bouton montées / descentes par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont les secteurs représentent respectivement les montées et les descentes à cet arrêt. La taille du diagramme est proportionnelle à la somme des montées et des descentes à cet arrêt.

* ![Montées par arrêts](../static/btn_m_arrets.png "Montées par arrêts") **Bouton montées par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des montées à cet arrêt.

* ![Descentes par arrêts](../static/btn_d_arrets.png "Descentes par arrêts") **Bouton Descentes par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des descentes à cet arrêt.

* ![Montées par sens par arrêts](../static/btn_ms_arrets.png "Montées par sens par arrêts") **Bouton montées par sens par arrêts \:**
	Ce bouton permet d’afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont les secteurs représentent respectivement les montées dans le sens 1 et le sens 2 à cet arrêt. La taille du diagramme est proportionnelle à la somme des montées dans les deux sens à cet arrêt. Cette analyse est conçue pour visualiser les déséquilibres de montées entre le sens aller et le sens retour, tels qu’ils apparaissent dans la liste des lignes / sens (rubrique sous-réseau). Si un seul sens est sélectionné, elle donne le même résultat que les montées par arrêts. Si plusieurs lignes sont sélectionnées, elle fonctionne mais ses résultats peuvent être difficiles à interpréter.

* ![Descentes par sens par arrêts](../static/btn_ds_arrets.png "Descentes par sens par arrêts") **Bouton Descentes par sens par arrêts \:**
		Ce bouton permet d’afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont les secteurs représentent respectivement les descentes dans le sens 1 et le sens 2 à cet arrêt. La taille du diagramme est proportionnelle à la somme des descentes dans les deux sens à cet arrêt. Cette analyse est conçue pour visualiser les déséquilibres de descentes entre le sens aller et le sens retour, tels qu’ils apparaissent dans la liste des lignes / sens (rubrique sous-réseau). Si un seul sens est sélectionné, elle donne le même résultat que les descentes par arrêts. Si plusieurs lignes sont sélectionnées, elle fonctionne mais ses résultats peuvent être difficiles à interpréter.

* ![Montées par variable par arrêts](../static/btn_mvar_arrets.png "Montées par variable par arrêts") **Bouton montées par variable par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des montées à cet arrêt et dont les secteurs correspondent à la répartition des montées pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](../static/lst_variables.png "Liste des variables").
		Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux montées sur chaque ligne.
	* L'option « **Lignes amont** » pour afficher les lignes de correspondance amont sous la forme de camemberts dont les secteurs sont proportionnels aux montées en provenance de chaque ligne de correspondance amont.
	* Le bouton montées par variable par arrêts est désactivé lorsque l'option « **Lignes aval** » est choisie car cette combinaison n'est pas pertinente.

	> Le changement de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des variables.

* ![Descentes par variable par arrêts](../static/btn_dvar_arrets.png "Descentes par variable par arrêts") **Bouton descentes par variable par arrêts \:**
		Ce bouton permet d'afficher sur chaque arrêt du sous-réseau sélectionné un diagramme de type camembert dont la taille est proportionnelle à la somme des descentes à cet arrêt et dont les secteurs correspondent à la répartition des descentes pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](../static/lst_variables.png "Liste des variables").
		Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux descentes sur chaque ligne.
	* Le bouton descentes par variable par arrêts est désactivé lorsque l'option « **Lignes amont** » est choisie car cette combinaison n'est pas pertinente.
	* L'option « **Lignes aval** » pour afficher les lignes de correspondance aval sous la forme de camemberts dont les secteurs sont proportionnels aux descentes à destination de chaque ligne de correspondance aval.

	> Le changement de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des variables.

* ![Effacer analyses par arrêts](../static/btn_md_arrets_del.png "Effacer analyses par arrêts") **Bouton effacer les analyses cartographiques par arrêts \:**
		Ce bouton permet de supprimer toutes les analyses cartographiques par arrêts.

#### 1.2.2. Onglet d'analyses cartographiques sur les zonages \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses sur les zonages, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:

![Zonages](../static/onglet_zonages.png "Zonages")

Le fonctionnement des boutons de cet onglet est décrit ci-après \:

* ![Montées / descentes par zones](../static/btn_md_zones.png "Montées / descentes par zones") **Bouton montées / descentes par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camembert dont les secteurs représentent respectivement les montées et les descentes dans cette zone. La taille du diagramme est proportionnelle à la somme des montées et des descentes dans cette zone.

* ![Montées par zones](../static/btn_m_zones.png "Montées par zones") **Bouton montées par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camemberts dont la taille est proportionnelle à la somme des montées dans cette zone.

* ![Descentes par zones](../static/btn_d_zones.png "Descentes par zones") **Bouton Descentes par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camemberts dont la taille est proportionnelle à la somme des descentes dans cette zone.

* ![Montées par variable par zones](../static/btn_mvar_zones.png "Montées par variable par zones") **Bouton montées par variable par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camembert dont la taille est proportionnelle à la somme des montées dans cette zone et dont les secteurs correspondent à la répartition des montées pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](../static/lst_variables.png "Liste des variables").
Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (dans les troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux montées sur chaque ligne dans la zone.
	* L'option « **Lignes amont** » pour afficher les lignes de correspondance amont sous la forme de camemberts dont les secteurs sont proportionnels aux montées en provenance de chaque ligne de correspondance amont dans la zone.
	* Le bouton montées par variable par zones est désactivé lorsque l'option « **Lignes aval** » est choisie car cette combinaison n'est pas pertinente.

> Le changement de zonage ou de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des zonages ou dans la liste déroulante des variables.

* ![Descentes par variable par zones](../static/btn_dvar_zones.png "Descentes par variable par zones") **Bouton descentes par variable par zones \:**
		Ce bouton permet d'afficher, sur chaque zone du zonage sélectionné dans la liste ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones"), un diagramme de type camembert dont la taille est proportionnelle à la somme des descentes dans cette zone et dont les secteurs correspondent à la répartition des descentes pour chaque valeur de la variable sélectionnée dans la liste déroulante des variables enquêtées ![Liste des variables](../static/lst_variables.png "Liste des variables").
		Outre les variables enquêtées, cette liste permet de sélectionner trois options supplémentaires \:
	* L'option « **Lignes** » pour afficher les montées par ligne (dans les troncs communs) sous la forme de camemberts dont les secteurs sont proportionnels aux descentes sur chaque ligne dans la zone.
	* Le bouton montées par variable par zones est désactivé lorsque l'option « **Lignes amont** » est choisie car cette combinaison n'est pas pertinente.
	* L'option « **Lignes aval** » pour afficher les lignes de correspondance aval sous la forme de camemberts dont les secteurs sont proportionnels aux descentes à destination de chaque ligne de correspondance aval dans la zone.

> Le changement de zonage ou de variable analysée est ***dynamique***. Les éventuelles analyses cartographiques existantes sont donc automatiquement mises à jour lorsque l'on sélectionne une entrée dans la liste déroulante des zonages ou dans la liste déroulante des variables.

* ![Effacer analyses par zones](../static/btn_md_zones_del.png "Effacer analyses par zones") **Bouton effacer les analyses cartographiques par zones \:**
		Ce bouton permet de supprimer toutes les analyses cartographiques par zones.

#### 1.2.3. Onglet d'analyses cartographiques sur les tronçons \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses sur les tronçons, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:  

![Tronçons](../static/onglet_troncons.png "Tronçons")

Le fonctionnement des boutons de cet onglet est décrit ci-après \:

* ![Serpent d'offre](../static/btn_serp_offre.png "Serpent d'offre") **Bouton serpent d'offre \:**
		Ce bouton permet d'afficher le serpent d'offre du sous-réseau sélectionné. Chaque tronçon est affiché avec une épaisseur correspondant à sa classe d'offre exprimée **en nombre de courses**.
		> Si la table des courses existe dans la base de données, le serpent d'offre tient compte de l'éventuel filtrage par tranche horaire. Dans le cas contraire, lorsque toutes les tranches horaires ne sont pas cochées, la fonctionnalité d'affichage du serpent d'offre est désactivée.

* ![Serpent de charge](../static/btn_serp_charge.png "Serpent de charge") **Bouton serpent de charge \:**
		Ce bouton permet d'afficher le serpent de charge du sous-réseau sélectionné. Chaque tronçon est affiché avec une épaisseur correspondant à sa classe de charge exprimée **en nombre de passagers**.

* ![Serpent de performance](../static/btn_serp_perf.png "Serpent de performance") **Bouton serpent de performance \:**
		Ce bouton permet d'afficher le serpent de performance du sous-réseau sélectionné. Chaque tronçon est affiché avec une épaisseur correspondant à sa classe de performance exprimée **en nombre de passagers rapporté au nombre de courses**.
			> Si la table des courses existe dans la base de données, le serpent de performance tient compte de l'éventuel filtrage par tranche horaire. Dans le cas contraire, lorsque toutes les tranches horaires ne sont pas cochées, la fonctionnalité d'affichage du serpent de performance est désactivée.

* ![Effacer analyses par tronçons](../static/btn_serp_del.png "Effacer analyses par tronçons") **Bouton effacer les analyses cartographiques par tronçons \:**
		Ce bouton permet de supprimer toutes les analyses cartographiques par tronçons.

#### 1.2.4. Onglet d'analyses cartographiques de flux \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Cet onglet contient les boutons et les outils permettant d'afficher sur la carte des analyses de flux sur les zonages et sur les arrêts, en tenant compte des critères de filtrage d'enquêtes, des critères de filtrage cartographiques et des paramètres de visualisation sélectionnés \:  

![Flux](../static/onglet_fluxpp.png "Flux")

>Les visualisations de flux sont affichées conformément au type de représentation spécifié dans les paramètres (rubrique flux par zones).

Le fonctionnement des outils et des boutons de cet onglet est décrit ci-après \:

- ![Flux principaux](../static/btn_fluxpp.png "Flux principaux") **Bouton flux principaux \:**
		Ce bouton permet d'afficher, sous forme de flèches proportionnelles, les N principaux flux de zone à zone, pour le zonage sélectionné dans la liste zonage ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones") et pour le sous ensemble d'enquêtes courant. Le nombre de flux affichés peut-être ajusté au moyen de la boite de saisie incrémentale ![Nb flux principaux](../static/spx_nb_fluxpp.png "Nb flux principaux")située à droite de ce bouton.

- ![Flux principaux par ligne](../static/btn_fluxppl.png "Flux principaux par ligne") **Bouton flux principaux par ligne \:**
		Ce bouton permet d'afficher, sous forme de flèches proportionnelles, les N principaux flux de zone à zone, distingués par sens, pour le zonage sélectionné dans la liste zonage ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones") et pour le sous ensemble d'enquêtes courant. Le nombre de flux affichés peut-être ajusté au moyen de la boite de saisie incrémentale ![Nb flux principaux](../static/spx_nb_fluxpp.png "Nb flux principaux")située à droite de ce bouton. Cette fonctionnalité est conçue pour fonctionner sur la base d'un filtrage sur une seule ligne, afin que la distinction par sens soit cohérente.

> Pour des raisons de lisibilité graphique, les flux réflexifs (d'une zone vers cette même zone) ne sont pas affichés par ces fonctionnalités. Ils sont cependant visibles dans la table attributaire de la couche de visualisation.

- ![Flux par zone(s) de montée](../static/tbn_flux_zones_m.png "Flux par zone(s) de montée") **Outil flux par zone(s) de montée \:**
		Cet outil permet de visualiser les descentes pour une ou plusieurs zones de montée du zonage sélectionné dans la liste zonage ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones"). Pour sélectionner la zone de montée, il convient d'activer l'outil, puis de cliquer sur la zone désirée. Pour sélectionner plusieurs zones, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les zones désirées.

* ![Flux par zone(s) de descente](../static/tbn_flux_zones_m.png "Flux par zone(s) de descente ") **Outil flux par zone(s) de descente \:**
		Cet outil permet de visualiser les montées pour une ou plusieurs zones de descente du zonage sélectionné dans la liste zonage ![Montées / descentes par zones](../static/lst_zonages.png "Montées / descentes par zones"). Pour sélectionner la zone de descente, il convient d'activer l'outil, puis de cliquer sur la zone désirée. Pour sélectionner plusieurs zones, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les zones désirées.

* ![Suppression des flux par zones](../static/btn_eff_flux_zones.png "Suppression des flux par zones") **Bouton suppression des flux par zones \:**
		Cet outil permet de supprimer les visualisations de flux par zone(s).

* ![Flux par arrêt(s) de montée](../static/tbn_flux_arrets_m.png "Flux par arrêt(s) de montée") **Outil flux par arrêt(s) de montée \:**
		Cet outil permet de visualiser les descentes pour un ou plusieurs arrêts de montée. Pour sélectionner l'arrêt de montée, il convient d'activer l'outil, puis de cliquer sur l'arrêt désiré. Pour sélectionner plusieurs arrêts, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les arrêts désirés.

* ![Flux par arrêt(s) de descente](../static/tbn_flux_arrets_m.png "Flux par arrêt(s) de descente ") **Outil flux par arrêt(s) de descente \:**
		Cet outil permet de visualiser les montées pour une ou plusieurs arrêts de descente. Pour sélectionner l'arrêt  de descente, il convient d'activer l'outil, puis de cliquer sur l'arrêt désiré. Pour sélectionner plusieurs arrêts, il suffit de maintenir la touche MAJ ou CTRL du clavier enfoncée tout en cliquant sur les arrêts désirés.

* ![Suppression des flux par zones](../static/btn_eff_flux_arrets.png "Suppression des flux par zones") **Bouton suppression des flux par arrêts \:**
		Ce bouton permet de supprimer les visualisations de flux par arrêt(s).

### 1.3. Autres fonctionnalités \:
#### 1.3.1. Impression de cartes \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")

Un projet QGIS - *QMapOD.qgs* -  contenant plusieurs modèles de mise en page est fourni avec l'application QMapOD. Situé dans le dossier d'installation de l'extension, il contient six modèles différents permettant de traiter les cas les plus courants \:

- Format A4 vertical ou portrait (A4V)
- Format A4 horizontal ou paysage (A4H)
- Format A3 vertical ou portrait (A3V)
- Format A3 horizontal ou paysage (A3H)
- Format A0 vertical ou portrait (A0V)
- Format A0 horizontal ou paysage (A0H)

Chacune de ces mises en page contient deux éléments de base permettant de construire une carte mise en page avec sa légende \:

- Un cadre carte remplissant la mise en page avec une marge de 10 mm
- Un cadre texte permettant de copier le récapitulatif du filtrage contenu dans l'onglet « **Outils** » de l'application.

> Le projet *QMapOD.qgs* peut donc être utilisé comme un modèle pour
> l'élaboration d'analyses cartographiques destinées à l'impression.

Lors de l'ouverture d'un modèle de mise en page, le cadre carte apparaît vide. Pour y faire apparaître la carte, il convient de le sélectionner, puis de cliquer sur le bouton « **Fixer sur l'emprise courante du canevas de la carte** ». Le bouton « **Déplacer le contenu de l'objet** » peut ensuite être utilisé, conjointement avec la molette et la touche CTRL, pour ajuster le cadrage de la carte.

#### 1.3.2. Export d'analyses cartographiques \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Il est possible de sauvegarder une analyse cartographique complexe en l'exportant sous la forme d'un projet autonome, déconnecté de la base de donnée de l'application dont les données temporaires sont susceptibles de changer à chaque nouveau filtrage. Pour cela, une extension de QGIS doit être utilisée \: ***QConsolidate3***.

Cette extension, fournie avec l'application QMapOD, doit-être activée par le menu Extensions >  Gérer les extensions.

Pour exporter une analyse cartographique, il convient de faire le filtrage et d'appliquer les traitements cartographiques souhaités.

Il faut ensuite lancer l'extension en cliquant sur le bouton![Extension QConsolidate3](../static/btn_qconsolidate3.png "Extension QConsolidate3") de la barre d'outils Extensions \:  
![Extension QConsolidate3](../static/dlg_qconsolidate3.png "Extension QConsolidate3")

Il convient de spécifier le dossier dans lequel la carte doit être enregistrée, dans un sous-dossier portant le nom de projet spécifié, puis de préciser quel doit être le format d'enregistrement des couches vectorielles (SHP ou GeoPackage).

L'ensemble des fichiers peut être optionnellement être compressé dans une archive .zip.

Enfin, il faut cliquer sur **Ok** pour lancer l'export. Un message de confirmation s'affiche à la fin de l'opération \:  
![Confirmation QPackage](../static/msg_qconsolidate3_confirm.png "Confirmation  QPackage")

Le projet QGIS est enregistré dans le dossier spécifié, avec une copie au format sélectionné de toutes les données qu'il référence. On peut donc le ré-ouvrir pour vérifier son bon fonctionnement.

#### 1.3.3. Export de couches cartographiques \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Il est possible de sauvegarder au format shape (.shp) ou GeoPackage (.gpkg) une couche vectorielle correspondant à une analyse cartographique réalisée avec l'application. Il suffit pour cela de cliquer avec le bouton droit sur le nom de la couche dans la liste des couches puis de sélectionner le menu « **Sauvegarder sous** ».

## 2. Rappels sur les enquêtes O/D et leur exploitation \:
### 2.1. La pondération des enquêtes \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")
Deux types de pondération sont utilisés pour le traitement des enquêtes \:

- La pondération ligne,
- la pondération réseau.

La première correspond au poids de l’enquête pour la ligne sur laquelle l’enquête a été réalisée. Elle est utilisée pour les analyses sur les voyages (montées/descentes sur la ligne enquêtée).
La seconde correspond au poids de l’enquête pour l’ensemble du réseau de transport en commun, c’est à dire qu’elle tient compte des correspondances réalisées. Elle est donc utilisée pour les analyses sur les déplacements (avec éventuellement des correspondances).
La pondération réseau n’est utilisée que dans le cas où l’ensemble du réseau est sélectionné.
Lorsque plusieurs lignes sont sélectionnées, c’est la pondération ligne qui est utilisée. Dans ce cas, de légères distorsions peuvent être constatées sur les résultats, par exemple si des enquêtes avec correspondances comprennent des lignes ne faisant pas partie du sous-réseau sélectionné (le poids de ces enquêtes peut selon le cas être un peu sur-évalué ou sous-évalué).

### 2.2. Les logiques voyage et déplacement \: [@icon-home](#consultation-cartographique-denquêtes-origine-destination-sous-qgis-3x "Début") | [@icon-bars](#table-des-matières "Table des matières")  
Un voyage correspond à un trajet réalisé sur une seule ligne de transport en commun. On travaille donc en logique voyage lorsque l'on considère les arrêts de début et de fin du trajet effectué **sur la ligne enquêtée**.

Un déplacement correspond à un trajet réalisé avec éventuellement une ou plusieurs correspondances sur le réseau de transport en commun. On travaille donc en logique déplacement lorsque l'on considère les arrêts de début (pour le premier trajet du déplacement) et de fin (pour le dernier trajet du déplacement) du déplacement effectué sur le réseau de transport en commun, c'est à dire **en incluant les correspondances en amont et en aval de la ligne enquêtée**.
Le schéma ci-après illustre les différentes composantes d'une enquête \:  

![Voyage / Déplacement](../static/enquetes_voyage_deplacement.png "Voyage / Déplacement")

Les logiques d'analyse utilisées par défaut par QMapOD sont les suivantes \:

* **Logique voyage \:**
	* Affichage des montées/descentes par arrêts (une ou plusieurs lignes)
	* Affichage d'une variable par arrêts (une ou plusieurs lignes)
	* Affichage des serpents de charge et de performance

* **Logique déplacement \:**
	* Affichage d'une variable par zones (toutes les lignes)
	* Affichage des flux par zones (toutes les lignes)
	* Affichage des flux par arrêts (toutes les lignes)

* **Logique paramétrable \:**
	* Affichage des montées/descentes par arrêts (toutes les lignes)
	* Affichage d'une variable par arrêts (toutes les lignes)
	* Affichage des montée/descentes par zones
	* Affichage d'une variable par zones (une ou plusieurs lignes)

Le tableau ci-dessous résume les combinaisons mode d'analyse / pondération utilisées selon les différents cas de figure \:  
![Mode analyse](../static/enquetes_mode_analyse.png "Mode analyse")
*Dans les cas de figure correspondant aux cases rouges, la possibilité de choix du mode d'analyse (voyage / déplacement) est verrouillée.*
